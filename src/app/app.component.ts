import { Component, ViewChild } from "@angular/core";
import { Nav, NavController, Platform, ToastController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { SplashScreen } from "@ionic-native/splash-screen";
import { PrincipalPage } from "../pages/principal/principal";
import { CodigoPage } from "../pages/codigo/codigo";
import { MenuController } from "ionic-angular";
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { MicasaPage } from "../pages/micasa/micasa";
import { SessionService } from "../providers/session.service";
import { StorageService } from "../providers/storage.service";
import { ModalService } from "../providers/modal.service";
import { ConfigService } from "../providers/config.service";
import { PaymentResumePage } from "../pages/payment-resume/payment-resume";
/* import { Deeplinks } from "@ionic-native/deeplinks/ngx";
import { PaymentSuccessPage } from "../pages/payment-success/payment-success";
import { PaymentErrorPage } from "../pages/payment-error/payment-error"; */

@Component({
  templateUrl: "app.html",
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  loader: any;
  type: number;
  httpResultCode: any;
  responseJSON: JSON;
  perfil: any;
  user_email: string;
  token: string;
  introDone = false;

  constructor(
    public platform: Platform,
    private toastCtrl: ToastController,
    private push: Push,
    public statusBar: StatusBar,
    public menu: MenuController,
    public splashScreen: SplashScreen,
    private sessionService: SessionService,
    private storageService: StorageService,
    private configService: ConfigService,
    public modalService: ModalService
  ) {
    this.initializeApp();
  }

  hideCamera() {
    (window.document.querySelector("ion-app") as HTMLElement).classList.remove(
      "cameraView"
    );
  }

  async initializeApp() {
    // await this.storageService.clearAll();
    this.platform.ready().then(async () => {
      // this.statusBar.styleDefault();
      // this.statusBar.overlaysWebView(true);
      // this.statusBar.styleLightContent();
      // this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();
      this.introDone = await this.sessionService.isIntroDone();
      console.log("INTRO SHOWN -> ", this.introDone);
      // this.rootPage = HelpPage;
      // this.hideCamera();
      // this.setupDeeplinks();
      this.initPushNotification();
      this.backPressHandle();
      setTimeout(() => {
        this.configService.swithChristmasActive(true);
      }, 3000);
      if (this.introDone) {
        this.rootPage = PrincipalPage;
      } else {
        this.rootPage = CodigoPage;
      }
      /*      if ( environment.production ) {
      this.initPushNotification();
      this.backPressHandle();

      if (this.introDone) {
        this.rootPage = PrincipalPage;
      } else {
        this.rootPage = CodigoPage;
      }
     } else {
      this.rootPage = HelpPage;
     } */
    });
  }
  /*  setupDeeplinks() {
    this.deeplinks
      .routeWithNavController(this.navCtrl, {
        "/payment-success/local": PaymentSuccessPage,
        "/payment-error/local": PaymentErrorPage,
      })
      .subscribe(
        (match) => {
          console.log("MATCH_ROUTE", match);
        },
        (nomatch) => {
          // nomatch.$link - the full link data
          console.error("UNMATCH_ROUTE", nomatch);
        }
      );
  } */
  backPressHandle() {
    var lastTimeBackPress = 0;
    var timePeriodToExit = 2000;

    this.platform.registerBackButtonAction(() => {
      // get current active page
      let view = this.nav.getActive();
      let childView = this.nav.getActiveChildNavs();

      console.log("active :", view.component.name);
      switch (view.component.name) {
        case "PrincipalPage":
          if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
            this.platform.exitApp(); //Exit from app
          } else {
            let toast = this.toastCtrl.create({
              message: "Pulsa otra vez si quieres salir de Icomunity",
              duration: 3000,
              position: "bottom",
            });
            toast.present();
            lastTimeBackPress = new Date().getTime();
          }
          break;
        case "AcercadePage":
        case "TelefonosPage":
        case "PerfilPage":
        case "DocumentoPage":
        case "InformacionPage":
        case "IncidenciaPage":
        case "MicasaPage":
          this.nav.setRoot(PrincipalPage);
          break;
        case "ListadoPage":
          this.nav.setRoot(MicasaPage);
          break;
        default:
          break;
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  initPushNotification() {
    // to check if we have permission
    this.push.hasPermission().then((res: any) => {
      if (res.isEnabled) {
        console.log("We have permission to send push notifications");
      } else {
        console.log("We don't have permission to send push notifications");
      }
    });

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: "634760725285",
      },
      ios: {
        alert: "true",
        badge: true,
        sound: "false",
        clearBadge: true,
      },
      windows: {},
    };
    const pushObject: PushObject = this.push.init(options);
    pushObject.on("notification").subscribe((notification: any) => {
      console.log("Received a notification", notification);
      let type = notification.type;
      /*     if(type=='incidencia'){
      this.nav.setRoot(IncidenciaPage);
    }else if(type=='noticia'){
      this.nav.setRoot(NoticiasPage);
    }else{
      this.nav.setRoot(HomePage);
    } */

      //
    });

    pushObject.on("registration").subscribe((registration: any) => {
      this.token = registration.registrationId;
      console.log("SAVING_PUSH_NOTIFICATION_TOKEN_1 -> ", this.token);
      this.savePushToken(this.token);
      // this.updateToken(this.token);
    });

    pushObject
      .on("error")
      .subscribe((error) => console.error("Error with Push plugin", error));
  }

  savePushToken(token) {
    this.storageService.savePushToken(token);
  }
}
