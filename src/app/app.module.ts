import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule, LOCALE_ID } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { PrincipalPage } from "../pages/principal/principal";
import { PerfilPage } from "../pages/perfil/perfil";
import { PadelPage } from "../pages/padel/padel";
import { PoliticaPage } from "../pages/politica/politica";
import { LocalPage } from "../pages/local/local";
import { ScanPage } from "../pages/scan/scan";
import { NoticiasPage } from "../pages/noticias/noticias";
import { IncidenciaPage } from "../pages/incidencia/incidencia";
import { DocumentoPage } from "../pages/documento/documento";
import { InformacionPage } from "../pages/informacion/informacion";
import { AcercadePage } from "../pages/acercade/acercade";
import { TelefonosPage } from "../pages/telefonos/telefonos";
import { CodigoPage } from "../pages/codigo/codigo";
import { MicasaPage } from "../pages/micasa/micasa";
import { ListadoPage } from "../pages/listado/listado";
import { RegistroPage } from "../pages/registro/registro";
import { PaymentPage } from "../pages/payment/payment";
import { BlankPage } from "../pages/blank/blank";
import { LocalResumePage } from "../pages/local-resume/local-resume";
import { NativeStorage } from "@ionic-native/native-storage";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { SplashScreen } from "@ionic-native/splash-screen";
import { HTTP } from "@ionic-native/http";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Camera } from "@ionic-native/camera";
import { IonicStorageModule } from "@ionic/storage";
import { Push } from "@ionic-native/push";
import { CalendarModule } from "ion2-calendar";
import { RestProvider } from "../providers/rest/rest";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Market } from "@ionic-native/market";
import { AppVersion } from "@ionic-native/app-version";
import { TruncatePipe } from "./truncate.pipe";
import { MomentPipe } from "../pipe/date.pipe";
import { ComponentsHeaderComponent } from "../components/components-header/components-header";
import { ComponentsFooterComponent } from "../components/components-footer/components-footer";
import { StorageService } from "../providers/storage.service";
import { SessionService } from "../providers/session.service";
import { LoginPage } from "../pages/login/login";
import { ComponentsSidemenuComponent } from "../components/components-sidemenu/components-sidemenu";
import { ReservasPage } from "../pages/reservas/reservas";
import { TenisPage } from "../pages/tenis/tenis";
import { PiscinaPage } from "../pages/piscina/piscina";
import { AlertComponent } from "../components/alert/alert";
import { LoadingModalComponent } from "../components/loading-modal/loading-modal";
import { ModalService } from "../providers/modal.service";
import { CustomHttpInterceptor } from "../common/interceptors/http-interceptor";
import { registerLocaleData } from "@angular/common";
import localSpain from "@angular/common/locales/es";
import { SolariumPage } from "../pages/solarium/solarium";
import { HttpService } from "../providers/http.service";
import { HelpPage } from "../pages/help/help";
import { NoDataComponent } from "../components/no-data/no-data";
import { DataErrorComponent } from "../components/data-error/data-error";
import { HelpTicketPage } from "../pages/help-ticket/help-ticket";
import { FiltersHeaderComponent } from "../components/filters-header/filters-header";
import { ConfigService } from "../providers/config.service";
import { PaymentResumePage } from "../pages/payment-resume/payment-resume";
import { Deeplinks } from "@ionic-native/deeplinks/ngx";
import { PaymentSuccessPage } from "../pages/payment-success/payment-success";
import { PaymentErrorPage } from "../pages/payment-error/payment-error";
import { SafePipe } from "../common/pipes/safeHtml.pipe";
import { UtilsService } from "../common/utils.service";
import { PaymentComponent } from "../components/payment/payment";

registerLocaleData(localSpain);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IncidenciaPage,
    CodigoPage,
    PerfilPage,
    PadelPage,
    LocalPage,
    TelefonosPage,
    AcercadePage,
    InformacionPage,
    RegistroPage,
    PrincipalPage,
    DocumentoPage,
    PoliticaPage,
    NoticiasPage,
    MicasaPage,
    ListadoPage,
    TruncatePipe,
    ScanPage,
    PaymentPage,
    LocalResumePage,
    BlankPage,
    LoginPage,
    ReservasPage,
    TenisPage,
    PiscinaPage,
    HelpPage,
    HelpTicketPage,
    PaymentResumePage,
    PaymentSuccessPage,
    PaymentErrorPage,
    ComponentsHeaderComponent,
    ComponentsFooterComponent,
    ComponentsSidemenuComponent,
    LoadingModalComponent,
    NoDataComponent,
    DataErrorComponent,
    FiltersHeaderComponent,
    PaymentComponent,
    AlertComponent,
    SolariumPage,
    MomentPipe,
    SafePipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CalendarModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
      statusbarPadding: true,
      platforms: {
        ios: {
          statusbarPadding: true,
        },
      },
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    IncidenciaPage,
    TelefonosPage,
    AcercadePage,
    PadelPage,
    LocalPage,
    PerfilPage,
    InformacionPage,
    RegistroPage,
    CodigoPage,
    PrincipalPage,
    DocumentoPage,
    PoliticaPage,
    NoticiasPage,
    MicasaPage,
    ListadoPage,
    ScanPage,
    PaymentPage,
    LocalResumePage,
    ComponentsHeaderComponent,
    ComponentsFooterComponent,
    ComponentsSidemenuComponent,
    LoadingModalComponent,
    NoDataComponent,
    DataErrorComponent,
    FiltersHeaderComponent,
    AlertComponent,
    LoginPage,
    ReservasPage,
    TenisPage,
    PiscinaPage,
    SolariumPage,
    HelpPage,
    HelpTicketPage,
    PaymentResumePage,
    PaymentSuccessPage,
    PaymentErrorPage,
    PaymentComponent,
    BlankPage,
  ],
  providers: [
    Push,
    // ConfigProvider,
    StatusBar,
    NativeStorage,
    HTTP,
    SplashScreen,
    HTTP,
    Camera,
    File,
    FilePath,
    FileTransfer,
    Deeplinks,
    FileTransferObject,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true,
    },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    Market,
    AppVersion,
    StorageService,
    SessionService,
    ModalService,
    HttpService,
    UtilsService,
    ConfigService,
    { provide: LOCALE_ID, useValue: "es-*" },
  ],
})
export class AppModule {}
