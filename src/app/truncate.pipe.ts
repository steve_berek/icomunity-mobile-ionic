import {Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
    transform(value: any): any {
       let   description = '';
            if( value.length > 40){
                description = value.substring(0, 40) + '...';
            }
        

        return description;
    }
}