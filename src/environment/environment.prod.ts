export const environment = {
  production: true,
  stripePubKey:
    "pk_live_51Jbj7XIewsJxGWuXFzCVX7bOhyrxHZa8ewnn4JR4KvLXBzgjqSVAAT3wC9vd9YSxJ86Fq1mkX6SRSKfDzb5kAm4H004gyvFDiL",
  stripeGatewayApiUrl: "https://payment-gateway.berekstan.com/api/v1",
  versions: { android: "1.3.54", ios: "1.2.43" },
  apiUrl: "https://backbone.berekstan.com:4432",
  apiUrlCore: "https://backbone.berekstan.com:4433/core",
  apiUrlZeus: "https://backbone.berekstan.com:4434",
  apiUrlPush: "https://backbone.berekstan.com:4431/push",
  apiUrlAccount: "https://backbone.berekstan.com:4472/core",
  apiUrlBookingLocal: "https://backbone.berekstan.com:4439/booking",
  apiUrlBookingPadel: "https://backbone.berekstan.com:4430/booking",
  apiUrlBookingPiscina: "https://backbone.berekstan.com:4490/booking",
  apiUrlBookingSolarium: "https://backbone.berekstan.com:4492/booking",
  migrationApiUrl: "https://backbone.berekstan.com:4081/api/v1",
  paymentConfig: {
    timeout: 5 * 60 * 1000,
    pollInterval: 5 * 1000,
    types: { local: "local", padel: "padel", tenis: "tenis" },
  },
};
