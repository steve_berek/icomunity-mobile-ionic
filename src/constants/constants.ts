export class Constants{

    stripe_pub_keys:StripeObject[]=
    [
        {code:"CVH2015",pub_key:"pk_test_6jBWjtgK3X9bIM6SUPbQVo5X"},
        {code:"TSL2014",pub_key:"pk_test_6jBWjtgK3X9bIM6SUPbQVo5X"},
        {code:"2017venecia",pub_key:"pk_test_6jBWjtgK3X9bIM6SUPbQVo5X"},
        {code:"P14VALDESPARTERA",pub_key:""},
        {code:"Rosales9",pub_key:""},
        {code:"Prima6",pub_key:""}
];

    public getStripeKeys(){
        return this.stripe_pub_keys;
    }
}
interface StripeObject{
    code:string,
    pub_key:string
}