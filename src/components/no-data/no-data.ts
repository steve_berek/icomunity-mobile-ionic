import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'no-data',
  templateUrl: 'no-data.html',
})
export class NoDataComponent implements OnInit {
  nodata = 'assets/imgs/nodata.png';
  @Input() text = 'No hay datos';
  constructor() { }

  ngOnInit() {
  }

}
