import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'data-error',
  templateUrl: 'data-error.html',
})
export class DataErrorComponent implements OnInit {
  dataerror = 'assets/imgs/dataerror.png';
  @Input() text = 'Error al cargar los datos';
  constructor() { }

  ngOnInit() {
  }

}
