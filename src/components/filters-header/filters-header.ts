import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'filters-header',
  templateUrl: 'filters-header.html',
})
export class FiltersHeaderComponent implements OnInit {
  refreshIcon = 'assets/imgs/refresh.png';
  clearIcon = 'assets/imgs/close.png';
  @Input() selectLabel = 'Selecciona';
  @Input() list = false;
  @Input() refresh = true;
  @Input() items = [];
  @Output() onRefresh = new EventEmitter(null);
  @Output() onSelect = new EventEmitter(null);
  selectedItem;
  constructor() { }

  ngOnInit() {
  }

  triggerRefresh() {
    this.onRefresh.emit(true);
  }
  triggerSelect(item) {
    this.onSelect.emit(item);
  }
  triggerClear() {
    this.selectedItem = null;
    console.log('clear');
    this.onSelect.emit(null);
  }
}
