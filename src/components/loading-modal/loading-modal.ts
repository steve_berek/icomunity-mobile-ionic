import { Component } from '@angular/core';
import { ModalService } from '../../providers/modal.service';

/**
 * Generated class for the LoadingModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'loading-modal',
  templateUrl: 'loading-modal.html'
})
export class LoadingModalComponent {

  text: string;

  constructor(
    public modalService: ModalService,
  ) {
  }

}
