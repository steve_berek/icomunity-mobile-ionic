import { Component } from '@angular/core';

/**
 * Generated class for the ComponentsHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'components-header',
  templateUrl: 'components-header.html'
})
export class ComponentsHeaderComponent {

  text: string;

  constructor() {
    console.log('Hello ComponentsHeaderComponent Component');
    this.text = 'Hello World';
  }

}
