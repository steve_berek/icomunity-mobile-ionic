import { Component, OnInit, ViewChild } from '@angular/core';
import { HomePage } from '../../pages/home/home';
import { IncidenciaPage } from '../../pages/incidencia/incidencia';
import { InformacionPage } from '../../pages/informacion/informacion';
import { TelefonosPage } from '../../pages/telefonos/telefonos';
import { DocumentoPage } from '../../pages/documento/documento';
import { MicasaPage } from '../../pages/micasa/micasa';
import { PerfilPage } from '../../pages/perfil/perfil';
import { AcercadePage } from '../../pages/acercade/acercade';
import { Nav } from 'ionic-angular';

/**
 * Generated class for the ComponentsSidemenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'components-sidemenu',
  templateUrl: 'components-sidemenu.html'
})
export class ComponentsSidemenuComponent implements OnInit {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any, icon : any}>;

  constructor(

  ) {
  }

  ngOnInit(): void {
    this.initSideMenu();
  }

  initSideMenu() {
    this.pages = [
      { title: 'Pagina principal', component: HomePage , icon :'home' },
      { title: 'Incidencias', component: IncidenciaPage, icon :'warning' },
      { title: 'Solicitar informacion', component: InformacionPage, icon :'information-circle' },
      { title: 'Telefonos de interes', component: TelefonosPage, icon :'call' },
      { title: 'Mandar documento', component: DocumentoPage , icon :'document'},
      { title: 'Mi casa', component: MicasaPage , icon :'construct'},
      { title: 'Perfilooo', component: PerfilPage , icon :'person'},
      { title: 'Acerca de', component: AcercadePage, icon :'at' }
  ];
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
