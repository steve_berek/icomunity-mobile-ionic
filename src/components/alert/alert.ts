import { Component } from '@angular/core';
import { ModalService } from '../../providers/modal.service';

/**
 * Generated class for the AlertComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'alert-modal',
  templateUrl: 'alert.html'
})
export class AlertComponent {

  text: string;

  constructor(
    public modalService: ModalService,
  ) {
    console.log('Hello AlertComponent Component');
    this.text = 'Hello World';
  }

}
