import { NgModule } from '@angular/core';
import { ComponentsHeaderComponent } from './components-header/components-header';
import { ComponentsFooterComponent } from './components-footer/components-footer';
import { ComponentsSidemenuComponent } from './components-sidemenu/components-sidemenu';
import { LoadingModalComponent } from './loading-modal/loading-modal';
import { AlertComponent } from './alert/alert';
@NgModule({
	declarations: [ComponentsHeaderComponent,
    ComponentsFooterComponent,
    ComponentsSidemenuComponent,
    LoadingModalComponent,
    AlertComponent,

],
	imports: [],
	exports: [ComponentsHeaderComponent,
    ComponentsFooterComponent,
    ComponentsSidemenuComponent,
    LoadingModalComponent,
    AlertComponent,
    ]
})
export class ComponentsModule {}
