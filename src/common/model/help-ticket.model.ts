import { HelpTIcketMessageModel } from "./help-ticket-message.model";

export class HelpTicketModel {
  id: string;
  code: string;
  comunity: string;
  username: string;
  email: string;
  adminEmail: string;
  reason: string;
  address: string;
  target: string;
  closed: boolean;
  resolved: boolean;
  messages: HelpTIcketMessageModel[];
  createdAt: string;
  updatedAt: string;
}