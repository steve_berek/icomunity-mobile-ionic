

export class PadelBookModel {
    id?: string;
    userId: string;
    code: string;
    day: Date;
    formattedDay: string;
    hour: string;
    active: boolean;
    state?: string;
    username?: string;
    address?: string;
    amount?: string;
    rangeTime?: string;
    createdAt: Date;
    formattedDate: string;
}
