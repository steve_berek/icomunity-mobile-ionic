export class UserModel {
  id?: number;
  state: number;
  fb_token: string;
  notificationActive: boolean;
  auth_token: string;
  code_comunity: string;
  name: string;
  email: string;
  phone: string;
  adress: string;
  code_house: string;
  created_at?: Date;
}
