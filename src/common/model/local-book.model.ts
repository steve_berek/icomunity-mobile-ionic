

export class LocalBookModel {
    id: string;
    userId: string;
    code: string;
    day: Date;
    formattedDay: string;
    hours: string[];
    active: boolean;
    username?: string;
    address?: string;
    amount?: string;
    rangeTime?: string;
    start?: number;
    end?: number;
    state?: string;
    createdAt: Date;
    formattedDate: string;
}
