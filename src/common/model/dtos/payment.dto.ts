export class PaymentDto {
  time?: Date;
  product: string;
  paymentType: string;
  amount: string;
  userId: number;
  username: string;
  email: string;
  comunityName: string;
  comunityCode: string;
  comunityEmail: string;
  detail?: string;
}
