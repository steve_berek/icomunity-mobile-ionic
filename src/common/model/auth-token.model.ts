

export class AuthTokenModel {
    accessToken: string;
    refreshToken: string;
    tokenType: string;
}