

export class SolariumBookModel {
    id: string;
    userId: string;
    username: string;
    useraddress: string;
    code: string;
    day: Date;
    formattedDay: string;
    hour: string;
    persons: number;
    active: boolean;
    state?: string;
    amount?: string;
    rangeTime?: string;
    createdAt: Date;
    formattedDate: string;
}
