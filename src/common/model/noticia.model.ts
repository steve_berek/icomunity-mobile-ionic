

export class NoticiaModel {
    id: number;
    title: string;
    description: string;
    created_at: Date;
    formattedDate: string;
}
