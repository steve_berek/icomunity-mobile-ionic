export class HelpTIcketMessageModel {
    id: string;
    ticketId: string;
    isUser: boolean;
    origin: string;
    target: string;
    message: string;
    createdAt: string;
    updatedAt: string;
}