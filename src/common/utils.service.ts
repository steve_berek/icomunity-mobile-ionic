import { Injectable } from "@angular/core";
import { AlertController } from "ionic-angular";
import { PaymentDto } from "./model/dtos/payment.dto";

@Injectable()
export class UtilsService {
  constructor(private alertCtrl: AlertController) {}

  setupPaymentDto(
    paymentType: string,
    product: string,
    amount: string,
    userId: number,
    username: string,
    email: string,
    comunityName: string,
    comunityCode: string,
    comunityEmail: string,
    detail?: string
  ): PaymentDto {
    return {
      paymentType,
      product,
      amount,
      userId,
      username,
      email,
      comunityName,
      comunityCode,
      comunityEmail,
      detail: detail ? detail : "",
    };
  }
  calculateAmount(
    units: number,
    unitPrice: string,
    unitPriceMtp: string,
    addOn: string,
    addPlus: string
  ): string {
    const unitsNb = units;
    const unitPriceNb = Number(unitPrice);
    const unitPriceMtpNb = Number(unitPriceMtp);
    const addOnNb = Number(addOn);
    const addPlusNb = Number(addPlus);
    if (unitsNb > 0 && unitPriceNb > 0 && unitPriceMtpNb > 0 && addOnNb >= 0) {
      const amountBase = unitsNb * unitPriceNb;
      const amountMtp = amountBase * unitPriceMtpNb + addOnNb + addPlusNb;
      const amountToPay = Math.ceil((amountBase + amountMtp) * 100);
      if (String(amountToPay)) return String(amountToPay);
      else this.alertErrorCalculating();
    } else {
      this.alertErrorCalculating();
    }
  }

  alertErrorCalculating() {
    let confirm = this.alertCtrl.create({
      title: "Error",
      message:
        "No se ha podido generar su resumen de compra, por favor intenta más tarde. Si persiste, contacta con nuestro soporte para resolver la incidencia. Gracias",
      buttons: [
        {
          text: "Ok",
          handler: () => {},
        },
      ],
    });
    confirm.present();
  }

  formatAmountDecimal(amount: string): string {
    return String((Number(amount) / 100).toFixed(2));
  }
}
