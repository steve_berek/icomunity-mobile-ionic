import { Component, OnInit } from "@angular/core";
import {
  NavController,
  AlertController,
  LoadingController,
} from "ionic-angular";
import { HTTP } from "@ionic-native/http";
import { PrincipalPage } from "../principal/principal";
import { RestProvider } from "./../../providers/rest/rest";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
@Component({
  selector: "page-informacion",
  templateUrl: "informacion.html",
  providers: [RestProvider],
})
export class InformacionPage implements OnInit {
  description: any;
  information: any;

  email: any;
  nombre: any;
  telefono: any;
  comunidad: ComunidadModel;
  direccion: any;
  apellido: any;
  com_code: string;
  com_name: string;
  com_admin_name: string;
  com_admin_email: string;
  com_admin_phone: string;
  com_admin_adress: string;
  com_type: string;
  base64Image: any;
  base64ImageString: string;
  perfil: any = {};
  subject: any;
  body: any;
  tipo: any;
  rest: any;
  /*correoFinal:string="ana@gestionfincasbarrena.es";
  adminUrl:string="https://work.berekstan.com/comunidades/admin_info_email_ana.php";
  */
  infoURL: string =
    "https://work.berekstan.com/comunidades/icomunity_info_email_pdf.php";
  active = false;
  infoIcon = "assets/icon/informacion.svg";
  constructor(
    public navCtrl: NavController,
    private http: HTTP,
    private restProvider: RestProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private storageService: StorageService
  ) {
    this.rest = restProvider;
  }
  async ngOnInit() {
    this.init();
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      for (const feat of this.comunidad.features) {
        if (feat === "informacion") {
          this.active = true;
        }
      }
    }
    this.fetchUserInfos();
  }
  async fetchUserInfos() {
    this.perfil = await this.storageService.fetchUserInfos();
  }

  sendInformationEmail() {
    if (this.tipo != undefined) {
      let loader = this.loadingCtrl.create({
        content: "Mandando el correo ...",
      });
      loader.present();

      this.rest
        .sendInformacion(
          this.com_code,
          this.perfil["name"],
          this.perfil["adress"],
          this.perfil["phone"],
          this.com_name,
          this.perfil["email"],
          this.com_admin_email,
          this.tipo,
          this.description
        )
        .subscribe(
          (data) => {
            let result = data["result"];

            loader.dismiss();
            if (!result) {
              let alert = this.alertCtrl.create({
                title: "Email Error",
                subTitle: "Ha habido un error, pruebe más tarde por favor",
                buttons: ["OK"],
              });

              alert.present();
            } else {
              this.showConfirm();
            }
          },
          (error) => {
            loader.dismiss();
            let alert = this.alertCtrl.create({
              title: "Email Error 2",
              subTitle: "Ha habido un error, pruebe más tarde por favor",
              buttons: ["OK"],
            });

            alert.present();
          }
        );
    } else {
      let alert = this.alertCtrl.create({
        title: "Campo requerido",
        subTitle: "Falta el tipo de información",
        buttons: ["OK"],
      });

      alert.present();
    }
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: "Email confirmacion",
      message: "El administrador ha recibido correctamente su consulta",
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.navCtrl.setRoot(PrincipalPage);
          },
        },
      ],
    });
    confirm.present();
  }

  checkFields() {
    let message = "";

    for (var index = 0; index < 2; index++) {
      if (this.tipo == undefined) message = "Debes elegir un tipo ";
      else if (this.description == undefined)
        message = "Debes poner una descripcion ";
    }

    if (message == "") {
      this.sendInformationEmail();
    } else {
      let alert = this.alertCtrl.create({
        title: "Campo requerido",
        subTitle: message,
        buttons: ["OK"],
      });
      alert.present();
    }
  }
}
