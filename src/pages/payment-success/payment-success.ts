import { Component, OnInit } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

@Component({
  selector: "page-payment-success",
  templateUrl: "payment-success.html",
})
export class PaymentSuccessPage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  async ngOnInit() {
    console.log("INIT_PAYMENT_SUCCESS");
  }
}
