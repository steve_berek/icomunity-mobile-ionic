import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { UserModel } from "../../common/model/user.model";
import { HttpService } from "../../providers/http.service";
import { StorageService } from "../../providers/storage.service";
import { RestProvider } from "./../../providers/rest/rest";

@Component({
  selector: "page-documento",
  templateUrl: "documento.html",
  providers: [RestProvider],
})
export class DocumentoPage implements OnInit {
  infoIcon = "assets/icon/informacion.svg";
  nodataIcon = "assets/imgs/nodata.png";
  filesTypes = [];
  selectedType = "";
  documents = [];
  comunity: ComunidadModel;
  user: UserModel;
  constructor(
    public navCtrl: NavController,
    private httpService: HttpService,
    private storageService: StorageService
  ) {}
  async ngOnInit() {
    this.init();
  }

  async init() {
    this.comunity = await this.storageService.fetchComunidad();
    this.user = await this.storageService.fetchUserInfos();
    this.loadFilesTypes();
  }
  refresh() {
    this.loadFilesTypes();
  }
  loadFilesTypes() {
    this.httpService.getFilesTypes().subscribe((response) => {
      this.filesTypes = response;
    });
  }
  onSelectType(type) {
    console.log("SELECT_TYPE -> ", type);
    console.log("COMUNITY -> ", this.comunity.code);
    console.log("USER -> ", JSON.stringify(this.user));
    if (this.comunity && this.user) {
      this.selectedType = type;
      this.httpService
        .getDocumentsByUser(this.comunity.code, type, this.user.id)
        .subscribe(
          (response) => {
            console.log("DOCUMENTS -> ", response);
            this.documents = response;
          },
          (error) => {
            console.log("ERROR -> ", JSON.stringify(error));
          }
        );
    }
  }
}
