import { Component, OnInit } from "@angular/core";
import {
  MenuController,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  AlertController,
} from "ionic-angular";
import { PrincipalPage } from "../principal/principal";
import { ViewChild } from "@angular/core";
import { Slides } from "ionic-angular";
import { RestProvider } from "./../../providers/rest/rest";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { PLANTA_LIST, PUERTA_LIST } from "./registro.constants";
import { SessionService } from "../../providers/session.service";
import { StorageService } from "../../providers/storage.service";
import { UserModel } from "../../common/model/user.model";
import { PhoneModel } from "../../common/model/phone.model";
import { ServiceModel } from "../../common/model/service.model";
import { ComunidadModel } from "../../common/model/comunidad.model";

export interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  selector: "page-registro",
  templateUrl: "registro.html",
})
export class RegistroPage implements OnInit {
  @ViewChild(Slides) slides: Slides;

  showSkip = true;
  isConnected: boolean = false;
  okeyConnected: boolean = false;
  PortalsJSON: JSON;
  tipoPortal: any[];
  firstChar: number;
  secondChar: string;
  numerosPlantas: string[];
  portalSelected: any;
  countPortals: number;

  nombre: any;
  apellido: any;

  code: any;
  door: any;
  planta: any;
  portal: string;
  phone: number;
  email: any;
  name: any;
  fk_id_portal: number;
  code_house: any;

  portals: string[];
  adresses: string[];
  plantas: string[];
  puertas: string[];
  portalIndex: number;
  tipoPlanta: any;

  responseJSON: JSON;
  httpResultCode: any;
  httpResultBoolean: boolean;
  httpResponseMessage: any;

  loader: any;
  sincroOK: boolean = false;
  comunidad: string;
  type_com: number;
  RegisterUserURL: string =
    "https://work.berekstan.com/icomunity/register_user.php";

  chalet_state: boolean = false;
  chalet_adress: string;

  slideOneForm: FormGroup;
  submitAttempt: boolean = false;
  @ViewChild("signupSlider") signupSlider: any;

  plantaList;
  puertaList;
  formHouseInfos: FormGroup;
  formUserInfos: FormGroup;
  formChaletInfos: FormGroup;

  validationMessages = {
    chaletAddress: [
      { type: "required", message: "Debe introducir la dirección completa." },
    ],
    portal: [{ type: "required", message: "Debe seleccionar el portal." }],
    planta: [{ type: "required", message: "Debe seleccionar la planta." }],
    puerta: [{ type: "required", message: "Debe seleccionar la puerta." }],
    name: [{ type: "required", message: "Debe introducir el nombre" }],
    surname: [{ type: "required", message: "Debe introducir el apellido" }],
    email: [
      { type: "required", message: "Debe introducir el email" },
      { type: "pattern", message: "Debe introducir un email valido" },
    ],
    phone: [
      { type: "required", message: "Debe introducir un telefono." },
      { type: "minlength", message: "Debe introducir un telefono valido." },
      { type: "maxlength", message: "Debe introducir un telefono valido." },
    ],
    terms: [{ type: "pattern", message: "Debe aceptar los terminos de uso" }],
  };
  slide1Icon = "assets/imgs/slide1.png";
  slide2Icon = "assets/imgs/slide2.jpg";
  slide3Icon = "assets/imgs/slide3.png";
  fb_token = "";

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    private rest: RestProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private storageService: StorageService,
    public formBuilder: FormBuilder,
    private sessionService: SessionService
  ) {
    this.plantaList = PLANTA_LIST;
    this.puertaList = PUERTA_LIST;

    this.PortalsJSON = navParams.get("portalsList");
    this.code = navParams.get("code");
    this.comunidad = navParams.get("comunidad");
    this.type_com = navParams.get("type");

    if (this.type_com == 1) {
      this.chalet_state = false;
    } else {
      this.chalet_state = true;
    }
  }

  // VERSION 2020
  ngOnInit() {
    this.loadPushToken();
    this.initForms();
  }
  async loadPushToken() {
    this.fb_token = await this.storageService.fetchPushTokn();
  }
  async clearStorage() {
    await this.storageService.clearAll();
  }
  initForms() {
    this.formChaletInfos = this.formBuilder.group({
      address: new FormControl("", Validators.required),
    });

    this.formHouseInfos = this.formBuilder.group({
      portal: new FormControl("", Validators.required),
      planta: new FormControl("", Validators.required),
      puerta: new FormControl("", Validators.required),
    });

    this.formUserInfos = this.formBuilder.group({
      name: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required,
        ])
      ),
      surname: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required,
        ])
      ),
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      phone: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(9),
          Validators.maxLength(9),
          Validators.required,
        ])
      ),
      terms: new FormControl(false, Validators.pattern("true")),
    });
  }

  finalizar() {
    this.submitAttempt = true;

    if (
      (this.formUserInfos.valid && this.formHouseInfos.valid) ||
      (this.formUserInfos.valid && this.formChaletInfos.valid)
    ) {
      this.submitAttempt = false;
      this.loader = this.loadingCtrl.create({
        content: "Cargando...",
      });
      this.clearStorage();
      this.registerUser();
      this.loader.onDidDismiss(() => {
        if (this.sincroOK == true) {
          this.setIntroShown();
          this.storageService.setComunidadCode(this.code);
          // FETCH ALL APP DATA
          this.fetchMandatoryData();
        }
      });
    } else {
      // SHOW ERROR MESSAGE
    }
  }
  async fetchMandatoryData() {
    await this.fetchComunidad();
  }
  openPrincipal() {
    this.navCtrl.setRoot(PrincipalPage);
  }
  async fetchPhones() {
    await this.rest.getPhonesByCode(this.code).subscribe(
      (response) => {
        console.log(" PHONES DATA -> ", response);
        if (response) {
          const phones: PhoneModel[] = response;
          this.storageService.savePhones(phones);
          // THEN MOVE TO HOME PAGE
          this.openPrincipal();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchServices() {
    await this.rest.getServicesByCode(this.code).subscribe(
      (response) => {
        console.log(" SERVICES DATA -> ", response);
        if (response) {
          const services: ServiceModel[] = response;
          this.storageService.saveServices(services);

          this.fetchPhones();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchComunidad() {
    await this.rest.getComunityByCode(this.code).subscribe(
      (response) => {
        console.log(" COMUNIDAD DATA -> ", response[0]);
        if (response[0]) {
          const comunidad: ComunidadModel = response[0];
          this.storageService.saveComunidad(comunidad);

          this.fetchServices();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  registerUser() {
    this.loader.present();
    var count = Object.keys(this.PortalsJSON).length;
    for (let index = 0; index < count; index++) {
      let portal = this.PortalsJSON;

      if (portal[index]["adress"] == this.formHouseInfos.value.portal)
        this.fk_id_portal = portal[index]["id"];
    }

    let portal = this.formHouseInfos.value.portal;
    this.name =
      this.formUserInfos.value.name + "  " + this.formUserInfos.value.surname;
    this.phone = this.formUserInfos.value.phone;
    this.email = this.formUserInfos.value.email;
    this.planta = this.formHouseInfos.value.planta;
    this.door = this.formHouseInfos.value.puerta;

    // CHALET
    this.chalet_adress = this.formChaletInfos.value.address;

    if (this.chalet_state) {
      this.code_house = this.code + this.chalet_adress;
      this.planta = "vacio";
      this.door = "vacio";
      portal = this.chalet_adress;
    } else {
      this.code_house =
        this.code +
        this.fk_id_portal +
        this.formHouseInfos.value.planta +
        this.formHouseInfos.value.puerta;
      portal = this.formHouseInfos.value.portal;
    }

    console.log(
      "house infos : " +
        portal +
        " " +
        this.planta +
        " " +
        this.door +
        " " +
        this.code_house +
        " " +
        this.email +
        " " +
        this.code +
        " " +
        this.phone +
        " " +
        this.name
    );

    this.rest
      .registerUser(
        this.code,
        this.name,
        this.email,
        this.phone,
        portal,
        this.planta,
        this.door,
        this.code_house,
        this.fb_token
      )
      .subscribe(
        (data) => {
          if (!data) {
            let alert = this.alertCtrl.create({
              title: "Fallo de sincronizacion",
              subTitle: "Por favor prueba mâs tarde",
              buttons: ["OK"],
            });
            this.loader.dismiss();
            alert.present();
          } else {
            this.sincroOK = true;

            let adress = "";
            if (!this.chalet_state) {
              adress = portal + " " + this.planta + " " + this.door;
            } else {
              adress = this.chalet_adress;
            }

            console.log("datos saved in database : " + this.name);

            let token = data["token"];
            //localStorage.setItem("auth_token",token);
            console.log("auth token :" + token);
            this.sessionService.setAuthToken(token);
            this.savePerfil(
              this.name,
              this.email,
              this.phone,
              adress,
              this.comunidad,
              this.code_house,
              data["state"],
              token,
              this.fb_token
            );

            this.loader.dismiss();
          }
        },
        (error) => {
          let alert = this.alertCtrl.create({
            title: "Problema de sincronización",
            subTitle:
              "Por favor compruebe que tienes internet y prueba otra vez",
            buttons: ["OK"],
          });
          this.loader.dismiss();
          alert.present();
        }
      );
  }

  savePerfil(
    name,
    email,
    phone,
    adress,
    comunidad,
    code_house,
    state,
    auth_token,
    fb_token
  ) {
    const profil: UserModel = {
      name,
      email,
      phone,
      adress,
      code_house,
      state,
      auth_token,
      fb_token,
      notificationActive: fb_token && fb_token !== "" ? true : false,
      code_comunity: comunidad,
    };
    console.log("SAVE_USER ->" + profil);
    this.storageService.saveUserInfos(profil);
  }

  getIndex(index: any) {
    //this.portal = this.PortalsJSON[index]['adress'];
    //this.presentToast(portal);
    // this.firstChar= this.tipoPlanta.charAt(0);
    // this.presentToast(this.firstChar);
    // this.secondChar=this.tipoPlanta.subString(1);
    // this.presentToast(this.secondChar);
  }

  presentToast(text: any) {
    let toast = this.toastCtrl.create({
      message: "info : " + text,
      duration: 3000,
      position: "top",
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  setPlantas() {}

  /*
  startApp() {
    this.storage.set('introShown', true);
    this.navCtrl.setRoot(HomePage, {}, {
      animate: true,
      direction: 'forward'
    });
  }
*/

  isValidate() {
    let message = "";

    for (var index = 0; index < 7; index++) {
      if (this.chalet_state) {
        if (this.chalet_adress == undefined) {
          message = "Debes introducir una direccion correcta para tu chalet";
        } else if (this.nombre == undefined) message = "Debes poner tu nombre";
        else if (this.apellido == undefined)
          message = "Debes poner tu apellido";
        else if (this.phone == undefined) message = "Debes poner tu telefono";
        else if (this.email == undefined) message = "Debes poner tu email";
      } else {
        if (this.portal == undefined) message = "Debes elegir un portal";
        else if (this.planta == undefined) message = "Debes elegir una planta";
        else if (this.door == undefined) message = "Debes elegir una puerta";
        else if (this.nombre == undefined) message = "Debes poner tu nombre";
        else if (this.apellido == undefined)
          message = "Debes poner tu apellido";
        else if (this.phone == undefined) message = "Debes poner tu telefono";
        else if (this.email == undefined) message = "Debes poner tu email";
      }
    }
    if (message == "") {
      this.finalizar();
    } else {
      let alert = this.alertCtrl.create({
        title: "Campo requerido",
        subTitle: message,
        buttons: ["OK"],
      });
      alert.present();
    }
  }

  setIntroShown() {
    this.sessionService.setIntroDone();
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  nextSlide() {
    this.slides.slideNext();
  }
  modificar() {
    this.slides.slideTo(0);
  }
}

export class GlobalValidator {
  public static EMAIL_REGEX =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}
