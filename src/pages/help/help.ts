import { Component, OnInit } from "@angular/core";
import { AlertController, NavController, NavParams } from "ionic-angular";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { HelpTicketModel } from "../../common/model/help-ticket.model";
import { UserModel } from "../../common/model/user.model";
import { HttpService } from "../../providers/http.service";
import { StorageService } from "../../providers/storage.service";
import { HelpTicketPage } from "../help-ticket/help-ticket";

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-help",
  templateUrl: "help.html",
})
export class HelpPage implements OnInit {
  reasons = ["Consulta", "Problema", "Queja", "Otro"];
  targets = ["Administrador/a", "Soporte"];
  selectedType;
  selectedTarget;
  message;
  showError;
  comunity: ComunidadModel;
  user: UserModel;
  tickets: HelpTicketModel[];
  ticketsBase: HelpTicketModel[] = [];
  swicthSelect = "solicitudes";
  filterItems = ["activo", "inactivo"];
  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private http: HttpService,
    private storageService: StorageService
  ) {}
  async ngOnInit() {
    this.comunity = await this.storageService.fetchComunidad();
    this.user = await this.storageService.fetchUserInfos();
    this.loadHelpTickets();
  }

  selectFilter(item) {
    if (item) {
      this.filterByEstado(item);
    } else {
      this.clearFilters();
    }
  }
  refresh() {
    this.loadHelpTickets();
  }
  loadHelpTickets() {
    this.http.findHelpTicketsByUser(this.user.id).subscribe((tickets) => {
      console.log("TICKETS -> ", tickets);
      if (tickets && tickets.length > 0) {
        this.tickets = tickets;
        this.ticketsBase = tickets;
      } else {
        this.tickets = null;
        this.ticketsBase = null;
      }
    });
  }
  openHelpTicket() {
    // this.openHelpTicketTest();
    if (this.message && this.selectedType) {
      this.showError = false;
      if (this.comunity && this.user) {
        this.http
          .openHelpTicket(
            this.user.id,
            this.user.fb_token,
            this.comunity.code,
            this.comunity.name,
            this.user.name,
            this.comunity.admin_email,
            this.user.email,
            this.user.adress,
            this.selectedType,
            this.selectedTarget,
            this.message
          )
          .subscribe(
            (response) => {
              this.message = null;
              this.selectedType = null;
              this.showError = false;
              let alert = this.alertCtrl.create({
                title: "Solicitud enviada",
                subTitle:
                  "Su solicitud ha sido correctamente notificada a nuestro equipo de soporte",
                buttons: ["OK"],
              });
              alert.present();
              this.reload();
            },
            (error) => {
              let alert = this.alertCtrl.create({
                title: "Error",
                subTitle:
                  "No se ha podido enviar su solicitud, por favor compruebe que tiene buena conexión internet y vuelve a intentarlo",
                buttons: ["OK"],
              });
              alert.present();
            }
          );
      } else {
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: "No se ha podido realizar la operación",
          buttons: ["OK"],
        });
        alert.present();
      }
    } else {
      this.showError = true;
    }
  }
  navigateToDetail(ticket: HelpTicketModel) {
    this.navCtrl.push(HelpTicketPage, {
      ticket: JSON.stringify(ticket),
    });
  }

  reload() {
    this.loadHelpTickets();
    this.swicthSelect = "solicitudes";
    this.selectedTarget = null;
    this.selectedType = null;
  }

  filterByEstado(estado) {
    this.tickets = this.ticketsBase
      ? this.ticketsBase.filter(
          (ticket) => ticket.closed === (estado === "inactivo" ? true : false)
        )
      : null;
  }
  clearFilters() {
    this.tickets = this.ticketsBase ? [...this.ticketsBase] : null;
  }
}
