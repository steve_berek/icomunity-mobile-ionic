import { Component, ViewChild, OnInit } from "@angular/core";
import {
  NavController,
  Nav,
  LoadingController,
  AlertController,
} from "ionic-angular";
import { HomePage } from "../home/home";
import { PerfilPage } from "../perfil/perfil";
import { MicasaPage } from "../micasa/micasa";
import { IncidenciaPage } from "../incidencia/incidencia";
import { DocumentoPage } from "../documento/documento";
import { AcercadePage } from "../acercade/acercade";
import { TelefonosPage } from "../telefonos/telefonos";
import { RestProvider } from "./../../providers/rest/rest";
import { SessionService } from "../../providers/session.service";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { ReservasPage } from "../reservas/reservas";
import { PhoneModel } from "../../common/model/phone.model";
import { ServiceModel } from "../../common/model/service.model";
import { CodigoPage } from "../codigo/codigo";
import { HelpPage } from "../help/help";
import { NoticiasPage } from "../noticias/noticias";
import { UserModel } from "../../common/model/user.model";
import { ConfigService } from "../../providers/config.service";
import { HttpService } from "../../providers/http.service";
import { Deeplinks } from "@ionic-native/deeplinks/ngx";
import { PaymentResumePage } from "../payment-resume/payment-resume";
@Component({
  selector: "page-principal",
  templateUrl: "principal.html",
  providers: [RestProvider],
})
export class PrincipalPage implements OnInit {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;
  pages: Array<{ title: string; component: any; icon: any }>;
  introDone = false;
  comunidad: ComunidadModel;
  loader: any;
  on_off_icon = "assets/icon/logout.png";
  holaIcon = "assets/imgs/hola.png";
  user: UserModel;
  userChristmasActive = true;
  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private sessionService: SessionService,
    private storageService: StorageService,
    private configService: ConfigService,
    public restProvider: RestProvider,
    public httpService: HttpService,
    private loadingCtrl: LoadingController
  ) {
    console.log("in principal page");
  }
  async ngOnInit() {
    this.fetchMasterData();
    this.initSideMenu();
    // this.setupDeeplinks();
    this.configService.listenChristmasActive().subscribe((christmasActive) => {
      this.userChristmasActive = christmasActive;
    });
  }
  switchChristmasAnimation(active: boolean) {
    this.configService.swithChristmasActive(active);
  }
  /*   setupDeeplinks() {
    this.deeplinks
      .routeWithNavController(this.navCtrl, {
        "/payment-success/local": PaymentSuccessPage,
        "/payment-error/local": PaymentErrorPage,
      })
      .subscribe(
        (match) => {
          console.log("MATCH_ROUTE", match);
        },
        (nomatch) => {
          // nomatch.$link - the full link data
          console.error("UNMATCH_ROUTE", nomatch);
        }
      );
  } */
  async fetchMasterData() {
    this.comunidad = await this.storageService.fetchComunidad();
    console.log("STORAGE COMUNIDAD PRINCIPAL -> ", this.comunidad);
    if (this.comunidad && this.comunidad.code) {
      await this.fetchComunidad();
      await this.fetchServices();
      await this.fetchPhones();
      // await this.reloadOnFirstSync();
    }
  }
  async fetchPhones() {
    await this.restProvider.getPhonesByCode(this.comunidad.code).subscribe(
      (response) => {
        console.log(" PHONES DATA PRINCIPAL-> ", response);
        if (response) {
          const phones: PhoneModel[] = response;
          this.storageService.savePhones(phones);
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchServices() {
    await this.restProvider.getServicesByCode(this.comunidad.code).subscribe(
      (response) => {
        console.log(" SERVICES DATA PRINCIPAL-> ", response);
        if (response) {
          const services: ServiceModel[] = response;
          this.storageService.saveServices(services);
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchComunidad() {
    await this.restProvider.getComunityByCode(this.comunidad.code).subscribe(
      (response) => {
        if (response[0]) {
          const comunidad: ComunidadModel = response[0];
          this.storageService.saveComunidad(comunidad);
          this.comunidad = comunidad;
          console.log(" COMUNIDAD DATA PRINCIPAL-> ", response[0]);
          this.openPage(HomePage);
          // this.loadComunidad();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }

  async initSideMenu() {
    this.introDone = await this.sessionService.isIntroDone();
    this.comunidad = await this.storageService.fetchComunidad();
    console.log("comunidad -> ", this.comunidad);

    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      this.pages = [
        { title: "Pagina principal", component: HomePage, icon: "home" },
        { title: "Noticias", component: NoticiasPage, icon: "paper" },
        { title: "Reservas", component: ReservasPage, icon: "book" },
        { title: "Incidencias", component: IncidenciaPage, icon: "warning" },
        { title: "Documentos", component: DocumentoPage, icon: "document" },
        {
          title: "Telefonos de interes",
          component: TelefonosPage,
          icon: "call",
        },
        { title: "Mi casa", component: MicasaPage, icon: "construct" },
        { title: "Perfil", component: PerfilPage, icon: "person" },
        { title: "Acerca de", component: AcercadePage, icon: "at" },
        { title: "Ayuda", component: HelpPage, icon: "help" },
      ];
    } else {
      this.pages = [
        { title: "Pagina principal", component: HomePage, icon: "home" },
        { title: "Noticias", component: NoticiasPage, icon: "paper" },
        { title: "Incidencias", component: IncidenciaPage, icon: "warning" },
        { title: "Documentos", component: DocumentoPage, icon: "document" },
        {
          title: "Telefonos de interes",
          component: TelefonosPage,
          icon: "call",
        },
        { title: "Mi casa", component: MicasaPage, icon: "construct" },
        { title: "Perfil", component: PerfilPage, icon: "person" },
        { title: "Acerca de", component: AcercadePage, icon: "at" },
        { title: "Ayuda", component: HelpPage, icon: "help" },
      ];
    }

    this.user = await this.storageService.fetchUserInfos();
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.alertConfirmLogout();
  }

  async alertConfirmLogout() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      title: "Cierre de sesión",
      message: "Confirmar cierre de sesión",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {},
        },
        {
          text: "Cerrar sesión",
          handler: () => {
            this.sessionService.setIntroUndone();
            this.storageService.clearAll();
            this.navCtrl.setRoot(CodigoPage);
          },
        },
      ],
    });

    await alert.present();
  }
}
