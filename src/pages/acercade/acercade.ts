import { Component } from "@angular/core";
import { NavController, Platform } from "ionic-angular";
import { environment } from "../../environment/environment";
import { PoliticaPage } from "../politica/politica";

@Component({
  selector: "page-acercade",
  templateUrl: "acercade.html",
})
export class AcercadePage {
  version = "1.0.0";
  comunidad: any;
  politicaUrl = "https://app.berekstan.com/index.php/condiciones-de-uso/";
  constructor(public navCtrl: NavController, private platform: Platform) {}
  ngOnInit() {
    if (this.platform.is("ios")) {
      this.version = environment.versions.ios;
    } else {
      this.version = environment.versions.android;
    }
  }

  openPolitica() {
    this.navCtrl.push(PoliticaPage);
  }
}
