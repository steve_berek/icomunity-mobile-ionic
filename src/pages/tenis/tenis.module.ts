import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TenisPage } from './tenis';

@NgModule({
  declarations: [
    TenisPage,
  ],
  imports: [
    IonicPageModule.forChild(TenisPage),
  ],
})
export class TenisPageModule {}
