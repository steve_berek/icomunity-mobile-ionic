import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  AlertController,
  IonicPage,
} from "ionic-angular";
import { CalendarComponentOptions } from "ion2-calendar";
import * as moment from "moment";
import { RestProvider } from "./../../providers/rest/rest";
import { StorageService } from "../../providers/storage.service";
import { SessionService } from "../../providers/session.service";
import { UserModel } from "../../common/model/user.model";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { TenisBookModel } from "../../common/model/tenis-book.model";

/**
 * Generated class for the TenisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-tenis",
  templateUrl: "tenis.html",
})
export class TenisPage implements OnInit {
  comunidad: ComunidadModel;
  list: any = "mis-res";
  date: string;
  dateRange: { from: string; to: string };
  type: "string"; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
    pickMode: "single",
    from: moment().toDate(),
    to: moment().add(1, "month").toDate(),
    monthPickerFormat: [
      "ENE",
      "FEB",
      "MAR",
      "ABR",
      "MAY",
      "JUN",
      "JUL",
      "AGO",
      "SEP",
      "OCT",
      "NOV",
      "DIC",
    ],
    weekdays: ["D", "L", "M", "M", "J", "V", "S"],
    weekStart: 1,
  };
  response: string;
  loader: any;
  hoursList: Array<string> = [];
  hour_selected: string;
  bool_hours: boolean = false;
  perfil: UserModel;
  reservationList: Array<any> = [];
  bool_res_list: boolean = false;
  rest: any;

  constructor(
    public navCtrl: NavController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storageService: StorageService,
    private sessionSession: SessionService
  ) {
    this.rest = restProvider;
  }

  async ngOnInit() {
    this.init();
    this.initAuthProcess();
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    this.fetchUserInfos();
  }
  async fetchUserInfos() {
    this.perfil = await this.storageService.fetchUserInfos();
    this.getUserReservation();
  }
  async initAuthProcess() {
    const authToken = await this.sessionSession.getAuthToken();
    console.log("token process -> ", authToken);
    this.restProvider.setAuthToken(authToken);
  }
  onTabsChange() {
    console.log("TABS CHANGES");
    this.hour_selected = null;
  }
  getUserReservation() {
    this.presentLoading();
    console.log("code house :" + this.perfil["code_house"]);
    this.restProvider.tenisGetUserReservas(this.perfil["code_house"]).subscribe(
      (response) => {
        this.dismissLoading();
        console.log("USER TENIS -> ", response);
        const padels: TenisBookModel[] = response;
        if (response && response.length > 0) {
          for (let padel of padels) {
            const hours = padel.hour.split(",");
            const start = hours[0];
            const end = Number(hours[hours.length - 1]) + 1;
            padel.state = padel.active ? "activo" : "cancelado";
            padel.rangeTime = `${start}h - ${end}h`;
          }
          this.reservationList = padels;
          this.bool_res_list = true;
          this.mapUserReservations();
        } else {
          this.bool_res_list = false;
        }
      },
      (error) => {
        console.log("USER TENIS ERROR ->", error);
      }
    );
  }
  mapUserReservations() {
    const today = new Date();
    if (this.reservationList && this.reservationList.length > 0) {
      this.reservationList.forEach((item) => {
        var day = item.day;
        var date = new Date(day);
        var diff = date.getTime() - today.getTime();
        console.log("FECHA -> ", day);
        console.log("DIFF -> ", diff);
        if (diff < 0) {
          item.state = "caducado";
          item.active = false;
        }
      });
    }
  }
  anularReserva(index: number) {
    this.presentLoading();
    let day = this.reservationList[index]["day"];
    let hour = this.reservationList[index]["hour"];
    this.rest
      .tenisCancelarReserva(
        this.comunidad.code,
        day,
        hour,
        this.perfil.code_house
      )
      .subscribe(
        (response) => {
          this.dismissLoading();
          let alert = this.alertCtrl.create({
            title: "¡ Exito !",
            subTitle: "Esta reserva ha sido cancelada correctamente",
            buttons: ["OK"],
          });
          alert.present();
          alert.onDidDismiss((res) => {
            this.reservationList = null;
            this.getUserReservation();
            this.list = "mis-res";
          });
        },
        (error) => {
          console.log("error cancelar : " + error);
        }
      );
  }
  successAnular() {}

  reservar() {
    if (this.hour_selected != undefined && this.hour_selected != null) {
      this.presentLoading();
      this.rest
        .tenisReservar(
          this.comunidad.code,
          this.perfil["code_house"],
          this.date,
          this.hour_selected
        )
        .subscribe(
          (response) => {
            this.dismissLoading();
            this.successReserva();
          },
          (error) => {
            console.log(" res error :" + error);
          }
        );
    } else {
      let alert = this.alertCtrl.create({
        title: "Ojo !",
        subTitle: "No has seleccionado ninguna hora",
        buttons: ["OK"],
      });
      alert.present();
    }
  }

  successReserva() {
    let alert = this.alertCtrl.create({
      title: "¡ Enhorabuena !",
      subTitle: "reserva finalizada con exito",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      this.getUserReservation();
      this.list = "mis-res";
    });
  }

  getHoursByDay(day: string) {
    this.bool_hours = false;
    this.hour_selected = null;
    this.presentLoading();
    this.rest.tenisGetHoursByDay(this.comunidad.code, day).subscribe(
      (res) => {
        var result = null;
        let table = [];
        this.dismissLoading();
        this.response = JSON.stringify(res);
        console.log("json response : " + JSON.stringify(res));
        result = res;

        var size = Object.keys(res).length;
        for (var i = 0; i < size; i++) {
          if (result[i]["hour"] != undefined) {
            table.push(result[i]["hour"]);
            console.log("hour : " + table[i]);
          }
        }

        if (this.isEmpty(table)) {
          this.bool_hours = true;
          this.hoursList = [
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
          ];
        } else {
          this.bool_hours = true;
          var len = Object.keys(table).length;
          let temp = [
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
          ];

          for (var jdex = 0; jdex < len; jdex++) {
            for (var index = 0; index < 13; index++) {
              if (table[jdex] === temp[index]) temp.splice(index, 1);
            }
          }

          this.hoursList = temp;
        }
      },
      (error) => {
        console.log("hours error :" + error);
      }
    );
  }

  onChange(date: string) {
    this.date = moment(date).format("YYYY-MM-DD");
    let day = this.date.toString();
    this.getHoursByDay(day);
    this.hour_selected = undefined;
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }
  isReservable() {
    var size = Object.keys(this.reservationList).length;
    var block = false;
    if (size > 0) {
      for (const reserva of this.reservationList) {
        var today = new Date();
        var day = reserva.day;
        var date = new Date(day);
        var diff = date.getTime() - today.getTime();
        if (diff > 0 && reserva.active) {
          block = true;
        }
      }

      if (block) {
        let alert = this.alertCtrl.create({
          title: "Oops !",
          subTitle: "Esta casa ya tiene una reserva pendiente",
          buttons: ["OK"],
        });
        alert.present();
        alert.onDidDismiss((result) => {
          this.list = "mis-res";
        });
      } else {
        this.reservar();
      }
    } else {
      this.reservar();
    }
  }
}
