import { Component, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
} from "ionic-angular";
import { RegistroPage } from "../registro/registro";
import { HTTP } from "@ionic-native/http";
import { RestProvider } from "./../../providers/rest/rest";
import { Storage } from "@ionic/storage";
import { LoginPage } from "../login/login";

@Component({
  selector: "page-codigo",
  templateUrl: "codigo.html",
})
export class CodigoPage implements OnInit {
  codigo: string;
  listCodigos: Array<string>;
  //Comunidad variables
  comunidad: string;
  adress: string;
  phone: string;
  email: string;
  type_comunity: string;
  adminName: string;
  //Services variables
  servicesList: any = [];
  type_service: string;
  name_service: string;
  email_service: string;
  phone_service: string;

  listComunidades: Array<string>;
  okey: boolean = false;
  type: number;

  httpResultCode: any;
  responseJSON: JSON;
  responseJSON2: JSON;
  responseJSON3: JSON;
  httpResultBoolean: boolean;
  portalsJSON: JSON;
  comunityJSON: JSON;

  loader: any;
  sincroOK: boolean = false;
  phonesJSON: JSON;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private rest: RestProvider,
    private loadingCtrl: LoadingController,
    private http: HTTP,
    private navParams: NavParams,
    private storage: Storage
  ) {}
  ngOnInit(): void {
    console.log("APP_LOGS -> IN CODIGO");
  }
  public ionViewWillEnter() {
    console.log("APP_LOGS -> IN CODIGO WILL");

    /*   let code = this.navParams.get('code')|| null;
  if(code!=null){
    console.log('code readed is',code);
    this.codigo=code;
    this.verifyCodigo();
  }
 */
  }
  openLogin() {
    this.navCtrl.push(LoginPage);
  }
  openScan() {
    // this.navCtrl.push(ScanPage);
  }

  verifyCodigo() {
    if (this.codigo && this.codigo !== "") {
      this.loader = this.loadingCtrl.create({
        content: "Sincronización...",
      });

      this.getPortals(this.codigo);
      this.loader.onDidDismiss(() => {
        if (this.sincroOK == true) {
          this.navCtrl.setRoot(RegistroPage, {
            portalsList: this.portalsJSON,
            code: this.codigo,
            comunidad: this.comunidad,
            type: this.type,
          });
        }
      });
    } else {
      let alert = this.alertCtrl.create({
        title: "Aviso",
        subTitle: "Introduce el código de tu comunidad",
        buttons: ["Ok"],
      });
      alert.present();
    }
  }
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: "Codigo error ",
      subTitle: "Este codigo no pertenece a ninguna comunidad ",
      buttons: ["Ok"],
    });
    alert.present();
  }

  public getPortals(code: string) {
    this.loader.present();

    this.rest.getPortalsByCode(code).subscribe(
      (data) => {
        if (data === false) {
          let alert = this.alertCtrl.create({
            title: "Codigo inexistante",
            subTitle: "El codigo de comunidad entrado no existe",
            buttons: ["OK"],
          });
          this.loader.dismiss();
          alert.present();
        } else {
          this.portalsJSON = data;
          this.getPhones(this.codigo);
        }
      },
      (error) => {
        let alert = this.alertCtrl.create({
          title: "Problema de sincronización",
          subTitle:
            "Por favor compruebe que tienes internet y prueba otra vez portals",
          buttons: ["OK"],
        });
        this.loader.dismiss();
        alert.present();
      }
    );
  }

  public getComunityByCode(code: string) {
    this.rest.getComunityByCode(code).subscribe(
      (data) => {
        if (data === false) {
          let alert = this.alertCtrl.create({
            title: "codigo Error",
            subTitle:
              "An error occur while getting codigo , please try later comunity code ",
            buttons: ["OK"],
          });
          this.loader.dismiss();
          alert.present();
        } else {
          this.adminName = data[0]["admin_name"];
          this.email = data[0]["admin_email"];
          this.phone = data[0]["admin_phone"];
          this.adress = data[0]["admin_adress"];
          this.comunidad = data[0]["name"];
          this.type = data[0]["type"];
          let type_comunity = data[0]["type_comunity"];
          let com_image = data[0]["com_image"];
          console.log("type_com : " + this.type);
          console.log("com_image : " + com_image);
          this.getServicesByComunity(this.codigo);
        }
      },
      (error) => {
        let alert = this.alertCtrl.create({
          title: "Problema de sincronización",
          subTitle:
            "Por favor compruebe que tienes internet y prueba otra vez ",
          buttons: ["OK"],
        });
        alert.present();
      }
    );
  }

  public getServicesByComunity(code: string) {
    this.rest.getServicesByCode(code).subscribe(
      (data) => {
        if (data === false) {
          let alert = this.alertCtrl.create({
            title: "Problema de sincronización",
            subTitle:
              "Por favor compruebe que tienes internet y prueba otra vez service by com",
            buttons: ["OK"],
          });
          this.loader.dismiss();
          alert.present();
        } else {
          var count = Object.keys(data).length;

          for (let index = 0; index < count; index++) {
            let service = data[index];
            console.log("service received :" + JSON.stringify(service));

            this.type_service = service["type"];
            this.email_service = service["email"];
            this.phone_service = service["phone"];
            this.name_service = service["name"];
          }
          this.sincroOK = true;
          this.loader.dismiss();
        }
      },
      (error) => {
        let alert = this.alertCtrl.create({
          title: "code Error",
          subTitle: error.status + error.error,
          buttons: ["OK"],
        });
        this.loader.dismiss();
        alert.present();
      }
    );
  }

  qrError() {
    let confirm = this.alertCtrl.create({
      title: "QRCODE invalido",
      message: "el codigo escaneado no esta reconocido",
      buttons: [
        {
          text: "Ok",
          handler: () => {
            console.log("Okey clicked");
          },
        },
      ],
    });
    confirm.present();
  }

  showConfirm(text: string) {
    let confirm = this.alertCtrl.create({
      title: "Codigo confirmation",
      message: text,
      buttons: [
        {
          text: "Okey",
          handler: () => {
            console.log("Okey clicked");
          },
        },
      ],
    });
    confirm.present();
  }

  public getPhones(code: string) {
    this.rest.getPhonesByCode(code).subscribe(
      (data) => {
        if (data === false) {
          let alert = this.alertCtrl.create({
            title: "Internet problem",
            subTitle:
              "Por favor compruebe que tienes internet y prueba otra vez phones ",
            buttons: ["OK"],
          });
          this.loader.dismiss();
          alert.present();
        } else {
          var count = Object.keys(data).length;

          for (let index = 0; index < count; index++) {
            let phone = data[index];
            let name = phone["name"];
            let phone_number = phone["number"];
          }

          this.getComunityByCode(this.codigo);
        }
      },
      (error) => {
        let alert = this.alertCtrl.create({
          title: "Internet Problem",
          subTitle:
            "Por favor compruebe que tienes internet y prueba otra vez phones catch",
          buttons: ["OK"],
        });
        this.loader.dismiss();
        alert.present();
      }
    );
  }
}
