import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LocalPage } from "../local/local";
import * as moment from "moment";
/**
 * Generated class for the LocalResumePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-local-resume",
  templateUrl: "local-resume.html",
})
export class LocalResumePage {
  reserva: Reserva = {
    comunity: "Comunidad de propietarios",
    name: "Usuario",
    adress: "Portal del usuario",
    day: "2018-12-15",
    hours: [],
    start: 0,
    end: 0,
  };
  nbHours = this.reserva.hours.length;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.reserva.comunity = navParams.get("comunidad");
    this.reserva.name = navParams.get("name");
    this.reserva.adress = navParams.get("adress");
    this.reserva.day = moment(navParams.get("day")).format("DD-MM-YYYY");
    this.reserva.hours = navParams.get("hours");
    this.reserva.start = navParams.get("start");
    this.reserva.end = navParams.get("end");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LocalResumePage");
  }

  back() {
    this.navCtrl.setRoot(LocalPage);
  }
}
interface Reserva {
  comunity: string;
  name: string;
  adress: string;
  day: string;
  hours: number[];
  start: number;
  end: number;
}
