import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalResumePage } from './local-resume';

@NgModule({
  declarations: [
    LocalResumePage,
  ],
  imports: [
    IonicPageModule.forChild(LocalResumePage),
  ],
})
export class LocalResumePageModule {}
