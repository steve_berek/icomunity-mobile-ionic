import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HTTP } from '@ionic-native/http';

export class Service {

    httpResultCode:any;
    responseJSON:JSON;
    httpResultBoolean:boolean;
    getPortalURL:string="https://work.berekstan.com/icomunity/get_portals_list.php";

  constructor(public navCtrl: NavController,private http:HTTP,public alertCtrl:AlertController,
     private nativeStorage: NativeStorage) {
   
  }

  public getPortals(code:number){

  this.http.useBasicAuth('client','12345client67890client');
  
    this.http.post(this.getPortalURL, {
      code:code,
    }, {})
      .then(data => {
      this.httpResultCode = data.status;
      this.responseJSON = JSON.parse(data.data);
      this.httpResultBoolean = this.responseJSON['success'];
    
      if( (this.httpResultCode.valueOf() == 200)  && this.httpResultBoolean){
        console.log('code reveived succesfully');
        this.showConfirm(this.responseJSON);
   
      }else{
        let alert = this.alertCtrl.create({
        title: 'codigo Error',
        subTitle:'An error occur while getting codigo , please try later',
        buttons: ['OK']
      });
      alert.present();
      }
  
    })
    .catch(error => {
  
       let alert = this.alertCtrl.create({
        title: 'code Error',
        subTitle:error.status + error.error,
        buttons: ['OK']
      });
      alert.present();
  
    });

  }

  public getServices(code:number){

  }

  public saveComunityInfo(code:string,name:string, email:string, phone:string){

  }

  public savePortalInfo(adress:string){

  }

  public setUserInfo(phone:string,code_house:string,name:string,
    email:string,planta:string,door:string,fk_id_portal:number){

    }

    public saveUserInfo(phone:string,code_house:string,name:string,
        email:string,planta:string,door:string,fk_id_portal:number){
        
    }


    showConfirm(text:any) {
        let confirm = this.alertCtrl.create({
          title: 'Codigo confirmation',
          message: text,
          buttons: [
            
            {
              text: 'Okey',
              handler: () => {
                console.log('Okey clicked');
              }
            }
          ]
        });
        confirm.present();
      }
      
      
  }
