import { Component, ElementRef, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  NavParams,
} from "ionic-angular";
import { HelpTicketModel } from "../../common/model/help-ticket.model";
import { HttpService } from "../../providers/http.service";

/**
 * Generated class for the HelpTicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-help-ticket",
  templateUrl: "help-ticket.html",
})
export class HelpTicketPage {
  @ViewChild("chatbox") private chatbox: ElementRef;
  ticket: HelpTicketModel;
  message;
  lastMessage;
  sendIcon = "assets/imgs/send.png";
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    private http: HttpService
  ) {
    this.ticket = navParams.get("ticket")
      ? JSON.parse(navParams.get("ticket"))
      : null;
    this.processTicket();
  }

  ionViewDidLoad() {
    this.reloadTicketById();
  }
  processTicket() {
    if (
      this.ticket &&
      this.ticket.messages &&
      this.ticket.messages.length > 0
    ) {
      this.lastMessage = this.ticket.messages[this.ticket.messages.length - 1];
    }
  }
  refresh() {
    this.reloadTicketById();
  }
  reloadTicketById() {
    this.http.findTicketById(this.ticket.id).subscribe((response) => {
      this.ticket = response;
      this.processTicket();
      this.scrollToBottom();
    });
  }
  send(origin: string, target: string, isUser: boolean) {
    if (this.checkCanSend()) {
      this.http
        .sendNewTicketMessage(
          this.ticket.id,
          origin,
          target,
          this.message,
          isUser
        )
        .subscribe((response) => {
          this.message = null;
          this.reloadTicketById();
        });
    }
  }
  checkCanSend(): boolean {
    if (!this.message || this.message.length === 0) {
      let alert = this.alertCtrl.create({
        title: "AVISO",
        subTitle: "No se puede enviar un mensaje vacio",
        buttons: ["OK"],
      });
      alert.present();
      return false;
    }
    if (this.lastMessage && this.lastMessage.isUser) {
      let alert = this.alertCtrl.create({
        title: "AVISO",
        subTitle:
          "Por favor, espere a que le respondemos primero antes de enviar otros mensajes. Gracias",
        buttons: ["OK"],
      });
      alert.present();
      return false;
    }
    if (this.ticket && this.ticket.closed) {
      let alert = this.alertCtrl.create({
        title: "TICKET CERRADO",
        subTitle:
          "No se puede enviar un mensaje en este ticket porque esta cerrado",
        buttons: ["OK"],
      });
      alert.present();
      return false;
    }
    return true;
  }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.chatbox.nativeElement.scrollTop =
          this.chatbox.nativeElement.scrollHeight;
      }, 500);
    } catch (err) {
      console.log("error -> ", err);
    }
  }
}
