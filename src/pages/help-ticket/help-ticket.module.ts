import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpTicketPage } from './help-ticket';

@NgModule({
  declarations: [
    HelpTicketPage,
  ],
  imports: [
    IonicPageModule.forChild(HelpTicketPage),
  ],
})
export class HelpTicketPageModule {}
