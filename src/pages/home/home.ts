import { Component, OnInit } from "@angular/core";
import {
  NavController,
  AlertController,
  Platform,
  LoadingController,
} from "ionic-angular";
import { NoticiasPage } from "../noticias/noticias";
import { PadelPage } from "../padel/padel";
import { LocalPage } from "../local/local";
import { RestProvider } from "./../../providers/rest/rest";
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { Storage } from "@ionic/storage";
import { Market } from "@ionic-native/market";
import { AppVersion } from "@ionic-native/app-version";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { StorageService } from "../../providers/storage.service";
import { PhoneModel } from "../../common/model/phone.model";
import { ServiceModel } from "../../common/model/service.model";
import { SessionService } from "../../providers/session.service";
import { PiscinaPage } from "../piscina/piscina";
import { UserModel } from "../../common/model/user.model";
import { SolariumPage } from "../solarium/solarium";
import { ConfigService } from "../../providers/config.service";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
  providers: [RestProvider],
})
export class HomePage implements OnInit {
  id: number;

  user: UserModel;
  fb_token;
  loader: any;
  sincroOK: boolean = false;
  httpResultCode: any;
  responseJSON: JSON;
  newsJSON: JSON;
  time: string;

  com_admin_name: string;
  com_admin_phone: string;
  com_admin_email: string;
  com_admin_adress: string;
  com_code: string;
  com_name: string;
  com_type: string;
  com_type_comunity: string;

  perfil: any;
  images: any = {};
  com_image: string;
  user_email: string;
  token: string;
  rest: any;
  mostrarPiscina = false;
  mostrarSolarium = false;
  comunidad: ComunidadModel;
  buildingIcon = "assets/icon/building.png";
  tutoReservaUrl =
    "https://work.berekstan.com/icomunity/videos/tutorial_reserva_icomunity.mp4";
  userChristmasActive = true;

  constructor(
    public navCtrl: NavController,
    private push: Push,
    private restProvider: RestProvider,
    private storage: Storage,
    private platform: Platform,
    private market: Market,
    private app: AppVersion,
    private alertCtrl: AlertController,
    private storageService: StorageService,
    private sessionService: SessionService,
    private configService: ConfigService,
    private loadingCtrl: LoadingController
  ) {
    this.rest = restProvider;
  }
  async ngOnInit() {
    this.initAuthProcess();
    this.initApp();
    this.icomunityUpdate();
    this.updatePushToken();
    this.configService.listenChristmasActive().subscribe((christmasActive) => {
      this.userChristmasActive = christmasActive;
    });
  }
  async initApp() {
    this.comunidad = await this.storageService.fetchComunidad();
    console.log("FETCH COMUNIDAD -> ", this.comunidad);
    if (this.comunidad && this.comunidad.code) {
      this.showPiscinaButton();
      this.showSolariumButton();
      this.fetchMasterData();
    }
  }
  async initAuthProcess() {
    const authToken = await this.sessionService.getAuthToken();
    console.log("token process -> ", authToken);
    this.restProvider.setAuthToken(authToken);
  }
  async loadComunidad() {
    this.comunidad = await this.storageService.fetchComunidad();
    console.log("STORAGE COMUNIDAD -> ", this.comunidad);
  }
  openReservaPiscina() {
    this.navCtrl.push(PiscinaPage);
  }
  openReservaSolarium() {
    this.navCtrl.push(SolariumPage);
  }
  async fetchMasterData() {
    await this.fetchComunidad();
    await this.fetchServices();
    await this.fetchPhones();
    await this.fetchUserInfos();
  }

  async fetchUserInfos() {
    const user = await this.storageService.fetchUserInfos();
    if (user) {
      const data = {
        email: user.email,
        code_comunity: user.code,
      };
      await this.restProvider.doGetUserInfos(data).subscribe(
        (response) => {
          console.log(" USER INFOS DATA -> ", JSON.stringify(response));
          if (response.result) {
            // LOGIN OK , WE DOWNLOAD ALL THE INFOS WE NEED
            const userInfos: UserModel = response.result;
            userInfos.notificationActive =
              userInfos.fb_token && userInfos.fb_token !== "" ? true : false;
            this.storageService.saveUserInfos(userInfos);
            this.sessionService.setAuthToken(userInfos.auth_token);
          } else {
            // SHOW ERROR MESSAGE
          }
        },
        (error) => {
          console.log(" ERROR MANDATORY APP DATA -> ", error);
        }
      );
    }
  }
  async showPiscinaButton() {
    if (this.comunidad && this.comunidad.features.length > 0) {
      for (const feat of this.comunidad.features) {
        if (feat === "piscina") this.mostrarPiscina = true;
      }
    }
  }
  async showSolariumButton() {
    if (this.comunidad && this.comunidad.features.length > 0) {
      for (const feat of this.comunidad.features) {
        if (feat === "solarium") this.mostrarSolarium = true;
      }
    }
  }
  async updatePushToken() {
    await this.loadUserInfos();
    await this.loadPushToken();
    if (this.fb_token && this.user) {
      console.log("UPDATE_PUSH_TOKEN -> ", this.fb_token, this.user.email);
      this.restProvider.updatePushToken(this.user.email, this.fb_token);
    } else {
      this.initPushNotification();
    }
  }
  async loadUserInfos() {
    this.user = await this.storageService.fetchUserInfos();
  }
  async loadPushToken() {
    this.fb_token = await this.storageService.fetchPushTokn();
  }
  async fetchPhones() {
    await this.restProvider.getPhonesByCode(this.comunidad.code).subscribe(
      (response) => {
        console.log(" PHONES DATA -> ", response);
        if (response) {
          const phones: PhoneModel[] = response;
          this.storageService.savePhones(phones);
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchServices() {
    await this.restProvider.getServicesByCode(this.comunidad.code).subscribe(
      (response) => {
        console.log(" SERVICES DATA -> ", response);
        if (response) {
          const services: ServiceModel[] = response;
          this.storageService.saveServices(services);
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchComunidad() {
    console.log("FETCH LOAD COMUNIDAD -> ", this.comunidad);
    await this.restProvider.getComunityByCode(this.comunidad.code).subscribe(
      (response) => {
        if (response[0]) {
          const comunidad: ComunidadModel = response[0];
          this.storageService.saveComunidad(comunidad);
          this.comunidad = comunidad;
          console.log(" COMUNIDAD DATA -> ", response[0]);
          // this.loadComunidad();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }

  hideCamera() {
    (window.document.querySelector("ion-app") as HTMLElement).classList.remove(
      "cameraView"
    );
  }

  goToNoticiasPage() {
    this.navCtrl.push(NoticiasPage);
  }
  goToPadelPage() {
    this.navCtrl.push(PadelPage);
  }
  goToLocalPage() {
    this.navCtrl.push(LocalPage);
  }

  updateAuthTokenLocally() {
    this.storage
      .get("auth_token")
      .then((result) => {
        if (result) {
          let auth_token = result;
          console.log("auth_token : " + auth_token);
        } else {
          this.rest.updateAuthToken(this.perfil["email"]).subscribe(
            (data) => {
              let result = data["result"];
              if (!result) {
                console.log("auth token updated error");
              } else {
                let auth_token = result;
                this.storage.set("auth_token", auth_token);
              }
            },
            (error) => {}
          );
        }
      })
      .catch((error) => {});
  }
  async icomunityUpdate() {
    const show = await this.sessionService.isUpdateShown();
    console.log("UPDATE SHOWN -> ", show);
    if (!show) {
      if (this.platform.is("ios")) {
        this.icomunityUpdateAlert("ios");
      } else if (this.platform.is("android")) {
        this.icomunityUpdateAlert("android");
      }
      this.sessionService.setUpdateShown();
    }
  }
  icomunityUpdateAlert(store) {
    this.rest.getVersion(store).subscribe((response) => {
      let versionWeb = response["result"].version;
      console.log("version web :", versionWeb);
      this.getVersionNumber(versionWeb, store);
    });
  }

  async getVersionNumber(versionWeb, store) {
    const versionNumber = await this.app.getVersionNumber();
    console.log("version number:", versionNumber);
    if (versionNumber == versionWeb) {
    } else {
      let alert = this.alertCtrl.create({
        title: "iComunity actualización",
        subTitle:
          "Una nueva versión de iComunity está disponible, actualizala para disfrutar de las novedades",
        buttons: [
          {
            text: "Aceptar y actualizar",
            handler: () => {
              if (store == "ios") this.market.open("icomunity/id1325772871");
              else this.market.open("developerteam.icomunity");
            },
          },
        ],
      });
      alert.present();
    }
  }

  successAlert() {
    let alert = this.alertCtrl.create({
      title: "Exito !",
      subTitle: "Se ha realizado la operación correctamente",
      buttons: ["Ok"],
    });
    alert.present();
  }

  errorAlert() {
    let alert = this.alertCtrl.create({
      title: "Error !",
      subTitle:
        "No se ha podido realizar la operación, por favor compruebe más tarde",
      buttons: ["Ok"],
    });
    alert.present();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }

  initPushNotification() {
    // to check if we have permission
    this.push.hasPermission().then((res: any) => {
      if (res.isEnabled) {
        console.log("We have permission to send push notifications");
      } else {
        console.log("We don't have permission to send push notifications");
      }
    });

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: "634760725285",
      },
      ios: {
        alert: "true",
        badge: true,
        sound: "false",
        clearBadge: true,
      },
      windows: {},
    };
    const pushObject: PushObject = this.push.init(options);
    pushObject.on("notification").subscribe((notification: any) => {
      console.log("Received a notification", notification);
    });

    pushObject.on("registration").subscribe((registration: any) => {
      this.token = registration.registrationId;
      let token_str = JSON.stringify(this.token);
      console.log("SAVING_PUSH_NOTIFICATION_TOKEN -> ", token_str);
      this.savePushToken(token_str);
    });

    pushObject
      .on("error")
      .subscribe((error) => console.error("Error with Push plugin", error));
  }

  savePushToken(token) {
    this.storageService.savePushToken(token);
  }
}
