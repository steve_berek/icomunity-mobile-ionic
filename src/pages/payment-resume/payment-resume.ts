import { Component, OnDestroy, OnInit } from "@angular/core";
// import { Stripe } from "@ionic-native/stripe";
import { AlertController, NavController, NavParams } from "ionic-angular";
import { environment } from "../../environment/environment";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { PaymentDto } from "../../common/model/dtos/payment.dto";
import { UserModel } from "../../common/model/user.model";
import { HttpService } from "../../providers/http.service";
import { StorageService } from "../../providers/storage.service";
import { ModalService } from "../../providers/modal.service";
import { PAYMENT_STATUS_ENUM } from "../../common/enums/payment-status.enum";

declare var Stripe;
/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-payment-resume",
  templateUrl: "payment-resume.html",
})
export class PaymentResumePage implements OnInit, OnDestroy {
  comunity: ComunidadModel;
  user: UserModel;
  payment: PaymentDto;
  terms = false;
  fakeProduct = "Pago reserva local: ";
  stripe = Stripe(environment.stripePubKey);
  paymentProcessing = false;
  paymentUrl = "";
  paymentTimeout;
  paymentInterval;
  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private httpService: HttpService,
    private modalervice: ModalService,
    private storageService: StorageService
  ) {}
  ngOnDestroy(): void {
    this.resetPayment();
  }
  async ngOnInit() {
    try {
      // this.setupDeeplinks();
      // this.initStripe();
      await this.init();
    } catch (error) {
      console.log("BRUUUUUUHHHHHHHH ->", error);
      this.payment = null;
      this.alertErrorCalculating();
    }
  }
  /*   setupDeeplinks() {
    this.deeplinks
      .routeWithNavController(this.navCtrl, {
        "/payment-success/local": PaymentSuccessPage,
        "/payment-error/local": PaymentErrorPage,
      })
      .subscribe(
        (match) => {
          console.log("MATCH_ROUTE", match);
        },
        (nomatch) => {
          // nomatch.$link - the full link data
          console.error("UNMATCH_ROUTE", nomatch);
        }
      );
  } */
  initStripe() {
    // this.stripe.setPublishableKey(environment.stripePuKey);
  }
  async init() {
    const units = "5";
    const unitPrice = "3";
    const unitPriceMtp = "0.014";
    const addOn = "0.25";
    const addPlus = "0";
    const detail = "Hora: 10h - 16h / Total: 5h";
    const paymentType = "local";
    this.comunity = await this.storageService.fetchComunidad();
    this.user = await this.storageService.fetchUserInfos();
    console.log("user -> ", this.user);
    console.log("comunidad -> ", this.comunity);
    if (this.comunity && this.user) {
      const product = `${this.fakeProduct} ${this.comunity.name}`;
      const amount = this.calculateAmount(
        units,
        unitPrice,
        unitPriceMtp,
        addOn,
        addPlus
      );
      this.setupPaymentDto(
        paymentType,
        product,
        amount,
        this.user.id,
        this.user.name,
        this.user.email,
        this.comunity.name,
        this.comunity.code,
        this.comunity.admin_email,
        detail
      );
    } else {
      this.alertErrorCalculating();
    }
  }
  setupPaymentDto(
    paymentType: string,
    product: string,
    amount: string,
    userId: number,
    username: string,
    email: string,
    comunityName: string,
    comunityCode: string,
    comunityEmail: string,
    detail?: string
  ) {
    this.payment = {
      paymentType,
      product,
      amount,
      userId,
      username,
      email,
      comunityName,
      comunityCode,
      comunityEmail,
      detail: detail ? detail : "",
    };
    console.log("payment -> ", this.payment);
  }

  pay() {
    this.beforeSubmitPayment();
    this.redirectToCheckout();
  }
  beforeSubmitPayment() {
    const time = new Date();
    this.payment.time = time;
  }
  initPaymentClock() {
    this.paymentTimeout = setTimeout(() => {
      // TIMEOUT END
      this.onTimeout();
    }, environment.paymentConfig.timeout);

    this.paymentInterval = setInterval(async () => {
      // INTERVAL RUNNING
      await this.checkPaymentStatus();
    }, environment.paymentConfig.pollInterval);
  }
  onTimeout() {
    let confirm = this.alertCtrl.create({
      title: "Info",
      message: "Ha caducado la sesión de pago, por favor inicia otra",
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.resetPayment();
          },
        },
      ],
    });
    confirm.present();
  }
  resetPayment() {
    clearTimeout(this.paymentTimeout);
    clearInterval(this.paymentInterval);
    this.paymentTimeout = null;
    this.paymentInterval = null;
    this.paymentProcessing = false;
    this.paymentUrl = null;
    this.storageService.removePaymentIntentID();
  }
  async checkPaymentStatus() {
    const paymentIntentID = await this.storageService.fetchPaymentIntentID();
    console.log("Payment status-> ", paymentIntentID);
    this.httpService.getPaymentStatus(paymentIntentID).subscribe(
      async (response) => {
        console.log("payment intent status response -> ", response);
        if (response && response.status === PAYMENT_STATUS_ENUM.SUCCESS) {
          this.resetPayment();
          this.modalervice.showPopupOK("Info", "Pago correctamente procesado");
        }
        if (
          response &&
          (response.status === PAYMENT_STATUS_ENUM.CANCELLED ||
            response.status === PAYMENT_STATUS_ENUM.TIMEOUT ||
            response.status === PAYMENT_STATUS_ENUM.ERROR)
        ) {
          this.resetPayment();
          this.modalervice.showPopupOK(
            "Info",
            "Hubo un error en el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
          );
        }
      },
      (error) => {
        this.modalervice.showPopupOK(
          "Error",
          "No se ha podido cancelar el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
        );
        console.log("checkout session error -> ", JSON.stringify(error));
      }
    );
  }
  async requestCancelPayment() {
    let confirm = this.alertCtrl.create({
      title: "Confirmación",
      message:
        "¿ Esta seguro que desea cancelar el proceso de pago ? Tendrá que volver a iniciar uno nuevo",
      buttons: [
        {
          text: "Si",
          handler: () => {
            this.cancelPayment();
          },
        },
        {
          text: "No",
          handler: () => {},
        },
      ],
    });
    confirm.present();
  }
  async cancelPayment() {
    const paymentIntentID = await this.storageService.fetchPaymentIntentID();
    console.log("Canceling payment -> ", paymentIntentID);
    if (this.paymentProcessing && paymentIntentID) {
      this.httpService.cancelPaymentIntent(paymentIntentID).subscribe(
        async (response) => {
          console.log("cancel payment intent response -> ", response);
          this.resetPayment();
          this.modalervice.showPopupOK(
            "Info",
            "Proceso de pago cancelado correctamente"
          );
        },
        (error) => {
          this.modalervice.showPopupOK(
            "Error",
            "No se ha podido cancelar el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
          );
          console.log("checkout session error -> ", JSON.stringify(error));
        }
      );
    }
  }
  redirectToCheckout() {
    this.httpService.createPaymentIntent(this.payment).subscribe(
      async (response) => {
        console.log("checkout session response -> ", response);
        const paymentID = response.id;
        const paymentUrl = response.url;
        if (paymentUrl && paymentUrl !== "") {
          this.paymentUrl = paymentUrl;
          this.paymentProcessing = true;
        }
        await this.storageService.savePaymentIntentID(paymentID);
        this.initPaymentClock();
      },
      (error) => {
        this.modalervice.showPopupOK(
          "Error",
          "No se ha podido iniciar el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
        );
        console.log("checkout session error -> ", JSON.stringify(error));
      }
    );
  }
  calculateAmount(
    units: string,
    unitPrice: string,
    unitPriceMtp: string,
    addOn: string,
    addPlus: string
  ): string {
    const unitsNb = Number(units);
    const unitPriceNb = Number(unitPrice);
    const unitPriceMtpNb = Number(unitPriceMtp);
    const addOnNb = Number(addOn);
    const addPlusNb = Number(addPlus);
    if (unitsNb > 0 && unitPriceNb > 0 && unitPriceMtpNb > 0 && addOnNb >= 0) {
      const amountBase = unitsNb * unitPriceNb;
      const amountMtp = amountBase * unitPriceMtpNb + addOnNb + addPlusNb;
      const amountToPay = Math.ceil((amountBase + amountMtp) * 100);
      if (String(amountToPay)) return String(amountToPay);
      else this.alertErrorCalculating();
    } else {
      this.alertErrorCalculating();
    }
  }

  alertErrorCalculating() {
    let confirm = this.alertCtrl.create({
      title: "Error",
      message:
        "No se ha podido generar su resumen de compra, por favor intenta más tarde. Si persiste, contacta con nuestro soporte para resolver la incidencia. Gracias",
      buttons: [
        {
          text: "Ok",
          handler: () => {},
        },
      ],
    });
    confirm.present();
  }

  formatAmountDecimal(amount: string): string {
    return String((Number(amount) / 100).toFixed(2));
  }
}
