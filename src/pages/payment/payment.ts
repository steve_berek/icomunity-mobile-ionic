import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController,
} from "ionic-angular";
// import { Stripe } from '@ionic-native/stripe';
import { Storage } from "@ionic/storage";
import { RestProvider } from "./../../providers/rest/rest";
import { FormBuilder, Validators } from "@angular/forms";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { UserModel } from "../../common/model/user.model";
import { environment } from "../../environment/environment";
// const STRIPE_PUBLISHABLE_KEY = "pk_live_tp3Zm0TC9lifghzEdMhppaKQ"; // VIEJA KEY
const STRIPE_PUBLISHABLE_KEY =
  "pk_live_51Jbj7XIewsJxGWuXFzCVX7bOhyrxHZa8ewnn4JR4KvLXBzgjqSVAAT3wC9vd9YSxJ86Fq1mkX6SRSKfDzb5kAm4H004gyvFDiL"; // NUEVA KEY
declare var Stripe;
/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-payment",
  templateUrl: "payment.html",
  providers: [RestProvider],
})
export class PaymentPage {
  //stripe = Stripe('pk_test_6jBWjtgK3X9bIM6SUPbQVo5X');
  //card: any;

  cardNumber: string;
  cardMonth: number;
  cardYear: number;
  cardCVV: string;
  countHours: number = 0;
  price: number = 0;
  priceExtra: number = 0;
  priceFinal: number = 0;
  priceHours: number = 1;
  datos_incorrectos: boolean = false;
  cardToken: any;
  bool_save_token: boolean = false;
  stripeObject = {
    email: "user email",
    name: "username",
    amount: 1,
    description: "Pago local",
  };
  reserva = {
    date: "",
    hours: [],
  };
  bool_card_saved: boolean = false;
  bool_change_card: boolean = false;
  cardLastNumbers: string;
  rest: any;
  loader: any;
  reservationList: any[] = [];
  code: any;
  paymentForm: any;
  validation_messages = {
    cardNumber: [
      { type: "required", message: "Debe introducir el numero de tarjeta." },
      {
        type: "maxLength",
        message: "El numero de tarjeta debe ser 16 digitos.",
      },
    ],
    cardMonth: [
      { type: "required", message: "Debe introducir el mes de caducidad." },
      { type: "maxLength", message: "El mes tiene que ser 2 digitos." },
    ],
    cardYear: [
      { type: "required", message: "Debe introducir el año de caducidad." },
      { type: "maxLength", message: "El año tiene que ser 2 digitos." },
    ],
    cardCVV: [
      { type: "required", message: "Debe introducir el CVV ." },
      { type: "maxLength", message: "El CVV tiene que ser 3 digitos." },
    ],
  };
  months = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
  ];
  years = [
    { name: "2021", value: "21" },
    { name: "2022", value: "22" },
    { name: "2023", value: "23" },
    { name: "2024", value: "24" },
    { name: "2025", value: "25" },
    { name: "2026", value: "26" },
    { name: "2027", value: "27" },
    { name: "2028", value: "28" },
    { name: "2029", value: "29" },
    { name: "2030", value: "30" },
    { name: "2031", value: "31" },
    { name: "2032", value: "32" },
    { name: "2033", value: "33" },
    { name: "2034", value: "34" },
    { name: "2035", value: "35" },
    { name: "2036", value: "36" },
    { name: "2037", value: "37" },
    { name: "2038", value: "38" },
    { name: "2039", value: "39" },
    { name: "2040", value: "40" },
  ];
  unitPrice;
  perfil: UserModel;
  comunidad: ComunidadModel;
  stripe = Stripe(environment.stripePubKey);
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    public formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private storageService: StorageService,
    public alertCtrl: AlertController,
    private storage: Storage
  ) {
    this.countHours = navParams.get("count");
    this.reserva.date = navParams.get("date");
    this.reserva.hours = navParams.get("hours");
    this.code = navParams.get("code");
    this.price = this.countHours * this.priceHours;
    this.rest = restProvider;
  }

  ionViewDidLoad() {
    // this.initForm();
    this.loadComunidad();
    // this.getComunidad();
    // this.getcardToken();
    console.log("ionViewDidLoad PaymentPage");
  }

  calculatePrices() {
    this.price = this.price * this.unitPrice;
    this.priceExtra = (this.price * 14) / 1000 + 0.25;
    this.priceFinal = this.price + this.priceExtra;
    this.priceFinal = Number(this.priceFinal.toFixed(2));
  }

  async loadComunidad() {
    this.comunidad = await this.storageService.fetchComunidad();
    this.perfil = await this.storageService.fetchUserInfos();
    console.log("STORAGE_COMUNIDAD -> ", JSON.stringify(this.comunidad));
    console.log("STORAGE_PERFIL -> ", JSON.stringify(this.perfil));
    this.code = this.comunidad.code;
    this.stripeObject.email = this.perfil.email;
    this.stripeObject.name = this.perfil.name;
    this.stripeObject.description = "Pago local " + this.comunidad.name;
    this.unitPrice = Number(this.comunidad.unitPriceLocal);
    console.log("STORAGE_UNIT_PRICE -> ", JSON.stringify(this.unitPrice));
    this.calculatePrices();
  }

  initForm() {
    this.paymentForm = this.formBuilder.group({
      cardNumber: [
        "",
        Validators.compose([Validators.maxLength(16), , Validators.required]),
      ],
      cardMonth: [
        "",
        Validators.compose([Validators.maxLength(2), Validators.required]),
      ],
      cardYear: [
        "",
        Validators.compose([Validators.maxLength(2), Validators.required]),
      ],
      cardCVV: [
        "",
        Validators.compose([Validators.maxLength(3), Validators.required]),
      ],
    });
  }
  async init() {
    this.perfil = await this.storageService.fetchUserInfos();
    this.comunidad = await this.storageService.fetchComunidad();
  }

  validateCard() {
    this.presentLoading();
    if (this.bool_card_saved) {
      this.stripeObject.amount = this.price;
      this.proceedPayment();
    } else {
      this.checkInfo();
      let card = {
        number: this.cardNumber,
        expMonth: this.cardMonth,
        expYear: this.cardYear,
        cvc: this.cardCVV,
      };
      console.log(JSON.stringify(card));

      if (this.datos_incorrectos) this.alertBadInfo();
      else {
        this.stripe
          .createCardToken(card)
          .then((token) => {
            console.log("STRIPE RESPONSE TOKEN", JSON.stringify(token));
            this.cardToken = token.id;
            if (this.bool_save_token) {
              if (!this.bool_card_saved) this.saveCardToken();

              this.stripeObject.amount = this.price;
              this.proceedPayment();

              console.log("stripe obj", JSON.stringify(this.stripeObject));
            } else {
              this.stripeObject.amount = this.price;
              this.proceedPayment();

              console.log("stripe obj", JSON.stringify(this.stripeObject));
            }
          })
          .catch((error) => {
            this.alertBadResult();
            console.error(error);
          });
      }
    }

    // Run card validation here and then attempt to tokenise
  }

  checkInfo() {
    console.log("card num", Object.keys(this.cardNumber).length);
    console.log("card month", this.cardMonth);
    console.log("card year", this.cardYear);
    console.log("card cvv", this.cardCVV);
    if (Object.keys(this.cardNumber).length != 16)
      this.datos_incorrectos = true;
    else if (this.cardMonth > 12 || this.cardMonth < 0)
      this.datos_incorrectos = true;
    else if (this.cardYear < 18 || this.cardYear > 30)
      this.datos_incorrectos = true;
    else if (Object.keys(this.cardCVV).length != 3)
      this.datos_incorrectos = true;
    else this.datos_incorrectos = false;
    //  console.log('VALIDATION FORM',this.paymentForm);
    /*     if(!this.paymentForm.valid)
    this.datos_incorrectos=true;
    else{
      if(Object.keys(this.cardNumber).length!=16)
      this.datos_incorrectos=true;
      else if(this.cardMonth>12 || this.cardMonth <0)
      this.datos_incorrectos=true;
      else if(this.cardYear<18 || this.cardYear > 30)
      this.datos_incorrectos=true;
      else if(Object.keys(this.cardCVV).length!=3)
      this.datos_incorrectos=true;
      else this.datos_incorrectos=false;
    } */
  }

  alertBadResult() {
    let alert = this.alertCtrl.create({
      title: " Fallo al proceder al pago ",
      subTitle:
        " ¡Por favor compruebe que todos los datos están correctos " +
        "y intenta más tarde !",
      buttons: ["OK"],
    });
    alert.present();
  }
  alertBadInfo() {
    let alert = this.alertCtrl.create({
      title: " Error ",
      subTitle:
        " ¡ Los datos proporcionados no son correctos, solo se aceptan Mastercad, Visa y American Express !",
      buttons: ["OK"],
    });
    alert.present();
  }
  saveCardToken() {
    this.storage.set("cardToken", this.cardToken);
    this.saveCardLastNumbers();
  }
  saveCardLastNumbers() {
    let number = this.cardNumber.substring(
      Object.keys(this.cardNumber).length - 4
    );
    this.storage.set("cardNumbers", number);
  }
  getCardLastNumbers() {
    this.storage.get("cardNumbers").then((val) => {
      console.log("card last numbers", val);
      if (val != null && val != undefined && val != "") {
        this.cardLastNumbers = val;
      }
    });
  }

  getcardToken() {
    this.storage.get("cardToken").then((val) => {
      console.log("card token", val);
      if (val != null && val != undefined && val != "") {
        this.cardToken = val;
        this.bool_card_saved = true;
        this.getCardLastNumbers();
      }
    });
  }

  deleteCard() {
    this.bool_card_saved = false;
    this.storage.remove("cardToken");
  }

  proceedPayment() {
    console.log("proceeding payment...");
    let start = 0;
    let end = 0;
    let username = this.perfil.name;
    if (this.countHours > 1) {
      start = this.reserva.hours[0];
      end = parseInt(this.reserva.hours[this.countHours - 1]) + 1;
    } else {
      start = this.reserva.hours[0];
      end = parseInt(this.reserva.hours[0]) + 1;
    }
    console.log("START HOUR", start);
    console.log("END HOUR", end);
    console.log("COMUNITY OBJECT", JSON.stringify(this.comunidad));
    console.log("STRIPE OBJECT", JSON.stringify(this.stripeObject));
    console.log("PERFIL OBJECT", JSON.stringify(this.perfil));
    let reserva = {
      comunidad: this.comunidad.name,
      adress: this.perfil.adress,
      username: username,
      day: this.reserva.date,
      start: start,
      end: end,
      totalHours: this.countHours,
      adminEmail: this.comunidad.admin_email,
      email: this.perfil.email,
      description: this.stripeObject.description,
      amount: this.stripeObject.amount,
      id: this.cardToken,
      code: this.code,
    };

    console.log("RESERVA OBJECT", JSON.stringify(reserva));
    this.rest.MakePaymentLocal(reserva).subscribe(
      (result) => {
        console.log("payment result", JSON.stringify(result));
        let value = result["result"];
        if (value) this.reservar();
        else this.failedReserva();
      },
      (error) => {
        this.dismissLoading();
        console.log("PROCCESS PAYMENT REQUEST ERROR", error);
        this.failedReserva();
      }
    );
  }

  reservar() {
    console.log(
      "proceeding local nodejs reserva...",
      JSON.stringify(this.reserva)
    );
    this.rest
      .reservarLocal(
        this.code,
        this.perfil["code_house"],
        this.reserva.date,
        this.reserva.hours
      )
      .subscribe(
        (response) => {
          this.dismissLoading();
          this.successReserva();
        },
        (error) => {
          this.dismissLoading();
          console.log("RESERVA REQUEST ERROR", error);
          this.failedReserva();
        }
      );
  }

  successReserva() {
    let alert = this.alertCtrl.create({
      title: "¡ Enhorabuena !",
      subTitle: "reserva finalizada con exito",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      //ROUTING TO LOCAL PAGE AND SHOW RESERVATIONS
      console.log("RESERVATION SUCCESS, ROUTING TO LOCAL PAGE....");
      this.goToReservations();
    });
  }

  failedReserva() {
    let alert = this.alertCtrl.create({
      title: "¡ Lo siento !",
      subTitle:
        "No se ha podido realizar el cobro, vuelve a intentarlo más tarde",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      //ROUTING TO LOCAL PAGE AND SHOW RESERVATIONS
      console.log("RESERVATION SUCCESS, ROUTING TO LOCAL PAGE....");
      this.goToReservations();
    });
  }

  goToReservations() {
    console.log("poping back to reserva...");
    this.navCtrl.pop();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 5000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }
}
interface StripeObject {
  code: string;
  pub_key: string;
}
