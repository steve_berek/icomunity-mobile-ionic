import { Component, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
} from "ionic-angular";
import { StorageService } from "../../providers/storage.service";
import { UserModel } from "../../common/model/user.model";
import { ComunidadModel } from "../../common/model/comunidad.model";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest";
import { HttpService } from "../../providers/http.service";
import { PushOptions, PushObject, Push } from "@ionic-native/push";

@Component({
  selector: "page-perfil",
  templateUrl: "perfil.html",
})
export class PerfilPage implements OnInit {
  email: any;
  nombre: any;
  apellido: any;
  adress: any;
  telefono: any;

  door: any;
  planta: any;
  portal: string;
  phone: number;
  name: any;
  fk_id_portal: number;
  code_house: any;
  perfil: UserModel;
  comunidad: ComunidadModel;
  avatarIcon = "assets/icon/avatar.png";
  edit = false;
  profileForm: FormGroup;
  validationMessages = {
    name: [{ type: "required", message: "Debe introducir el nombre" }],
    phone: [{ type: "required", message: "Debe introducir el telefono" }],
  };
  loader: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storageService: StorageService,
    public formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private httpService: HttpService,
    private push: Push,
    private restProvider: RestProvider
  ) {}
  async ngOnInit() {
    this.init();
    this.initForms();
  }

  async init() {
    this.perfil = await this.storageService.fetchUserInfos();
    this.comunidad = await this.storageService.fetchComunidad();
  }
  notificationChange(event) {
    this.perfil.notificationActive = !this.perfil.notificationActive;
    if (this.perfil.notificationActive) {
      this.requestNotificationToken();
    } else {
      this.updateUserNotificationState(false, "");
    }
  }
  editProfile() {
    this.edit = true;
    this.profileForm.controls.name.setValue(this.perfil.name);
    this.profileForm.controls.phone.setValue(this.perfil.phone);
  }
  cancelEdit() {
    this.edit = false;
  }

  updateUserNotificationState(active: boolean, fb_token: string) {
    const text = active
      ? "Va a activar las notificaciones"
      : "Va a desactivar las notificaciones";
    let confirm = this.alertCtrl.create({
      title: "Confirmación",
      message: text,
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.httpService
              .updateUserNotificationState(this.perfil.id, fb_token)
              .subscribe((response) => {
                return;
              });
          },
        },
      ],
    });
    confirm.present();
  }
  updateProfile() {
    console.log("PROFILE FORM ->", this.profileForm);
    if (this.profileForm.valid) {
      this.presentLoading();
      this.restProvider
        .updateProfileInfos(
          this.perfil.code_comunity,
          this.perfil.email,
          this.profileForm.value.name,
          this.profileForm.value.phone
        )
        .subscribe((response) => {
          console.log(" UPDATE USER RESPONSE => ", response);
          this.dismissLoading();
        });
    }
  }

  initForms() {
    this.profileForm = this.formBuilder.group({
      name: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required,
        ])
      ),
      phone: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(9),
          Validators.maxLength(9),
          Validators.required,
        ])
      ),
    });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }

  requestNotificationToken() {
    // to check if we have permission
    this.push.hasPermission().then((res: any) => {
      if (res.isEnabled) {
        console.log("We have permission to send push notifications");
      } else {
        this.pushErrorAlert();
        console.log("We don't have permission to send push notifications");
      }
    });

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: "634760725285",
      },
      ios: {
        alert: "true",
        badge: true,
        sound: "false",
        clearBadge: true,
      },
      windows: {},
    };
    const pushObject: PushObject = this.push.init(options);
    pushObject.on("registration").subscribe((registration: any) => {
      const token = registration.registrationId;
      this.updateUserNotificationState(true, token);
    });
    pushObject.on("error").subscribe((error) => {
      this.pushErrorAlert();
    });
  }

  pushErrorAlert() {
    let confirm = this.alertCtrl.create({
      title: "Error - Faltan Permisos",
      message:
        "No tenemos los permisos para activar las notificaciones Pushes en su dispositivo",
      buttons: [
        {
          text: "Ok",
          handler: () => {},
        },
      ],
    });
    confirm.present();
  }
}
