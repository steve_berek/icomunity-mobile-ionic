import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest";
import { UserModel } from "../../common/model/user.model";
import { StorageService } from "../../providers/storage.service";
import { SessionService } from "../../providers/session.service";
import { ServiceModel } from "../../common/model/service.model";
import { PhoneModel } from "../../common/model/phone.model";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { PrincipalPage } from "../principal/principal";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html",
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  codeForm: FormGroup;
  validationMessages = {
    code: [{ type: "required", message: "Debe introducir el codigo." }],
    email: [
      { type: "required", message: "Debe introducir el email" },
      { type: "pattern", message: "Debe introducir un email valido" },
    ],
  };
  code;
  isCodeRequested = false;
  errorCodeEmailMessage = "Codigo/Email incorrecto";
  errorCodeMessage = "Codigo  incorrecto";
  requestErrorEmail = false;
  requestErrorCode = false;
  loader: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private restProvider: RestProvider,
    private storageService: StorageService,
    private loadingCtrl: LoadingController,
    private sessionService: SessionService
  ) {}

  ngOnInit() {
    this.initForms();
  }

  volver() {
    this.navCtrl.pop();
  }
  async clearStorage() {
    await this.storageService.clearAll();
  }
  initForms() {
    this.loginForm = this.formBuilder.group({
      code: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required,
        ])
      ),
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
    });

    this.codeForm = this.formBuilder.group({
      code: new FormControl(
        "",
        Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required,
        ])
      ),
    });
  }

  openPrincipal() {
    this.navCtrl.setRoot(PrincipalPage);
  }
  async fetchMandatoryData() {
    this.presentLoading();
    await this.fetchComunidad();
    this.dismissLoading();
    // THEN MOVE TO HOME PAGE
  }
  async verifyLoginCode() {
    this.presentLoading();
    const data = {
      generatedCode: this.codeForm.value.code,
      code_comunity: this.loginForm.value.code,
      email: this.loginForm.value.email,
    };
    await this.restProvider.doRequestLogin(data).subscribe(
      (response) => {
        this.dismissLoading();
        console.log(" LOGIN RESULT ->", response);
        if (response.result) {
          this.requestErrorCode = false;
          console.log(" LOGIN SUCCESS ->", response.result);
          this.fetchUserInfos();
        } else {
          this.requestErrorCode = true;
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        this.dismissLoading();
        this.requestErrorCode = true;
        console.log(" ERROR LOGIN -> ", error);
      }
    );
  }

  async fetchUserInfos() {
    await this.clearStorage();
    const data = {
      email: this.loginForm.value.email,
      code_comunity: this.loginForm.value.code,
    };
    await this.restProvider.doGetUserInfos(data).subscribe(
      (response) => {
        console.log(" USER INFOS DATA -> ", response);
        if (response.result) {
          // LOGIN OK , WE DOWNLOAD ALL THE INFOS WE NEED
          const userInfos: UserModel = response.result;
          this.storageService.saveUserInfos(userInfos);
          this.sessionService.setAuthToken(userInfos.auth_token);
          this.sessionService.setIntroDone();
          this.storageService.setComunidadCode(this.loginForm.value.code);

          // WE FETCH THE MANDATORY DATA AND SAVE IT
          this.fetchMandatoryData();
        } else {
          // SHOW ERROR MESSAGE
        }
      },
      (error) => {
        console.log(" ERROR MANDATORY APP DATA -> ", error);
      }
    );
  }
  async fetchPhones() {
    await this.restProvider
      .getPhonesByCode(this.loginForm.value.code)
      .subscribe(
        (response) => {
          console.log(" PHONES DATA -> ", response);
          if (response) {
            const phones: PhoneModel[] = response;
            this.storageService.savePhones(phones);
            this.openPrincipal();
          } else {
            // SHOW ERROR MESSAGE
          }
        },
        (error) => {
          console.log(" ERROR MANDATORY APP DATA -> ", error);
        }
      );
  }
  async fetchServices() {
    await this.restProvider
      .getServicesByCode(this.loginForm.value.code)
      .subscribe(
        (response) => {
          console.log(" SERVICES DATA -> ", response);
          if (response) {
            const services: ServiceModel[] = response;
            this.storageService.saveServices(services);

            this.fetchPhones();
          } else {
            // SHOW ERROR MESSAGE
          }
        },
        (error) => {
          console.log(" ERROR MANDATORY APP DATA -> ", error);
        }
      );
  }
  async fetchComunidad() {
    await this.restProvider
      .getComunityByCode(this.loginForm.value.code)
      .subscribe(
        (response) => {
          console.log(" COMUNIDAD DATA -> ", response[0]);
          if (response[0]) {
            const comunidad: ComunidadModel = response[0];
            this.storageService.saveComunidad(comunidad);

            this.fetchServices();
          } else {
            // SHOW ERROR MESSAGE
          }
        },
        (error) => {
          console.log(" ERROR MANDATORY APP DATA -> ", error);
        }
      );
  }
  doLogin() {
    if (this.codeForm.valid) {
      this.verifyLoginCode();
    }
  }
  requestCode() {
    this.presentLoading();
    if (this.loginForm.valid) {
      const data = {
        email: this.loginForm.value.email,
        code_comunity: this.loginForm.value.code,
      };
      this.restProvider.doRequestLoginCode(data).subscribe(
        (response) => {
          this.dismissLoading();

          console.log(" REQUEST CODE RESULT -> ", response);
          if (response.result) {
            this.requestErrorEmail = false;
            this.isCodeRequested = true;
          } else {
            this.requestErrorEmail = true;
            // SHOW ERROR MESSAGE
          }
        },
        (error) => {
          this.dismissLoading();
          this.requestErrorEmail = true;
          console.log(" ERROR LOGIN CODE -> ", error);
        }
      );
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }
}
