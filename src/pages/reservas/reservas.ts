import { Component, OnInit } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { StorageService } from "../../providers/storage.service";
import { LocalPage } from "../local/local";
import { PadelPage } from "../padel/padel";
import { PiscinaPage } from "../piscina/piscina";
import { TenisPage } from "../tenis/tenis";
import { SolariumPage } from "../solarium/solarium";

/**
 * Generated class for the ReservasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-reservas",
  templateUrl: "reservas.html"
})
export class ReservasPage implements OnInit {
  features = [];
  active = false;
  infoIcon = "assets/icon/informacion.svg";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storageService: StorageService
  ) {}
  ngOnInit() {
    this.initCards();
  }

  async initCards() {
    const comunidad = await this.storageService.fetchComunidad();
    if (comunidad && comunidad.features && comunidad.features.length > 0) {
      const feats = comunidad.features;
      feats.forEach(element => {
        if (
          element === "local" ||
          element === "padel" ||
          element === "tenis" ||
          element === "piscina" ||
          element === "solarium"
        ) {
          this.active = true;
          this.features.push(element);
        }
      });
    }
  }

  openPage(feat) {
    let route;
    switch (feat) {
      case "local":
        route = LocalPage;
        break;
      case "padel":
        route = PadelPage;
        break;
      case "piscina":
        route = PiscinaPage;
        break;
      case "solarium":
        route = SolariumPage;
        break;
      case "tenis":
        route = TenisPage;
        break;
    }
    this.navCtrl.push(route);
  }
}
