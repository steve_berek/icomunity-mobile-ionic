import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

  scanSub:any;
  loader:boolean=false;

  constructor(
    public navCtrl: NavController,
    // private qrScanner: QRScanner,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.showCamera();
   // this.scan();
    console.log('ionViewDidLoad ScanPage');
  }
  ionViewWillLeave(){
   // this.qrScanner.hide();
    this.scanSub.unsubscribe(); 
    this.hideCamera(); 
 }

  /* scan(){
    this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if (status.authorized) {
        console.log('Camera Permission Given');
         this.scanSub = this.qrScanner.scan().subscribe((text: string) => {
         console.log('Scanned something', text);
         if(text!=undefined && text!=null && text!=''){
          this.qrScanner.hide();
          this.scanSub.unsubscribe(); 
          this.hideCamera();
          this.popBack(text);
         }
      
        });

        this.qrScanner.show();
      } else if (status.denied) {
        console.log('Camera permission denied');
      } else {
        console.log('Permission denied for this runtime.');
      }
    })
    .catch((e: any) => console.log('Error is', e));
} */

popBack(code){
  this.navCtrl.getPrevious().data.code = code;
  this.navCtrl.pop();
}

  
  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }
  
  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }


}
