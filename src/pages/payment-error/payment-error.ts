import { Component, OnInit } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

@Component({
  selector: "page-payment-error",
  templateUrl: "payment-error.html",
})
export class PaymentErrorPage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  async ngOnInit() {
    console.log("INIT_PAYMENT_ERROR");
  }
}
