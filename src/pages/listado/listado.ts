import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-listado',
  templateUrl: 'listado.html',
})
export class ListadoPage {

  title:string="titulo";
  list:Array<any>=[];
  bool_empty:boolean=true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list=navParams.get('list');
    this.title=navParams.get('type');

    if(this.list!=undefined){
      if(Object.keys(this.list).length>0){
        this.bool_empty=false;
      }else{
        this.bool_empty=true;
      }
    }else{
      this.bool_empty=true;
    }
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListadoPage');
  }

}
