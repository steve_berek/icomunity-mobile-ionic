import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  AlertController,
} from "ionic-angular";
import { CalendarComponentOptions } from "ion2-calendar";
import * as moment from "moment";
import { RestProvider } from "./../../providers/rest/rest";
import { PaymentPage } from "../payment/payment";
import { LocalResumePage } from "../local-resume/local-resume";
import { StorageService } from "../../providers/storage.service";
import { SessionService } from "../../providers/session.service";
import { UserModel } from "../../common/model/user.model";
import { LocalBookModel } from "../../common/model/local-book.model";
import { UtilsService } from "../../common/utils.service";
import { environment } from "../../environment/environment";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { ModalService } from "../../providers/modal.service";
import { PaymentDto } from "../../common/model/dtos/payment.dto";

@Component({
  selector: "page-local",
  templateUrl: "local.html",
})
export class LocalPage implements OnInit {
  comunidad: ComunidadModel;
  list: any = "mis-res";
  date: string;
  dateToShow: string;
  dateRange: { from: string; to: string };
  type: "string"; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
    pickMode: "single",
    from: moment().toDate(),
    to: moment().add(1, "month").endOf("month").toDate(),
    showMonthPicker: true,
    monthPickerFormat: [
      "ENE",
      "FEB",
      "MAR",
      "ABR",
      "MAY",
      "JUN",
      "JUL",
      "AGO",
      "SEP",
      "OCT",
      "NOV",
      "DIC",
    ],
    weekdays: ["D", "L", "M", "M", "J", "V", "S"],
    weekStart: 1,
  };
  response: string;
  loader: any;
  hoursList: Array<string> = [];
  hour_selected: string;
  bool_hours: boolean = false;
  user: UserModel;
  localReservations: LocalBookModel[] = [];
  bool_res_list: boolean = false;
  rest: any;
  selectedHours: any = [];
  countHours: number = 0;
  message: string = "Usted no tiene ninguna reserva pendiente";
  activeCalendar: boolean = true;
  date1: string = "";
  date2: string = "";
  paymentDto: PaymentDto;
  /*   fakeLocalBook: LocalBookModel = {
    id: '0',
    userId: 'test',
    code: 'AAA222',
    day: new Date(),
    formattedDay: '',
    hours: ['10','12','13'],
    active: true,
    username: 'steve berek',
    address: 'cabezo buenavista',
    amount: '10',
    rangeTime: '',
    createdAt: new Date(),
    formattedDate: ''
  }; */

  constructor(
    public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private restProvider: RestProvider,
    private storageService: StorageService,
    private sessionService: SessionService,
    private modalService: ModalService,
    private utilsService: UtilsService
  ) {
    this.rest = restProvider;
  }
  async ngOnInit() {
    this.init();
    this.initAuthProcess();
  }
  ionViewDidEnter() {
    console.log("ION_VIEW_ENTER");
    this.init();
  }

  onPaymentResult(result: boolean) {
    this.bool_hours = false;
    console.log("PAYMENT RESULT -> ", result);
    if (result) {
      this.finalizeReserva(true);
    }
    this.paymentDto = null;
  }
  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    this.fetchUserInfos();
    this.bool_hours = false;
  }
  async fetchUserInfos() {
    this.user = await this.storageService.fetchUserInfos();
    this.getUserReservation();
  }
  async initAuthProcess() {
    const authToken = await this.sessionService.getAuthToken();
    console.log("token process -> ", authToken);
    this.restProvider.setAuthToken(authToken);
  }

  async setHoursStartEndReservas() {
    await this.localReservations.forEach(async (item) => {
      item.end = 0;
      item.end = Number(item.hours[item.hours.length - 1]);
      item.end++;
      item.start = Number(item.hours[0]);
      item.rangeTime = `${item.start}h - ${item.end}h`;
      item.state = item.active ? "activo" : "cancelado";
      console.log("RESERVA ITEM START", item.start);
      console.log("RESERVA ITEM END", item.end);
    });
  }
  onTabsChange() {
    console.log("TABS CHANGES");
    this.hour_selected = null;
  }
  setCalendarRange(showFirstMonth, showSecondMonth) {
    console.log(
      "SETTING CALENDAR RANGE  ",
      showFirstMonth + " ---- " + showSecondMonth
    );

    if (!showFirstMonth && !showSecondMonth) {
      this.activeCalendar = false;
      this.message =
        "Usted ya tiene dos reservas pendientes, tendrá que esperar que pase la fecha de la primera antes de poder hacer una nueva reserva";
    } else if (!showSecondMonth) {
      this.activeCalendar = true;
      this.optionsRange = {
        pickMode: "single",
        from: moment().toDate(),
        to: moment().add(1, "month").add(1, "day").date(0).toDate(),
        showMonthPicker: true,
      };

      this.message = "Usted ya tiene una reserva pendiente el " + this.date2;
    } else if (!showFirstMonth) {
      this.activeCalendar = true;
      this.optionsRange = {
        pickMode: "single",
        from: moment().add(1, "month").startOf("month").toDate(),
        to: moment().add(1, "month").endOf("month").toDate(),
        showMonthPicker: true,
      };
      this.message = "Usted ya tiene una reserva pendiente el " + this.date1;
    } else {
      this.activeCalendar = true;
      this.optionsRange = {
        pickMode: "single",
        from: moment().toDate(),
        to: moment().endOf("month").toDate(),
        showMonthPicker: true,
      };
    }
    console.log("OPTION RANGE", JSON.stringify(this.optionsRange));
  }

  async checkReservations() {
    let showFirstMonth = true;
    let showSecondMonth = true;
    console.log(
      "end first month",
      moment().add(1, "month").date(0).toDate().toDateString()
    );
    console.log(
      "end second month",
      moment().add(2, "month").date(0).toDate().toDateString()
    );

    await this.localReservations.forEach(async (item) => {
      console.log("FOR EACH ITEM:", JSON.stringify(item));
      try {
        let element = moment(item.day);
        let date1 = moment().toDate();
        let date2 = moment().add(1, "month").toDate();

        console.log("date 1: ", date1);
        console.log("date 2: ", date2);
        console.log("element: ", element);
        //  let diff1 = moment().endOf('month').diff(element,'days');
        // let diff2 = moment().add(1,'month').endOf('month').diff(element,'days');
        if (
          moment(date1).isSame(element, "month") &&
          moment(date1).isSame(element, "year")
        ) {
          showFirstMonth = false;
          this.date1 = moment(item.day).format("DD-MM-YYYY");
        }

        if (
          moment(date2).isSame(element, "month") &&
          moment(date2).isSame(element, "year")
        ) {
          showSecondMonth = false;
          this.date2 = moment(item.day).format("DD-MM-YYYY");
        }

        console.log("show first month: ", showFirstMonth);
        console.log("show second month: ", showSecondMonth);
        /*   console.log('diff 1',diff1);
        console.log('diff 2',diff2);
        if(diff1<30)
        showFirstMonth=false;
        if(diff2<30)
        showSecondMonth=false; */
      } catch (error) {
        console.log("CATCH ERROR", error);
      }
    });

    console.log("boolean first month", showFirstMonth);
    console.log("boolean second month", showSecondMonth);
    this.setCalendarRange(showFirstMonth, showSecondMonth);
  }

  showReserva(item) {
    console.log("opening reserva...", JSON.stringify(item));

    this.navCtrl.setRoot(LocalResumePage, {
      comunity: this.comunidad,
      adress: this.user.adress,
      name: this.user.name,
      day: item.day,
      hours: item.hours,
      start: item.start,
      end: item.end,
    });
  }

  anularReserva(index: number) {
    this.presentLoading();
    let day = this.localReservations[index]["day"];
    let hour = this.localReservations[index]["hour"];
    this.rest.anularReserva(this.comunidad.code, day, hour).subscribe(
      (response) => {
        this.dismissLoading();
        let alert = this.alertCtrl.create({
          title: "¡ Exito !",
          subTitle: "Esta reserva ha sido cancelada correctamente",
          buttons: ["OK"],
        });
        alert.present();
        alert.onDidDismiss((res) => {
          this.localReservations = null;
          this.getUserReservation();
          this.list = "mis-res";
        });
      },
      (error) => {
        console.log("error anular : " + error);
      }
    );
  }
  successAnular() {}
  getUserReservation() {
    this.presentLoading();
    console.log("code house :" + this.user.code_house);
    this.rest.getUserReservationsLocal(this.user.code_house).subscribe(
      (response) => {
        console.log("USER RESERVATION RESPONSE:" + JSON.stringify(response));
        this.dismissLoading();
        console.log("user reservations response", JSON.stringify(response));
        this.localReservations = response;
        let size = Object.keys(response).length;

        if (size > 0) this.bool_res_list = true;
        else this.bool_res_list = false;

        if (size > 0) {
          this.checkReservations();
          this.setHoursStartEndReservas();
          this.mapUserReservations();
        }
      },
      (error) => {
        console.log("GET USER RESERVATION ERROR:" + error);
      }
    );
  }
  mapUserReservations() {
    const today = new Date();
    if (this.localReservations && this.localReservations.length > 0) {
      this.localReservations.forEach((item) => {
        var day = item.day;
        var date = new Date(day);
        var diff = date.getTime() - today.getTime();
        console.log("FECHA -> ", day);
        console.log("DIFF -> ", diff);
        if (diff < 0) {
          item.state = "caducado";
          item.active = false;
        }
      });
    }
  }
  finalizeReserva(isPayment: boolean) {
    if (isPayment) {
      this.afterPayment();
    } else {
      let confirm = this.alertCtrl.create({
        title: "Confirmación",
        message:
          "Las horas seleccionadas están disponibles. ¿ desea seguir reservando ? ",
        buttons: [
          {
            text: "Si",
            handler: () => {
              this.afterPayment();
            },
          },
          {
            text: "No",
            handler: () => {},
          },
        ],
      });
      confirm.present();
    }
  }
  afterPayment() {
    if (
      this.comunidad &&
      this.comunidad.code &&
      this.user &&
      this.user.code_house &&
      this.date &&
      this.selectedHours
    ) {
      this.rest
        .reservarLocal(
          this.comunidad.code,
          this.user.code_house,
          this.date,
          this.selectedHours
        )
        .subscribe(
          (response) => {
            this.successReserva();
          },
          (error) => {
            console.log(" res error :" + error);
          }
        );
    } else {
      let alert = this.alertCtrl.create({
        title: "Error",
        subTitle:
          "Hubo un error en el proceso, por favor contacte con Soporte mediante el menu Ayuda para resolver la incidencia",
        buttons: ["OK"],
      });
      alert.present();
    }
  }
  reservar() {
    if (this.hour_selected != undefined && this.hour_selected != null) {
      this.presentLoading();
      this.rest
        .reservarLocal(
          this.comunidad.code,
          this.user.code_house,
          this.date,
          this.selectedHours
        )
        .subscribe(
          (response) => {
            this.dismissLoading();
            this.successReserva();
          },
          (error) => {
            console.log(" res error :" + error);
          }
        );
    } else {
      let alert = this.alertCtrl.create({
        title: "Ojo !",
        subTitle: "No has seleccionado ninguna hora",
        buttons: ["OK"],
      });
      alert.present();
    }
  }

  successReserva() {
    let alert = this.alertCtrl.create({
      title: "¡ Enhorabuena !",
      subTitle: "La reserva se registro correctamente",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      this.getUserReservation();
      this.list = "mis-res";
    });
  }

  getHoursByDay(day: string) {
    this.bool_hours = false;
    this.hour_selected = null;
    this.presentLoading();
    console.log("code : " + this.comunidad.code + " day :" + day);
    this.rest.getHoursByDateLocal(this.comunidad.code, day).subscribe(
      (res) => {
        var result = null;
        let table = [];
        this.dismissLoading();
        this.response = JSON.stringify(res);
        console.log("json response : " + JSON.stringify(res));
        result = res;

        var size = Object.keys(res).length;
        for (var i = 0; i < size; i++) {
          if (result[i]["hours"] != undefined) {
            result[i]["hours"].forEach((element) => {
              table.push(element);
              console.log("hour : " + table[i]);
            });
          }
        }

        if (this.isEmpty(table)) {
          this.bool_hours = true;
          this.hoursList = [
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
          ];
        } else {
          this.bool_hours = true;
          var len = Object.keys(table).length;
          let temp = [
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
          ];

          for (var jdex = 0; jdex < len; jdex++) {
            for (var index = 0; index < 13; index++) {
              if (table[jdex] === temp[index]) temp.splice(index, 1);
            }
          }

          this.hoursList = temp;
        }
      },
      (error) => {
        console.log("hours error :" + error);
      }
    );
  }

  onChange(date: string) {
    this.date = moment(date).format("YYYY-MM-DD");
    this.dateToShow = moment(this.date).format("DD-MM-YYYY");
    let day = this.date.toString();
    this.getHoursByDay(day);
    this.selectedHours = [];
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }

  isReservable() {
    console.log(JSON.stringify(this.selectedHours));
    if (!this.checkHours()) this.alertBadHours();
    else if (this.comunidad && this.comunidad.localPay) {
      this.goToPayment();
    } else {
      this.finalizeReserva(false);
    }
  }

  goToPayment() {
    try {
      this.countHours = this.selectedHours.length;
      const paymentType = environment.paymentConfig.types.local;
      const product = `Pago reserva ${paymentType}: ${this.comunidad.name}`;
      const detail = `Hora: ${this.selectedHours[0]}h - ${
        this.selectedHours[this.selectedHours.length - 1]
      }h / Total: ${this.countHours}h`;
      console.log("COMUNITY -> ", this.comunidad);
      const amount = this.utilsService.calculateAmount(
        this.countHours,
        this.comunidad.unitPriceLocal,
        this.comunidad.unitPriceLocalMtp,
        this.comunidad.unitPriceLocalAddon,
        this.comunidad.unitPriceLocalAddPlus
      );
      if (!amount || amount === "NaN") {
        this.modalService.showPopupOK(
          "Info",
          "No se ha podido inicializar el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
        );
      } else {
        this.paymentDto = this.utilsService.setupPaymentDto(
          paymentType,
          product,
          amount,
          this.user.id,
          this.user.name,
          this.user.email,
          this.comunidad.name,
          this.comunidad.code,
          this.comunidad.admin_email,
          detail
        );
        console.log("PAYMENT_DTO ->", this.paymentDto);
      }
    } catch (error) {
      console.log("ERROR GO TO PAYMENT ->", error);
      this.modalService.showPopupOK(
        "Info",
        "No se ha podido inicializar el proceso de pago, por favor intenta más tarde o contacte con soporte desde el apartado ayuda de la App"
      );
    }
    /*     this.navCtrl.push(PaymentPage, {
      date: this.date,
      hours: this.selectedHours,
      count: this.countHours,
      code: this.comunidad.code,
    }); */
  }

  checkHours() {
    if (this.selectedHours.length == 1) {
      return true;
    } else if (this.selectedHours.length == 0) {
      return false;
    } else {
      for (let i = 1; i < this.selectedHours.length; i++) {
        if (this.selectedHours[i] - this.selectedHours[i - 1] != 1)
          return false;
      }
      return true;
    }
  }

  alertBadHours() {
    let alert = this.alertCtrl.create({
      title: " Error ",
      subTitle:
        " ¡ Las Horas seleccionadas no están correctas , no se pueden seleccionar horas sueltas !",
      buttons: ["OK"],
    });
    alert.present();
  }
}
interface Reserva {
  day: string;
  hours: number[];
  start: number;
  end: number;
}
