import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  LoadingController,
  AlertController,
  NavParams,
} from "ionic-angular";
import { RestProvider } from "./../../providers/rest/rest";
import { ListadoPage } from "../listado/listado";
import { PrincipalPage } from "../principal/principal";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
/**
 * Generated class for the MicasaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-micasa",
  templateUrl: "micasa.html",
})
export class MicasaPage implements OnInit {
  loader: any;
  itemList: Array<Item> = [
    {
      icon: "assets/icon/alba.png",
      name: "Albañileria",
      type: "albanileria",
    },
    {
      icon: "assets/icon/key.png",
      name: "Cerrajeria",
      type: "cerrajeria",
    },
    {
      icon: "assets/icon/ventana.png",
      name: "Cristaleria",
      type: "cristaleria",
    },
    {
      icon: "assets/icon/decora.png",
      name: "Decoracion",
      type: "decoracion",
    },
    {
      icon: "assets/icon/water.png",
      name: "Desatascos",
      type: "desatascos",
    },
    {
      icon: "assets/icon/elec.png",
      name: "Electricidad",
      type: "electricidad",
    },
    {
      icon: "assets/icon/fonta.png",
      name: "Fontaneria",
      type: "fontaneria",
    },
    {
      icon: "assets/icon/herramientas.png",
      name: "Herramientas",
      type: "herramientas",
    },
    {
      icon: "assets/icon/inmo.png",
      name: "Inmobiliaria",
      type: "inmobiliaria",
    },
    {
      icon: "assets/icon/mudanzas.png",
      name: "Mudanzas",
      type: "mudanzas",
    },
    {
      icon: "assets/icon/paint.png",
      name: "Pintura",
      type: "pintura",
    },
    {
      icon: "assets/icon/reformas.png",
      name: "Reformas",
      type: "reformas",
    },
    {
      icon: "assets/icon/seguro.png",
      name: "Seguros del hogar",
      type: "seguros",
    },
  ];
  electricidad: Array<any> = [];
  fontaneria: Array<any> = [];
  cerrajeria: Array<any> = [];
  albanileria: Array<any> = [];
  cristaleria: Array<any> = [];
  decoracion: Array<any> = [];
  desatascos: Array<any> = [];
  herramientas: Array<any> = [];
  inmobiliaria: Array<any> = [];
  mudanzas: Array<any> = [];
  pintura: Array<any> = [];
  reformas: Array<any> = [];
  seguros: Array<any> = [];
  comunidad: ComunidadModel;
  active = false;
  infoIcon = "assets/icon/informacion.svg";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private restProvider: RestProvider,
    private alertCtrl: AlertController,
    private storageService: StorageService
  ) {}
  async ngOnInit() {
    this.init();
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      for (const feat of this.comunidad.features) {
        if (feat === "micasa") {
          this.active = true;
          this.getServices();
        }
      }
    }
  }
  getServices() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
      duration: 5000,
    });
    this.loader.present();

    this.restProvider.getServicesMiCasa().subscribe(
      (data) => {
        this.loader.dismiss();
        let response = data["result"];
        if (!response) {
        } else {
          for (var i in response) {
            if (response[i]["type"] === "electricidad")
              this.electricidad.push(response[i]);
            else if (response[i]["type"] === "fontaneria")
              this.fontaneria.push(response[i]);
            else if (response[i]["type"] === "cerrajeria")
              this.cerrajeria.push(response[i]);
            else if (response[i]["type"] === "albanileria")
              this.albanileria.push(response[i]);
            else if (response[i]["type"] === "cristaleria")
              this.cristaleria.push(response[i]);
            else if (response[i]["type"] === "decoracion")
              this.decoracion.push(response[i]);
            else if (response[i]["type"] === "desatascos")
              this.desatascos.push(response[i]);
            else if (response[i]["type"] === "herramientas")
              this.herramientas.push(response[i]);
            else if (response[i]["type"] === "inmobiliaria")
              this.inmobiliaria.push(response[i]);
            else if (response[i]["type"] === "mudanzas")
              this.mudanzas.push(response[i]);
            else if (response[i]["type"] === "pintura")
              this.pintura.push(response[i]);
            else if (response[i]["type"] === "reformas")
              this.reformas.push(response[i]);
            else if (response[i]["type"] === "seguros")
              this.seguros.push(response[i]);
          }
        }

        console.log("elec list : " + JSON.stringify(this.electricidad));
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openListado(type: string) {
    console.log("type: " + type);
    let list;
    let title;

    switch (type) {
      case "electricidad":
        list = this.electricidad;
        break;
      case "fontaneria":
        list = this.fontaneria;
        break;
      case "cerrajeria":
        list = this.cerrajeria;
        break;
      case "albanileria":
        list = this.albanileria;
        break;
      case "cristaleria":
        list = this.cristaleria;
        break;
      case "decoracion":
        list = this.decoracion;
        break;
      case "desatascos":
        list = this.desatascos;
        break;
      case "herramientas":
        list = this.herramientas;
        break;
      case "inmobiliaria":
        list = this.inmobiliaria;
        break;
      case "mudanzas":
        list = this.mudanzas;
        break;
      case "pintura":
        list = this.pintura;
        break;
      case "reformas":
        list = this.reformas;
        break;
      case "seguros":
        list = this.seguros;
        break;
    }

    this.navCtrl.push(ListadoPage, {
      list: list,
      type: type,
    });
  }

  showNotAllow() {
    let confirm = this.alertCtrl.create({
      title: "Opsss...",
      message: "No puedes acceder a este apartado ",
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.navCtrl.setRoot(PrincipalPage);
          },
        },
      ],
    });
    confirm.present();
  }
}

interface Item {
  icon: string;
  name: string;
  type: string;
}
