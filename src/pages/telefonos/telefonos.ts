import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { PhoneModel } from "../../common/model/phone.model";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";

@Component({
  selector: "page-telefonos",
  templateUrl: "telefonos.html",
})
export class TelefonosPage implements OnInit {
  id: number;
  loader: any;
  sincroOK: boolean = false;
  phones: PhoneModel[] = [];
  httpResultCode: any;
  infoIcon = "assets/icon/informacion.svg";
  active = false;
  truncating = true;
  comunidad: ComunidadModel;
  constructor(
    public navCtrl: NavController,
    private storageService: StorageService
  ) {}
  async ngOnInit() {
    this.init();
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      for (const feat of this.comunidad.features) {
        if (feat === "phones") {
          this.active = true;
          this.phones = await this.storageService.fetchPhones();
        }
      }
    }
  }
}
