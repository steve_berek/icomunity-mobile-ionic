import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  AlertController,
  IonicPage,
} from "ionic-angular";
import { CalendarComponentOptions } from "ion2-calendar";
import * as moment from "moment";
import { RestProvider } from "./../../providers/rest/rest";
import { StorageService } from "../../providers/storage.service";
import { SessionService } from "../../providers/session.service";
import { UserModel } from "../../common/model/user.model";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { PiscinaBookModel } from "../../common/model/piscina-book.model";
import { DEFAULT_HOURS } from "./piscina.constants";
import { PoolHoursDayDto } from "../../common/model/dtos/poolHoursPersons.dto";

/**
 * Generated class for the PiscinaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-piscina",
  templateUrl: "piscina.html",
})
export class PiscinaPage implements OnInit {
  comunidad: ComunidadModel;
  list: any = "mis-res";
  date: string;
  dateRange: { from: string; to: string };
  type: "string"; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
    pickMode: "single",
    from: moment().toDate(),
    to: moment().add(1, "month").toDate(),
    monthPickerFormat: [
      "ENE",
      "FEB",
      "MAR",
      "ABR",
      "MAY",
      "JUN",
      "JUL",
      "AGO",
      "SEP",
      "OCT",
      "NOV",
      "DIC",
    ],
    weekdays: ["D", "L", "M", "M", "J", "V", "S"],
    weekStart: 1,
  };
  response: string;
  loader: any;
  hoursList: PoolHoursDayDto[] = [];
  hour_selected: string;
  bool_hours: boolean = false;
  bool_list_fetch = false;
  perfil: UserModel;
  reservationList: Array<PiscinaBookModel> = [];
  bool_res_list: boolean = false;
  rest: any;
  numberPersons: number;
  listNumberPersons = [];
  tempSelectedHour;
  tempSelectedLimit = 0;

  constructor(
    public navCtrl: NavController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storageService: StorageService,
    private sessionSession: SessionService
  ) {
    this.rest = restProvider;
  }

  async ngOnInit() {
    this.init();
    this.initAuthProcess();
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (this.comunidad && this.comunidad.piscinaLimit) {
      this.initPiscinaLimit();
    } else {
      this.listNumberPersons = DEFAULT_HOURS;
    }
    this.fetchUserInfos();
  }

  initPiscinaLimit() {
    this.listNumberPersons = [];
    for (let i = 0; i < this.tempSelectedLimit; i++) {
      const tuti = i + 1;
      console.log(" ITEM -> ", tuti);
      this.listNumberPersons.push(tuti);
    }
  }
  async fetchUserInfos() {
    this.perfil = await this.storageService.fetchUserInfos();
    this.getUserReservation();
  }
  async initAuthProcess() {
    const authToken = await this.sessionSession.getAuthToken();
    console.log("token process -> ", authToken);
    this.restProvider.setAuthToken(authToken);
  }
  onHourSelected(hour) {
    console.log("SELECTED HOUR -> ", hour);
    this.tempSelectedHour = hour;
    const persons = this.hoursList.find((item) => item.hour === hour).persons;
    this.tempSelectedLimit = this.comunidad.piscinaLimit - persons;
    if (this.tempSelectedLimit > this.comunidad.piscinaHouseLimit)
      this.tempSelectedLimit = this.comunidad.piscinaHouseLimit;
    this.initPiscinaLimit();
  }
  onTabsChange() {
    console.log("TABS CHANGES");
    this.numberPersons = null;
    this.hour_selected = null;
  }
  getUserReservation() {
    this.presentLoading();
    console.log("code house :" + this.perfil["code_house"]);
    this.restProvider.poolGetUserReservas(this.perfil["code_house"]).subscribe(
      (response) => {
        this.dismissLoading();
        console.log("USER PISICNA -> ", response);
        const pools: PiscinaBookModel[] = response;
        if (response && response.length > 0) {
          for (let pool of pools) {
            const start = pool.hour;
            const end = Number(pool.hour) + 1;
            pool.state = pool.active ? "activo" : "cancelado";
            pool.rangeTime = `${start}h - ${end}h`;
          }
          this.reservationList = pools;
          this.bool_res_list = true;
          this.mapUserReservations();
        } else {
          this.bool_res_list = false;
        }
      },
      (error) => {
        console.log("USER PISCINA ERROR ->", error);
      }
    );
  }
  mapUserReservations() {
    const today = new Date();
    if (this.reservationList && this.reservationList.length > 0) {
      this.reservationList.forEach((item) => {
        var day = item.day;
        var date = new Date(day);
        var diff = date.getTime() - today.getTime();
        console.log("FECHA -> ", day);
        console.log("DIFF -> ", diff);
        /*        if (diff < 0) {
          item.state = 'caducado';
          item.active = false;
        } */
      });
    }
  }
  anularReserva(index: number) {
    this.presentLoading();
    let day = this.reservationList[index]["day"];
    let hour = this.reservationList[index]["hour"];
    this.rest
      .poolCancelarReserva(
        this.comunidad.code,
        day,
        hour,
        this.perfil.code_house
      )
      .subscribe(
        (response) => {
          this.dismissLoading();
          let alert = this.alertCtrl.create({
            title: "¡ Exito !",
            subTitle: "Esta reserva ha sido cancelada correctamente",
            buttons: ["OK"],
          });
          alert.present();
          alert.onDidDismiss((res) => {
            this.reservationList = null;
            this.getUserReservation();
            this.list = "mis-res";
          });
        },
        (error) => {
          this.errorAlert();
          console.log("error cancelar : " + error);
        }
      );
  }
  errorAlert() {
    let alert = this.alertCtrl.create({
      title: "¡ Error !",
      subTitle:
        "No se ha podido realizar la operacón, por favor intente más tarde",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      this.getUserReservation();
      this.list = "mis-res";
    });
  }
  successAnular() {}

  reservar() {
    if (this.hour_selected != undefined && this.hour_selected != null) {
      this.presentLoading();
      this.rest
        .poolReservar(
          this.comunidad.code,
          this.perfil.code_house,
          this.perfil.name,
          this.perfil.adress,
          this.numberPersons,
          this.date,
          this.hour_selected
        )
        .subscribe(
          (response) => {
            console.log("CREATE_RESERVA -> ", response);
            if (response) {
              this.dismissLoading();
              this.successReserva();
            } else {
              this.errorAlert();
            }
          },
          (error) => {
            this.errorAlert();
            console.log(" res error :" + error);
          }
        );
    } else {
      let alert = this.alertCtrl.create({
        title: "Ojo !",
        subTitle: "No has seleccionado ninguna hora",
        buttons: ["OK"],
      });
      alert.present();
    }
  }

  successReserva() {
    let alert = this.alertCtrl.create({
      title: "¡ Enhorabuena !",
      subTitle: "reserva finalizada con exito",
      buttons: ["OK"],
    });
    alert.present();
    alert.onDidDismiss((res) => {
      this.getUserReservation();
      this.list = "mis-res";
    });
  }

  getHoursByDay(day: string) {
    this.bool_hours = false;
    this.hour_selected = null;
    this.presentLoading();
    this.rest.poolGetHoursByDay(this.comunidad.code, day).subscribe(
      (res) => {
        console.log(" POOL RES RESPONSE -> ", res);
        if (res.result && res.result.length > 0) {
          this.hoursList = res.result;
          this.bool_hours = true;
        } else {
          this.bool_hours = false;
          // DATOS NO DISPONIBLES
        }
        this.bool_list_fetch = true;
      },
      (error) => {
        console.log("hours error :" + error);
      }
    );
  }

  onChange(date: string) {
    this.date = moment(date).format("YYYY-MM-DD");
    let day = this.date.toString();
    this.getHoursByDay(day);
    this.hour_selected = undefined;
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando ...",
      duration: 3000,
    });
    this.loader.present();
  }

  dismissLoading() {
    this.loader.dismiss();
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }
  isReservable() {
    var size = Object.keys(this.reservationList).length;
    var block = false;
    if (size > 0) {
      for (const reserva of this.reservationList) {
        var today = new Date();
        var day = reserva.day;
        var date = new Date(day);
        var diff = date.getTime() - today.getTime();
        if (diff > 0 && reserva.active) {
          block = true;
        }
      }

      if (block) {
        let alert = this.alertCtrl.create({
          title: "OPERACIÓN NO PERMITIDA",
          subTitle: "Esta casa ya tiene una reserva pendiente",
          buttons: ["OK"],
        });
        alert.present();
        alert.onDidDismiss((result) => {
          this.list = "mis-res";
        });
      } else {
        this.reservar();
      }
    } else {
      this.reservar();
    }
  }
}
