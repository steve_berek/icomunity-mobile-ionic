import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PiscinaPage } from './piscina';

@NgModule({
  declarations: [
    PiscinaPage,
  ],
  imports: [
    IonicPageModule.forChild(PiscinaPage),
  ],
})
export class PiscinaPageModule {}
