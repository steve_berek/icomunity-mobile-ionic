import { Component, OnInit } from "@angular/core";
import {
  NavController,
  AlertController,
  ActionSheetController,
  ToastController,
} from "ionic-angular";
import { Camera } from "@ionic-native/camera";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { SessionService } from "../../providers/session.service";
import { ModalService } from "../../providers/modal.service";
import { HttpService } from "../../providers/http.service";
import { RestProvider } from "../../providers/rest/rest";
import { UserModel } from "../../common/model/user.model";
import { ConfigService } from "../../providers/config.service";

@Component({
  selector: "page-incidencia",
  templateUrl: "incidencia.html",
  providers: [RestProvider],
})
export class IncidenciaPage implements OnInit {
  categoriaEmail: string;
  categoriaType: string;
  tipo: any;
  description: any;

  email: string;
  name: string;
  adress: string;
  phone: string;

  subject: string;
  body: string;

  httpResultCode: any;
  responseJSON: any;
  httpResultBoolean: any;

  image: any = null;
  lastImage: string = null;
  responseImage: any;

  services: any = [
    {
      id: 1,
      active: true,
      fk_code_comunity: "AAA222",
      type: "electricidad",
      email: "steveberek@gmail.com",
      phone: "600600600",
    },
  ];
  correoFinal: Array<string>;
  id: number;
  isFoto: boolean = false;
  test: any;
  perfil: UserModel;
  loader: any;

  incidenciaJSON: JSON;
  tmpIncidenciaJSON: JSON;
  list: any = "reciente";
  category: any;

  incidenciaVacia: Incidencias = {
    title: "Incidencias",
    category: null,
    description: "No hay incidencias recientes ",
    type: null,
    created_at: "",
    day: "",
    state: 0,
    truncating: false,
  };
  incidenciaVaciaTable: Incidencias[] = [this.incidenciaVacia];
  Incidencias: any = [];
  incidenciasBase;
  bool_empty: boolean = false;
  limit: number = 40;
  active = true;
  infoIcon = "assets/icon/informacion.svg";
  nodataIcon = "assets/imgs/nodata.png";
  truncating = true;
  comunidad: ComunidadModel;
  rest: any;
  filterItems = ["Pendientes", "En progreso", "Solucionados"];
  incidenciaImage = { data: null, url: null, publicUrlPath: null };
  userChristmasActive = true;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private camera: Camera,
    private actionSheetCtrl: ActionSheetController,
    private toastCtrl: ToastController,
    private httpService: HttpService,
    private storageService: StorageService,
    private sessionService: SessionService,
    public modalService: ModalService,
    private configService: ConfigService,
    private restProvider: RestProvider
  ) {
    this.rest = restProvider;
  }
  async ngOnInit() {
    this.initAuthProcess();
    this.init();
    this.configService.listenChristmasActive().subscribe((christmasActive) => {
      this.userChristmasActive = christmasActive;
    });
  }

  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      for (const feat of this.comunidad.features) {
        if (feat === "incidencias") {
          this.active = true;
          this.services = await this.storageService.fetchServices();
          this.fetchIncidencias();
        }
      }
    }

    this.fetchUserInfos();
  }
  selectFilter(item) {
    if (item) {
      this.filterByEstado(item);
    } else {
      this.clearFilters();
    }
  }
  refresh() {
    this.categoriaType = null;
    this.tipo = null;
    this.description = null;
    this.fetchIncidencias();
  }
  async fetchUserInfos() {
    this.perfil = await this.storageService.fetchUserInfos();
  }
  async initAuthProcess() {
    const authToken = await this.sessionService.getAuthToken();
    this.httpService.setAuthToken(authToken);
  }
  onFileChange(event, id) {
    if (event.target.files && event.target.files[0]) {
      let imgUrl;
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        imgUrl = reader.result;
        this.saveImage(event.target.files[0], imgUrl);
      };
    }
  }
  async saveImage(data, url) {
    this.httpService.uploadFile(data).subscribe(
      (response: any) => {
        console.log("FILE_UPLOAD_RESPONSE ->", response);
        const publicUrlPath = response.publicUrlPath;
        this.incidenciaImage = { data, url, publicUrlPath };
        console.log("new image ->", this.incidenciaImage);
      },
      (error) => {
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle:
            "Hubo un error procesando la imagen, por favor espere unos minutos y vuelve a intentarlo",
          buttons: ["OK"],
        });
        alert.present();
      }
    );
  }
  public fetchIncidencias() {
    if (this.comunidad) {
      this.httpService.getIncidencias(this.comunidad.code).subscribe(
        (response) => {
          if (response && response.result && response.result.length > 0) {
            this.Incidencias = response.result;
            this.incidenciasBase = response.result;
          }
        },
        (error) => {
          console.log(" ERROR FETCH INCIDENCIAS DATA -> ", error);
        }
      );
    }
  }
  initializeItems(): void {
    this.tmpIncidenciaJSON = this.incidenciaJSON;
  }
  enviar() {
    this.sendEmailToAdmin();
  }

  /*   public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Selecciona una opción",
      buttons: [
        {
          text: "Coger de la galeria",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: "Usar Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: "Cancelar",
          role: "cancel",
        },
      ],
    });
    actionSheet.present();
  } */
  /* 
  public takePicture(sourceType) {
    // Create options for the Camera Dialog

    var options = {
      quality: 50,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      (imagePath) => {
        this.base64Image = "data:image/jpeg;base64," + imagePath;
        this.base64ImageString = imagePath;
      },
      (err) => {
        console.error(err);
        this.presentToast("Error al seleccionar la imagen");
      }
    );
  }
 */
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top",
    });
    toast.present();
  }

  sendEmailToAdmin() {
    let confirm = this.alertCtrl.create({
      title: "Confirmación",
      message: "¿Confirma el envio de su incidencia?",
      buttons: [
        {
          text: "Si",
          handler: () => {
            let email;
            for (let i = 0; i < this.services.length; i++) {
              if (this.services[i]["type"] == this.categoriaType) {
                email = this.services[i]["email"];
              }
            }

            let options;
            if (this.incidenciaImage && this.incidenciaImage.publicUrlPath) {
              if (email == "vacio" || email == "" || email == undefined) {
                options = {
                  category: this.categoriaType,
                  comunidad: this.comunidad.name,
                  code: this.comunidad.code,
                  image: this.incidenciaImage.publicUrlPath,
                  admin_email: this.comunidad.admin_email,
                  adress: this.perfil["adress"],
                  phone: this.perfil["phone"],
                  user_email: this.perfil["email"],
                  description: this.description,
                  username: this.perfil["name"],
                  type: this.tipo,
                };
              } else {
                options = {
                  category: this.categoriaType,
                  comunidad: this.comunidad.name,
                  code: this.comunidad.code,
                  image: this.incidenciaImage.publicUrlPath,
                  admin_email: this.comunidad.admin_email,
                  adress: this.perfil["adress"],
                  phone: this.perfil["phone"],
                  user_email: this.perfil["email"],
                  description: this.description,
                  username: this.perfil["name"],
                  type: this.tipo,
                  destinatorio: email,
                };
              }
            } else {
              if (email == "vacio" || email == "" || email == undefined) {
                options = {
                  category: this.categoriaType,
                  comunidad: this.comunidad.name,
                  code: this.comunidad.code,
                  admin_email: this.comunidad.admin_email,
                  adress: this.perfil["adress"],
                  phone: this.perfil["phone"],
                  user_email: this.perfil["email"],
                  description: this.description,
                  username: this.perfil["name"],
                  type: this.tipo,
                };
              } else {
                options = {
                  category: this.categoriaType,
                  comunidad: this.comunidad.name,
                  code: this.comunidad.code,
                  admin_email: this.comunidad.admin_email,
                  adress: this.perfil["adress"],
                  phone: this.perfil["phone"],
                  user_email: this.perfil["email"],
                  description: this.description,
                  destinatorio: email,
                  username: this.perfil["name"],
                  type: this.tipo,
                };
              }
            }
            console.log("new incidencia dto ->", options);
            this.rest.sendIncidencia(options).subscribe(
              (data) => {
                let result = data["result"];
                if (!result) {
                  let alert = this.alertCtrl.create({
                    title: "Error envio incidencia",
                    subTitle:
                      "Ha habido un error, no se ha podido enviar su incidencia, pruebe más tarde por favor",
                    buttons: ["OK"],
                  });

                  alert.present();
                } else {
                  this.incidenciaImage = {
                    data: null,
                    url: null,
                    publicUrlPath: null,
                  };
                  this.showConfirm();
                  this.sendIncidenciaPush(this.categoriaType, this.tipo);
                }
              },
              (error) => {
                let alert = this.alertCtrl.create({
                  title: "Error envio incidencia",
                  subTitle: "Ha habido un error, pruebe más tarde por favor",
                  buttons: ["OK"],
                });

                alert.present();
              }
            );
          },
        },
        {
          text: "Cancelar",
          handler: () => {
            confirm.dismiss();
          },
        },
      ],
    });
    confirm.present();
  }
  getBase64FromFile(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }
  sendIncidenciaPush(category: string, type: string) {
    this.httpService
      .sendNewIncidenciaPush(
        this.comunidad.code,
        this.comunidad.name,
        this.perfil.id,
        this.perfil.fb_token,
        category,
        type
      )
      .subscribe(
        (response) => {},
        (error) => {
          console.log(" ERROR SENDING PUSH -> ", error);
        }
      );
  }
  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: "¡ Incidencia enviada !",
      message: "Se ha enviado correctamente su incidencia",
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.list = "reciente";
            this.refresh();
          },
        },
      ],
    });
    confirm.present();
  }

  checkFields() {
    let message = "";

    console.log(
      "categoria : " +
        this.categoriaType +
        "tipo : " +
        this.tipo +
        "descripcion : " +
        this.description
    );

    for (var index = 0; index < 3; index++) {
      if (this.categoriaType == undefined)
        message = "Debe elegir una categoria";
      else if (this.tipo == undefined) message = "Debe elegir un tipo ";
      else if (this.description == undefined)
        message = "Debe poner una descripción ";
    }

    if (message == "") {
      this.enviar();
    } else {
      let alert = this.alertCtrl.create({
        title: "Campo requerido",
        subTitle: message,
        buttons: ["OK"],
      });
      alert.present();
    }
  }

  filterByEstado(estado) {
    this.Incidencias = this.incidenciasBase
      ? this.incidenciasBase.filter(
          (incidencia) =>
            incidencia.state ===
            (estado === "Pendientes" ? 1 : estado === "Solucionados" ? 2 : 3)
        )
      : null;
  }
  clearFilters() {
    this.Incidencias = this.incidenciasBase ? [...this.incidenciasBase] : null;
  }
}

interface Incidencias {
  title: string;
  category: string;
  type: string;
  description: string;
  created_at: string;
  day: string;
  state: number;
  truncating: boolean;
}
