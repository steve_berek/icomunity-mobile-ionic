import { Component, OnInit } from "@angular/core";
import { NavController, LoadingController } from "ionic-angular";
import { RestProvider } from "./../../providers/rest/rest";
import { StorageService } from "../../providers/storage.service";
import { ComunidadModel } from "../../common/model/comunidad.model";
import { SessionService } from "../../providers/session.service";
import { ConfigService } from "../../providers/config.service";
const SERVER_URL = "http://45.55.251.94:3008/";
@Component({
  selector: "noticias-page",
  templateUrl: "noticias.html",
  providers: [RestProvider],
})
export class NoticiasPage implements OnInit {
  id: number;
  loader: any;
  sincroOK: boolean = false;
  httpResultCode: any;

  responseJSON: JSON;
  Noticias: Noticias[] = [];
  newsJSON: JSON;
  time: string;
  com_code: string;
  bool_empty: boolean = true;
  rest: any;

  limit: number = 40;
  truncating = true;
  active = false;
  infoIcon = "assets/icon/informacion.svg";
  comunidad: ComunidadModel;
  userChristmasActive = true;
  constructor(
    public navCtrl: NavController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private storageService: StorageService,
    private configService: ConfigService,
    private sessionService: SessionService
  ) {
    this.rest = restProvider;
  }
  async ngOnInit() {
    this.initAuthProcess();
    this.init();
    this.configService.listenChristmasActive().subscribe((christmasActive) => {
      this.userChristmasActive = christmasActive;
    });
  }
  async initAuthProcess() {
    const authToken = await this.sessionService.getAuthToken();
    console.log("token process -> ", authToken);
    this.restProvider.setAuthToken(authToken);
  }
  async init() {
    this.comunidad = await this.storageService.fetchComunidad();
    if (
      this.comunidad &&
      this.comunidad.features &&
      this.comunidad.features.length > 0
    ) {
      for (const feat of this.comunidad.features) {
        if (feat === "noticias") {
          this.active = true;
          this.getNews(this.comunidad.code);
        }
      }
    }
  }

  public getNews(code: string) {
    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
      duration: 5000,
    });
    this.loader.present();
    console.log("0NOTICIAS CODE-> ", code);

    this.rest.doGetNews(code).subscribe(
      (data) => {
        console.log("0NOTICIAS -> ", JSON.stringify(data));
        this.loader.dismiss();
        let result = data["result"];
        if (!result) {
          this.bool_empty = false;
        } else {
          this.bool_empty = true;
          this.Noticias = data["result"];
          for (let noticia of this.Noticias) {
            if (noticia.publicUrlPath && noticia.publicUrlPath !== "") {
              noticia.image = noticia.publicUrlPath;
            } else if (
              noticia.image != null &&
              noticia.image != undefined &&
              noticia.image != ""
            ) {
              noticia.image = SERVER_URL + noticia.image;
            }
          }
        }
      },
      (error) => {
        this.loader.dismiss();
        console.log(error);
      }
    );
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.getNews(this.comunidad.code);
      refresher.complete();
    }, 2000);
  }
}

interface Noticias {
  title: string;
  image: string;
  publicUrlPath: string;
  description: string;
  created_at: string;
  day: string;
  truncating: boolean;
  create: string;
}
