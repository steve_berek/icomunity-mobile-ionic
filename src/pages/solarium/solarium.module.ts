import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolariumPage } from './solarium';

@NgModule({
  declarations: [
    SolariumPage,
  ],
  imports: [
    IonicPageModule.forChild(SolariumPage),
  ],
})
export class SolariumPageModule {}
