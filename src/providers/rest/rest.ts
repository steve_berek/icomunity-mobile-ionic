import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SessionService } from '../session.service';
import { StorageService } from '../storage.service';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const headers = new Headers({'Content-type': 'application/json; charset=utf-8'});
const options = {headers:headers};
@Injectable()
export class RestProvider {
      // RESERVA SOLARIUM
      solariumDoReservarURL:string="https://backbone.berekstan.com:4492/booking/solarium/add";
      solariumUserReservaURL:string="https://backbone.berekstan.com:4492/booking/solarium/all/user";
     solariumCancelReservaURL:string="https://backbone.berekstan.com:4492/booking/solarium/deleteReserva";
     solariumHoursByDateURL:string="https://backbone.berekstan.com:4492/booking/solarium/all/byday";
    // RESERVA PISCINA
    poolDoReservarURL:string="https://backbone.berekstan.com:4490/booking/pool/add";
     poolUserReservaURL:string="https://backbone.berekstan.com:4490/booking/pool/all/user";
    poolCancelReservaURL:string="https://backbone.berekstan.com:4490/booking/pool/deleteReserva";
    poolHoursByDateURL:string="https://backbone.berekstan.com:4490/booking/pool/all/byday";
  // RESERVA TENIS
  tenisDoReservarURL:string="https://backbone.berekstan.com:4480/booking/tenis";
  tenisUserReservaURL:string="https://backbone.berekstan.com:4480/booking/tenis/all/user";
  tenisCancelReservaURL:string="https://backbone.berekstan.com:4480/booking/tenis/deleteReserva";
  tenisHoursByDateURL:string="https://backbone.berekstan.com:4480/booking/tenis/all/byday";
  ////
  hoursByDateURL:string="https://backbone.berekstan.com:4430/booking/padel/all/byday";
  onlineURL:string="https://backbone.berekstan.com:4430/booking/online";
  //RESERVA PADEL
  reservarURL:string="https://backbone.berekstan.com:4430/booking/padel";
  userReservaURL:string="https://backbone.berekstan.com:4430/booking/padel/all/user";
  deleteReservaURL:string="https://backbone.berekstan.com:4430/booking/padel/deleteReserva";
  //RESERVA LOCAL
  hoursByDateLocalURL:string="https://backbone.berekstan.com:4439/booking/local/all/byday";
  pagarLocalURL:string="https://backbone.berekstan.com:4439/charge/local";
  reservarLocalURL:string="https://backbone.berekstan.com:4439/booking/local";
  userReservaLocalURL:string="https://backbone.berekstan.com:4439/booking/local/all/user";
  deleteReservaLocalURL:string="https://backbone.berekstan.com:4439/booking/local/deleteReserva";

  incidenciaPushURL:string="https://backbone.berekstan.com:4431/push/incidencia";

  //MIGRATION NODE
  getPortalURL:string="https://backbone.berekstan.com:4433/core/get/portals";
  getComunityURL:string="https://backbone.berekstan.com:4433/core/get/comunity";
  getPhonesURL:string="https://backbone.berekstan.com:4433/core/get/phones";
  getIncidenciasURL:string="https://backbone.berekstan.com:4433/core/get/incidencias";
  getNewsURL:string="https://backbone.berekstan.com:4433/core/get/news";
getServicesURL:string="https://backbone.berekstan.com:4433/core/get/services";
addIncidenciaURL:string="https://backbone.berekstan.com:4433/core/add/incidencia";
addInformacionURL:string="https://backbone.berekstan.com:4433/core/add/informacion";
addDocumentoURL:string="https://backbone.berekstan.com:4433/core/add/documento";
updatePushTokenURL:string="https://backbone.berekstan.com:4433/core/update/pushToken";
updateAuthTokenURL:string="https://backbone.berekstan.com:4433/core/update/authToken";
registerUserURL:string="https://backbone.berekstan.com:4432/register/user";
servicesMiCasaURL:string="https://backbone.berekstan.com:4433/core/get/servicesMiCasa";
aceptarRGPDURL:string="https://backbone.berekstan.com:4433/core/update/rgpd";
getRGPDURL:string="https://backbone.berekstan.com:4433/core/get/rgpd";
getVersionURL:string="https://backbone.berekstan.com:4433/core/get/version";
getFileURL:string="https://backbone.berekstan.com:4438";
authToken:string;

// COMUNITY EXTRAS
getComunityExtrasURL = 'https://backbone.berekstan.com:4433/core/get/comunity-extras';

// LOGIN PROCESS
urlRequestLoginCode = 'https://backbone.berekstan.com:4433/core/send/login/code';
urlDoLogin = 'https://backbone.berekstan.com:4433/core/verify/login/code';

// USER INFOS
urlGetUserInfos = 'https://backbone.berekstan.com:4433/core/get/user/infos';
// UPDATE USER INFOS 
urlUpdateUserInfos = 'https://backbone.berekstan.com:4432/update/user';

auth_token = '';

  constructor(
    public http: HttpClient,
    private storage:Storage, 
    private sessionService: SessionService,
    private storageService: StorageService,
    ) {
/*     console.log('pffff');
    this.storage.get('auth_token').then((result) => {
      if(result){
        this.authToken = result;
        console.log("rest auth_token : ", this.auth_token);
        options.headers.set('x-access-token', this.auth_token);
      }else{
        console.log("rest auth_token error");
      }
    })
    .catch( error =>{
   
    }); */
  }

  updateProfileInfos(code_comunity: string, email: string, name: string, phone: string){
    return this.http.post<any>(this.urlUpdateUserInfos,
      {
        code_comunity,
        email,
        name,
        phone
      }, {}).pipe(
      map( response => {
        return response;
      }),
      catchError(err => {
        return throwError(err);
      }),
    );
  }

  setAuthToken(token) {
    this.auth_token = token;
  }
  doGetIncidencias(data): Observable<any> {
    return this.doPostAuthRequest(this.getIncidenciasURL, data);
  }
  doGetNews(data): Observable<any> {
    console.log('auth_token -> ', this.auth_token);
    options.headers.set('x-access-token', this.auth_token);
    return this.http.post<any>(this.getNewsURL,
      {
        code: data
      }, {})
              .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );;
  }
  doGetUserInfos(data): Observable<any> {
    return this.doPostAuthRequest(this.urlGetUserInfos, data);
  }

  doRequestLoginCode(data): Observable<any> {
    return this.doPostAuthRequest(this.urlRequestLoginCode, data);
  }
  
  doRequestLogin(data): Observable<any> {
    return this.doPostAuthRequest(this.urlDoLogin, data);
  }

  getComunityExtras(code_comunity){
    return this.http.post<any>(this.getComunityExtrasURL,
    {
      code:code_comunity
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  MakePaymentLocal(reserva){
    console.log('REST_PAYMENT_LOCAL');
    return this.http.post<any>(this.pagarLocalURL,
    {
      reserva:reserva
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }


  getVersion(store:string){
    return this.http.post<any>(this.getVersionURL,
    {
      store:store
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  aceptarRGPD(email:string){
    return this.http.post<any>(this.aceptarRGPDURL,
    {
      email:email
    },{})
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  getRGPD(email:string){
    return this.http.post<any>(this.getRGPDURL,
    {
      email:email
    },{})
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getNews(code:string){

    return this.http.post<any>(this.getNewsURL,
    {
      code:code
    },{})
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  getIncidencias(code:string){
    return this.http.post<any>(this.getIncidenciasURL,
    {
      code:code
    },{}
  )
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  getServicesByCode(code:string){
    return this.http.post<any>(this.getServicesURL,
    {
      code:code
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getPhonesByCode(code:string){
    return this.http.post<any>(this.getPhonesURL,
    {
      code:code
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getComunityByCode(code:string): Observable<any>{
    return this.http.post<any>(this.getComunityURL,
    {
      code:code
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getServicesMiCasa(){
    return this.http.post<any>(this.servicesMiCasaURL,
    {},{})
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getPortalsByCode(code:string){
    return this.http.post<any>(this.getPortalURL,
    {
      code:code
    })
    .pipe(map( response => {
      return response;
    }),
    catchError(err => {
      return throwError(err);
    }),
  );
    
  }
  registerUser(code:string,name:string,email:string,phone:number,
    adress:string,planta:string,door:string,code_house:string, fb_token: string){
    return this.http.post<any>(this.registerUserURL,
    {
      code:code,
      name:name,
      email:email,
      phone:phone,
      adress:adress,
      planta:planta,
      door:door,
      code_house:code_house,
      fb_token
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  updatePushToken(email:string, token:string){
    return this.http.post<any>(this.updatePushTokenURL,
      {
        token:token,
        email:email 
      })
      .pipe(map( response => {
        return response;
      }),
      catchError(err => {
        return throwError(err);
      }),
    );
      
  }

  updateAuthToken(email:string){
    return this.http.post<any>(this.updateAuthTokenURL,
      {
        email:email 
      })
      .pipe(map( response => {
        return response;
      }),
      catchError(err => {
        return throwError(err);
      }),
    );
      
  }


     sendIncidencia(incidencia){
      console.log("options : "+JSON.stringify(options));
    var data;
    if(!incidencia.image && !incidencia.destinatorio){
       data = {
        code:incidencia.code,
        comunidad:incidencia.comunidad,
        admin_email:incidencia.admin_email,
        username:incidencia.username,
        user_email:incidencia.user_email,
        phone:incidencia.phone,
        description:incidencia.description,
        adress:incidencia.adress,
        type:incidencia.type,
        category:incidencia.category 
      };
    
    }else if(!incidencia.image){
      data = {
        code:incidencia.code,
        comunidad:incidencia.comunidad,
        admin_email:incidencia.admin_email,
        username:incidencia.username,
        user_email:incidencia.user_email,
        phone:incidencia.phone,
        description:incidencia.description,
        destinatorio:incidencia.destinatorio,
        adress:incidencia.adress,
        type:incidencia.type,
        category:incidencia.category 
      };
    }else if(!incidencia.destinatorio){
      data = {
        code:incidencia.code,
        comunidad:incidencia.comunidad,
        admin_email:incidencia.admin_email,
        username:incidencia.username,
        user_email:incidencia.user_email,
        phone:incidencia.phone,
        description:incidencia.description,
        image:incidencia.image,
        adress:incidencia.adress,
        type:incidencia.type,
        category:incidencia.category 
      };
    } else{
      data = {
        code:incidencia.code,
        comunidad:incidencia.comunidad,
        admin_email:incidencia.admin_email,
        username:incidencia.username,
        user_email:incidencia.user_email,
        phone:incidencia.phone,
        description:incidencia.description,
        image:incidencia.image,
        destinatorio:incidencia.destinatorio,
        adress:incidencia.adress,
        type:incidencia.type,
        category:incidencia.category 
      };
    }

console.log("incidencias data :"+ incidencia.username);
    return this.http.post<any>(this.addIncidenciaURL,
      incidencia,{})
              .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
      
  }

  sendDocumento(code:string,username:string,comunidad:string,admin_email:string,titulo:string,
    adress:string,phone:string,user_email:string,image:any){
    return this.http.post<any>(this.addDocumentoURL,
      {
        comunidad:comunidad,
        admin_email: admin_email,
        title: titulo,
        adress: adress,
        phone:phone,
        user_email:user_email,
        username: username,
        image: image
      })
              .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
      
  }

  sendInformacion(code:string,username:string,adress:string,phone:string,comunidad:string,
    user_email:string,admin_email:string,type:string,description:string,){
    return this.http.post<any>(this.addInformacionURL,
      {
        comunidad:comunidad,
        admin_email: admin_email,
        adress:adress,
        phone:phone,
        user_email:user_email,
        description:description,
        username: username,
        type:type
      },{})
              .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
      
  }

  sendIncidenciaPush(code:string){
    return this.http.post<any>(this.incidenciaPushURL,
    {
      code:code
 
    }, {})
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  /* RESERVA LOCAL */


  anularReservaLocal(code:string,day:string,hour:string,userId: string){
    return this.http.post<any>(this.deleteReservaLocalURL,
    {
      code:code,
      day:day,
      hour:hour,
      userId:userId
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getUserReservationsLocal(code_house:string){
    return this.http.post<any>(this.userReservaLocalURL,
    {
      userId:code_house
 
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  reservarLocal(code:string,userId:string,day:string,hours:string[]){
    return this.http.post<any>(this.reservarLocalURL,
    {
      code:code,
      userId:userId,
      day:day,
      hours:hours
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  /* RESERVA PADEL */

  anularReserva(code:string,day:string,hour:string, userId: string){
    return this.http.post<any>(this.deleteReservaURL,
    {
      code:code,
      day:day,
      hour:hour,
      userId:userId
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  getUserReservations(code_house:string){
    return this.http.post<any>(this.userReservaURL,
    {
      userId:code_house
 
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  reservar(code:string,userId:string,day:string,hour:string){
    return this.http.post<any>(this.reservarURL,
    {
      code:code,
      userId:userId,
      pisteId:'piste01',
      day:day,
      hour:hour
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  getHoursByDateLocal(code:string,day:string){
    return this.http.post<any>(this.hoursByDateLocalURL,
    {
      code:code,
      day:day
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  getHoursByDate(code:string,day:string){
    return this.http.post<any>(this.hoursByDateURL,
    {
      code:code,
      day:day
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  /* RESERVA TENNIS */

  tenisCancelarReserva(code:string,day:string,hour:string, userId: string){
    return this.http.post<any>(this.tenisCancelReservaURL,
    {
      code:code,
      day:day,
      hour:hour,
      userId:userId
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  tenisGetUserReservas(code_house:string){
    return this.http.post<any>(this.tenisUserReservaURL,
    {
      userId:code_house
 
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  tenisReservar(code:string,userId:string,day:string,hour:string){
    return this.http.post<any>(this.tenisDoReservarURL,
    {
      code:code,
      userId:userId,
      pisteId:'piste01',
      day:day,
      hour:hour
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  tenisGetHoursByDay(code:string,day:string){
    return this.http.post<any>(this.tenisHoursByDateURL,
    {
      code:code,
      day:day
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  /* RESERVA PISCINA */

  poolCancelarReserva(code:string,day:string,hour:string, userId:string){
    return this.http.post<any>(this.poolCancelReservaURL,
    {
      code:code,
      day:day,
      hour:hour,
      userId:userId
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  poolGetUserReservas(code_house:string){
    return this.http.post<any>(this.poolUserReservaURL,
    {
      userId:code_house
 
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  poolReservar(
    code:string,
    userId:string, 
    username: string, 
    useraddress: string, 
    persons: number,
     day:string,
     hour:string
     ){
    return this.http.post<any>(this.poolDoReservarURL,
    {
      code,
      username,
      useraddress,
      persons,
      userId,
      poolId:'pool01',
      day,
      hour
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  poolGetHoursByDay(code:string,day:string){
    return this.http.post<any>(this.poolHoursByDateURL,
    {
      code:code,
      day:day
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  
   /* RESERVA SOLARIUM */

   solariumCancelarReserva(code:string,day:string,hour:string,userId:string){
    return this.http.post<any>(this.solariumCancelReservaURL,
    {
      code:code,
      day:day,
      hour:hour,
      userId:userId
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  solariumGetUserReservas(code_house:string){
    return this.http.post<any>(this.solariumUserReservaURL,
    {
      userId:code_house
 
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  solariumReservar(
    code:string,
    userId:string, 
    username: string, 
    useraddress: string, 
    persons: number,
     day:string,
     hour:string
     ){
    return this.http.post<any>(this.solariumDoReservarURL,
    {
      code,
      username,
      useraddress,
      persons,
      userId,
      solariumId:'solarium01',
      day,
      hour
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }

  solariumGetHoursByDay(code:string,day:string){
    return this.http.post<any>(this.solariumHoursByDateURL,
    {
      code:code,
      day:day
    })
            .pipe(map( response => {
              return response;
            }),
            catchError(err => {
              return throwError(err);
            }),
          );
    
  }
  doPostAuthRequest(url: string, body: any): Observable<any> {
    const headers = {
      'x-access-token': this.auth_token
    }
    return this.http.post<any>(url, body, {headers}).pipe(
        map( response => {
          return response;
        }),
        catchError(err => {
          return throwError(err);
        }),
      );
}
  doPostRequest(url: string, body: any): Observable<any> {
    return this.http.post<any>(url, body).pipe(
        map( response => {
          return response;
        }),
        catchError(err => {
          return throwError(err);
        }),
      );
}
  extractData(res:Response){
    let body = res.json();
    return body;
  }

  handleError(error:Response|any){
    console.error(error.message||error);
    return throwError(error.message||error);
  }

}
