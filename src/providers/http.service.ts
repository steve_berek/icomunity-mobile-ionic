import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { environment } from "../environment/environment";
import { PaymentDto } from "../common/model/dtos/payment.dto";
import { StorageService } from "./storage.service";

@Injectable()
export class HttpService {
  auth_token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTUzMiwiaWF0IjoxNjMxODE3MDk4LCJleHAiOjE2NDc1ODUwOTh9.ceHVWHRV7w6c6bOJ-KTeeoGArs8rfPKGaE67tC0BF1c";
  constructor(private http: HttpClient, private storage: StorageService) {}
  setAuthToken(token) {
    this.auth_token = token;
  }
  async getComunityCode() {
    return await this.storage.fetchComunidadCode();
  }
  findTicketById(ticketId: string): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/find-help-ticket`,
      { ticketId }
    );
  }
  openHelpTicket(
    userId: number,
    fb_token: string,
    code: string,
    comunity: string,
    username: string,
    adminEmail: string,
    email: string,
    address: string,
    reason: string,
    target: string,
    message: string
  ): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/open-help-ticket`,
      {
        userId,
        fb_token,
        code,
        comunity,
        username,
        adminEmail,
        email,
        address,
        reason,
        target,
        message,
      }
    );
  }
  sendNewTicketMessage(
    ticketId: string,
    origin: string,
    target: string,
    message: string,
    isUser: boolean
  ): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/add-ticket-message`,
      { ticketId, origin, target, message, isUser }
    );
  }
  getPaymentStatus(paymentID: string): Observable<any> {
    return this.doPostRequest(
      `${environment.stripeGatewayApiUrl}/payment-intent-status`,
      { id: paymentID }
    );
  }
  cancelPaymentIntent(paymentID: string): Observable<any> {
    return this.doPostRequest(
      `${environment.stripeGatewayApiUrl}/payment-intent-cancel`,
      { id: paymentID }
    );
  }
  createPaymentIntent(payment: PaymentDto): Observable<any> {
    return this.doPostRequest(
      `${environment.stripeGatewayApiUrl}/create-payment-intent`,
      payment
    );
  }
  createStripeCheckoutSession(payment: PaymentDto): Observable<any> {
    return this.doPostRequest(
      `${environment.stripeGatewayApiUrl}/create-checkout-session`,
      payment
    );
  }
  findHelpTicketsByUser(userId: number): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/find-help-tickets-by-user`,
      { userId }
    );
  }
  sendIncidencia(incidencia): Observable<any> {
    return this.doPostRequest(
      `${environment.apiUrlCore}/add/incidencia`,
      incidencia
    );
  }
  updateUserNotificationState(
    userId: number,
    fb_token: string
  ): Observable<any> {
    return this.doPostRequest(
      `${environment.apiUrlCore}/update/user-notification`,
      {
        userId,
        fb_token,
      }
    );
  }
  getIncidencias(code: string): Observable<any> {
    return this.doPostAuthRequest(`${environment.apiUrlCore}/get/incidencias`, {
      code,
    });
  }

  getFilesTypes(): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/find-files-types`,
      {}
    );
  }
  sendNewIncidenciaPush(
    code: string,
    comunity: string,
    userId: number,
    fb_token: string,
    category: string,
    type: string
  ): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/send-push-new-incidencia`,
      { code, comunity, userId, fb_token, category, type }
    );
  }
  getDocumentsByTypes(code_comunity: string, type: string): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/documents-by-type`,
      { code_comunity, type }
    );
  }
  getDocumentsByUser(
    code_comunity: string,
    type: string,
    userId: number
  ): Observable<any> {
    return this.doPostRequest(
      `${environment.migrationApiUrl}/migration/documents-for-user`,
      { code_comunity, type, userId }
    );
  }
  uploadFile(file: any): Observable<any> {
    const formData = new FormData();
    formData.append("file", file);
    return this.http
      .post<any>(
        `${environment.migrationApiUrl}/file-uploader/upload-file`,
        formData
      )
      .pipe(
        map((response) => {
          return response;
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }
  doPostRequest(url: string, body: any): Observable<any> {
    return this.http.post<any>(url, body).pipe(
      map((response) => {
        return response;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }
  doPostAuthRequest(url: string, body: any): Observable<any> {
    const headers = {
      "x-access-token": this.auth_token,
    };
    return this.http.post<any>(url, body, { headers }).pipe(
      map((response) => {
        return response;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }
  doGetRequest(url: string, params: any): Observable<any> {
    return this.http.get<any>(url, params).pipe(
      map((response) => {
        return response;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }
}
