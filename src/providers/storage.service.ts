import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  STORAGE_PREFIX,
  USER_INFOS_KEY,
  COMUNIDAD_INFOS_KEY,
  INCIDENCIAS_KEY,
  NOTICIAS_KEY,
  PHONES_KEY,
  SERVICES_KEY,
  LOCAL_BOOKING_KEY,
  PISCINA_BOOKING_KEY,
  TENIS_BOOKING_KEY,
  PADEL_BOOKING_KEY,
  FIRST_SYNC_DONE,
  COMUNIDAD_CODE_KEY,
  PUSH_TOKEN_KEY,
  FILES_TYPES_KEY,
  DOCUMENTS_KEY,
  CONFIG_KEY,
  PAYMENT_INTENT_ID,
} from "./provider.constants";
import { UserModel } from "../common/model/user.model";
import { ComunidadModel } from "../common/model/comunidad.model";
import { IncidenciaModel } from "../common/model/incidencia.model";
import { NoticiaModel } from "../common/model/noticia.model";
import { PhoneModel } from "../common/model/phone.model";
import { ServiceModel } from "../common/model/service.model";
import { LocalBookModel } from "../common/model/local-book.model";
import { PiscinaBookModel } from "../common/model/piscina-book.model";
import { TenisBookModel } from "../common/model/tenis-book.model";
import { PadelBookModel } from "../common/model/padel-book.model";
import { ConfigModel } from "../common/model/config.model";

@Injectable()
export class StorageService {
  constructor(private storage: Storage) {}
  saveFilesTypes(list) {
    this.save(FILES_TYPES_KEY, list);
  }
  fetchFilesTypes() {
    return this.fetch(FILES_TYPES_KEY);
  }
  savePaymentIntentID(id: string) {
    this.save(PAYMENT_INTENT_ID, id);
  }
  fetchPaymentIntentID() {
    return this.fetch(PAYMENT_INTENT_ID);
  }
  removePaymentIntentID() {
    this.clear(PAYMENT_INTENT_ID);
  }
  saveConfig(config: ConfigModel) {
    this.save(CONFIG_KEY, config);
  }
  fetchConfig() {
    return this.fetch(CONFIG_KEY);
  }
  saveDocuments(docs) {
    this.save(DOCUMENTS_KEY, docs);
  }
  fetchDocuments() {
    return this.fetch(DOCUMENTS_KEY);
  }
  savePushToken(token) {
    this.save(PUSH_TOKEN_KEY, token);
  }
  fetchPushTokn() {
    return this.fetch(PUSH_TOKEN_KEY);
  }
  setComunidadCode(code: string) {
    this.save(COMUNIDAD_CODE_KEY, code);
  }
  fetchComunidadCode() {
    return this.fetch(COMUNIDAD_CODE_KEY);
  }
  setFirstSyncDone() {
    this.save(FIRST_SYNC_DONE, true);
  }
  async isFirstSyncDone() {
    return await this.fetch(FIRST_SYNC_DONE);
  }
  saveComunidad(com: ComunidadModel) {
    this.save(COMUNIDAD_INFOS_KEY, com);
  }
  async fetchComunidad() {
    return await this.fetch(COMUNIDAD_INFOS_KEY);
  }
  saveIncidencias(incidencias: IncidenciaModel[]) {
    this.save(INCIDENCIAS_KEY, incidencias);
  }
  fetchIncidencias() {
    return this.fetch(INCIDENCIAS_KEY);
  }
  saveNoticias(noticias: NoticiaModel[]) {
    this.save(NOTICIAS_KEY, noticias);
  }
  fetchNoticias() {
    return this.fetch(NOTICIAS_KEY);
  }
  savePhones(phones: PhoneModel[]) {
    this.save(PHONES_KEY, phones);
  }
  fetchPhones() {
    return this.fetch(PHONES_KEY);
  }
  saveServices(services: ServiceModel[]) {
    this.save(SERVICES_KEY, services);
  }
  fetchServices() {
    return this.fetch(SERVICES_KEY);
  }
  saveLocalBooking(locals: LocalBookModel[]) {
    this.save(LOCAL_BOOKING_KEY, locals);
  }
  fetchLocalBooking() {
    return this.fetch(LOCAL_BOOKING_KEY);
  }
  savePadelBooking(padels: PadelBookModel[]) {
    this.save(PADEL_BOOKING_KEY, padels);
  }
  fetchPadelBooking() {
    return this.fetch(PADEL_BOOKING_KEY);
  }
  saveTenisBooking(tenis: TenisBookModel[]) {
    this.save(TENIS_BOOKING_KEY, tenis);
  }
  fetchTenisBooking() {
    return this.fetch(TENIS_BOOKING_KEY);
  }
  savePiscinaBooking(pis: PiscinaBookModel[]) {
    this.save(PISCINA_BOOKING_KEY, pis);
  }
  fetchPiscinaBooking() {
    return this.fetch(PISCINA_BOOKING_KEY);
  }
  saveUserInfos(user: UserModel) {
    this.save(USER_INFOS_KEY, user);
  }
  fetchUserInfos() {
    return this.fetch(USER_INFOS_KEY);
  }
  save(key: string, payload: any) {
    this.storage.set(`${STORAGE_PREFIX}${key}`, JSON.stringify(payload));
  }

  async fetch(key: string) {
    const data = await this.storage.get(`${STORAGE_PREFIX}${key}`);
    return JSON.parse(data);
  }

  clear(key: string) {
    this.storage.remove(`${STORAGE_PREFIX}${key}`);
  }

  async clearAll() {
    await this.storage.clear();
  }
}
