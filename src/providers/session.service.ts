import { Injectable } from "@angular/core";
import { StorageService } from "./storage.service";
import { INTRO_DONE_KEY, AUTH_TOKEN_KEY, UPDATE_SHOWN_KEY } from "./provider.constants";


@Injectable()
export class SessionService {

    introDone = false;
    constructor(
        private storageService: StorageService,
    ) {

    }

    setUpdateShown() {
        this.storageService.save(UPDATE_SHOWN_KEY, true);
    }
   async isUpdateShown() {
        return  await this.storageService.fetch(UPDATE_SHOWN_KEY);
    }

    setIntroDone() {
        this.storageService.save(INTRO_DONE_KEY, true);
    }
    setIntroUndone() {
        this.storageService.save(INTRO_DONE_KEY, false);
    }
     async isIntroDone() {
        return  await this.storageService.fetch(INTRO_DONE_KEY);
    }

    setAuthToken(token) {
        this.storageService.save(AUTH_TOKEN_KEY, token);
    }

    async getAuthToken() {
        return  await this.storageService.fetch(AUTH_TOKEN_KEY);
    }
}