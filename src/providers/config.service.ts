import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable()
export class ConfigService {
  christmasActive = new Subject<boolean>();
  constructor() {
    this.christmasActive.next(true);
  }

  listenChristmasActive(): Observable<boolean> {
    return this.christmasActive;
  }
  swithChristmasActive(active: boolean) {
    this.christmasActive.next(active);
  }
}
