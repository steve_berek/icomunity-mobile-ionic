import { Injectable } from "@angular/core";
import { AlertController } from "ionic-angular";
import { Observable, Subject } from "rxjs";

@Injectable()
export class ModalService {
  visibleLoadingModal = false;
  visiblePhoneModal = false;
  visibleServiceModal = false;
  alert = { success: true, visible: false };
  DEFAULT_ALERT_TIMER = 3000;
  christmasActive = new Subject<boolean>();
  constructor(private alertCtrl: AlertController) {}

  showPopupOK(title: string, message: string) {
    let alert = this.alertCtrl.create({
      title,
      subTitle: message,
      buttons: ["OK"],
    });
    alert.present();
  }
  listenChristmasActive(): Observable<boolean> {
    return this.christmasActive;
  }
  swithChristmasActive(active: boolean) {
    this.christmasActive.next(active);
  }
  showLoadingModal(timer?: number) {
    if (timer) {
      this.visibleLoadingModal = true;
      setTimeout(() => {
        this.visibleLoadingModal = false;
      }, timer);
    }
    this.visibleLoadingModal = true;
  }
  hideLoadingModal() {
    this.visibleLoadingModal = false;
  }
  showPhoneModal() {
    this.visiblePhoneModal = true;
  }
  hidePhoneModal() {
    this.visiblePhoneModal = false;
  }
  showServiceModal() {
    this.visibleServiceModal = true;
  }
  hideServiceModal() {
    this.visibleServiceModal = false;
  }
  showAlert(success: boolean, timer?: number) {
    const TIMER = timer ? timer : this.DEFAULT_ALERT_TIMER;
    this.alert.success = success;
    this.alert.visible = true;
    setTimeout(() => {
      this.alert.visible = false;
    }, TIMER);
  }
}
