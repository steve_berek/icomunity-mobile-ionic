webpackJsonp([6],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PiscinaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__piscina_constants__ = __webpack_require__(625);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the PiscinaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PiscinaPage = /** @class */ (function () {
    function PiscinaPage(navCtrl, restProvider, loadingCtrl, alertCtrl, storageService, sessionSession) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.sessionSession = sessionSession;
        this.list = "mis-res";
        this.optionsRange = {
            pickMode: "single",
            from: __WEBPACK_IMPORTED_MODULE_2_moment__().toDate(),
            to: __WEBPACK_IMPORTED_MODULE_2_moment__()
                .add(1, "month")
                .toDate(),
            monthPickerFormat: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
            weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekStart: 1,
        };
        this.hoursList = [];
        this.bool_hours = false;
        this.bool_list_fetch = false;
        this.reservationList = [];
        this.bool_res_list = false;
        this.listNumberPersons = [];
        this.tempSelectedLimit = 0;
        this.rest = restProvider;
    }
    PiscinaPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                this.initAuthProcess();
                return [2 /*return*/];
            });
        });
    };
    PiscinaPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        if (this.comunidad && this.comunidad.piscinaLimit) {
                            this.initPiscinaLimit();
                        }
                        else {
                            this.listNumberPersons = __WEBPACK_IMPORTED_MODULE_6__piscina_constants__["a" /* DEFAULT_HOURS */];
                        }
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    PiscinaPage.prototype.initPiscinaLimit = function () {
        this.listNumberPersons = [];
        for (var i = 0; i < this.tempSelectedLimit; i++) {
            var tuti = i + 1;
            console.log(' ITEM -> ', tuti);
            this.listNumberPersons.push(tuti);
        }
    };
    PiscinaPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        this.getUserReservation();
                        return [2 /*return*/];
                }
            });
        });
    };
    PiscinaPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionSession.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    PiscinaPage.prototype.onHourSelected = function (hour) {
        console.log('SELECTED HOUR -> ', hour);
        this.tempSelectedHour = hour;
        var persons = this.hoursList.find(function (item) { return item.hour === hour; }).persons;
        this.tempSelectedLimit = this.comunidad.piscinaLimit - persons;
        if (this.tempSelectedLimit > this.comunidad.piscinaHouseLimit)
            this.tempSelectedLimit = this.comunidad.piscinaHouseLimit;
        this.initPiscinaLimit();
    };
    PiscinaPage.prototype.onTabsChange = function () {
        console.log('TABS CHANGES');
        this.numberPersons = null;
        this.hour_selected = null;
    };
    PiscinaPage.prototype.getUserReservation = function () {
        var _this = this;
        this.presentLoading();
        console.log("code house :" + this.perfil["code_house"]);
        this.restProvider.poolGetUserReservas(this.perfil["code_house"]).subscribe(function (response) {
            _this.dismissLoading();
            console.log("USER PISICNA -> ", response);
            var pools = response;
            if (response && response.length > 0) {
                for (var _i = 0, pools_1 = pools; _i < pools_1.length; _i++) {
                    var pool = pools_1[_i];
                    var start = pool.hour;
                    var end = Number(pool.hour) + 1;
                    pool.state = pool.active ? 'activo' : 'cancelado';
                    pool.rangeTime = start + "h - " + end + "h";
                }
                _this.reservationList = pools;
                _this.bool_res_list = true;
                _this.mapUserReservations();
            }
            else {
                _this.bool_res_list = false;
            }
        }, function (error) {
            console.log("USER PISCINA ERROR ->", error);
        });
    };
    PiscinaPage.prototype.mapUserReservations = function () {
        var today = new Date();
        if (this.reservationList && this.reservationList.length > 0) {
            this.reservationList.forEach(function (item) {
                var day = item.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                console.log('FECHA -> ', day);
                console.log('DIFF -> ', diff);
                if (diff < 0) {
                    item.state = 'caducado';
                    item.active = false;
                }
            });
        }
    };
    PiscinaPage.prototype.anularReserva = function (index) {
        var _this = this;
        this.presentLoading();
        var day = this.reservationList[index]["day"];
        var hour = this.reservationList[index]["hour"];
        this.rest.poolCancelarReserva(this.comunidad.code, day, hour).subscribe(function (response) {
            _this.dismissLoading();
            var alert = _this.alertCtrl.create({
                title: "¡ Exito !",
                subTitle: "Esta reserva ha sido cancelada correctamente",
                buttons: ["OK"]
            });
            alert.present();
            alert.onDidDismiss(function (res) {
                _this.reservationList = null;
                _this.getUserReservation();
                _this.list = "mis-res";
            });
        }, function (error) {
            console.log("error cancelar : " + error);
        });
    };
    PiscinaPage.prototype.successAnular = function () { };
    PiscinaPage.prototype.reservar = function () {
        var _this = this;
        if (this.hour_selected != undefined && this.hour_selected != null) {
            this.presentLoading();
            this.rest
                .poolReservar(this.comunidad.code, this.perfil.code_house, this.perfil.name, this.perfil.adress, this.numberPersons, this.date, this.hour_selected)
                .subscribe(function (response) {
                _this.dismissLoading();
                _this.successReserva();
            }, function (error) {
                console.log(" res error :" + error);
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Ojo !",
                subTitle: "No has seleccionado ninguna hora",
                buttons: ["OK"]
            });
            alert_1.present();
        }
    };
    PiscinaPage.prototype.successReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Enhorabuena !",
            subTitle: "reserva finalizada con exito",
            buttons: ["OK"]
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            _this.getUserReservation();
            _this.list = "mis-res";
        });
    };
    PiscinaPage.prototype.getHoursByDay = function (day) {
        var _this = this;
        this.bool_hours = false;
        this.hour_selected = null;
        this.presentLoading();
        this.rest.poolGetHoursByDay(this.comunidad.code, day).subscribe(function (res) {
            console.log(' POOL RES RESPONSE -> ', res);
            if (res.result && res.result.length > 0) {
                _this.hoursList = res.result;
                _this.bool_hours = true;
            }
            else {
                _this.bool_hours = false;
                // DATOS NO DISPONIBLES
            }
            _this.bool_list_fetch = true;
        }, function (error) {
            console.log("hours error :" + error);
        });
    };
    PiscinaPage.prototype.onChange = function (date) {
        this.date = __WEBPACK_IMPORTED_MODULE_2_moment__(date).format("YYYY-MM-DD");
        var day = this.date.toString();
        this.getHoursByDay(day);
        this.hour_selected = undefined;
    };
    PiscinaPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    PiscinaPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    PiscinaPage.prototype.isEmpty = function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    PiscinaPage.prototype.isReservable = function () {
        var _this = this;
        var size = Object.keys(this.reservationList).length;
        var block = false;
        if (size > 0) {
            for (var _i = 0, _a = this.reservationList; _i < _a.length; _i++) {
                var reserva = _a[_i];
                var today = new Date();
                var day = reserva.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                if (diff > 0 && reserva.active) {
                    block = true;
                }
            }
            if (block) {
                var alert_2 = this.alertCtrl.create({
                    title: "OPERACIÓN NO PERMITIDA",
                    subTitle: "Esta casa ya tiene una reserva pendiente",
                    buttons: ["OK"]
                });
                alert_2.present();
                alert_2.onDidDismiss(function (result) {
                    _this.list = "mis-res";
                });
            }
            else {
                this.reservar();
            }
        }
        else {
            this.reservar();
        }
    };
    PiscinaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-piscina',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/piscina/piscina.html"*/'<ion-header>\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Reserva de piscina</ion-title>\n    </ion-navbar>\n    <ion-segment (ionChange)="onTabsChange()" [(ngModel)]="list" padding>\n      <ion-segment-button value="nueva-res">\n        Nueva reserva\n      </ion-segment-button>\n      <ion-segment-button value="mis-res">\n        Historico res.\n      </ion-segment-button>\n    </ion-segment>\n  </ion-header>\n  <div class="c-pool">\n    <ion-content>\n      <div class="c-pool__container">\n        <div [ngSwitch]="list">\n          <ion-list class="c-pool__container__list" *ngSwitchCase="\'nueva-res\'">\n            <ion-calendar [(ngModel)]="date" [options]="optionsRange" [type]="type" [format]="\'YYYY-MM-DD\'"\n              (onChange)="onChange($event)">\n            </ion-calendar>\n            <div class="c-pool__container__selection" *ngIf="bool_hours" padding>\n                <div class="c-pool__container__selection__date">\n                    <p class="title-text"> Disponibilidades para el : {{date}} </p>\n                </div>\n            \n              <ion-item>\n                <ion-label>Hora - Aforo</ion-label>\n                <ion-select \n                #hourSSelect\n                [(ngModel)]="hour_selected" \n                multiple="false" \n                interface="action-sheet"\n                (ionChange)="onHourSelected(hourSSelect.value)"\n                >\n                  <ion-option *ngFor="let item of hoursList" [value]="item.hour">\n                    <p>{{item.hour}}h</p> \n                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n                    <p>{{item.persons}} /{{comunidad.piscinaLimit}} pers.</p>\n                  </ion-option>\n                </ion-select>\n              </ion-item>\n              <ion-item class="c-pool__container__input" *ngIf="tempSelectedHour">\n                  <ion-label>Numero pers.</ion-label>\n                  <ion-select [(ngModel)]="numberPersons" multiple="false" interface="action-sheet">\n                      <ion-option  *ngFor="let numb of listNumberPersons" [value]="numb">{{numb}} pers.\n                      </ion-option>\n                    </ion-select>\n              <!-- <ion-label>Numero personas</ion-label>\n                  <ion-input placeholder="Numero personas" type="number" [(ngModel)]="numberPersons"></ion-input> -->\n              </ion-item>\n              <button class="button-res c-pool__container__list__row__item__btn " \n              (click)="isReservable()" \n              ion-button\n                  padding>\n                Reservar\n              </button>\n            </div>\n            <div *ngIf="!bool_hours  && bool_list_fetch">\n              <p>\n                No hay datos disponible, pro favor vuelve a probar más tarde\n              </p>\n            </div>\n  \n          </ion-list>\n  \n          <ion-list class="c-pool__container__list"  *ngSwitchCase="\'mis-res\'" padding>\n            <div *ngIf="bool_res_list">\n              <ion-card class="c-pool__container__list__card" *ngFor="let reserva of reservationList; let index = index"\n                padding>\n                <p class="label-reserva">reserva piscina</p>\n                <div class="c-pool__container__list__row__item">\n                  <p class="c-pool__container__list__row__item__label">Fecha alta:</p>\n                  <p class="c-pool__container__list__row__item__value">{{reserva.createdAt | date: \'dd/MM/yyyy HH:mm\'}}\n                  </p>\n                </div>\n                <div class="c-pool__container__list__row__item">\n                  <p class="c-pool__container__list__row__item__label">Fecha reserva:</p>\n                  <p class="c-pool__container__list__row__item__value">{{reserva.day | date: \'dd/MM/yyyy\'}}</p>\n                </div>\n                <div class="c-pool__container__list__row__item">\n                  <p class="c-pool__container__list__row__item__label">Franja horaria:</p>\n                  <p class="c-pool__container__list__row__item__value">{{reserva.rangeTime}}</p>\n                </div>\n                <div class="c-pool__container__list__row__item">\n                    <p class="c-pool__container__list__row__item__label">Numero personas:</p>\n                    <p class="c-pool__container__list__row__item__value">{{reserva.persons}} pers.</p>\n                  </div>\n                <div class="c-pool__container__list__row__item">\n                  <p class="c-pool__container__list__row__item__label">Estado:</p>\n                  <p class="c-pool__container__list__row__item__value" \n                    [class.active]="reserva.active === true"\n                    [class.inactive]="reserva.state === \'cancelado\'"\n                    [class.caducado]="reserva.state === \'caducado\'"\n                    >{{reserva.state}}</p>\n                </div>\n                <button \n                *ngIf="reserva.active"\n                class="btn-res-list c-pool__container__list__row__item__btn" \n                (click)="anularReserva(index)"\n                  ion-button full padding>\n                  Cancelar\n                </button>\n              </ion-card>\n            </div>\n            <div class="c-pool__container__empty" *ngIf="!bool_res_list">\n                <p >No se han encontrado reservas de piscina.</p>\n            </div>\n  \n          </ion-list>\n        </div>\n  \n      </div>\n    </ion-content>\n  </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/piscina/piscina.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_session_service__["a" /* SessionService */]])
    ], PiscinaPage);
    return PiscinaPage;
}());

//# sourceMappingURL=piscina.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticiasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_session_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var SERVER_URL = 'http://45.55.251.94:3008/';
var NoticiasPage = /** @class */ (function () {
    function NoticiasPage(navCtrl, restProvider, loadingCtrl, storageService, sessionService) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        this.storageService = storageService;
        this.sessionService = sessionService;
        this.sincroOK = false;
        this.Noticias = [];
        this.bool_empty = true;
        this.limit = 40;
        this.truncating = true;
        this.active = false;
        this.infoIcon = 'assets/icon/informacion.svg';
        this.rest = restProvider;
    }
    NoticiasPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.initAuthProcess();
                this.init();
                return [2 /*return*/];
            });
        });
    };
    NoticiasPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionService.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    NoticiasPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _c.sent();
                        if (this.comunidad && this.comunidad.features && this.comunidad.features.length > 0) {
                            for (_i = 0, _b = this.comunidad.features; _i < _b.length; _i++) {
                                feat = _b[_i];
                                if (feat === 'noticias') {
                                    this.active = true;
                                    this.getNews(this.comunidad.code);
                                }
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    NoticiasPage.prototype.getNews = function (code) {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: "Cargando...",
            duration: 5000
        });
        this.loader.present();
        console.log('0NOTICIAS CODE-> ', code);
        this.rest.doGetNews(code)
            .subscribe(function (data) {
            console.log('0NOTICIAS -> ', data);
            _this.loader.dismiss();
            var result = data['result'];
            if (!result) {
                _this.bool_empty = false;
            }
            else {
                _this.bool_empty = true;
                _this.Noticias = data['result'];
                for (var _i = 0, _a = _this.Noticias; _i < _a.length; _i++) {
                    var noticia = _a[_i];
                    if (noticia.image != null && noticia.image != undefined && noticia.image != '') {
                        noticia.image = SERVER_URL + noticia.image;
                    }
                }
            }
        }, function (error) {
            _this.loader.dismiss();
            console.log(error);
        });
    };
    NoticiasPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.getNews(_this.comunidad.code);
            refresher.complete();
        }, 2000);
    };
    NoticiasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'noticias-page',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/noticias/noticias.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Noticias</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content class="c-noticia">\n\n\n\n  <div class="c-noticia__inactive" *ngIf="!active">\n\n    <img [src]="infoIcon" />\n\n    <p>La gestión de noticias está inabilitada en tu comunidad,\n\n      contacta con tu administrador/a para más informaciones </p>\n\n  </div>\n\n  <div class="c-noticia__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n      </ion-refresher-content>\n\n    </ion-refresher>\n\n    <br>\n\n\n\n    <div class="c-noticia__list" *ngIf="bool_empty">\n\n      <div id="content-noticia">\n\n        <ion-card class="c-noticia__card" *ngFor="let noticia of Noticias">\n\n          <ion-card-content class="chat-item item-remove-animate item-avatar item-icon-right">\n\n            <ion-card-title>\n\n              <div class="block">\n\n                <p class="c-noticia__card__title" id="title"> {{noticia.title}} </p>\n\n                <p class="c-noticia__card__created" id="create">{{noticia.create}}</p>\n\n              </div>\n\n\n\n            </ion-card-title>\n\n\n\n            <div *ngIf="noticia.description">\n\n              <div *ngIf="noticia.description.length <= limit"> {{noticia.description}}</div>\n\n              <div class="group-truncate" *ngIf="!noticia.truncating && noticia.description.length > limit">\n\n                {{noticia.description | truncate }}\n\n                <p class="btn-truncate" (click)="noticia.truncating = true">mostrar más...</p>\n\n              </div>\n\n              <div class="group-truncate" *ngIf="noticia.truncating && noticia.description.length > limit">\n\n                {{noticia.description}}\n\n                <p class="btn-truncate" (click)="noticia.truncating = false"> ...mostrar menos</p>\n\n              </div>\n\n            </div>\n\n\n\n            <a id="archivo" *ngIf="noticia.image!=undefined && noticia.image!=\'\'" href="{{noticia.image}}" ion-button\n\n              secondary>\n\n              Descargar archivo adjunto\n\n            </a>\n\n\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </div>\n\n    </div>\n\n    <div class="c-noticia__empty" *ngIf="!bool_empty">\n\n        <p>No hay noticias recientes</p>\n\n      </div>\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/noticias/noticias.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_session_service__["a" /* SessionService */]])
    ], NoticiasPage);
    return NoticiasPage;
}());

//# sourceMappingURL=noticias.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PadelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_session_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PadelPage = /** @class */ (function () {
    function PadelPage(navCtrl, restProvider, loadingCtrl, alertCtrl, storageService, sessionSession) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.sessionSession = sessionSession;
        this.list = "mis-res";
        this.optionsRange = {
            pickMode: "single",
            from: __WEBPACK_IMPORTED_MODULE_2_moment__().toDate(),
            to: __WEBPACK_IMPORTED_MODULE_2_moment__()
                .add(1, "month")
                .toDate(),
            monthPickerFormat: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
            weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekStart: 1,
        };
        this.hoursList = [];
        this.bool_hours = false;
        this.reservationList = [];
        this.bool_res_list = false;
        this.rest = restProvider;
    }
    PadelPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                this.initAuthProcess();
                return [2 /*return*/];
            });
        });
    };
    PadelPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    PadelPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        this.getUserReservation();
                        return [2 /*return*/];
                }
            });
        });
    };
    PadelPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionSession.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    PadelPage.prototype.onTabsChange = function () {
        console.log('TABS CHANGES');
        this.hour_selected = null;
    };
    PadelPage.prototype.getUserReservation = function () {
        var _this = this;
        this.presentLoading();
        console.log("code house :" + this.perfil["code_house"]);
        this.rest.getUserReservations(this.perfil["code_house"]).subscribe(function (response) {
            _this.dismissLoading();
            console.log("USER PADEL -> ", response);
            var padels = response;
            if (response && response.length > 0) {
                for (var _i = 0, padels_1 = padels; _i < padels_1.length; _i++) {
                    var padel = padels_1[_i];
                    var hours = padel.hour.split(',');
                    var start = hours[0];
                    var end = Number(hours[hours.length - 1]) + 1;
                    padel.state = padel.active ? 'activo' : 'cancelado';
                    padel.rangeTime = start + "h - " + end + "h";
                }
                _this.reservationList = padels;
                _this.bool_res_list = true;
                _this.mapUserReservations();
            }
            else {
                _this.bool_res_list = false;
            }
        }, function (error) {
            console.log("USER PADEL ERROR ->", error);
        });
    };
    PadelPage.prototype.mapUserReservations = function () {
        var today = new Date();
        if (this.reservationList && this.reservationList.length > 0) {
            this.reservationList.forEach(function (item) {
                var day = item.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                console.log('FECHA -> ', day);
                console.log('DIFF -> ', diff);
                if (diff < 0) {
                    item.state = 'caducado';
                    item.active = false;
                }
            });
        }
    };
    PadelPage.prototype.anularReserva = function (index) {
        var _this = this;
        this.presentLoading();
        var day = this.reservationList[index]["day"];
        var hour = this.reservationList[index]["hour"];
        this.rest.anularReserva(this.comunidad.code, day, hour).subscribe(function (response) {
            _this.dismissLoading();
            var alert = _this.alertCtrl.create({
                title: "¡ Exito !",
                subTitle: "Esta reserva ha sido anulada correctamente",
                buttons: ["OK"]
            });
            alert.present();
            alert.onDidDismiss(function (res) {
                _this.reservationList = null;
                _this.getUserReservation();
                _this.list = "mis-res";
            });
        }, function (error) {
            console.log("error anular : " + error);
        });
    };
    PadelPage.prototype.successAnular = function () { };
    PadelPage.prototype.reservar = function () {
        var _this = this;
        if (this.hour_selected != undefined && this.hour_selected != null) {
            this.presentLoading();
            this.rest
                .reservar(this.comunidad.code, this.perfil["code_house"], this.date, this.hour_selected)
                .subscribe(function (response) {
                _this.dismissLoading();
                _this.successReserva();
            }, function (error) {
                console.log(" res error :" + error);
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Ojo !",
                subTitle: "No has seleccionado ninguna hora",
                buttons: ["OK"]
            });
            alert_1.present();
        }
    };
    PadelPage.prototype.successReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Enhorabuena !",
            subTitle: "reserva finalizada con exito",
            buttons: ["OK"]
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            _this.getUserReservation();
            _this.list = "mis-res";
        });
    };
    PadelPage.prototype.getHoursByDay = function (day) {
        var _this = this;
        this.bool_hours = false;
        this.hour_selected = null;
        this.presentLoading();
        this.rest.getHoursByDate(this.comunidad.code, day).subscribe(function (res) {
            var result = null;
            var table = [];
            _this.dismissLoading();
            _this.response = JSON.stringify(res);
            console.log("json response : " + JSON.stringify(res));
            result = res;
            var size = Object.keys(res).length;
            for (var i = 0; i < size; i++) {
                if (result[i]["hour"] != undefined) {
                    table.push(result[i]["hour"]);
                    console.log("hour : " + table[i]);
                }
            }
            if (_this.isEmpty(table)) {
                _this.bool_hours = true;
                _this.hoursList = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
            }
            else {
                _this.bool_hours = true;
                var len = Object.keys(table).length;
                var temp = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
                for (var jdex = 0; jdex < len; jdex++) {
                    for (var index = 0; index < 13; index++) {
                        if (table[jdex] === temp[index])
                            temp.splice(index, 1);
                    }
                }
                _this.hoursList = temp;
            }
        }, function (error) {
            console.log("hours error :" + error);
        });
    };
    PadelPage.prototype.onChange = function (date) {
        this.date = __WEBPACK_IMPORTED_MODULE_2_moment__(date).format("YYYY-MM-DD");
        var day = this.date.toString();
        this.getHoursByDay(day);
        this.hour_selected = undefined;
    };
    PadelPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    PadelPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    PadelPage.prototype.isEmpty = function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    PadelPage.prototype.isReservable = function () {
        var _this = this;
        var size = Object.keys(this.reservationList).length;
        var block = false;
        if (size > 0) {
            for (var _i = 0, _a = this.reservationList; _i < _a.length; _i++) {
                var reserva = _a[_i];
                var today = new Date();
                var day = reserva.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                if (diff > 0 && reserva.active) {
                    block = true;
                }
            }
            if (block) {
                var alert_2 = this.alertCtrl.create({
                    title: "OPERACIÓN NO PERMITIDA",
                    subTitle: "Esta casa ya tiene una reserva pendiente",
                    buttons: ["OK"]
                });
                alert_2.present();
                alert_2.onDidDismiss(function (result) {
                    _this.list = "mis-res";
                });
            }
            else {
                this.reservar();
            }
        }
        else {
            this.reservar();
        }
    };
    PadelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-padel",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/padel/padel.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reserva de pista de Padel</ion-title>\n  </ion-navbar>\n  <ion-segment  (ionChange)="onTabsChange()" [(ngModel)]="list" padding>\n    <ion-segment-button value="nueva-res">\n      Nueva reserva\n    </ion-segment-button>\n    <ion-segment-button value="mis-res">\n      Historico res.\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n<div class="c-padel">\n  <ion-content>\n    <div class="c-padel__container">\n      <div [ngSwitch]="list">\n        <ion-list *ngSwitchCase="\'nueva-res\'">\n          <ion-calendar [(ngModel)]="date" [options]="optionsRange" [type]="type" [format]="\'YYYY-MM-DD\'"\n            (onChange)="onChange($event)">\n          </ion-calendar>\n          <div class="c-padel__container__selection" *ngIf="bool_hours" padding>\n              <div class="c-padel__container__selection__date">\n                  <p class="title-text"> Horas disponibles para el : {{date}} </p>\n              </div>\n          \n            <ion-item>\n              <ion-label>Horas</ion-label>\n              <ion-select [(ngModel)]="hour_selected" multiple="true" interface="action-sheet">\n                <ion-option *ngFor="let hour of hoursList" [value]="hour">{{hour}} h\n                </ion-option>\n              </ion-select>\n            </ion-item>\n            <button class="button-res c-padel__container__list__row__item__btn " \n            (click)="isReservable()" \n            ion-button\n                padding>\n              Reservar\n            </button>\n          </div>\n\n\n        </ion-list>\n\n        <ion-list *ngSwitchCase="\'mis-res\'" padding>\n          <div *ngIf="bool_res_list">\n            <ion-card class="c-padel__container__list__card" *ngFor="let reserva of reservationList; let index = index"\n              padding>\n              <p class="label-reserva">reserva pista padel</p>\n              <div class="c-padel__container__list__row__item">\n                <p class="c-padel__container__list__row__item__label">Fecha alta:</p>\n                <p class="c-padel__container__list__row__item__value">{{reserva.createdAt | date: \'dd/MM/yyyy HH:mm\'}}\n                </p>\n              </div>\n              <div class="c-padel__container__list__row__item">\n                <p class="c-padel__container__list__row__item__label">Fecha reserva:</p>\n                <p class="c-padel__container__list__row__item__value">{{reserva.day | date: \'dd/MM/yyyy\'}}</p>\n              </div>\n              <div class="c-padel__container__list__row__item">\n                <p class="c-padel__container__list__row__item__label">Franja horaria:</p>\n                <p class="c-padel__container__list__row__item__value">{{reserva.rangeTime}}</p>\n              </div>\n              <div class="c-padel__container__list__row__item">\n                <p class="c-padel__container__list__row__item__label">Estado:</p>\n                <p class="c-padel__container__list__row__item__value" \n                [class.active]="reserva.active === true"\n                  [class.inactive]="reserva.state === \'cancelado\'"\n                  [class.caducado]="reserva.state === \'caducado\'"\n                  >{{reserva.state}}</p>\n              </div>\n              <button \n              *ngIf="reserva.active"\n              class="btn-res-list c-padel__container__list__row__item__btn" \n              (click)="anularReserva(index)"\n                ion-button full padding>\n                Cancelar\n              </button>\n            </ion-card>\n          </div>\n          <div class="c-padel__container__empty" *ngIf="!bool_res_list">\n              <p >No se han encontrado reservas de padel.</p>\n          </div>\n\n        </ion-list>\n      </div>\n\n    </div>\n  </ion-content>\n</div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/padel/padel.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_session_service__["a" /* SessionService */]])
    ], PadelPage);
    return PadelPage;
}());

//# sourceMappingURL=padel.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, databaseprovider, storageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.storageService = storageService;
        this.avatarIcon = 'assets/icon/avatar.png';
    }
    PerfilPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                return [2 /*return*/];
            });
        });
    };
    PerfilPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 2:
                        _b.comunidad = _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/perfil/perfil.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Perfil</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="c-profil" padding >\n  <div id="content-profil">\n    <ion-scroll scrollY="true" style="width: 100%; height: 100%;">\n      <div class="c-profil__container" *ngIf="perfil && comunidad">\n        <img class="c-profil__image" [src]="avatarIcon" />\n        <ion-list class="c-profil__list">\n          <ion-item class="c-profil__item">\n            <ion-icon color="primary" name="home" item-start></ion-icon>\n            <p>{{comunidad.name}}</p>\n          </ion-item>\n\n          <ion-item class="c-profil__item">\n            <ion-icon color="primary" name="home" item-start></ion-icon>\n            <p>{{perfil.adress}}</p>\n          </ion-item>\n\n          <ion-item class="c-profil__item">\n            <ion-icon color="primary" name="person" item-start></ion-icon>\n            <p>{{perfil.name}}</p>\n          </ion-item>\n\n          <ion-item class="c-profil__item">\n            <ion-icon color="primary" name="call" item-start></ion-icon>\n            <p>{{perfil.phone}}</p>\n          </ion-item>\n\n          <ion-item class="c-profil__item">\n            <ion-icon color="primary" name="mail" item-start></ion-icon>\n            <p>{{perfil.email}}</p>\n          </ion-item>\n        </ion-list>\n      </div>\n\n    </ion-scroll>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/perfil/perfil.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_storage_service__["a" /* StorageService */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var InformacionPage = /** @class */ (function () {
    function InformacionPage(navCtrl, http, restProvider, alertCtrl, loadingCtrl, databaseprovider, storageService) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.databaseprovider = databaseprovider;
        this.storageService = storageService;
        this.perfil = {};
        /*correoFinal:string="ana@gestionfincasbarrena.es";
        adminUrl:string="https://work.berekstan.com/comunidades/admin_info_email_ana.php";
        */
        this.infoURL = "https://work.berekstan.com/comunidades/icomunity_info_email_pdf.php";
        this.active = false;
        this.infoIcon = 'assets/icon/informacion.svg';
        this.rest = restProvider;
    }
    InformacionPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                return [2 /*return*/];
            });
        });
    };
    InformacionPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _c.sent();
                        if (this.comunidad && this.comunidad.features && this.comunidad.features.length > 0) {
                            for (_i = 0, _b = this.comunidad.features; _i < _b.length; _i++) {
                                feat = _b[_i];
                                if (feat === 'informacion') {
                                    this.active = true;
                                }
                            }
                        }
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    InformacionPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    InformacionPage.prototype.getComunidad = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getComunity().then(function (data) {
                    var comunidad = data;
                    _this.com_code = comunidad['code'];
                    _this.com_type = comunidad['type'];
                    _this.com_name = comunidad['name'];
                    _this.com_admin_name = comunidad['admin_name'];
                    _this.com_admin_email = comunidad['admin_email'];
                    _this.com_admin_phone = comunidad['admin_phone'];
                    _this.com_admin_adress = comunidad['admin_adress'];
                });
            }
        });
    };
    InformacionPage.prototype.getUserInfo = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getUser().then(function (data) {
                    _this.perfil = data;
                })
                    .catch(function (error) {
                    console.log(error);
                });
                ;
            }
        });
    };
    InformacionPage.prototype.sendInformationEmail = function () {
        var _this = this;
        if (this.tipo != undefined) {
            var loader_1 = this.loadingCtrl.create({
                content: "Mandando el correo ..."
            });
            loader_1.present();
            this.rest.sendInformacion(this.com_code, this.perfil['name'], this.perfil['adress'], this.perfil['phone'], this.com_name, this.perfil['email'], this.com_admin_email, this.tipo, this.description)
                .subscribe(function (data) {
                var result = data['result'];
                loader_1.dismiss();
                if (!result) {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Email Error',
                        subTitle: 'Ha habido un error, pruebe más tarde por favor',
                        buttons: ['OK']
                    });
                    alert_1.present();
                }
                else {
                    _this.showConfirm();
                }
            }, function (error) {
                loader_1.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Email Error 2',
                    subTitle: 'Ha habido un error, pruebe más tarde por favor',
                    buttons: ['OK']
                });
                alert.present();
            });
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Campo requerido',
                subTitle: 'Falta el tipo de información',
                buttons: ['OK']
            });
            alert_2.present();
        }
    };
    InformacionPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Email confirmacion',
            message: 'El administrador ha recibido correctamente su consulta',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__principal_principal__["a" /* PrincipalPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    InformacionPage.prototype.checkFields = function () {
        var message = "";
        for (var index = 0; index < 2; index++) {
            if (this.tipo == undefined)
                message = "Debes elegir un tipo ";
            else if (this.description == undefined)
                message = "Debes poner una descripcion ";
        }
        if (message == "") {
            this.sendInformationEmail();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: "Campo requerido",
                subTitle: message,
                buttons: ['OK']
            });
            alert_3.present();
        }
    };
    InformacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-informacion',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/informacion/informacion.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Pedir Informacion</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="c-informacion" padding>\n    <div class="c-informacion__inactive" *ngIf="!active">\n        <img [src]="infoIcon" />\n        <p>Pedir informaciones está inabilitado en tu comunidad, \n          contacta con tu administrador/a para más información </p>\n      </div>\n        <div class="c-informacion__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n            <div id="content-info">\n\n                <div class="c-informacion__list">\n          \n                    <div class="c-informacion__block">\n                        <p class="c-informacion__label"> Tipo de Informacion </p>\n                        <ion-item class="c-informacion_spinner">\n                          <ion-label>Tipo</ion-label>\n                          <ion-select [(ngModel)]="tipo" interface="action-sheet">\n                            <ion-option value="Consulta">Consulta</ion-option>\n                            <ion-option value="Duda">Duda</ion-option>\n                            <ion-option value="Cuentas">Cuentas</ion-option>\n                            <ion-option value="Queja">Queja</ion-option>\n                            <ion-option value="Otros">Otros</ion-option>\n                          </ion-select>\n                        </ion-item>\n                    \n                      </div>\n                      <div class="c-informacion__block">\n                        <p class="c-informacion__label"> Descripcion </p>\n                        <div class="c-informacion__description">\n                          <textarea class="c-informacion__description__area" [(ngModel)]="description" name="text" id="" cols="38" rows="5"> </textarea>\n                        </div>\n                        </div>\n                         \n                          <div class="c-informacion__block">\n                            <button class="c-informacion__button" ion-button icon-left  (click)="checkFields()">\n                              <ion-icon name="send"></ion-icon>\n                              Enviar\n                            </button>\n                          </div>\n                    \n                </div>\n              \n            </div>\n        </div>\n \n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/informacion/informacion.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_storage_service__["a" /* StorageService */]])
    ], InformacionPage);
    return InformacionPage;
}());

//# sourceMappingURL=informacion.js.map

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__provider_constants__ = __webpack_require__(398);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var StorageService = /** @class */ (function () {
    function StorageService(storage) {
        this.storage = storage;
    }
    StorageService.prototype.setComunidadCode = function (code) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["b" /* COMUNIDAD_CODE_KEY */], code);
    };
    StorageService.prototype.fetchComunidadCode = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["b" /* COMUNIDAD_CODE_KEY */]);
    };
    StorageService.prototype.setFirstSyncDone = function () {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["d" /* FIRST_SYNC_DONE */], true);
    };
    StorageService.prototype.isFirstSyncDone = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["d" /* FIRST_SYNC_DONE */])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    StorageService.prototype.saveComunidad = function (com) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["c" /* COMUNIDAD_INFOS_KEY */], com);
    };
    StorageService.prototype.fetchComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["c" /* COMUNIDAD_INFOS_KEY */])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    StorageService.prototype.saveIncidencias = function (incidencias) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["e" /* INCIDENCIAS_KEY */], incidencias);
    };
    StorageService.prototype.fetchIncidencias = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["e" /* INCIDENCIAS_KEY */]);
    };
    StorageService.prototype.saveNoticias = function (noticias) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["h" /* NOTICIAS_KEY */], noticias);
    };
    StorageService.prototype.fetchNoticias = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["h" /* NOTICIAS_KEY */]);
    };
    StorageService.prototype.savePhones = function (phones) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["j" /* PHONES_KEY */], phones);
    };
    StorageService.prototype.fetchPhones = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["j" /* PHONES_KEY */]);
    };
    StorageService.prototype.saveServices = function (services) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["l" /* SERVICES_KEY */], services);
    };
    StorageService.prototype.fetchServices = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["l" /* SERVICES_KEY */]);
    };
    StorageService.prototype.saveLocalBooking = function (locals) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["g" /* LOCAL_BOOKING_KEY */], locals);
    };
    StorageService.prototype.fetchLocalBooking = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["g" /* LOCAL_BOOKING_KEY */]);
    };
    StorageService.prototype.savePadelBooking = function (padels) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["i" /* PADEL_BOOKING_KEY */], padels);
    };
    StorageService.prototype.fetchPadelBooking = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["i" /* PADEL_BOOKING_KEY */]);
    };
    StorageService.prototype.saveTenisBooking = function (tenis) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["n" /* TENIS_BOOKING_KEY */], tenis);
    };
    StorageService.prototype.fetchTenisBooking = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["n" /* TENIS_BOOKING_KEY */]);
    };
    StorageService.prototype.savePiscinaBooking = function (pis) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["k" /* PISCINA_BOOKING_KEY */], pis);
    };
    StorageService.prototype.fetchPiscinaBooking = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["k" /* PISCINA_BOOKING_KEY */]);
    };
    StorageService.prototype.saveUserInfos = function (user) {
        this.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["p" /* USER_INFOS_KEY */], user);
    };
    StorageService.prototype.fetchUserInfos = function () {
        return this.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["p" /* USER_INFOS_KEY */]);
    };
    StorageService.prototype.save = function (key, payload) {
        this.storage.set("" + __WEBPACK_IMPORTED_MODULE_2__provider_constants__["m" /* STORAGE_PREFIX */] + key, JSON.stringify(payload));
    };
    StorageService.prototype.fetch = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get("" + __WEBPACK_IMPORTED_MODULE_2__provider_constants__["m" /* STORAGE_PREFIX */] + key)];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, JSON.parse(data)];
                }
            });
        });
    };
    StorageService.prototype.clear = function (key) {
        this.storage.remove("" + __WEBPACK_IMPORTED_MODULE_2__provider_constants__["m" /* STORAGE_PREFIX */] + key);
    };
    StorageService.prototype.clearAll = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.clear()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    StorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], StorageService);
    return StorageService;
}());

//# sourceMappingURL=storage.service.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var DocumentoPage = /** @class */ (function () {
    function DocumentoPage(navCtrl, alertCtrl, loadingCtrl, camera, actionSheetCtrl, toastCtrl, storageService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.storageService = storageService;
        this.image = null;
        this.lastImage = null;
        this.infoURL = "https://work.berekstan.com/comunidades/icomunity_docu_email_pdf.php";
        this.active = false;
        this.infoIcon = "assets/icon/informacion.svg";
        /*    this.getUserInfo();
          this.getComunidad();
          this.rest=restProvider; */
    }
    DocumentoPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                return [2 /*return*/];
            });
        });
    };
    DocumentoPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _c.sent();
                        if (this.comunidad &&
                            this.comunidad.features &&
                            this.comunidad.features.length > 0) {
                            for (_i = 0, _b = this.comunidad.features; _i < _b.length; _i++) {
                                feat = _b[_i];
                                if (feat === "documento") {
                                    this.active = true;
                                }
                            }
                        }
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    DocumentoPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DocumentoPage.prototype.enviar = function () {
        this.sendEmailToAdmin();
    };
    DocumentoPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: "Selecciona",
            buttons: [
                {
                    text: "Coger de la galeria",
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: "Usar Camera",
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel"
                }
            ]
        });
        actionSheet.present();
    };
    DocumentoPage.prototype.takePicture = function (sourceType) {
        // Create options for the Camera Dialog
        var _this = this;
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            _this.base64Image = "data:image/jpeg;base64," + imagePath;
            _this.base64imagestring = imagePath;
        }, function (err) {
            console.error(err);
            _this.presentToast("Error while selecting image.");
        });
    };
    DocumentoPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: "top"
        });
        toast.present();
    };
    DocumentoPage.prototype.sendEmailToAdmin = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mandando el correo ..."
        });
        var options;
        if (!this.base64imagestring ||
            !this.titulo ||
            (!this.base64imagestring && !this.titulo)) {
            var alert_1 = this.alertCtrl.create({
                title: "Campo requerido",
                subTitle: "Imagen o titulo vacio",
                buttons: ["OK"]
            });
            alert_1.present();
        }
        else {
            options = {
                comunidad: this.comunidad.name,
                admin_email: this.comunidad.name,
                titulo: this.titulo,
                adress: this.perfil["adress"],
                phone: this.perfil["phone"],
                user_email: this.perfil["email"],
                username: this.perfil["name"],
                image: this.base64imagestring
            };
            loader.present();
            this.rest
                .sendDocumento(this.comunidad.code, this.perfil["name"], this.comunidad.name, this.comunidad.admin_email, this.titulo, this.perfil["adress"], this.perfil["phone"], this.perfil["email"], this.base64imagestring)
                .subscribe(function (data) {
                var result = data["result"];
                loader.dismiss();
                if (!result) {
                    var alert_2 = _this.alertCtrl.create({
                        title: "Email Error",
                        subTitle: "Ha habido un error, pruebe más tarde por favor",
                        buttons: ["OK"]
                    });
                    alert_2.present();
                }
                else {
                    _this.showConfirm();
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: "Email Error 2",
                    subTitle: "Ha habido un error, pruebe más tarde por favor",
                    buttons: ["OK"]
                });
                alert.present();
            });
        }
    };
    DocumentoPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: "Email confirmation",
            message: "Se ha mandado correctamente el documento al administrador",
            buttons: [
                {
                    text: "Ok",
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__principal_principal__["a" /* PrincipalPage */]);
                        console.log("Okey clicked");
                    }
                }
            ]
        });
        confirm.present();
    };
    DocumentoPage.prototype.presentLoading = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mandando el correo ...",
            duration: 3000
        });
        loader.present();
        loader.onDidDismiss(function () {
            _this.showConfirm();
        });
    };
    DocumentoPage.prototype.checkFields = function () {
        var message = "";
        if (this.titulo == undefined)
            message = "Debes poner un titulo";
        if (message == "") {
            this.enviar();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: "Campo requerido",
                subTitle: message,
                buttons: ["OK"]
            });
            alert_3.present();
        }
    };
    DocumentoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-documento",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/documento/documento.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Mandar documento</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="c-documento" padding>\n    <div class="c-incidencia__inactive" *ngIf="!active">\n        <img [src]="infoIcon" />\n        <p>El envio de documentos está inabilitada en tu comunidad, \n          contacta con tu administrador/a para más información </p>\n      </div>\n        <div class="c-incidencia__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n            <div id="content-doc">\n\n                <div class="c-documento__block">\n                    <p class="c-documento__label"> Titulo del documento </p>\n                    <ion-item class="c-documento__item">\n                      <ion-label>Titulo</ion-label>\n                      <ion-input [(ngModel)]="titulo" type="text"> </ion-input>\n                    </ion-item>\n                </div>\n                <div class="c-documento__block">\n                    <button class="c-documento__button" ion-button icon-left  (tap)="presentActionSheet()" >\n                        <ion-icon name="document"></ion-icon>\n                        \n                       Escanear documento\n                      </button>\n                  </div>\n                  <div class="c-documento__block">\n                      <div class="profile-image-wrapper" >\n                          <div class="profile-image-placeholder" >\n                          </div>\n                          <div class="profile-image" *ngIf="!base64Image" >\n                            <ion-icon class="icon-add" ></ion-icon>\n                          </div>\n                            <div class="profile-image" *ngIf="base64Image" >\n                            <img [src]="base64Image"/>\n                          </div>\n                    </div>\n                  </div>\n                    <div class="c-documento__block">\n                        <button  class="c-documento__button" (click)="checkFields()"  ion-button icon-left >\n                            <ion-icon  name="send" ></ion-icon>\n                            Enviar\n                          </button>\n                      </div>\n            </div>\n        </div>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/documento/documento.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_storage_service__["a" /* StorageService */]])
    ], DocumentoPage);
    return DocumentoPage;
}());

//# sourceMappingURL=documento.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcercadePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__politica_politica__ = __webpack_require__(404);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AcercadePage = /** @class */ (function () {
    function AcercadePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.politicaUrl = 'https://app.berekstan.com/index.php/condiciones-de-uso/';
    }
    AcercadePage.prototype.openPolitica = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__politica_politica__["a" /* PoliticaPage */]);
    };
    AcercadePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-acercade',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/acercade/acercade.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Acerca de</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="c-about" id="content" padding>\n  <ion-list class="c-about__list">\n    <div class="c-about__block c-about__block__image">\n      <img class="c-about__image" src="assets/imgs/info.png">\n    </div>\n    <div class="c-about__block">\n      <a class="c-about__block__button" [href]="politicaUrl" >\n        Politica de privacidad\n      </a>\n    </div>\n    <div class="c-about__block">\n      <a class="c-about__block__button" [href]="politicaUrl" >\n        Condiciones general de uso\n      </a>\n    </div>\n    <div class="c-about__block">\n      <ion-item class="c-about__version">\n        <!-- Android -->\n   <!--       Version 1.3.33 -->\n        <!-- IOS -->\n        Version 1.2.24\n      </ion-item>\n    </div>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/acercade/acercade.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], AcercadePage);
    return AcercadePage;
}());

//# sourceMappingURL=acercade.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelefonosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var TelefonosPage = /** @class */ (function () {
    function TelefonosPage(navCtrl, storageService) {
        this.navCtrl = navCtrl;
        this.storageService = storageService;
        this.sincroOK = false;
        this.phones = [];
        this.infoIcon = 'assets/icon/informacion.svg';
        this.active = false;
        this.truncating = true;
    }
    TelefonosPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                return [2 /*return*/];
            });
        });
    };
    TelefonosPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _d.sent();
                        if (!(this.comunidad && this.comunidad.features && this.comunidad.features.length > 0)) return [3 /*break*/, 5];
                        _i = 0, _b = this.comunidad.features;
                        _d.label = 2;
                    case 2:
                        if (!(_i < _b.length)) return [3 /*break*/, 5];
                        feat = _b[_i];
                        if (!(feat === 'phones')) return [3 /*break*/, 4];
                        this.active = true;
                        _c = this;
                        return [4 /*yield*/, this.storageService.fetchPhones()];
                    case 3:
                        _c.phones = _d.sent();
                        _d.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    TelefonosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-telefonos",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/telefonos/telefonos.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon id="navbar" name="menu"></ion-icon>\n    </button>\n    <ion-title>Teléfonos de interés</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="c-phone" padding>\n    <div class="c-phone__inactive" *ngIf="!active">\n        <img [src]="infoIcon" />\n        <p>La gestión de telefonos está inabilitada en tu comunidad, \n          contacta con tu administrador/a para más información </p>\n      </div>\n      <div class="c-phone__active" *ngIf="active">\n          <div class="c-phone__list" id="content-phone" *ngIf="phones.length > 0">\n      <p class="label">Servicios contratados</p>\n      <ion-list *ngFor="let phone of phones;let index=index;">\n      <ion-item class="c-phone__list__item"> \n      <a class="item" href="tel: {{phone.number}}" >\n<!--       <p class="c-phone__list__item__index">{{index+1}}.</p> -->\n      <p class="c-phone__list__item__name"> {{phone.name}}</p>\n      <ion-icon class="c-phone__list__item__icon" name="call" end ></ion-icon> \n      </a>\n      </ion-item>\n      </ion-list>\n      \n      \n  </div>\n  <div class="c-phone__empty" *ngIf="phones.length === 0">\n      <img [src]="infoIcon" />\n    <p>No se han encontrado servicios contratados en esta comunidad</p>\n  </div>\n      </div>\n\n\n</ion-content>\n\n\n\n\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/telefonos/telefonos.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_storage_service__["a" /* StorageService */]])
    ], TelefonosPage);
    return TelefonosPage;
}());

//# sourceMappingURL=telefonos.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalResumePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__local_local__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LocalResumePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LocalResumePage = /** @class */ (function () {
    function LocalResumePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.reserva = {
            comunity: 'Comunidad de propietarios',
            name: 'Usuario',
            lastname: 'Usuario',
            adress: 'Portal del usuario',
            day: '2018-12-15',
            hours: [],
            start: 0,
            end: 0,
        };
        this.nbHours = this.reserva.hours.length;
        this.reserva.comunity = navParams.get('comunidad');
        this.reserva.name = navParams.get('name');
        this.reserva.lastname = navParams.get('lastname');
        this.reserva.adress = navParams.get('adress');
        this.reserva.day = __WEBPACK_IMPORTED_MODULE_3_moment__(navParams.get('day')).format("DD-MM-YYYY");
        this.reserva.hours = navParams.get('hours');
        this.reserva.start = navParams.get('start');
        this.reserva.end = navParams.get('end');
    }
    LocalResumePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LocalResumePage');
    };
    LocalResumePage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__local_local__["a" /* LocalPage */]);
    };
    LocalResumePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-local-resume',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/local-resume/local-resume.html"*/'<!--\n  Generated template for the LocalResumePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color="primary">\n        <ion-title>Reserva de Local</ion-title>\n        <ion-buttons start>\n            <button ion-button (click)="back()">\n              <ion-icon class="btn-back" name="arrow-back"></ion-icon>back\n            </button>\n          </ion-buttons>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div id="content-resume">\n            <div class="c-local-resume">\n                    <div class="c-local-resume__status">\n                        PAGADO\n                    </div>\n            \n                    <div class="c-local-resume__comunity">\n                        {{reserva.comunity}}\n                    </div>\n                    <div class="c-local-resume__adress">\n                        {{reserva.adress}}\n                    </div>\n                    <div class="c-local-resume__name">\n                        <p> {{reserva.lastname}}</p>\n                        <p>{{reserva.name}}</p>\n                    </div>\n                    <div class="c-local-resume__hour">\n                        <img class="c-local-resume__hour__clock" src="assets/icon/horloge.png">\n                        <div class="c-local-resume__hour__txt">\n                            <p>{{reserva.start}}h00 </p>\n                            <p>-</p>\n                            <p>{{reserva.end}}h00</p>\n                        </div>\n            \n                        <div class="c-local-resume__day">\n                            <img class="c-local-resume__day__date" src="assets/icon/date.png">\n                            <p class="c-local-resume__day__txt"> {{reserva.day}}</p>\n                        </div>\n            \n                    </div>\n                </div>\n            \n    </div>\n    \n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/local-resume/local-resume.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], LocalResumePage);
    return LocalResumePage;
}());

//# sourceMappingURL=local-resume.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__principal_principal__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder, restProvider, storageService, loadingCtrl, sessionService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.restProvider = restProvider;
        this.storageService = storageService;
        this.loadingCtrl = loadingCtrl;
        this.sessionService = sessionService;
        this.validationMessages = {
            code: [{ type: "required", message: "Debe introducir el codigo." }],
            email: [
                { type: "required", message: "Debe introducir el email" },
                { type: "pattern", message: "Debe introducir un email valido" }
            ]
        };
        this.isCodeRequested = false;
        this.errorCodeEmailMessage = "Codigo/Email incorrecto";
        this.errorCodeMessage = "Codigo  incorrecto";
        this.requestErrorEmail = false;
        this.requestErrorCode = false;
    }
    LoginPage.prototype.ngOnInit = function () {
        this.initForms();
    };
    LoginPage.prototype.volver = function () {
        this.navCtrl.pop();
    };
    LoginPage.prototype.clearStorage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.clearAll()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.initForms = function () {
        this.loginForm = this.formBuilder.group({
            code: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]("", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].maxLength(30),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
            ])),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]("", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
            ]))
        });
        this.codeForm = this.formBuilder.group({
            code: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]("", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].maxLength(30),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
            ]))
        });
    };
    LoginPage.prototype.openPrincipal = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__principal_principal__["a" /* PrincipalPage */]);
    };
    LoginPage.prototype.fetchMandatoryData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.presentLoading();
                        return [4 /*yield*/, this.fetchComunidad()];
                    case 1:
                        _a.sent();
                        this.dismissLoading();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.verifyLoginCode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.presentLoading();
                        data = {
                            generatedCode: this.codeForm.value.code,
                            code_comunity: this.loginForm.value.code,
                            email: this.loginForm.value.email
                        };
                        return [4 /*yield*/, this.restProvider.doRequestLogin(data).subscribe(function (response) {
                                _this.dismissLoading();
                                console.log(" LOGIN RESULT ->", response);
                                if (response.result) {
                                    _this.requestErrorCode = false;
                                    console.log(" LOGIN SUCCESS ->", response.result);
                                    _this.fetchUserInfos();
                                }
                                else {
                                    _this.requestErrorCode = true;
                                    // SHOW ERROR MESSAGE
                                }
                            }, function (error) {
                                _this.dismissLoading();
                                _this.requestErrorCode = true;
                                console.log(" ERROR LOGIN -> ", error);
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.clearStorage()];
                    case 1:
                        _a.sent();
                        data = {
                            email: this.loginForm.value.email,
                            code_comunity: this.loginForm.value.code
                        };
                        return [4 /*yield*/, this.restProvider.doGetUserInfos(data).subscribe(function (response) {
                                console.log(" USER INFOS DATA -> ", response);
                                if (response.result) {
                                    // LOGIN OK , WE DOWNLOAD ALL THE INFOS WE NEED
                                    var userInfos = response.result;
                                    _this.storageService.saveUserInfos(userInfos);
                                    _this.sessionService.setAuthToken(userInfos.auth_token);
                                    _this.sessionService.setIntroDone();
                                    _this.storageService.setComunidadCode(_this.loginForm.value.code);
                                    // WE FETCH THE MANDATORY DATA AND SAVE IT
                                    _this.fetchMandatoryData();
                                }
                                else {
                                    // SHOW ERROR MESSAGE
                                }
                            }, function (error) {
                                console.log(" ERROR MANDATORY APP DATA -> ", error);
                            })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.fetchPhones = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider
                            .getPhonesByCode(this.loginForm.value.code)
                            .subscribe(function (response) {
                            console.log(" PHONES DATA -> ", response);
                            if (response) {
                                var phones = response;
                                _this.storageService.savePhones(phones);
                                _this.openPrincipal();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(" ERROR MANDATORY APP DATA -> ", error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.fetchServices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider
                            .getServicesByCode(this.loginForm.value.code)
                            .subscribe(function (response) {
                            console.log(" SERVICES DATA -> ", response);
                            if (response) {
                                var services = response;
                                _this.storageService.saveServices(services);
                                _this.fetchPhones();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(" ERROR MANDATORY APP DATA -> ", error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.fetchComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider
                            .getComunityByCode(this.loginForm.value.code)
                            .subscribe(function (response) {
                            console.log(" COMUNIDAD DATA -> ", response[0]);
                            if (response[0]) {
                                var comunidad = response[0];
                                _this.storageService.saveComunidad(comunidad);
                                _this.fetchServices();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(" ERROR MANDATORY APP DATA -> ", error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.doLogin = function () {
        if (this.codeForm.valid) {
            this.verifyLoginCode();
        }
    };
    LoginPage.prototype.requestCode = function () {
        var _this = this;
        this.presentLoading();
        if (this.loginForm.valid) {
            var data = {
                email: this.loginForm.value.email,
                code_comunity: this.loginForm.value.code
            };
            this.restProvider.doRequestLoginCode(data).subscribe(function (response) {
                _this.dismissLoading();
                console.log(" REQUEST CODE RESULT -> ", response);
                if (response.result) {
                    _this.requestErrorEmail = false;
                    _this.isCodeRequested = true;
                }
                else {
                    _this.requestErrorEmail = true;
                    // SHOW ERROR MESSAGE
                }
            }, function (error) {
                _this.dismissLoading();
                _this.requestErrorEmail = true;
                console.log(" ERROR LOGIN CODE -> ", error);
            });
        }
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad LoginPage");
    };
    LoginPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    LoginPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-login",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <components-header></components-header>\n<components-footer></components-footer> -->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Conectarse</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content class="c-login">\n  <div class="c-login__container">\n    <ion-card class="c-login__container__card" *ngIf="!isCodeRequested" >\n      <ion-card-content>\n        <ion-card-title>\n          <p class="blue-txt"> INTRODUZCA CODIGO DE SU COMUNIDAD Y EMAIL </p>\n        </ion-card-title>\n        <div class="blue-bg linea-blue">\n        </div>\n        <form [formGroup]="loginForm">\n          <div class="c-login__container__card__box">\n            <ion-input type="text" \n            formControlName="code"\n             placeholder="Codigo comunidad">\n            </ion-input>\n          </div>\n          <div class="validation-errors">\n              <ng-container *ngFor="let validation of validationMessages.code">\n                <p  style="display:block;float: none; width: 100%; color: red;" \n                class="error-message" *ngIf="loginForm.get(\'code\').hasError(validation.type) \n                && (loginForm.get(\'code\').dirty || loginForm.get(\'code\').touched)">\n                  <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n              </p>\n              </ng-container>\n            </div>\n          <div class="c-login__container__card__box">\n            <ion-input type="email"\n            formControlName="email"\n            placeholder="Email">\n            </ion-input>\n          </div>\n          <div class="validation-errors">\n              <ng-container *ngFor="let validation of validationMessages.email">\n                <p  style="display:block;float: none; width: 100%; color: red;" \n                class="error-message" *ngIf="loginForm.get(\'email\').hasError(validation.type) \n                && (loginForm.get(\'email\').dirty || loginForm.get(\'email\').touched)">\n                  <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n              </p>\n              </ng-container>\n            </div>\n          <button \n          class="c-login__container__card__btn" \n          ion-button full \n          [disabled]="!loginForm.valid"\n          (click)="requestCode()">Pedir codigo de accesso</button>\n          <button \n          class="c-login__container__card__btnback" \n          ion-button full  \n          (click)="volver()">Volver</button>\n          <div class="validation-errors" *ngIf="requestErrorEmail">\n              <p  style="display:block;float: none; width: 100%; color: red;" \n              class="error-message" > {{ errorCodeEmailMessage }}\n            </p>\n          </div>\n        </form>\n\n      </ion-card-content>\n    </ion-card>\n\n    <!-- CODE BLOCK -->\n\n    <ion-card class="c-login__container__card" *ngIf="isCodeRequested">\n      <ion-card-content>\n        <ion-card-title>\n          <p class="blue-txt"> INTRODUZCA EL CODIGO QUE TE HEMOS ENVIADO POR EMAIL </p>\n        </ion-card-title>\n        <div class="blue-bg linea-blue">\n        </div>\n        <form [formGroup]="codeForm">\n          <div class="c-login__container__card__box">\n            <ion-input type="text" \n            formControlName="code"\n             placeholder="Codigo de acceso">\n            </ion-input>\n          </div>\n          <div class="validation-errors">\n              <ng-container *ngFor="let validation of validationMessages.code">\n                <p  style="display:block;float: none; width: 100%; color: red;" \n                class="error-message" *ngIf="codeForm.get(\'code\').hasError(validation.type) \n                && (codeForm.get(\'code\').dirty || codeForm.get(\'code\').touched)">\n                  <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n              </p>\n              </ng-container>\n            </div>\n         \n          <button \n          class="c-login__container__card__btn" \n          ion-button full \n          [disabled]="!codeForm.valid"\n          (click)="doLogin()">Hacer login</button>\n          <div class="validation-errors" *ngIf="requestErrorCode">\n              <p  style="display:block;float: none; width: 100%; color: red;" \n              class="error-message" > {{ errorCodeMessage }}\n            </p>\n          </div>\n        </form>\n\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_session_service__["a" /* SessionService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__local_local__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__padel_padel__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__piscina_piscina__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tenis_tenis__ = __webpack_require__(179);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the ReservasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReservasPage = /** @class */ (function () {
    function ReservasPage(navCtrl, navParams, storageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storageService = storageService;
        this.features = [];
        this.active = false;
        this.infoIcon = 'assets/icon/informacion.svg';
    }
    ReservasPage.prototype.ngOnInit = function () {
        this.initCards();
    };
    ReservasPage.prototype.initCards = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var comunidad, feats;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        comunidad = _a.sent();
                        if (comunidad && comunidad.features && comunidad.features.length > 0) {
                            feats = comunidad.features;
                            feats.forEach(function (element) {
                                if (element === 'local' || element === 'padel' || element === 'tenis'
                                    || element === 'piscina') {
                                    _this.active = true;
                                    _this.features.push(element);
                                }
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ReservasPage.prototype.openPage = function (feat) {
        var route;
        switch (feat) {
            case 'local':
                route = __WEBPACK_IMPORTED_MODULE_3__local_local__["a" /* LocalPage */];
                break;
            case 'padel':
                route = __WEBPACK_IMPORTED_MODULE_4__padel_padel__["a" /* PadelPage */];
                break;
            case 'piscina':
                route = __WEBPACK_IMPORTED_MODULE_5__piscina_piscina__["a" /* PiscinaPage */];
                break;
            case 'tenis':
                route = __WEBPACK_IMPORTED_MODULE_6__tenis_tenis__["a" /* TenisPage */];
                break;
        }
        this.navCtrl.push(route);
    };
    ReservasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-reservas',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/reservas/reservas.html"*/'<ion-header>\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Reservas</ion-title>\n    </ion-navbar>\n  </ion-header>\n<div class="c-reservas">\n  <div class="c-reservas__container">\n    <ion-content padding class="c-reservas__container__list">\n      <div class="c-reservas__inactive" *ngIf="!active">\n        <img [src]="infoIcon" />\n        <p>La gestión de reservas está inabilitada en tu comunidad,\n          contacta con tu administrador/a para más información </p>\n      </div>\n      <div class="c-reservas__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n        <ion-card class="c-reservas__container__list__card" *ngFor="let feat of features" (click)="openPage(feat)">\n          <ion-card-content class="c-reservas__container__list__card__content">\n            <p>Reserva de {{feat}}</p>\n            <ion-icon class="arrow" name="arrow-forward" item-end></ion-icon>\n          </ion-card-content>\n        </ion-card>\n      </div>\n\n    </ion-content>\n  </div>\n</div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/reservas/reservas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_storage_service__["a" /* StorageService */]])
    ], ReservasPage);
    return ReservasPage;
}());

//# sourceMappingURL=reservas.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TenisPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_session_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the TenisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TenisPage = /** @class */ (function () {
    function TenisPage(navCtrl, restProvider, loadingCtrl, alertCtrl, storageService, sessionSession) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.sessionSession = sessionSession;
        this.list = "mis-res";
        this.optionsRange = {
            pickMode: "single",
            from: __WEBPACK_IMPORTED_MODULE_2_moment__().toDate(),
            to: __WEBPACK_IMPORTED_MODULE_2_moment__()
                .add(1, "month")
                .toDate(),
            monthPickerFormat: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
            weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekStart: 1,
        };
        this.hoursList = [];
        this.bool_hours = false;
        this.reservationList = [];
        this.bool_res_list = false;
        this.rest = restProvider;
    }
    TenisPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                this.initAuthProcess();
                return [2 /*return*/];
            });
        });
    };
    TenisPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    TenisPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        this.getUserReservation();
                        return [2 /*return*/];
                }
            });
        });
    };
    TenisPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionSession.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    TenisPage.prototype.onTabsChange = function () {
        console.log('TABS CHANGES');
        this.hour_selected = null;
    };
    TenisPage.prototype.getUserReservation = function () {
        var _this = this;
        this.presentLoading();
        console.log("code house :" + this.perfil["code_house"]);
        this.restProvider.tenisGetUserReservas(this.perfil["code_house"]).subscribe(function (response) {
            _this.dismissLoading();
            console.log("USER TENIS -> ", response);
            var padels = response;
            if (response && response.length > 0) {
                for (var _i = 0, padels_1 = padels; _i < padels_1.length; _i++) {
                    var padel = padels_1[_i];
                    var hours = padel.hour.split(',');
                    var start = hours[0];
                    var end = Number(hours[hours.length - 1]) + 1;
                    padel.state = padel.active ? 'activo' : 'cancelado';
                    padel.rangeTime = start + "h - " + end + "h";
                }
                _this.reservationList = padels;
                _this.bool_res_list = true;
                _this.mapUserReservations();
            }
            else {
                _this.bool_res_list = false;
            }
        }, function (error) {
            console.log("USER TENIS ERROR ->", error);
        });
    };
    TenisPage.prototype.mapUserReservations = function () {
        var today = new Date();
        if (this.reservationList && this.reservationList.length > 0) {
            this.reservationList.forEach(function (item) {
                var day = item.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                console.log('FECHA -> ', day);
                console.log('DIFF -> ', diff);
                if (diff < 0) {
                    item.state = 'caducado';
                    item.active = false;
                }
            });
        }
    };
    TenisPage.prototype.anularReserva = function (index) {
        var _this = this;
        this.presentLoading();
        var day = this.reservationList[index]["day"];
        var hour = this.reservationList[index]["hour"];
        this.rest.tenisCancelarReserva(this.comunidad.code, day, hour).subscribe(function (response) {
            _this.dismissLoading();
            var alert = _this.alertCtrl.create({
                title: "¡ Exito !",
                subTitle: "Esta reserva ha sido cancelada correctamente",
                buttons: ["OK"]
            });
            alert.present();
            alert.onDidDismiss(function (res) {
                _this.reservationList = null;
                _this.getUserReservation();
                _this.list = "mis-res";
            });
        }, function (error) {
            console.log("error cancelar : " + error);
        });
    };
    TenisPage.prototype.successAnular = function () { };
    TenisPage.prototype.reservar = function () {
        var _this = this;
        if (this.hour_selected != undefined && this.hour_selected != null) {
            this.presentLoading();
            this.rest
                .tenisReservar(this.comunidad.code, this.perfil["code_house"], this.date, this.hour_selected)
                .subscribe(function (response) {
                _this.dismissLoading();
                _this.successReserva();
            }, function (error) {
                console.log(" res error :" + error);
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Ojo !",
                subTitle: "No has seleccionado ninguna hora",
                buttons: ["OK"]
            });
            alert_1.present();
        }
    };
    TenisPage.prototype.successReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Enhorabuena !",
            subTitle: "reserva finalizada con exito",
            buttons: ["OK"]
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            _this.getUserReservation();
            _this.list = "mis-res";
        });
    };
    TenisPage.prototype.getHoursByDay = function (day) {
        var _this = this;
        this.bool_hours = false;
        this.hour_selected = null;
        this.presentLoading();
        this.rest.tenisGetHoursByDay(this.comunidad.code, day).subscribe(function (res) {
            var result = null;
            var table = [];
            _this.dismissLoading();
            _this.response = JSON.stringify(res);
            console.log("json response : " + JSON.stringify(res));
            result = res;
            var size = Object.keys(res).length;
            for (var i = 0; i < size; i++) {
                if (result[i]["hour"] != undefined) {
                    table.push(result[i]["hour"]);
                    console.log("hour : " + table[i]);
                }
            }
            if (_this.isEmpty(table)) {
                _this.bool_hours = true;
                _this.hoursList = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
            }
            else {
                _this.bool_hours = true;
                var len = Object.keys(table).length;
                var temp = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
                for (var jdex = 0; jdex < len; jdex++) {
                    for (var index = 0; index < 13; index++) {
                        if (table[jdex] === temp[index])
                            temp.splice(index, 1);
                    }
                }
                _this.hoursList = temp;
            }
        }, function (error) {
            console.log("hours error :" + error);
        });
    };
    TenisPage.prototype.onChange = function (date) {
        this.date = __WEBPACK_IMPORTED_MODULE_2_moment__(date).format("YYYY-MM-DD");
        var day = this.date.toString();
        this.getHoursByDay(day);
        this.hour_selected = undefined;
    };
    TenisPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    TenisPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    TenisPage.prototype.isEmpty = function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    TenisPage.prototype.isReservable = function () {
        var _this = this;
        var size = Object.keys(this.reservationList).length;
        var block = false;
        if (size > 0) {
            for (var _i = 0, _a = this.reservationList; _i < _a.length; _i++) {
                var reserva = _a[_i];
                var today = new Date();
                var day = reserva.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                if (diff > 0 && reserva.active) {
                    block = true;
                }
            }
            if (block) {
                var alert_2 = this.alertCtrl.create({
                    title: "Oops !",
                    subTitle: "Esta casa ya tiene una reserva pendiente",
                    buttons: ["OK"]
                });
                alert_2.present();
                alert_2.onDidDismiss(function (result) {
                    _this.list = "mis-res";
                });
            }
            else {
                this.reservar();
            }
        }
        else {
            this.reservar();
        }
    };
    TenisPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-tenis",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/tenis/tenis.html"*/'<ion-header>\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Reserva de pista de tenis</ion-title>\n    </ion-navbar>\n    <ion-segment  (ionChange)="onTabsChange()" [(ngModel)]="list" padding>\n      <ion-segment-button value="nueva-res">\n        Nueva reserva\n      </ion-segment-button>\n      <ion-segment-button value="mis-res">\n        Historico res.\n      </ion-segment-button>\n    </ion-segment>\n  </ion-header>\n  <div class="c-tenis">\n    <ion-content>\n      <div class="c-tenis__container">\n        <div [ngSwitch]="list">\n          <ion-list *ngSwitchCase="\'nueva-res\'">\n            <ion-calendar [(ngModel)]="date" [options]="optionsRange" [type]="type" [format]="\'YYYY-MM-DD\'"\n              (onChange)="onChange($event)">\n            </ion-calendar>\n            <div class="c-tenis__container__selection" *ngIf="bool_hours" padding>\n                <div class="c-tenis__container__selection__date">\n                    <p class="title-text"> Horas disponibles para el : {{date}} </p>\n                </div>\n            \n              <ion-item>\n                <ion-label>Horas</ion-label>\n                <ion-select [(ngModel)]="hour_selected" multiple="true" interface="action-sheet">\n                  <ion-option *ngFor="let hour of hoursList" [value]="hour">{{hour}} h\n                  </ion-option>\n                </ion-select>\n              </ion-item>\n              <button class="button-res c-tenis__container__list__row__item__btn " \n              (click)="isReservable()" \n              ion-button\n                  padding>\n                Reservar\n              </button>\n            </div>\n  \n  \n          </ion-list>\n  \n          <ion-list *ngSwitchCase="\'mis-res\'" padding>\n            <div *ngIf="bool_res_list">\n              <ion-card class="c-tenis__container__list__card" *ngFor="let reserva of reservationList; let index = index"\n                padding>\n                <p class="label-reserva">reserva pista tenis</p>\n                <div class="c-tenis__container__list__row__item">\n                  <p class="c-tenis__container__list__row__item__label">Fecha alta:</p>\n                  <p class="c-tenis__container__list__row__item__value">{{reserva.createdAt | date: \'dd/MM/yyyy HH:mm\'}}\n                  </p>\n                </div>\n                <div class="c-tenis__container__list__row__item">\n                  <p class="c-tenis__container__list__row__item__label">Fecha reserva:</p>\n                  <p class="c-tenis__container__list__row__item__value">{{reserva.day | date: \'dd/MM/yyyy\'}}</p>\n                </div>\n                <div class="c-tenis__container__list__row__item">\n                  <p class="c-tenis__container__list__row__item__label">Franja horaria:</p>\n                  <p class="c-tenis__container__list__row__item__value">{{reserva.rangeTime}}</p>\n                </div>\n                <div class="c-tenis__container__list__row__item">\n                  <p class="c-tenis__container__list__row__item__label">Estado:</p>\n                  <p class="c-tenis__container__list__row__item__value" \n                  [class.active]="reserva.active === true"\n                    [class.inactive]="reserva.state === \'cancelado\'"\n                    [class.inactive]="reserva.state === \'caducado\'"\n                    >{{reserva.state}}</p>\n                </div>\n                <button \n                *ngIf="reserva.active"\n                class="btn-res-list c-tenis__container__list__row__item__btn" \n                (click)="anularReserva(index)"\n                  ion-button full padding>\n                  Cancelar\n                </button>\n              </ion-card>\n            </div>\n            <div class="c-tenis__container__empty" *ngIf="!bool_res_list">\n                <p >No se han encontrado reservas de tenis.</p>\n            </div>\n  \n          </ion-list>\n        </div>\n  \n      </div>\n    </ion-content>\n  </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/tenis/tenis.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_session_service__["a" /* SessionService */]])
    ], TenisPage);
    return TenisPage;
}());

//# sourceMappingURL=tenis.js.map

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json; charset=utf-8' });
var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
var RestProvider = /** @class */ (function () {
    function RestProvider(http, storage, databaseProvider, sessionService, storageService) {
        this.http = http;
        this.storage = storage;
        this.databaseProvider = databaseProvider;
        this.sessionService = sessionService;
        this.storageService = storageService;
        // RESERVA PISCINA
        this.poolDoReservarURL = "https://backbone.berekstan.com:4490/booking/pool/add";
        this.poolUserReservaURL = "https://backbone.berekstan.com:4490/booking/pool/all/user";
        this.poolCancelReservaURL = "https://backbone.berekstan.com:4490/booking/pool/deleteReserva";
        this.poolHoursByDateURL = "https://backbone.berekstan.com:4490/booking/pool/all/byday";
        // RESERVA TENIS
        this.tenisDoReservarURL = "https://backbone.berekstan.com:4480/booking/tenis";
        this.tenisUserReservaURL = "https://backbone.berekstan.com:4480/booking/tenis/all/user";
        this.tenisCancelReservaURL = "https://backbone.berekstan.com:4480/booking/tenis/deleteReserva";
        this.tenisHoursByDateURL = "https://backbone.berekstan.com:4480/booking/tenis/all/byday";
        ////
        this.hoursByDateURL = "https://backbone.berekstan.com:4430/booking/padel/all/byday";
        this.onlineURL = "https://backbone.berekstan.com:4430/booking/online";
        //RESERVA PADEL
        this.reservarURL = "https://backbone.berekstan.com:4430/booking/padel";
        this.userReservaURL = "https://backbone.berekstan.com:4430/booking/padel/all/user";
        this.deleteReservaURL = "https://backbone.berekstan.com:4430/booking/padel/deleteReserva";
        //RESERVA LOCAL
        this.hoursByDateLocalURL = "https://backbone.berekstan.com:4439/booking/local/all/byday";
        this.pagarLocalURL = "https://backbone.berekstan.com:4439/charge/local";
        this.reservarLocalURL = "https://backbone.berekstan.com:4439/booking/local";
        this.userReservaLocalURL = "https://backbone.berekstan.com:4439/booking/local/all/user";
        this.deleteReservaLocalURL = "https://backbone.berekstan.com:4439/booking/local/deleteReserva";
        this.incidenciaPushURL = "https://backbone.berekstan.com:4431/push/incidencia";
        //MIGRATION NODE
        this.getPortalURL = "https://backbone.berekstan.com:4433/core/get/portals";
        this.getComunityURL = "https://backbone.berekstan.com:4433/core/get/comunity";
        this.getPhonesURL = "https://backbone.berekstan.com:4433/core/get/phones";
        this.getIncidenciasURL = "https://backbone.berekstan.com:4433/core/get/incidencias";
        this.getNewsURL = "https://backbone.berekstan.com:4433/core/get/news";
        this.getServicesURL = "https://backbone.berekstan.com:4433/core/get/services";
        this.addIncidenciaURL = "https://backbone.berekstan.com:4433/core/add/incidencia";
        this.addInformacionURL = "https://backbone.berekstan.com:4433/core/add/informacion";
        this.addDocumentoURL = "https://backbone.berekstan.com:4433/core/add/documento";
        this.updatePushTokenURL = "https://backbone.berekstan.com:4433/core/update/pushToken";
        this.updateAuthTokenURL = "https://backbone.berekstan.com:4433/core/update/authToken";
        this.registerUserURL = "https://backbone.berekstan.com:4432/register/user";
        this.servicesMiCasaURL = "https://backbone.berekstan.com:4433/core/get/servicesMiCasa";
        this.aceptarRGPDURL = "https://backbone.berekstan.com:4433/core/update/rgpd";
        this.getRGPDURL = "https://backbone.berekstan.com:4433/core/get/rgpd";
        this.getVersionURL = "https://backbone.berekstan.com:4433/core/get/version";
        this.getFileURL = "https://backbone.berekstan.com:4438";
        // COMUNITY EXTRAS
        this.getComunityExtrasURL = 'https://backbone.berekstan.com:4433/core/get/comunity-extras';
        // LOGIN PROCESS
        this.urlRequestLoginCode = 'https://backbone.berekstan.com:4433/core/send/login/code';
        this.urlDoLogin = 'https://backbone.berekstan.com:4433/core/verify/login/code';
        // USER INFOS
        this.urlGetUserInfos = 'https://backbone.berekstan.com:4433/core/get/user/infos';
        this.auth_token = '';
        /*     console.log('pffff');
            this.storage.get('auth_token').then((result) => {
              if(result){
                this.authToken = result;
                console.log("rest auth_token : ", this.auth_token);
                options.headers.set('x-access-token', this.auth_token);
              }else{
                console.log("rest auth_token error");
              }
            })
            .catch( error =>{
           
            }); */
    }
    RestProvider.prototype.setAuthToken = function (token) {
        this.auth_token = token;
    };
    RestProvider.prototype.doGetIncidencias = function (data) {
        return this.doPostAuthRequest(this.getIncidenciasURL, data);
    };
    RestProvider.prototype.doGetNews = function (data) {
        console.log('auth_token -> ', this.auth_token);
        options.headers.set('x-access-token', this.auth_token);
        return this.http.post(this.getNewsURL, {
            code: data
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.doGetUserInfos = function (data) {
        return this.doPostAuthRequest(this.urlGetUserInfos, data);
    };
    RestProvider.prototype.doRequestLoginCode = function (data) {
        return this.doPostAuthRequest(this.urlRequestLoginCode, data);
    };
    RestProvider.prototype.doRequestLogin = function (data) {
        return this.doPostAuthRequest(this.urlDoLogin, data);
    };
    RestProvider.prototype.getComunityExtras = function (code_comunity) {
        return this.http.post(this.getComunityExtrasURL, {
            code: code_comunity
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.MakePaymentLocal = function (reserva) {
        console.log('REST_PAYMENT_LOCAL');
        return this.http.post(this.pagarLocalURL, {
            reserva: reserva
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getVersion = function (store) {
        return this.http.post(this.getVersionURL, {
            store: store
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.aceptarRGPD = function (email) {
        return this.http.post(this.aceptarRGPDURL, {
            email: email
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getRGPD = function (email) {
        return this.http.post(this.getRGPDURL, {
            email: email
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getNews = function (code) {
        return this.http.post(this.getNewsURL, {
            code: code
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getIncidencias = function (code) {
        return this.http.post(this.getIncidenciasURL, {
            code: code
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getServicesByCode = function (code) {
        return this.http.post(this.getServicesURL, {
            code: code
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getPhonesByCode = function (code) {
        return this.http.post(this.getPhonesURL, {
            code: code
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getComunityByCode = function (code) {
        return this.http.post(this.getComunityURL, {
            code: code
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getServicesMiCasa = function () {
        return this.http.post(this.servicesMiCasaURL, {}, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getPortalsByCode = function (code) {
        return this.http.post(this.getPortalURL, {
            code: code
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.registerUser = function (code, name, email, phone, adress, planta, door, code_house) {
        return this.http.post(this.registerUserURL, {
            code: code,
            name: name,
            email: email,
            phone: phone,
            adress: adress,
            planta: planta,
            door: door,
            code_house: code_house
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.updatePushToken = function (email, token) {
        return this.http.post(this.updatePushTokenURL, {
            token: token,
            email: email
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.updateAuthToken = function (email) {
        return this.http.post(this.updateAuthTokenURL, {
            email: email
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.sendIncidencia = function (incidencia) {
        console.log("options : " + JSON.stringify(options));
        var data;
        if (!incidencia.image && !incidencia.destinatorio) {
            data = {
                code: incidencia.code,
                comunidad: incidencia.comunidad,
                admin_email: incidencia.admin_email,
                username: incidencia.username,
                user_email: incidencia.user_email,
                phone: incidencia.phone,
                description: incidencia.description,
                adress: incidencia.adress,
                type: incidencia.type,
                category: incidencia.category
            };
        }
        else if (!incidencia.image) {
            data = {
                code: incidencia.code,
                comunidad: incidencia.comunidad,
                admin_email: incidencia.admin_email,
                username: incidencia.username,
                user_email: incidencia.user_email,
                phone: incidencia.phone,
                description: incidencia.description,
                destinatorio: incidencia.destinatorio,
                adress: incidencia.adress,
                type: incidencia.type,
                category: incidencia.category
            };
        }
        else if (!incidencia.destinatorio) {
            data = {
                code: incidencia.code,
                comunidad: incidencia.comunidad,
                admin_email: incidencia.admin_email,
                username: incidencia.username,
                user_email: incidencia.user_email,
                phone: incidencia.phone,
                description: incidencia.description,
                image: incidencia.image,
                adress: incidencia.adress,
                type: incidencia.type,
                category: incidencia.category
            };
        }
        else {
            data = {
                code: incidencia.code,
                comunidad: incidencia.comunidad,
                admin_email: incidencia.admin_email,
                username: incidencia.username,
                user_email: incidencia.user_email,
                phone: incidencia.phone,
                description: incidencia.description,
                image: incidencia.image,
                destinatorio: incidencia.destinatorio,
                adress: incidencia.adress,
                type: incidencia.type,
                category: incidencia.category
            };
        }
        console.log("incidencias data :" + incidencia.username);
        return this.http.post(this.addIncidenciaURL, incidencia, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.sendDocumento = function (code, username, comunidad, admin_email, titulo, adress, phone, user_email, image) {
        return this.http.post(this.addDocumentoURL, {
            comunidad: comunidad,
            admin_email: admin_email,
            title: titulo,
            adress: adress,
            phone: phone,
            user_email: user_email,
            username: username,
            image: image
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.sendInformacion = function (code, username, adress, phone, comunidad, user_email, admin_email, type, description) {
        return this.http.post(this.addInformacionURL, {
            comunidad: comunidad,
            admin_email: admin_email,
            adress: adress,
            phone: phone,
            user_email: user_email,
            description: description,
            username: username,
            type: type
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.sendIncidenciaPush = function (code) {
        return this.http.post(this.incidenciaPushURL, {
            code: code
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    /* RESERVA LOCAL */
    RestProvider.prototype.anularReservaLocal = function (code, day, hour) {
        return this.http.post(this.deleteReservaLocalURL, {
            code: code,
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getUserReservationsLocal = function (code_house) {
        return this.http.post(this.userReservaLocalURL, {
            userId: code_house
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.reservarLocal = function (code, userId, day, hours) {
        return this.http.post(this.reservarLocalURL, {
            code: code,
            userId: userId,
            day: day,
            hours: hours
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    /* RESERVA PADEL */
    RestProvider.prototype.anularReserva = function (code, day, hour) {
        return this.http.post(this.deleteReservaURL, {
            code: code,
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getUserReservations = function (code_house) {
        return this.http.post(this.userReservaURL, {
            userId: code_house
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.reservar = function (code, userId, day, hour) {
        return this.http.post(this.reservarURL, {
            code: code,
            userId: userId,
            pisteId: 'piste01',
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getHoursByDateLocal = function (code, day) {
        return this.http.post(this.hoursByDateLocalURL, {
            code: code,
            day: day
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.getHoursByDate = function (code, day) {
        return this.http.post(this.hoursByDateURL, {
            code: code,
            day: day
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.doGetRequest = function (url, data) {
        return this.http.get(url, data)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    /* RESERVA TENNIS */
    RestProvider.prototype.tenisCancelarReserva = function (code, day, hour) {
        return this.http.post(this.tenisCancelReservaURL, {
            code: code,
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.tenisGetUserReservas = function (code_house) {
        return this.http.post(this.tenisUserReservaURL, {
            userId: code_house
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.tenisReservar = function (code, userId, day, hour) {
        return this.http.post(this.tenisDoReservarURL, {
            code: code,
            userId: userId,
            pisteId: 'piste01',
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.tenisGetHoursByDay = function (code, day) {
        return this.http.post(this.tenisHoursByDateURL, {
            code: code,
            day: day
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    /* RESERVA PISCINA */
    RestProvider.prototype.poolCancelarReserva = function (code, day, hour) {
        return this.http.post(this.poolCancelReservaURL, {
            code: code,
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.poolGetUserReservas = function (code_house) {
        return this.http.post(this.poolUserReservaURL, {
            userId: code_house
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.poolReservar = function (code, userId, username, useraddress, persons, day, hour) {
        return this.http.post(this.poolDoReservarURL, {
            code: code,
            username: username,
            useraddress: useraddress,
            persons: persons,
            userId: userId,
            poolId: 'pool01',
            day: day,
            hour: hour
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.poolGetHoursByDay = function (code, day) {
        return this.http.post(this.poolHoursByDateURL, {
            code: code,
            day: day
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.doPostAuthRequest = function (url, data) {
        console.log('auth_token -> ', this.auth_token);
        options.headers.set('x-access-token', this.auth_token);
        return this.http.post(url, {
            data: data
        }, options)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.doPostRequest = function (url, data) {
        return this.http.post(url, {
            data: data
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(this.extractData));
    };
    RestProvider.prototype.extractData = function (res) {
        var body = res.json();
        return body;
    };
    RestProvider.prototype.handleError = function (error) {
        console.error(error.message || error);
        return Object(__WEBPACK_IMPORTED_MODULE_6_rxjs__["throwError"])(error.message || error);
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_4__session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_5__storage_service__["a" /* StorageService */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 218:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 218;

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/blank/blank.module": [
		668,
		5
	],
	"../pages/local-resume/local-resume.module": [
		669,
		4
	],
	"../pages/login/login.module": [
		670,
		3
	],
	"../pages/piscina/piscina.module": [
		671,
		2
	],
	"../pages/reservas/reservas.module": [
		672,
		1
	],
	"../pages/tenis/tenis.module": [
		673,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 261;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__provider_constants__ = __webpack_require__(398);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var SessionService = /** @class */ (function () {
    function SessionService(storageService) {
        this.storageService = storageService;
        this.introDone = false;
    }
    SessionService.prototype.setUpdateShown = function () {
        this.storageService.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["o" /* UPDATE_SHOWN_KEY */], true);
    };
    SessionService.prototype.isUpdateShown = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["o" /* UPDATE_SHOWN_KEY */])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SessionService.prototype.setIntroDone = function () {
        this.storageService.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["f" /* INTRO_DONE_KEY */], true);
    };
    SessionService.prototype.isIntroDone = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["f" /* INTRO_DONE_KEY */])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SessionService.prototype.setAuthToken = function (token) {
        this.storageService.save(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["a" /* AUTH_TOKEN_KEY */], token);
    };
    SessionService.prototype.getAuthToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.fetch(__WEBPACK_IMPORTED_MODULE_2__provider_constants__["a" /* AUTH_TOKEN_KEY */])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SessionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__storage_service__["a" /* StorageService */]])
    ], SessionService);
    return SessionService;
}());

//# sourceMappingURL=session.service.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return INTRO_DONE_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return FIRST_SYNC_DONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return COMUNIDAD_CODE_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return UPDATE_SHOWN_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AUTH_TOKEN_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "p", function() { return USER_INFOS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return COMUNIDAD_INFOS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return PHONES_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return SERVICES_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return INCIDENCIAS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return NOTICIAS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return LOCAL_BOOKING_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return PADEL_BOOKING_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return TENIS_BOOKING_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return PISCINA_BOOKING_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return STORAGE_PREFIX; });
var INTRO_DONE_KEY = 'intro_done';
var FIRST_SYNC_DONE = 'first_sync_done';
var COMUNIDAD_CODE_KEY = 'com_code';
var UPDATE_SHOWN_KEY = 'update_shown';
var AUTH_TOKEN_KEY = 'auth_token';
var USER_INFOS_KEY = 'user_infos';
var COMUNIDAD_INFOS_KEY = 'comunidad';
var PHONES_KEY = 'phones';
var SERVICES_KEY = 'services';
var INCIDENCIAS_KEY = 'incidencias';
var NOTICIAS_KEY = 'noticias';
var LOCAL_BOOKING_KEY = 'local-booking';
var PADEL_BOOKING_KEY = 'padel-booking';
var TENIS_BOOKING_KEY = 'tenis-booking';
var PISCINA_BOOKING_KEY = 'piscina-booking';
var STORAGE_PREFIX = 'aka-';
//# sourceMappingURL=provider.constants.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_stripe__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var STRIPE_PUBLISHABLE_KEY = "pk_live_tp3Zm0TC9lifghzEdMhppaKQ";
//declare var Stripe;
/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = /** @class */ (function () {
    function PaymentPage(navCtrl, navParams, stripe, restProvider, formBuilder, databaseprovider, loadingCtrl, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.stripe = stripe;
        this.restProvider = restProvider;
        this.formBuilder = formBuilder;
        this.databaseprovider = databaseprovider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.countHours = 0;
        this.price = 0;
        this.priceExtra = 0;
        this.priceFinal = 0;
        this.priceHours = 1;
        this.datos_incorrectos = false;
        this.bool_save_token = false;
        this.stripeObject = {
            email: 'user email',
            name: 'username',
            amount: 1,
            description: 'Pago local'
        };
        this.reserva = {
            date: "",
            hours: []
        };
        this.bool_card_saved = false;
        this.bool_change_card = false;
        this.reservationList = [];
        this.validation_messages = {
            'cardNumber': [
                { type: 'required', message: 'Debe introducir el numero de tarjeta.' },
                { type: 'maxLength', message: 'El numero de tarjeta debe ser 16 digitos.' }
            ],
            'cardMonth': [
                { type: 'required', message: 'Debe introducir el mes de caducidad.' },
                { type: 'maxLength', message: 'El mes tiene que ser 2 digitos.' }
            ],
            'cardYear': [
                { type: 'required', message: 'Debe introducir el año de caducidad.' },
                { type: 'maxLength', message: 'El año tiene que ser 2 digitos.' }
            ],
            'cardCVV': [
                { type: 'required', message: 'Debe introducir el CVV .' },
                { type: 'maxLength', message: 'El CVV tiene que ser 3 digitos.' }
            ],
        };
        this.months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        this.years = [{ name: "2020", value: "20" }, { name: "2021", value: "21" },
            { name: "2022", value: "22" }, { name: "2023", value: "23" }, { name: "2024", value: "24" },
            { name: "2025", value: "25" }, { name: "2026", value: "26" }, { name: "2027", value: "27" },
            { name: "2028", value: "28" }, { name: "2029", value: "29" }, { name: "2030", value: "30" },
            { name: "2031", value: "31" }, { name: "2032", value: "32" }, { name: "2033", value: "33" },
            { name: "2034", value: "34" }, { name: "2035", value: "35" }, { name: "2036", value: "36" },
            { name: "2037", value: "37" }, { name: "2038", value: "38" }, { name: "2039", value: "39" },
            { name: "2040", value: "40" }
        ];
        this.countHours = navParams.get('count');
        this.reserva.date = navParams.get('date');
        this.reserva.hours = navParams.get('hours');
        this.code = navParams.get('code');
        this.price = this.countHours * this.priceHours;
        this.rest = restProvider;
    }
    PaymentPage.prototype.ionViewDidLoad = function () {
        // this.initForm();
        this.getComunidad();
        this.getUserInfo();
        this.getcardToken();
        this.stripe.setPublishableKey(STRIPE_PUBLISHABLE_KEY);
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymentPage.prototype.getUnitPrice = function () {
        var _this = this;
        // FIRST GET UNIT PRICE AND THEN CALCULATE PRICE
        this.restProvider.getComunityExtras(this.code)
            .subscribe(function (response) {
            console.log('COMUNITY_EXTRAS -> ', JSON.stringify(response));
            _this.unitPrice = response.result.UnitPriceLocal;
            console.log('COMUNITY_EXTRAS_PRICE -> ', _this.unitPrice);
            _this.calculatePrices();
        }, function (error) {
            console.log('COMUNITY_EXTRAS_ERROR -> ', error);
        });
    };
    PaymentPage.prototype.calculatePrices = function () {
        this.price = this.price * this.unitPrice;
        this.priceExtra = ((this.price * 14) / 1000) + 0.25;
        this.priceFinal = this.price + this.priceExtra;
        this.priceFinal = Number(this.priceFinal.toFixed(2));
    };
    PaymentPage.prototype.getComunidad = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getComunity().then(function (data) {
                    var comunidad = data;
                    _this.comunity = data;
                    _this.code = comunidad['code'];
                    _this.getUnitPrice();
                });
            }
        });
    };
    PaymentPage.prototype.initForm = function () {
        this.paymentForm = this.formBuilder.group({
            cardNumber: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].maxLength(16), , __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required])],
            cardMonth: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].maxLength(2), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required])],
            cardYear: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].maxLength(2), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required])],
            cardCVV: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].maxLength(3), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required])]
        });
    };
    PaymentPage.prototype.getUserInfo = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getUser().then(function (data) {
                    _this.perfil = data;
                    _this.stripeObject.email = _this.perfil.email;
                    _this.stripeObject.name = _this.perfil.name;
                    _this.stripeObject.description = 'Pago local ' + _this.perfil.comunidad;
                }).catch(function (error) {
                    console.log(error);
                });
            }
        });
    };
    PaymentPage.prototype.validateCard = function () {
        var _this = this;
        this.presentLoading();
        if (this.bool_card_saved) {
            this.stripeObject.amount = this.price;
            this.proceedPayment();
        }
        else {
            this.checkInfo();
            var card = {
                number: this.cardNumber,
                expMonth: this.cardMonth,
                expYear: this.cardYear,
                cvc: this.cardCVV
            };
            console.log(JSON.stringify(card));
            if (this.datos_incorrectos)
                this.alertBadInfo();
            else {
                this.stripe.createCardToken(card)
                    .then(function (token) {
                    console.log("STRIPE RESPONSE TOKEN", JSON.stringify(token));
                    _this.cardToken = token.id;
                    if (_this.bool_save_token) {
                        if (!_this.bool_card_saved)
                            _this.saveCardToken();
                        _this.stripeObject.amount = _this.price;
                        _this.proceedPayment();
                        console.log('stripe obj', JSON.stringify(_this.stripeObject));
                    }
                    else {
                        _this.stripeObject.amount = _this.price;
                        _this.proceedPayment();
                        console.log('stripe obj', JSON.stringify(_this.stripeObject));
                    }
                })
                    .catch(function (error) {
                    _this.alertBadResult();
                    console.error(error);
                });
            }
        }
        // Run card validation here and then attempt to tokenise
    };
    PaymentPage.prototype.checkInfo = function () {
        console.log('card num', Object.keys(this.cardNumber).length);
        console.log('card month', this.cardMonth);
        console.log('card year', this.cardYear);
        console.log('card cvv', this.cardCVV);
        if (Object.keys(this.cardNumber).length != 16)
            this.datos_incorrectos = true;
        else if (this.cardMonth > 12 || this.cardMonth < 0)
            this.datos_incorrectos = true;
        else if (this.cardYear < 18 || this.cardYear > 30)
            this.datos_incorrectos = true;
        else if (Object.keys(this.cardCVV).length != 3)
            this.datos_incorrectos = true;
        else
            this.datos_incorrectos = false;
        //  console.log('VALIDATION FORM',this.paymentForm);
        /*     if(!this.paymentForm.valid)
            this.datos_incorrectos=true;
            else{
              if(Object.keys(this.cardNumber).length!=16)
              this.datos_incorrectos=true;
              else if(this.cardMonth>12 || this.cardMonth <0)
              this.datos_incorrectos=true;
              else if(this.cardYear<18 || this.cardYear > 30)
              this.datos_incorrectos=true;
              else if(Object.keys(this.cardCVV).length!=3)
              this.datos_incorrectos=true;
              else this.datos_incorrectos=false;
            } */
    };
    PaymentPage.prototype.alertBadResult = function () {
        var alert = this.alertCtrl.create({
            title: " Fallo al proceder al pago ",
            subTitle: " ¡Por favor compruebe que todos los datos están correctos "
                + "y intenta más tarde !",
            buttons: ['OK']
        });
        alert.present();
    };
    PaymentPage.prototype.alertBadInfo = function () {
        var alert = this.alertCtrl.create({
            title: " Error ",
            subTitle: " ¡ Los datos proporcionados no son correctos, solo se aceptan Mastercad, Visa y American Express !",
            buttons: ['OK']
        });
        alert.present();
    };
    PaymentPage.prototype.saveCardToken = function () {
        this.storage.set('cardToken', this.cardToken);
        this.saveCardLastNumbers();
    };
    PaymentPage.prototype.saveCardLastNumbers = function () {
        var number = this.cardNumber.substring(Object.keys(this.cardNumber).length - 4);
        this.storage.set('cardNumbers', number);
    };
    PaymentPage.prototype.getCardLastNumbers = function () {
        var _this = this;
        this.storage.get('cardNumbers').then(function (val) {
            console.log('card last numbers', val);
            if (val != null && val != undefined && val != '') {
                _this.cardLastNumbers = val;
            }
        });
    };
    PaymentPage.prototype.getcardToken = function () {
        var _this = this;
        this.storage.get('cardToken').then(function (val) {
            console.log('card token', val);
            if (val != null && val != undefined && val != '') {
                _this.cardToken = val;
                _this.bool_card_saved = true;
                _this.getCardLastNumbers();
            }
        });
    };
    PaymentPage.prototype.deleteCard = function () {
        this.bool_card_saved = false;
        this.storage.remove('cardToken');
    };
    PaymentPage.prototype.proceedPayment = function () {
        var _this = this;
        console.log("proceeding payment...");
        var start = 0;
        var end = 0;
        var username = this.perfil.name;
        if (this.countHours > 1) {
            start = this.reserva.hours[0];
            end = parseInt(this.reserva.hours[this.countHours - 1]) + 1;
        }
        else {
            start = this.reserva.hours[0];
            end = parseInt(this.reserva.hours[0]) + 1;
        }
        console.log('START HOUR', start);
        console.log('END HOUR', end);
        console.log('COMUNITY OBJECT', JSON.stringify(this.comunity));
        console.log('STRIPE OBJECT', JSON.stringify(this.stripeObject));
        console.log('PERFIL OBJECT', JSON.stringify(this.perfil));
        var reserva = {
            comunidad: this.comunity['name'],
            adress: this.perfil['adress'],
            username: username,
            day: this.reserva.date,
            start: start,
            end: end,
            totalHours: this.countHours,
            adminEmail: this.comunity['admin_email'],
            email: this.perfil['email'],
            description: this.stripeObject.description,
            amount: this.stripeObject.amount,
            id: this.cardToken,
            code: this.code
        };
        console.log('RESERVA OBJECT', JSON.stringify(reserva));
        this.rest.MakePaymentLocal(reserva)
            .subscribe(function (result) {
            console.log('payment result', JSON.stringify(result));
            var value = result['result'];
            if (value)
                _this.reservar();
            else
                _this.failedReserva();
        }, function (error) {
            _this.dismissLoading();
            console.log('PROCCESS PAYMENT REQUEST ERROR', error);
            _this.failedReserva();
        });
    };
    PaymentPage.prototype.reservar = function () {
        var _this = this;
        console.log('proceeding local nodejs reserva...', JSON.stringify(this.reserva));
        this.rest.reservarLocal(this.code, this.perfil['code_house'], this.reserva.date, this.reserva.hours)
            .subscribe(function (response) {
            _this.dismissLoading();
            _this.successReserva();
        }, function (error) {
            _this.dismissLoading();
            console.log('RESERVA REQUEST ERROR', error);
            _this.failedReserva();
        });
    };
    PaymentPage.prototype.successReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Enhorabuena !",
            subTitle: "reserva finalizada con exito",
            buttons: ['OK']
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            //ROUTING TO LOCAL PAGE AND SHOW RESERVATIONS
            console.log('RESERVATION SUCCESS, ROUTING TO LOCAL PAGE....');
            _this.goToReservations();
        });
    };
    PaymentPage.prototype.failedReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Lo siento !",
            subTitle: "No se ha podido realizar el cobro, vuelve a intentarlo más tarde",
            buttons: ['OK']
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            //ROUTING TO LOCAL PAGE AND SHOW RESERVATIONS
            console.log('RESERVATION SUCCESS, ROUTING TO LOCAL PAGE....');
            _this.goToReservations();
        });
    };
    PaymentPage.prototype.goToReservations = function () {
        console.log('poping back to reserva...');
        this.navCtrl.pop();
    };
    PaymentPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 5000
        });
        this.loader.present();
    };
    PaymentPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/payment/payment.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Pago reserva del local</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="c-payment">\n      <div class="c-payment__container__false" *ngIf="!unitPrice">\n          <p>Faltán datos para poder proceder al pago,\n             por favor compruebe su conexión internet y vuelva a intentar</p>\n        </div>\n        <ion-list *ngIf="unitPrice">\n          <ion-item>\n            <ion-label class="label-txt"> Total horas : {{countHours}} </ion-label>\n          </ion-item>\n      <!--     <ion-item>\n            <ion-label class="label-txt"> Total Precio horas  : {{price}} € </ion-label>\n          </ion-item> -->\n          <ion-item>\n            <ion-label class="label-txt"> Total a pagar : {{priceFinal}} € </ion-label>\n          </ion-item>\n      \n      \n          <ion-card *ngIf="!bool_card_saved" class="pay-card" padding>\n            <ion-card-title class="pay-card-title">\n              Datos de la tarjeta\n            </ion-card-title>\n            <ion-item>\n              <ion-label color="primary" stacked>Numero Tarjeta</ion-label>\n              <ion-input class="pay-input" type="number" maxlength="16"\n              [(ngModel)]="cardNumber"></ion-input>\n            </ion-item>\n      \n            <div class="block-my">\n              <ion-item class="block-m">\n                <ion-label color="primary" stacked>Mes Caducidad</ion-label>\n                <ion-select [(ngModel)]="cardMonth">\n                  <ion-option *ngFor="let month of months" [value]="month">{{month}}</ion-option>\n                </ion-select>\n              </ion-item>\n        \n              <ion-item class="block-y">\n                <ion-label color="primary" stacked>Año Caducidad</ion-label>\n                <ion-select [(ngModel)]="cardYear">\n                  <ion-option *ngFor="let year of years" [value]="year.value">{{year.name}}</ion-option>\n                </ion-select>\n              </ion-item>\n            </div>\n        \n      \n      <!--      \n            <ion-item>\n              <ion-label color="primary" stacked>Mes Caducidad</ion-label>\n              <ion-input class="pay-input" type="number" maxlength="2"\n              [(ngModel)]="cardMonth"></ion-input>\n            </ion-item>\n           \n            <ion-item>\n              <ion-label color="primary" stacked>Año Caducidad</ion-label>\n              <ion-input class="pay-input" type="number" maxlength="2"\n              [(ngModel)]="cardYear"></ion-input>\n            </ion-item>\n            -->\n            <ion-item>\n              <ion-label color="primary" stacked>CVV</ion-label>\n              <ion-input class="pay-input" type="number" maxlength="3"\n              [(ngModel)]="cardCVV"></ion-input>\n            </ion-item>\n      \n            <p *ngIf="datos_incorrectos" class="datos-error">Datos incorrectos</p>\n          </ion-card>\n      \n          <ion-card *ngIf="bool_card_saved">\n            <ion-card-title class="pay-card-title">\n                Datos de la tarjeta\n            </ion-card-title>\n            <div class="block-card-ln">\n              <p class="card-ln-label">Se usarán los datos de la tarjeta siguiente</p>\n              <p class="card-ln"> *********{{cardLastNumbers}}</p>\n            </div>\n            <button ion-button outline rounded (click)="deleteCard()">Borrar tarjeta</button>\n         \n          </ion-card>\n       \n         \n        </ion-list>\n      \n         \n      <!--   <ion-item *ngIf="!bool_card_saved">\n            <ion-label class="save-cb">Guardar datos para proximo pago</ion-label>\n            <ion-checkbox color="dark" [(ngModel)]="bool_save_token" ></ion-checkbox>\n        </ion-item>\n       -->\n          <button *ngIf="unitPrice" ion-button block large (click)="validateCard()">Pagar</button>\n          <ion-item *ngIf="unitPrice"  >\n            <p>Por favor contactar con su administrador <br> si necesita información sobre el precio/hora <br> de su comunidad</p>\n          </ion-item>\n        \n  </div>\n  \n</ion-content>\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/payment/payment.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_stripe__["a" /* Stripe */], __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListadoPage = /** @class */ (function () {
    function ListadoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.title = "titulo";
        this.list = [];
        this.bool_empty = true;
        this.list = navParams.get('list');
        this.title = navParams.get('type');
        if (this.list != undefined) {
            if (Object.keys(this.list).length > 0) {
                this.bool_empty = false;
            }
            else {
                this.bool_empty = true;
            }
        }
        else {
            this.bool_empty = true;
        }
    }
    ListadoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListadoPage');
    };
    ListadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-listado',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/listado/listado.html"*/'<!--\n  Generated template for the ListadoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <div *ngIf="!bool_empty" class="list">\n\n    <ion-list>\n\n        <ion-card  *ngFor="let item of list ; let index=index">\n          <div class="element">\n              <img [src]="item.image">\n              <ion-card-content>\n              \n                <h1>{{item.name}}</h1>\n                <h2>{{item.adress}}</h2>\n              </ion-card-content>\n          </div>\n\n          <ion-row class="texto-row"> \n            <p class="texto-p"> {{item.texto}} </p>\n          </ion-row>\n        \n          <ion-row>\n            <ion-col>\n              <a  href="tel: {{item.phone}}"><button ion-button icon-left clear large >\n                  <ion-icon name="phone-portrait"></ion-icon>\n                  <div>Llamar</div>\n                </button></a>\n            </ion-col>\n            <ion-col>\n                <a  href="{{item.web}}">  <button ion-button icon-left clear large>\n                <ion-icon name="at"></ion-icon>\n                <div>Web</div>\n              </button>\n                </a>\n            </ion-col>\n          \n          </ion-row>\n        \n        \n        </ion-card>\n      </ion-list>\n  \n  </div>\n  <div *ngIf="bool_empty">\n    <ion-card>\n        <h1>De momento no hay gremio para {{title}}</h1>\n    </ion-card>\n   \n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/listado/listado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ListadoPage);
    return ListadoPage;
}());

//# sourceMappingURL=listado.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoliticaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PoliticaPage = /** @class */ (function () {
    function PoliticaPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    PoliticaPage.prototype.ionViewDidLoad = function () {
    };
    PoliticaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'politica-padel',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/politica/politica.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n      <ion-title>Politica de Privacidad</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n\n\n    \n\n  </ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/politica/politica.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], PoliticaPage);
    return PoliticaPage;
}());

//# sourceMappingURL=politica.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite_porter__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(618);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__(620);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(sqlitePorter, storage, sqlite, platform, http) {
        var _this = this;
        this.sqlitePorter = sqlitePorter;
        this.storage = storage;
        this.sqlite = sqlite;
        this.platform = platform;
        this.http = http;
        this.databaseReady = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"](false);
        this.platform.ready().then(function () {
            _this.sqlite.create({
                name: 'icomunity.db',
                location: 'default'
            })
                .then(function (db) {
                _this.database = db;
                _this.storage.get('database_filled').then(function (val) {
                    if (val) {
                        _this.databaseReady.next(true);
                    }
                    else {
                        _this.fillDatabase();
                    }
                });
            });
        });
    }
    DatabaseProvider.prototype.fillDatabase = function () {
        var _this = this;
        this.http.get('assets/dummyDump.sql')
            .map(function (res) { return res.text(); })
            .subscribe(function (sql) {
            _this.sqlitePorter.importSqlToDb(_this.database, sql)
                .then(function (data) {
                _this.databaseReady.next(true);
                _this.storage.set('database_filled', true);
            })
                .catch(function (e) { return console.error(e); });
        });
    };
    DatabaseProvider.prototype.addComunity = function (code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress) {
        var data = [code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress];
        return this.database.executeSql("INSERT INTO comunity (code, type,type_comunity, name, admin_name, admin_email,admin_phone,admin_adress) VALUES (?, ?, ?, ?,?, ?, ?, ?)", data).then(function (data) {
            return data;
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.getComunity = function () {
        return this.database.executeSql("SELECT * FROM comunity ", []).then(function (data) {
            var comunity = {};
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    comunity = { code: data.rows.item(i).code, type_comunity: data.rows.item(i).type_comunity, name: data.rows.item(i).name, admin_name: data.rows.item(i).admin_name,
                        admin_email: data.rows.item(i).admin_email, admin_phone: data.rows.item(i).admin_phone, admin_adress: data.rows.item(i).admin_adress
                    };
                }
            }
            return comunity;
        }, function (err) {
            console.log('Error: ', err);
            return [];
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.updateComunity = function (code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress) {
        var _this = this;
        //DELETE
        console.log('deleting comunity');
        this.database.executeSql("DELETE  FROM comunity ", []).then(function (result) {
            //INSERT
            console.log('inserting comunity');
            var data = [code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress];
            console.log('database updated comunity:', JSON.stringify(data));
            return _this.database.executeSql("INSERT INTO comunity (code, type,type_comunity, name, admin_name, admin_email,admin_phone,admin_adress) VALUES (?, ?, ?, ?,?, ?, ?, ?)", data).then(function (data) {
                console.log('database updated comunity:', JSON.stringify(data));
                return data;
            }, function (err) {
                console.log('Error: ', err);
                return err;
            })
                .catch(function (error) {
                console.log(error);
            });
        }, function (err) {
            console.log('database comunity update error ', JSON.stringify(err));
            return err;
        })
            .catch(function (error) {
            console.log('database comunity update error ', JSON.stringify(error));
        });
    };
    DatabaseProvider.prototype.getComunityCode = function () {
        return this.database.executeSql("SELECT code FROM comunity ", []).then(function (data) {
            var code;
            if (data.rows.length > 0) {
                code = data.rows.item(0).code;
            }
            return code;
        }, function (err) {
            console.log('Error: ', err);
            return [];
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.addImages = function (com_image) {
        var insert = [com_image, "nada"];
        return this.database.executeSql("INSERT INTO images (com_image, nada) VALUES (?, ?)", insert).then(function (data) {
            return data;
        }, function (err) {
            console.log('database images add error ', JSON.stringify(err));
            return err;
        })
            .catch(function (error) {
            console.log('database images add error ', JSON.stringify(error));
        });
    };
    DatabaseProvider.prototype.updateImages = function (com_image) {
        var _this = this;
        //DELETE
        this.database.executeSql("DELETE  FROM images ", []).then(function (data) {
            //INSERT
            var insert = [com_image, "nada"];
            _this.database.executeSql("INSERT INTO images (com_image, nada) VALUES (?,?) ", insert).then(function (data) {
                return data;
            }, function (err) {
                console.log('database images update error ', JSON.stringify(err));
                return err;
            })
                .catch(function (error) {
                console.log('database images update error ', JSON.stringify(error));
            });
        }, function (err) {
            console.log('database images update error ', JSON.stringify(err));
            return err;
        })
            .catch(function (error) {
            console.log('database images update error ', JSON.stringify(error));
        });
    };
    DatabaseProvider.prototype.getImages = function () {
        return this.database.executeSql("SELECT * FROM images ", []).then(function (data) {
            var user = {};
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    user = { com_image: data.rows.item(i).com_image };
                }
            }
            return user;
        }, function (err) {
            console.log('database images get error ', JSON.stringify(err));
            return [];
        })
            .catch(function (error) {
            console.log('database images get error ', JSON.stringify(error));
        });
    };
    DatabaseProvider.prototype.addService = function (type, name, email, phone) {
        var dataToInsert = [type, name, email, phone];
        return this.database.executeSql("INSERT INTO services (type, name, email, phone) VALUES (?, ?, ?, ? )", dataToInsert).then(function (data) {
            return data;
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.updateService = function (type, name, email, phone) {
        var _this = this;
        var dataToInsert = [type, name, email, phone];
        //DELETE
        this.database.executeSql("DELETE  FROM services ", []).then(function (data) {
            //INSERT
            _this.database.executeSql("INSERT INTO services (type, name, email, phone) VALUES (?, ?, ?, ? )", dataToInsert).then(function (data) {
                return data;
            }, function (err) {
                console.log('Error: ', err);
                return err;
            })
                .catch(function (error) {
                console.log(error);
            });
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.getAllServices = function () {
        return this.database.executeSql("SELECT * FROM services ", []).then(function (data) {
            var services = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    services.push({ type: data.rows.item(i).type, name: data.rows.item(i).name, email: data.rows.item(i).email,
                        phone: data.rows.item(i).phone
                    });
                }
            }
            return services;
        }, function (err) {
            console.log('Error: ', err);
            return [];
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.addPhone = function (name, phone_number) {
        var data = [name, phone_number];
        return this.database.executeSql("INSERT INTO phones (name, phone_number) VALUES (?, ? )", data).then(function (data) {
            return data;
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.updatePhone = function (name, phone_number) {
        var _this = this;
        var dataToInsert = [name, phone_number];
        //DELETE
        this.database.executeSql("DELETE FROM phones", []).then(function (data) {
            //INSERT
            _this.database.executeSql("INSERT INTO phones (name, phone_number) VALUES (?, ? )", dataToInsert).then(function (data) {
                return data;
            }, function (err) {
                console.log('Error: ', err);
                return err;
            });
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.getAllPhones = function () {
        return this.database.executeSql("SELECT * FROM phones ", []).then(function (data) {
            var phones = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    phones.push({ name: data.rows.item(i).name, phone_number: data.rows.item(i).phone_number
                    });
                }
            }
            return phones;
        }, function (err) {
            console.log('Error: ', err);
            return [];
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.addUser = function (name, email, phone, adress, comunidad, code_house) {
        var data = [name, email, phone, adress, comunidad, code_house];
        return this.database.executeSql("INSERT INTO user (name, email, phone, adress, comunidad, code_house) VALUES (?, ?, ? ,? ,? ,? )", data).then(function (data) {
            return data;
        }, function (err) {
            console.log('Error: ', err);
            return err;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.getUser = function () {
        return this.database.executeSql("SELECT * FROM user ", []).then(function (data) {
            var user = {};
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    user = { name: data.rows.item(i).name, email: data.rows.item(i).email, phone: data.rows.item(i).phone,
                        adress: data.rows.item(i).adress, comunidad: data.rows.item(i).comunidad, code_house: data.rows.item(i).code_house };
                }
            }
            return user;
        }, function (err) {
            console.log('Error: ', err);
            return [];
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.getDatabaseState = function () {
        return this.databaseReady.asObservable();
    };
    DatabaseProvider.prototype.deleteAllTable = function () {
        this.sqlitePorter.wipeDb('icomunity.db');
    };
    DatabaseProvider.prototype.deleteTableService = function () {
        return this.database.executeSql("DELETE * FROM services ", []).then(function (data) {
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider.prototype.deleteTablePhone = function () {
        return this.database.executeSql("DELETE * FROM phones ", []).then(function (data) {
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_sqlite_porter__["a" /* SQLitePorter */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CodigoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registro_registro__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CodigoPage = /** @class */ (function () {
    function CodigoPage(navCtrl, alertCtrl, databaseprovider, rest, loadingCtrl, http, navParams, storage) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseprovider = databaseprovider;
        this.rest = rest;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.navParams = navParams;
        this.storage = storage;
        //Services variables
        this.servicesList = [];
        this.okey = false;
        this.sincroOK = false;
    }
    CodigoPage.prototype.ngOnInit = function () {
        console.log("APP_LOGS -> IN CODIGO");
    };
    CodigoPage.prototype.ionViewWillEnter = function () {
        console.log("APP_LOGS -> IN CODIGO WILL");
        /*   let code = this.navParams.get('code')|| null;
          if(code!=null){
            console.log('code readed is',code);
            this.codigo=code;
            this.verifyCodigo();
          }
         */
    };
    CodigoPage.prototype.openLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
    };
    CodigoPage.prototype.openScan = function () {
        // this.navCtrl.push(ScanPage);
    };
    CodigoPage.prototype.verifyCodigo = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: "Sincronización..."
        });
        this.getPortals(this.codigo);
        this.loader.onDidDismiss(function () {
            if (_this.sincroOK == true) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__registro_registro__["a" /* RegistroPage */], {
                    portalsList: _this.portalsJSON,
                    code: _this.codigo,
                    comunidad: _this.comunidad,
                    type: _this.type
                });
            }
        });
    };
    CodigoPage.prototype.saveComunidad = function (code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress) {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addComunity(code, type, type_comunity, name, admin_name, admin_email, admin_phone, admin_adress);
            }
        });
    };
    CodigoPage.prototype.saveImages = function (com_image) {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addImages(com_image);
            }
        });
    };
    CodigoPage.prototype.saveService = function (type, name, email, phone) {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addService(type, name, email, phone);
            }
        });
    };
    CodigoPage.prototype.savePhone = function (name, phone_number) {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addPhone(name, phone_number);
            }
        });
    };
    CodigoPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Codigo error ',
            subTitle: 'Este codigo no pertenece a ninguna comunidad ',
            buttons: ['Ok']
        });
        alert.present();
    };
    CodigoPage.prototype.getPortals = function (code) {
        var _this = this;
        this.loader.present();
        this.rest.getPortalsByCode(code)
            .subscribe(function (data) {
            if (data === false) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Codigo inexistante',
                    subTitle: 'El codigo de comunidad entrado no existe',
                    buttons: ['OK']
                });
                _this.loader.dismiss();
                alert_1.present();
            }
            else {
                _this.portalsJSON = data;
                _this.getPhones(_this.codigo);
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Problema de sincronización',
                subTitle: 'Por favor compruebe que tienes internet y prueba otra vez portals',
                buttons: ['OK']
            });
            _this.loader.dismiss();
            alert.present();
        });
    };
    CodigoPage.prototype.getComunityByCode = function (code) {
        var _this = this;
        this.rest.getComunityByCode(code)
            .subscribe(function (data) {
            if (data === false) {
                var alert_2 = _this.alertCtrl.create({
                    title: 'codigo Error',
                    subTitle: 'An error occur while getting codigo , please try later comunity code ',
                    buttons: ['OK']
                });
                _this.loader.dismiss();
                alert_2.present();
            }
            else {
                _this.adminName = data[0]['admin_name'];
                _this.email = data[0]['admin_email'];
                _this.phone = data[0]['admin_phone'];
                _this.adress = data[0]['admin_adress'];
                _this.comunidad = data[0]['name'];
                _this.type = data[0]['type'];
                var type_comunity = data[0]['type_comunity'];
                var com_image = data[0]['com_image'];
                console.log("type_com : " + _this.type);
                console.log("com_image : " + com_image);
                _this.saveComunidad(_this.codigo, _this.type, type_comunity, _this.comunidad, _this.adminName, _this.email, _this.phone, _this.adress);
                _this.saveImages(com_image);
                _this.getServicesByComunity(_this.codigo);
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Problema de sincronización',
                subTitle: 'Por favor compruebe que tienes internet y prueba otra vez ',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    CodigoPage.prototype.getServicesByComunity = function (code) {
        var _this = this;
        this.rest.getServicesByCode(code)
            .subscribe(function (data) {
            if (data === false) {
                var alert_3 = _this.alertCtrl.create({
                    title: 'Problema de sincronización',
                    subTitle: 'Por favor compruebe que tienes internet y prueba otra vez service by com',
                    buttons: ['OK']
                });
                _this.loader.dismiss();
                alert_3.present();
            }
            else {
                var count = Object.keys(data).length;
                for (var index = 0; index < count; index++) {
                    var service = data[index];
                    console.log("service received :" + JSON.stringify(service));
                    _this.type_service = service['type'];
                    _this.email_service = service['email'];
                    _this.phone_service = service['phone'];
                    _this.name_service = service['name'];
                    _this.saveService(_this.type_service, _this.name_service, _this.email_service, _this.phone_service);
                }
                _this.sincroOK = true;
                _this.loader.dismiss();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'code Error',
                subTitle: error.status + error.error,
                buttons: ['OK']
            });
            _this.loader.dismiss();
            alert.present();
        });
    };
    CodigoPage.prototype.qrError = function () {
        var confirm = this.alertCtrl.create({
            title: 'QRCODE invalido',
            message: 'el codigo escaneado no esta reconocido',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        console.log('Okey clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    CodigoPage.prototype.showConfirm = function (text) {
        var confirm = this.alertCtrl.create({
            title: 'Codigo confirmation',
            message: text,
            buttons: [
                {
                    text: 'Okey',
                    handler: function () {
                        console.log('Okey clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    CodigoPage.prototype.getPhones = function (code) {
        var _this = this;
        this.rest.getPhonesByCode(code)
            .subscribe(function (data) {
            if (data === false) {
                var alert_4 = _this.alertCtrl.create({
                    title: 'Internet problem',
                    subTitle: 'Por favor compruebe que tienes internet y prueba otra vez phones ',
                    buttons: ['OK']
                });
                _this.loader.dismiss();
                alert_4.present();
            }
            else {
                var count = Object.keys(data).length;
                for (var index = 0; index < count; index++) {
                    var phone = data[index];
                    var name_1 = phone['name'];
                    var phone_number = phone['number'];
                    _this.savePhone(name_1, phone_number);
                }
                _this.getComunityByCode(_this.codigo);
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Internet Problem',
                subTitle: 'Por favor compruebe que tienes internet y prueba otra vez phones catch',
                buttons: ['OK']
            });
            _this.loader.dismiss();
            alert.present();
        });
    };
    CodigoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-codigo',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/codigo/codigo.html"*/'<!-- <ion-header>\n    <ion-navbar>\n      \n      <ion-title>Icomunity</ion-title>\n    </ion-navbar>\n  </ion-header>\n   -->\n<components-header></components-header>\n<components-footer></components-footer>\n<ion-content class="content c-codigo">\n  <ion-scroll scrollY="true" style="width: 100%; height: 100%;">\n\n    <ion-list class="c-codigo__container" padding>\n      <ion-card class="c-codigo__container__login">\n        <ion-card-content>\n          <ion-card-title>\n            <p class="blue-txt" > ¿ YA TIENES UNA CUENTA ? <br> HAZ LOGIN CON TU EMAIL</p>\n          </ion-card-title>\n          <div  class="blue-bg linea-blue">\n          </div>\n          <button class="btnLogin blue-bg" ion-button full  (click)="openLogin()">Hacer login</button>\n        </ion-card-content>\n      </ion-card> \n      <ion-card class="c-codigo__container__codigo">\n        <ion-card-content>\n          <ion-card-title>\n            <p class="teal-txt">¿ NO TIENES CUENTA ? <br> INTRODUZCA EL CODIGO DE TU COMUNIDAD PARA REGISTRARTE </p>\n          </ion-card-title>\n          <div id="codigo-box">\n            <ion-input type="text" [(ngModel)]="codigo" placeholder="Codigo...">\n            </ion-input>\n          </div>\n          <div class="linea">\n\n          </div>\n          <!-- \n              <div class="qrcode-block"> \n                  <p id="qr-text">Leer QR-CODE para entrar en su Comunidad</p>\n                <img (click)="openScan()" class="qrcode-img" src="assets/imgs/qrcode.png" >\n               </div> -->\n\n          <button class="button" ion-button full  (click)="verifyCodigo()">Nuevo usuario</button>\n\n        </ion-card-content>\n        <!--  <img src="assets/gif/casita.gif"/>  -->\n      </ion-card>\n    \n\n    </ion-list>\n  </ion-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/codigo/codigo.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"], __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], CodigoPage);
    return CodigoPage;
}());

//# sourceMappingURL=codigo.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistroPage; });
/* unused harmony export GlobalValidator */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__registro_constants__ = __webpack_require__(644);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var RegistroPage = /** @class */ (function () {
    function RegistroPage(navCtrl, menu, navParams, toastCtrl, rest, alertCtrl, loadingCtrl, storageService, formBuilder, sessionService) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.rest = rest;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storageService = storageService;
        this.formBuilder = formBuilder;
        this.sessionService = sessionService;
        this.showSkip = true;
        this.isConnected = false;
        this.okeyConnected = false;
        this.sincroOK = false;
        this.RegisterUserURL = "https://work.berekstan.com/icomunity/register_user.php";
        this.chalet_state = false;
        this.submitAttempt = false;
        this.validationMessages = {
            chaletAddress: [
                { type: 'required', message: 'Debe introducir la dirección completa.' }
            ],
            portal: [
                { type: 'required', message: 'Debe seleccionar el portal.' }
            ],
            planta: [
                { type: 'required', message: 'Debe seleccionar la planta.' }
            ],
            puerta: [
                { type: 'required', message: 'Debe seleccionar la puerta.' }
            ],
            name: [
                { type: 'required', message: 'Debe introducir el nombre' }
            ],
            surname: [
                { type: 'required', message: 'Debe introducir el apellido' }
            ],
            email: [
                { type: 'required', message: 'Debe introducir el email' },
                { type: 'pattern', message: 'Debe introducir un email valido' }
            ],
            phone: [
                { type: 'required', message: 'Debe introducir un telefono.' },
                { type: 'minlength', message: 'Debe introducir un telefono valido.' },
                { type: 'maxlength', message: 'Debe introducir un telefono valido.' },
            ],
            terms: [
                { type: 'pattern', message: 'Debe aceptar los terminos de uso' }
            ],
        };
        this.plantaList = __WEBPACK_IMPORTED_MODULE_5__registro_constants__["a" /* PLANTA_LIST */];
        this.puertaList = __WEBPACK_IMPORTED_MODULE_5__registro_constants__["b" /* PUERTA_LIST */];
        this.PortalsJSON = navParams.get("portalsList");
        this.code = navParams.get("code");
        this.comunidad = navParams.get("comunidad");
        this.type_com = navParams.get("type");
        if (this.type_com == 1) {
            this.chalet_state = false;
        }
        else {
            this.chalet_state = true;
        }
    }
    // VERSION 2020 
    RegistroPage.prototype.ngOnInit = function () {
        this.initForms();
    };
    RegistroPage.prototype.clearStorage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.clearAll()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.initForms = function () {
        this.formChaletInfos = this.formBuilder.group({
            address: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required),
        });
        this.formHouseInfos = this.formBuilder.group({
            portal: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required),
            planta: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required),
            puerta: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required),
        });
        this.formUserInfos = this.formBuilder.group({
            name: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].minLength(1),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].maxLength(30),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required
            ])),
            surname: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].minLength(1),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].maxLength(30),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required
            ])),
            email: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required,
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            phone: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].minLength(9),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].maxLength(9),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required
            ])),
            terms: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](false, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].pattern('true'))
        });
    };
    RegistroPage.prototype.finalizar = function () {
        var _this = this;
        this.submitAttempt = true;
        if ((this.formUserInfos.valid && this.formHouseInfos.valid)
            || (this.formUserInfos.valid && this.formChaletInfos.valid)) {
            this.submitAttempt = false;
            this.loader = this.loadingCtrl.create({
                content: "Cargando..."
            });
            this.clearStorage();
            this.registerUser();
            this.loader.onDidDismiss(function () {
                if (_this.sincroOK == true) {
                    _this.setIntroShown();
                    _this.storageService.setComunidadCode(_this.code);
                    // FETCH ALL APP DATA
                    _this.fetchMandatoryData();
                }
            });
        }
        else {
            // SHOW ERROR MESSAGE
        }
    };
    RegistroPage.prototype.fetchMandatoryData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fetchComunidad()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.openPrincipal = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__principal_principal__["a" /* PrincipalPage */]);
    };
    RegistroPage.prototype.fetchPhones = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rest.getPhonesByCode(this.code)
                            .subscribe(function (response) {
                            console.log(' PHONES DATA -> ', response);
                            if (response) {
                                var phones = response;
                                _this.storageService.savePhones(phones);
                                // THEN MOVE TO HOME PAGE
                                _this.openPrincipal();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.fetchServices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rest.getServicesByCode(this.code)
                            .subscribe(function (response) {
                            console.log(' SERVICES DATA -> ', response);
                            if (response) {
                                var services = response;
                                _this.storageService.saveServices(services);
                                _this.fetchPhones();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.fetchComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rest.getComunityByCode(this.code)
                            .subscribe(function (response) {
                            console.log(' COMUNIDAD DATA -> ', response[0]);
                            if (response[0]) {
                                var comunidad = response[0];
                                _this.storageService.saveComunidad(comunidad);
                                _this.fetchServices();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistroPage.prototype.registerUser = function () {
        var _this = this;
        this.loader.present();
        var count = Object.keys(this.PortalsJSON).length;
        for (var index = 0; index < count; index++) {
            var portal_1 = this.PortalsJSON;
            if (portal_1[index]["adress"] == this.formHouseInfos.value.portal)
                this.fk_id_portal = portal_1[index]["id"];
        }
        var portal = this.formHouseInfos.value.portal;
        this.name = this.formUserInfos.value.name + "  " + this.formUserInfos.value.surname;
        this.phone = this.formUserInfos.value.phone;
        this.email = this.formUserInfos.value.email;
        this.planta = this.formHouseInfos.value.planta;
        this.door = this.formHouseInfos.value.puerta;
        // CHALET
        this.chalet_adress = this.formChaletInfos.value.address;
        if (this.chalet_state) {
            this.code_house = this.code + this.chalet_adress;
            this.planta = "vacio";
            this.door = "vacio";
            portal = this.chalet_adress;
        }
        else {
            this.code_house = this.code + this.fk_id_portal + this.formHouseInfos.value.planta + this.formHouseInfos.value.puerta;
            portal = this.formHouseInfos.value.portal;
        }
        console.log("house infos : " +
            portal +
            " " +
            this.planta +
            " " +
            this.door +
            " " +
            this.code_house +
            " " +
            this.email +
            " " +
            this.code +
            " " +
            this.phone +
            " " +
            this.name);
        this.rest
            .registerUser(this.code, this.name, this.email, this.phone, portal, this.planta, this.door, this.code_house)
            .subscribe(function (data) {
            if (!data) {
                var alert_1 = _this.alertCtrl.create({
                    title: "Fallo de sincronizacion",
                    subTitle: "Por favor prueba mâs tarde",
                    buttons: ["OK"]
                });
                _this.loader.dismiss();
                alert_1.present();
            }
            else {
                _this.sincroOK = true;
                var adress = "";
                if (!_this.chalet_state) {
                    adress = portal + " " + _this.planta + " " + _this.door;
                }
                else {
                    adress = _this.chalet_adress;
                }
                console.log("datos saved in database : " + _this.name);
                var token = data["token"];
                //localStorage.setItem("auth_token",token);
                console.log("auth token :" + token);
                _this.sessionService.setAuthToken(token);
                _this.savePerfil(_this.name, _this.email, _this.phone, adress, _this.comunidad, _this.code_house, data['state'], token);
                _this.loader.dismiss();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: "Problema de sincronización",
                subTitle: "Por favor compruebe que tienes internet y prueba otra vez",
                buttons: ["OK"]
            });
            _this.loader.dismiss();
            alert.present();
        });
    };
    RegistroPage.prototype.savePerfil = function (name, email, phone, adress, comunidad, code_house, state, auth_token) {
        var profil = {
            name: name,
            email: email,
            phone: phone,
            adress: adress,
            code_house: code_house,
            state: state,
            auth_token: auth_token,
            code_comunity: comunidad
        };
        this.storageService.saveUserInfos(profil);
    };
    RegistroPage.prototype.getIndex = function (index) {
        //this.portal = this.PortalsJSON[index]['adress'];
        //this.presentToast(portal);
        // this.firstChar= this.tipoPlanta.charAt(0);
        // this.presentToast(this.firstChar);
        // this.secondChar=this.tipoPlanta.subString(1);
        // this.presentToast(this.secondChar);
    };
    RegistroPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: "info : " + text,
            duration: 3000,
            position: "top"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    RegistroPage.prototype.setPlantas = function () { };
    /*
    startApp() {
      this.storage.set('introShown', true);
      this.navCtrl.setRoot(HomePage, {}, {
        animate: true,
        direction: 'forward'
      });
    }
  */
    RegistroPage.prototype.isValidate = function () {
        var message = "";
        for (var index = 0; index < 7; index++) {
            if (this.chalet_state) {
                if (this.chalet_adress == undefined) {
                    message = "Debes introducir una direccion correcta para tu chalet";
                }
                else if (this.nombre == undefined)
                    message = "Debes poner tu nombre";
                else if (this.apellido == undefined)
                    message = "Debes poner tu apellido";
                else if (this.phone == undefined)
                    message = "Debes poner tu telefono";
                else if (this.email == undefined)
                    message = "Debes poner tu email";
            }
            else {
                if (this.portal == undefined)
                    message = "Debes elegir un portal";
                else if (this.planta == undefined)
                    message = "Debes elegir una planta";
                else if (this.door == undefined)
                    message = "Debes elegir una puerta";
                else if (this.nombre == undefined)
                    message = "Debes poner tu nombre";
                else if (this.apellido == undefined)
                    message = "Debes poner tu apellido";
                else if (this.phone == undefined)
                    message = "Debes poner tu telefono";
                else if (this.email == undefined)
                    message = "Debes poner tu email";
            }
        }
        if (message == "") {
            this.finalizar();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: "Campo requerido",
                subTitle: message,
                buttons: ["OK"]
            });
            alert_2.present();
        }
    };
    RegistroPage.prototype.setIntroShown = function () {
        this.sessionService.setIntroDone();
    };
    RegistroPage.prototype.onSlideChangeStart = function (slider) {
        this.showSkip = !slider.isEnd();
    };
    RegistroPage.prototype.ionViewDidEnter = function () {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
    };
    RegistroPage.prototype.ionViewWillLeave = function () {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    };
    RegistroPage.prototype.nextSlide = function () {
        this.slides.slideNext();
    };
    RegistroPage.prototype.modificar = function () {
        this.slides.slideTo(0);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"])
    ], RegistroPage.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("signupSlider"),
        __metadata("design:type", Object)
    ], RegistroPage.prototype, "signupSlider", void 0);
    RegistroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-registro",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/registro/registro.html"*/'<ion-header no-shadow>\n</ion-header>\n\n<ion-toolbar>\n  <ion-title>Registro</ion-title>\n</ion-toolbar>\n\n<ion-content>\n  <ion-slides #signupSlider pager="true" (ionSlideWillChange)="onSlideChangeStart($event)">\n    <ion-slide class="container" padding>\n      <ion-list>\n\n        <!-- CHALET SIDE -->\n       \n        <div class="chalet-container" *ngIf="chalet_state">\n          <form [formGroup]="formChaletInfos" >\n           <p>Dirreción completa del domicilio </p>\n          <ion-item class="form-item"> \n              <ion-label floating>Dirección</ion-label>\n              <ion-input formControlName="address"  type="text"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n              <ng-container *ngFor="let validation of validationMessages.chaletAddress">\n                <p  style="display:block;float: none; width: 100%; color: red;" \n                class="error-message" *ngIf="formChaletInfos.get(\'address\').hasError(validation.type) \n                && (formChaletInfos.get(\'address\').dirty || formChaletInfos.get(\'address\').touched)">\n                  <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n              </p>\n              </ng-container>\n            </div>\n            <button class="btn-submit-houseslide" ion-button icon-end large \n            [disabled]="!formChaletInfos.valid" (click)="nextSlide()">\n                Siguiente\n                <ion-icon name="arrow-forward"></ion-icon>\n              </button>\n        </form>\n\n        </div>\n\n\n\n        <!-- RESIDENCE SIDE -->\n        <div *ngIf="!chalet_state">\n            <form [formGroup]="formHouseInfos" >\n                <p> Dirección </p>\n              <!-- ITEM PORTAL -->\n                <ion-item>\n                  <ion-label>Portal</ion-label>\n                  <ion-select formControlName="portal" interface="action-sheet">\n                      <ion-option *ngFor="let portal of PortalsJSON" value="{{portal.adress}}">{{portal.adress}}\n                      </ion-option>\n                    </ion-select>\n                </ion-item>\n                <div class="validation-errors">\n                  <ng-container *ngFor="let validation of validationMessages.portal">\n                    <p  style="display:block;float: none; width: 100%; color: red;" \n                    class="error-message" *ngIf="formHouseInfos.get(\'portal\').hasError(validation.type) \n                    && (formHouseInfos.get(\'portal\').dirty || formHouseInfos.get(\'portal\').touched)">\n                      <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                  </p>\n                  </ng-container>\n                </div>\n                <!-- ITEM PLANTA -->\n                <ion-item>\n                    <ion-label>Planta</ion-label>\n                    <ion-select formControlName="planta" interface="action-sheet">\n                        <ion-option *ngFor="let planta of plantaList" [value]="planta">{{planta}}\n                        </ion-option>\n                      </ion-select>\n                  </ion-item>\n                  <div class="validation-errors">\n                    <ng-container *ngFor="let validation of validationMessages.planta">\n                      <p  style="display:block;float: none; width: 100%; color: red;" \n                      class="error-message" *ngIf="formHouseInfos.get(\'planta\').hasError(validation.type) \n                      && (formHouseInfos.get(\'planta\').dirty || formHouseInfos.get(\'planta\').touched)">\n                        <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                    </p>\n                    </ng-container>\n                  </div>\n\n                  <!-- ITEM PUERTA -->\n                <ion-item>\n                    <ion-label>Puerta</ion-label>\n                    <ion-select formControlName="puerta" interface="action-sheet">\n                        <ion-option *ngFor="let puerta of puertaList" [value]="puerta">{{puerta}}\n                        </ion-option>\n                      </ion-select>\n                  </ion-item>\n                  <div class="validation-errors">\n                    <ng-container *ngFor="let validation of validationMessages.puerta">\n                      <p  style="display:block;float: none; width: 100%; color: red;" \n                      class="error-message" *ngIf="formHouseInfos.get(\'puerta\').hasError(validation.type) \n                      && (formHouseInfos.get(\'puerta\').dirty || formHouseInfos.get(\'puerta\').touched)">\n                        <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                    </p>\n                    </ng-container>\n                  </div>\n\n                <button class="btn-submit-houseslide" ion-button icon-end large \n                [disabled]="!formHouseInfos.valid" (click)="nextSlide()">\n                    Siguiente\n                    <ion-icon name="arrow-forward"></ion-icon>\n                  </button>\n          </form>\n        </div>\n      </ion-list>\n    </ion-slide>\n\n      <!-- USER INFOS SLIDE -->\n      <ion-slide class="container" padding>\n          <form [formGroup]="formUserInfos" >\n              <p> Datos de contactos </p>\n            <!-- ITEM NAME -->\n              <ion-item class="form-item">\n                <ion-label floating>Nombre</ion-label>\n                <ion-input formControlName="name" type="text"></ion-input>\n              </ion-item>\n              <div class="validation-errors">\n                <ng-container *ngFor="let validation of validationMessages.name">\n                  <p  style="display:block;float: none; width: 100%; color: red;" \n                  class="error-message" *ngIf="formUserInfos.get(\'name\').hasError(validation.type) \n                  && (formUserInfos.get(\'name\').dirty || formUserInfos.get(\'name\').touched)">\n                    <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                </p>\n                </ng-container>\n              </div>\n              <!-- ITEM SURNAME -->\n              <ion-item class="form-item">\n                  <ion-label floating>Apellidos</ion-label>\n                  <ion-input formControlName="surname" type="text"></ion-input>\n                </ion-item>\n                <div class="validation-errors">\n                  <ng-container *ngFor="let validation of validationMessages.surname">\n                    <p  style="display:block;float: none; width: 100%; color: red;" \n                    class="error-message" *ngIf="formUserInfos.get(\'surname\').hasError(validation.type) \n                    && (formUserInfos.get(\'surname\').dirty || formUserInfos.get(\'surname\').touched)">\n                      <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                  </p>\n                  </ng-container>\n                </div>\n    \n                  <!-- ITEM EMAIL -->\n              <ion-item class="form-item"> \n                  <ion-label floating>Email</ion-label>\n                  <ion-input formControlName="email"  type="text"></ion-input>\n                </ion-item>\n                <div class="validation-errors">\n                  <ng-container *ngFor="let validation of validationMessages.email">\n                    <p  style="display:block;float: none; width: 100%; color: red;" \n                    class="error-message" *ngIf="formUserInfos.get(\'email\').hasError(validation.type) \n                    && (formUserInfos.get(\'email\').dirty || formUserInfos.get(\'email\').touched)">\n                      <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                  </p>\n                  </ng-container>\n                </div>\n    \n                   <!-- ITEM PHONE -->\n              <ion-item class="form-item">\n                  <ion-label floating>Telefono</ion-label>\n                  <ion-input formControlName="phone"  type="number"></ion-input>\n                </ion-item>\n                <div class="validation-errors">\n                  <ng-container *ngFor="let validation of validationMessages.phone">\n                    <p  style="display:block;float: none; width: 100%; color: red;" \n                    class="error-message" *ngIf="formUserInfos.get(\'phone\').hasError(validation.type) \n                    && (formUserInfos.get(\'phone\').dirty || formUserInfos.get(\'phone\').touched)">\n                      <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                  </p>\n                  </ng-container>\n                </div>\n\n                <div class="terms-container">\n                    <ion-checkbox color="primary" formControlName="terms"></ion-checkbox>\n                    <ion-label  class="text-terms" class="ion-text-wrap" >\n                        He leído, entiendo y acepto las \n                          <a href="https://app.berekstan.com/index.php/condiciones-de-uso/" >condiciones de uso</a>\n                          y la \n                          <a href="https://app.berekstan.com/index.php/condiciones-de-uso/" > politica de privacidad</a>\n                          de ICOMUNITY.  \n                    </ion-label>\n                   \n                  </div>\n                  <div class="validation-errors">\n                      <ng-container *ngFor="let validation of validationMessages.terms">\n                        <p  style="display:block;float: none; width: 100%; color: red;" \n                        class="error-message" *ngIf="formUserInfos.get(\'terms\').hasError(validation.type) \n                        && (formUserInfos.get(\'terms\').dirty || formUserInfos.get(\'terms\').touched)">\n                          <ion-icon name="information-circle-outline"></ion-icon> {{ validation.message }}\n                      </p>\n                      </ng-container>\n                    </div>\n                <button class="btn-submit-houseslide" ion-button icon-end large \n                [disabled]="!formUserInfos.valid" (click)="nextSlide()">\n                    Siguiente\n                    <ion-icon name="arrow-forward"></ion-icon>\n                  </button>\n          </form>\n      </ion-slide>\n \n    <ion-slide class="container" scroll *ngIf="formHouseInfos && formUserInfos">\n      <br>\n      <p>\n        Verificar datos\n      </p>\n      <ion-card>\n        <ion-card-content>\n          <div id="resume">\n            <div id="resume-item">\n              <p id="resume-item-label">NOMBRE:</p>\n              <p id="resume-item-text">{{formUserInfos.value.name}}</p>\n            </div>\n            <div id="resume-item">\n              <p id="resume-item-label">APELLIDOS:</p>\n              <p id="resume-item-text">{{formUserInfos.value.surname}}</p>\n            </div>\n\n            <div id="resume-item">\n                <p id="resume-item-label">EMAIL:</p>\n                <p id="resume-item-text">{{formUserInfos.value.email}}</p>\n              </div>\n              <div id="resume-item">\n                  <p id="resume-item-label">TELEFONO:</p>\n                  <p id="resume-item-text">{{formUserInfos.value.phone}}</p>\n                </div>\n                  <!-- NOT CHALET -->\n            <div id="resume-item" *ngIf="!chalet_state">\n              <p id="resume-item-label">DIRECCIÓN:</p>\n              <p id="resume-item-text">{{formHouseInfos.value.portal}} {{formHouseInfos.value.planta}} {{formHouseInfos.value.puerta}}</p>\n            </div>\n      \n            <!-- CHALET -->\n            <div id="resume-item" *ngIf="chalet_state">\n              <p id="resume-item-label">DOMICILIO:</p>\n              <p id="resume-item-text">{{formChaletInfos.value.address}}</p>\n            </div>\n          </div>\n        </ion-card-content>\n      </ion-card>\n     \n      <div class="floating-box">\n        <button ion-button icon-end (click)="modificar()">\n          Modificar datos\n        </button>\n        <button ion-button icon-end  \n         [disabled]=" !((this.formUserInfos.valid && this.formHouseInfos.valid) \n         || (this.formUserInfos.valid && this.formChaletInfos.valid))"\n         (click)="finalizar()">\n          Finalizar\n          <ion-icon name="arrow-forward"></ion-icon>\n        </button>\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/registro/registro.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_7__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__providers_session_service__["a" /* SessionService */]])
    ], RegistroPage);
    return RegistroPage;
}());

var GlobalValidator = /** @class */ (function () {
    function GlobalValidator() {
    }
    GlobalValidator.EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return GlobalValidator;
}());

//# sourceMappingURL=registro.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlankPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BlankPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BlankPage = /** @class */ (function () {
    function BlankPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        this.years = ["2019", "2020", "2021", "2022", "2023"];
    }
    BlankPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BlankPage');
    };
    BlankPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-blank',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/blank/blank.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Pago reserva del local</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list>\n \n    <ion-item>\n      <ion-label class="label-txt"> Total horas seleccionadas: {{countHours}} </ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label class="label-txt"> Total a pagar : {{price}} € </ion-label>\n    </ion-item>\n\n    <ion-card *ngIf="!bool_card_saved" class="pay-card" padding>\n      <ion-card-title class="pay-card-title">\n        Datos de la tarjeta\n      </ion-card-title>\n      <ion-item>\n        <ion-label color="primary" stacked>Numero Tarjeta</ion-label>\n        <ion-input class="pay-input" type="number" maxlength="16"\n        [(ngModel)]="cardNumber"></ion-input>\n      </ion-item>\n\n      <div class="block-my">\n        <ion-item class="block-m">\n          <ion-label color="primary" stacked>Mes Caducidad</ion-label>\n          <ion-select [(ngModel)]="cardMonth">\n            <ion-option *ngFor="let month of months" [value]="month">{{month}}</ion-option>\n          </ion-select>\n        </ion-item>\n  \n        <ion-item class="block-y">\n          <ion-label color="primary" stacked>Año Caducidad</ion-label>\n          <ion-select [(ngModel)]="cardYear">\n            <ion-option *ngFor="let year of years" [value]="year">{{year}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </div>\n  \n\n<!--      \n      <ion-item>\n        <ion-label color="primary" stacked>Mes Caducidad</ion-label>\n        <ion-input class="pay-input" type="number" maxlength="2"\n        [(ngModel)]="cardMonth"></ion-input>\n      </ion-item>\n     \n      <ion-item>\n        <ion-label color="primary" stacked>Año Caducidad</ion-label>\n        <ion-input class="pay-input" type="number" maxlength="2"\n        [(ngModel)]="cardYear"></ion-input>\n      </ion-item>\n      -->\n      <ion-item>\n        <ion-label color="primary" stacked>CVV</ion-label>\n        <ion-input class="pay-input" type="number" maxlength="3"\n        [(ngModel)]="cardCVV"></ion-input>\n      </ion-item>\n\n      <p *ngIf="datos_incorrectos" class="datos-error">Datos incorrectos</p>\n    </ion-card>\n\n    <ion-card *ngIf="bool_card_saved">\n      <ion-card-title class="pay-card-title">\n          Datos de la tarjeta\n      </ion-card-title>\n      <div class="block-card-ln">\n        <p class="card-ln-label">Se usarán los datos de la tarjeta guardada</p>\n        <p class="card-ln"> *********{{cardLastNumbers}}</p>\n      </div>\n      <button ion-button outline rounded (click)="deleteCard()">Borrar tarjeta</button>\n   \n    </ion-card>\n \n   \n  </ion-list>\n\n   \n  <ion-item *ngIf="!bool_card_saved">\n      <ion-label class="save-cb">Guardar datos para proximo pago</ion-label>\n      <ion-checkbox color="dark" [(ngModel)]="bool_save_token" ></ion-checkbox>\n  </ion-item>\n\n    <button ion-button block large (click)="validateCard()">Pagar</button>\n   \n  \n</ion-content>\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/blank/blank.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], BlankPage);
    return BlankPage;
}());

//# sourceMappingURL=blank.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrincipalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__micasa_micasa__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__incidencia_incidencia__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__informacion_informacion__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__documento_documento__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__acercade_acercade__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__telefonos_telefonos__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__reservas_reservas__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var PrincipalPage = /** @class */ (function () {
    function PrincipalPage(navCtrl, sessionService, storageService, restProvider, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.sessionService = sessionService;
        this.storageService = storageService;
        this.restProvider = restProvider;
        this.loadingCtrl = loadingCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.introDone = false;
        console.log('in principal page');
    }
    PrincipalPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.fetchMasterData();
                this.initSideMenu();
                return [2 /*return*/];
            });
        });
    };
    PrincipalPage.prototype.fetchMasterData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        console.log('STORAGE COMUNIDAD PRINCIPAL -> ', this.comunidad);
                        if (!(this.comunidad && this.comunidad.code)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.fetchComunidad()];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, this.fetchServices()];
                    case 3:
                        _b.sent();
                        return [4 /*yield*/, this.fetchPhones()];
                    case 4:
                        _b.sent();
                        _b.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PrincipalPage.prototype.fetchPhones = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider.getPhonesByCode(this.comunidad.code)
                            .subscribe(function (response) {
                            console.log(' PHONES DATA PRINCIPAL-> ', response);
                            if (response) {
                                var phones = response;
                                _this.storageService.savePhones(phones);
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PrincipalPage.prototype.fetchServices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider.getServicesByCode(this.comunidad.code)
                            .subscribe(function (response) {
                            console.log(' SERVICES DATA PRINCIPAL-> ', response);
                            if (response) {
                                var services = response;
                                _this.storageService.saveServices(services);
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PrincipalPage.prototype.fetchComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider.getComunityByCode(this.comunidad.code)
                            .subscribe(function (response) {
                            if (response[0]) {
                                var comunidad = response[0];
                                _this.storageService.saveComunidad(comunidad);
                                _this.comunidad = comunidad;
                                console.log(' COMUNIDAD DATA PRINCIPAL-> ', response[0]);
                                _this.openPage(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                                // this.loadComunidad();
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PrincipalPage.prototype.initSideMenu = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.sessionService.isIntroDone()];
                    case 1:
                        _a.introDone = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 2:
                        _b.comunidad = _c.sent();
                        console.log('comunidad -> ', this.comunidad);
                        if (this.comunidad && this.comunidad.features && this.comunidad.features.length > 0) {
                            this.pages = [
                                { title: 'Pagina principal', component: __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], icon: 'home' },
                                { title: 'Reservas', component: __WEBPACK_IMPORTED_MODULE_13__reservas_reservas__["a" /* ReservasPage */], icon: 'book' },
                                { title: 'Incidencias', component: __WEBPACK_IMPORTED_MODULE_5__incidencia_incidencia__["a" /* IncidenciaPage */], icon: 'warning' },
                                { title: 'Solicitar informacion', component: __WEBPACK_IMPORTED_MODULE_6__informacion_informacion__["a" /* InformacionPage */], icon: 'information-circle' },
                                { title: 'Telefonos de interes', component: __WEBPACK_IMPORTED_MODULE_9__telefonos_telefonos__["a" /* TelefonosPage */], icon: 'call' },
                                { title: 'Mandar documento', component: __WEBPACK_IMPORTED_MODULE_7__documento_documento__["a" /* DocumentoPage */], icon: 'document' },
                                { title: 'Mi casa', component: __WEBPACK_IMPORTED_MODULE_4__micasa_micasa__["a" /* MicasaPage */], icon: 'construct' },
                                { title: 'Perfil', component: __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__["a" /* PerfilPage */], icon: 'person' },
                                { title: 'Acerca de', component: __WEBPACK_IMPORTED_MODULE_8__acercade_acercade__["a" /* AcercadePage */], icon: 'at' }
                            ];
                        }
                        else {
                            this.pages = [
                                { title: 'Pagina principal', component: __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], icon: 'home' },
                                { title: 'Incidencias', component: __WEBPACK_IMPORTED_MODULE_5__incidencia_incidencia__["a" /* IncidenciaPage */], icon: 'warning' },
                                { title: 'Solicitar informacion', component: __WEBPACK_IMPORTED_MODULE_6__informacion_informacion__["a" /* InformacionPage */], icon: 'information-circle' },
                                { title: 'Telefonos de interes', component: __WEBPACK_IMPORTED_MODULE_9__telefonos_telefonos__["a" /* TelefonosPage */], icon: 'call' },
                                { title: 'Mandar documento', component: __WEBPACK_IMPORTED_MODULE_7__documento_documento__["a" /* DocumentoPage */], icon: 'document' },
                                { title: 'Mi casa', component: __WEBPACK_IMPORTED_MODULE_4__micasa_micasa__["a" /* MicasaPage */], icon: 'construct' },
                                { title: 'Perfil', component: __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__["a" /* PerfilPage */], icon: 'person' },
                                { title: 'Acerca de', component: __WEBPACK_IMPORTED_MODULE_8__acercade_acercade__["a" /* AcercadePage */], icon: 'at' }
                            ];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    PrincipalPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    PrincipalPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    PrincipalPage.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], PrincipalPage.prototype, "nav", void 0);
    PrincipalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-principal',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/principal/principal.html"*/'<div class="c-principal">\n    <ion-menu [content]="content">\n      <ion-header>\n        <ion-toolbar color="primary">\n          <ion-title>Mi Comunidad</ion-title>\n        </ion-toolbar>\n      </ion-header>\n      <ion-content class="c-principal__container">\n        <ion-list>\n          <button class="menu-item" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n            <ion-icon name="{{p.icon}}" item-left></ion-icon>\n            <div id="menu-item-text">{{p.title}}</div>\n          </button>\n        </ion-list>\n      </ion-content>\n    </ion-menu>\n    <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n  </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/principal/principal.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_10__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_11__providers_session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_12__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_10__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], PrincipalPage);
    return PrincipalPage;
}());

//# sourceMappingURL=principal.js.map

/***/ }),

/***/ 585:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(643);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_padel_padel__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_politica_politica__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_local_local__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_scan_scan__ = __webpack_require__(645);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_noticias_noticias__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_incidencia_incidencia__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_documento_documento__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_informacion_informacion__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_acercade_acercade__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_telefonos_telefonos__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_codigo_codigo__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_micasa_micasa__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_listado_listado__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_registro_registro__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_payment_payment__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_blank_blank__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_local_resume_local_resume__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_native_storage__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_status_bar__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_splash_screen__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_http__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__ = __webpack_require__(646);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__ = __webpack_require__(647);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer__ = __webpack_require__(648);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_http__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_sqlite_porter__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_sqlite__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_push__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_ion2_calendar__ = __webpack_require__(649);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_ion2_calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_38_ion2_calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__angular_common_http__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_market__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_app_version__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__truncate_pipe__ = __webpack_require__(659);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__ionic_native_stripe__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pipe_date_pipe__ = __webpack_require__(660);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__components_components_header_components_header__ = __webpack_require__(661);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__components_components_footer_components_footer__ = __webpack_require__(662);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_login_login__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__components_components_sidemenu_components_sidemenu__ = __webpack_require__(663);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_reservas_reservas__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_tenis_tenis__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_piscina_piscina__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__components_alert_alert__ = __webpack_require__(664);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__components_loading_modal_loading_modal__ = __webpack_require__(665);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__providers_modal_service__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__common_interceptors_http_interceptor__ = __webpack_require__(666);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__angular_common__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__angular_common_locales_es__ = __webpack_require__(667);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























































Object(__WEBPACK_IMPORTED_MODULE_59__angular_common__["i" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_60__angular_common_locales_es__["a" /* default */]);
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_incidencia_incidencia__["a" /* IncidenciaPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_codigo_codigo__["a" /* CodigoPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_padel_padel__["a" /* PadelPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_local_local__["a" /* LocalPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_telefonos_telefonos__["a" /* TelefonosPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_acercade_acercade__["a" /* AcercadePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_informacion_informacion__["a" /* InformacionPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_principal_principal__["a" /* PrincipalPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_documento_documento__["a" /* DocumentoPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_politica_politica__["a" /* PoliticaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_noticias_noticias__["a" /* NoticiasPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_micasa_micasa__["a" /* MicasaPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_listado_listado__["a" /* ListadoPage */],
                __WEBPACK_IMPORTED_MODULE_43__truncate_pipe__["a" /* TruncatePipe */],
                __WEBPACK_IMPORTED_MODULE_10__pages_scan_scan__["a" /* ScanPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_local_resume_local_resume__["a" /* LocalResumePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_blank_blank__["a" /* BlankPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_reservas_reservas__["a" /* ReservasPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_tenis_tenis__["a" /* TenisPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_piscina_piscina__["a" /* PiscinaPage */],
                __WEBPACK_IMPORTED_MODULE_46__components_components_header_components_header__["a" /* ComponentsHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_47__components_components_footer_components_footer__["a" /* ComponentsFooterComponent */],
                __WEBPACK_IMPORTED_MODULE_51__components_components_sidemenu_components_sidemenu__["a" /* ComponentsSidemenuComponent */],
                __WEBPACK_IMPORTED_MODULE_56__components_loading_modal_loading_modal__["a" /* LoadingModalComponent */],
                __WEBPACK_IMPORTED_MODULE_55__components_alert_alert__["a" /* AlertComponent */],
                __WEBPACK_IMPORTED_MODULE_45__pipe_date_pipe__["a" /* MomentPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_40__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_38_ion2_calendar__["CalendarModule"],
                __WEBPACK_IMPORTED_MODULE_34__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: true,
                    autoFocusAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/blank/blank.module#BlankPageModule', name: 'BlankPage', segment: 'blank', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/local-resume/local-resume.module#LocalResumePageModule', name: 'LocalResumePage', segment: 'local-resume', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/piscina/piscina.module#PiscinaPageModule', name: 'PiscinaPage', segment: 'piscina', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reservas/reservas.module#ReservasPageModule', name: 'ReservasPage', segment: 'reservas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tenis/tenis.module#TenisPageModule', name: 'TenisPage', segment: 'tenis', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_incidencia_incidencia__["a" /* IncidenciaPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_telefonos_telefonos__["a" /* TelefonosPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_acercade_acercade__["a" /* AcercadePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_padel_padel__["a" /* PadelPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_local_local__["a" /* LocalPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_informacion_informacion__["a" /* InformacionPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_codigo_codigo__["a" /* CodigoPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_principal_principal__["a" /* PrincipalPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_documento_documento__["a" /* DocumentoPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_politica_politica__["a" /* PoliticaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_noticias_noticias__["a" /* NoticiasPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_micasa_micasa__["a" /* MicasaPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_listado_listado__["a" /* ListadoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_scan_scan__["a" /* ScanPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_local_resume_local_resume__["a" /* LocalResumePage */],
                __WEBPACK_IMPORTED_MODULE_46__components_components_header_components_header__["a" /* ComponentsHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_47__components_components_footer_components_footer__["a" /* ComponentsFooterComponent */],
                __WEBPACK_IMPORTED_MODULE_51__components_components_sidemenu_components_sidemenu__["a" /* ComponentsSidemenuComponent */],
                __WEBPACK_IMPORTED_MODULE_56__components_loading_modal_loading_modal__["a" /* LoadingModalComponent */],
                __WEBPACK_IMPORTED_MODULE_55__components_alert_alert__["a" /* AlertComponent */],
                __WEBPACK_IMPORTED_MODULE_50__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_reservas_reservas__["a" /* ReservasPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_tenis_tenis__["a" /* TenisPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_piscina_piscina__["a" /* PiscinaPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_blank_blank__["a" /* BlankPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_push__["a" /* Push */],
                // ConfigProvider,
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_native_storage__["a" /* NativeStorage */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer__["b" /* FileTransferObject */],
                { provide: __WEBPACK_IMPORTED_MODULE_40__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: __WEBPACK_IMPORTED_MODULE_58__common_interceptors_http_interceptor__["a" /* CustomHttpInterceptor */], multi: true },
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_32__providers_database_database__["a" /* DatabaseProvider */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_sqlite_porter__["a" /* SQLitePorter */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_39__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_native_market__["a" /* Market */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_48__providers_storage_service__["a" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_49__providers_session_service__["a" /* SessionService */],
                __WEBPACK_IMPORTED_MODULE_57__providers_modal_service__["a" /* ModalService */],
                __WEBPACK_IMPORTED_MODULE_44__ionic_native_stripe__["a" /* Stripe */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["LOCALE_ID"], useValue: "es-*" }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModalService = /** @class */ (function () {
    function ModalService() {
        this.visibleLoadingModal = false;
        this.visiblePhoneModal = false;
        this.visibleServiceModal = false;
        this.alert = { success: true, visible: false };
        this.DEFAULT_ALERT_TIMER = 3000;
    }
    ModalService.prototype.showLoadingModal = function (timer) {
        var _this = this;
        if (timer) {
            this.visibleLoadingModal = true;
            setTimeout(function () {
                _this.visibleLoadingModal = false;
            }, timer);
        }
        this.visibleLoadingModal = true;
    };
    ModalService.prototype.hideLoadingModal = function () {
        this.visibleLoadingModal = false;
    };
    ModalService.prototype.showPhoneModal = function () {
        this.visiblePhoneModal = true;
    };
    ModalService.prototype.hidePhoneModal = function () {
        this.visiblePhoneModal = false;
    };
    ModalService.prototype.showServiceModal = function () {
        this.visibleServiceModal = true;
    };
    ModalService.prototype.hideServiceModal = function () {
        this.visibleServiceModal = false;
    };
    ModalService.prototype.showAlert = function (success, timer) {
        var _this = this;
        var TIMER = timer ? timer : this.DEFAULT_ALERT_TIMER;
        this.alert.success = success;
        this.alert.visible = true;
        setTimeout(function () {
            _this.alert.visible = false;
        }, TIMER);
    };
    ModalService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ModalService);
    return ModalService;
}());

//# sourceMappingURL=modal.service.js.map

/***/ }),

/***/ 617:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 264,
	"./af.js": 264,
	"./ar": 265,
	"./ar-dz": 266,
	"./ar-dz.js": 266,
	"./ar-kw": 267,
	"./ar-kw.js": 267,
	"./ar-ly": 268,
	"./ar-ly.js": 268,
	"./ar-ma": 269,
	"./ar-ma.js": 269,
	"./ar-sa": 270,
	"./ar-sa.js": 270,
	"./ar-tn": 271,
	"./ar-tn.js": 271,
	"./ar.js": 265,
	"./az": 272,
	"./az.js": 272,
	"./be": 273,
	"./be.js": 273,
	"./bg": 274,
	"./bg.js": 274,
	"./bm": 275,
	"./bm.js": 275,
	"./bn": 276,
	"./bn.js": 276,
	"./bo": 277,
	"./bo.js": 277,
	"./br": 278,
	"./br.js": 278,
	"./bs": 279,
	"./bs.js": 279,
	"./ca": 280,
	"./ca.js": 280,
	"./cs": 281,
	"./cs.js": 281,
	"./cv": 282,
	"./cv.js": 282,
	"./cy": 283,
	"./cy.js": 283,
	"./da": 284,
	"./da.js": 284,
	"./de": 285,
	"./de-at": 286,
	"./de-at.js": 286,
	"./de-ch": 287,
	"./de-ch.js": 287,
	"./de.js": 285,
	"./dv": 288,
	"./dv.js": 288,
	"./el": 289,
	"./el.js": 289,
	"./en-au": 290,
	"./en-au.js": 290,
	"./en-ca": 291,
	"./en-ca.js": 291,
	"./en-gb": 292,
	"./en-gb.js": 292,
	"./en-ie": 293,
	"./en-ie.js": 293,
	"./en-il": 294,
	"./en-il.js": 294,
	"./en-in": 295,
	"./en-in.js": 295,
	"./en-nz": 296,
	"./en-nz.js": 296,
	"./en-sg": 297,
	"./en-sg.js": 297,
	"./eo": 298,
	"./eo.js": 298,
	"./es": 299,
	"./es-do": 300,
	"./es-do.js": 300,
	"./es-us": 301,
	"./es-us.js": 301,
	"./es.js": 299,
	"./et": 302,
	"./et.js": 302,
	"./eu": 303,
	"./eu.js": 303,
	"./fa": 304,
	"./fa.js": 304,
	"./fi": 305,
	"./fi.js": 305,
	"./fil": 306,
	"./fil.js": 306,
	"./fo": 307,
	"./fo.js": 307,
	"./fr": 308,
	"./fr-ca": 309,
	"./fr-ca.js": 309,
	"./fr-ch": 310,
	"./fr-ch.js": 310,
	"./fr.js": 308,
	"./fy": 311,
	"./fy.js": 311,
	"./ga": 312,
	"./ga.js": 312,
	"./gd": 313,
	"./gd.js": 313,
	"./gl": 314,
	"./gl.js": 314,
	"./gom-deva": 315,
	"./gom-deva.js": 315,
	"./gom-latn": 316,
	"./gom-latn.js": 316,
	"./gu": 317,
	"./gu.js": 317,
	"./he": 318,
	"./he.js": 318,
	"./hi": 319,
	"./hi.js": 319,
	"./hr": 320,
	"./hr.js": 320,
	"./hu": 321,
	"./hu.js": 321,
	"./hy-am": 322,
	"./hy-am.js": 322,
	"./id": 323,
	"./id.js": 323,
	"./is": 324,
	"./is.js": 324,
	"./it": 325,
	"./it-ch": 326,
	"./it-ch.js": 326,
	"./it.js": 325,
	"./ja": 327,
	"./ja.js": 327,
	"./jv": 328,
	"./jv.js": 328,
	"./ka": 329,
	"./ka.js": 329,
	"./kk": 330,
	"./kk.js": 330,
	"./km": 331,
	"./km.js": 331,
	"./kn": 332,
	"./kn.js": 332,
	"./ko": 333,
	"./ko.js": 333,
	"./ku": 334,
	"./ku.js": 334,
	"./ky": 335,
	"./ky.js": 335,
	"./lb": 336,
	"./lb.js": 336,
	"./lo": 337,
	"./lo.js": 337,
	"./lt": 338,
	"./lt.js": 338,
	"./lv": 339,
	"./lv.js": 339,
	"./me": 340,
	"./me.js": 340,
	"./mi": 341,
	"./mi.js": 341,
	"./mk": 342,
	"./mk.js": 342,
	"./ml": 343,
	"./ml.js": 343,
	"./mn": 344,
	"./mn.js": 344,
	"./mr": 345,
	"./mr.js": 345,
	"./ms": 346,
	"./ms-my": 347,
	"./ms-my.js": 347,
	"./ms.js": 346,
	"./mt": 348,
	"./mt.js": 348,
	"./my": 349,
	"./my.js": 349,
	"./nb": 350,
	"./nb.js": 350,
	"./ne": 351,
	"./ne.js": 351,
	"./nl": 352,
	"./nl-be": 353,
	"./nl-be.js": 353,
	"./nl.js": 352,
	"./nn": 354,
	"./nn.js": 354,
	"./oc-lnc": 355,
	"./oc-lnc.js": 355,
	"./pa-in": 356,
	"./pa-in.js": 356,
	"./pl": 357,
	"./pl.js": 357,
	"./pt": 358,
	"./pt-br": 359,
	"./pt-br.js": 359,
	"./pt.js": 358,
	"./ro": 360,
	"./ro.js": 360,
	"./ru": 361,
	"./ru.js": 361,
	"./sd": 362,
	"./sd.js": 362,
	"./se": 363,
	"./se.js": 363,
	"./si": 364,
	"./si.js": 364,
	"./sk": 365,
	"./sk.js": 365,
	"./sl": 366,
	"./sl.js": 366,
	"./sq": 367,
	"./sq.js": 367,
	"./sr": 368,
	"./sr-cyrl": 369,
	"./sr-cyrl.js": 369,
	"./sr.js": 368,
	"./ss": 370,
	"./ss.js": 370,
	"./sv": 371,
	"./sv.js": 371,
	"./sw": 372,
	"./sw.js": 372,
	"./ta": 373,
	"./ta.js": 373,
	"./te": 374,
	"./te.js": 374,
	"./tet": 375,
	"./tet.js": 375,
	"./tg": 376,
	"./tg.js": 376,
	"./th": 377,
	"./th.js": 377,
	"./tl-ph": 378,
	"./tl-ph.js": 378,
	"./tlh": 379,
	"./tlh.js": 379,
	"./tr": 380,
	"./tr.js": 380,
	"./tzl": 381,
	"./tzl.js": 381,
	"./tzm": 382,
	"./tzm-latn": 383,
	"./tzm-latn.js": 383,
	"./tzm.js": 382,
	"./ug-cn": 384,
	"./ug-cn.js": 384,
	"./uk": 385,
	"./uk.js": 385,
	"./ur": 386,
	"./ur.js": 386,
	"./uz": 387,
	"./uz-latn": 388,
	"./uz-latn.js": 388,
	"./uz.js": 387,
	"./vi": 389,
	"./vi.js": 389,
	"./x-pseudo": 390,
	"./x-pseudo.js": 390,
	"./yo": 391,
	"./yo.js": 391,
	"./zh-cn": 392,
	"./zh-cn.js": 392,
	"./zh-hk": 393,
	"./zh-hk.js": 393,
	"./zh-mo": 394,
	"./zh-mo.js": 394,
	"./zh-tw": 395,
	"./zh-tw.js": 395
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 617;

/***/ }),

/***/ 625:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export POOL_PERSON_NUMBERS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DEFAULT_HOURS; });
var POOL_PERSON_NUMBERS = [1, 2, 3, 4, 5];
var DEFAULT_HOURS = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
//# sourceMappingURL=piscina.constants.js.map

/***/ }),

/***/ 643:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_codigo_codigo__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_incidencia_incidencia__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_micasa_micasa__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_noticias_noticias__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_modal_service__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};















var MyApp = /** @class */ (function () {
    function MyApp(platform, toastCtrl, push, statusBar, menu, splashScreen, sessionService, storageService, modalService) {
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.push = push;
        this.statusBar = statusBar;
        this.menu = menu;
        this.splashScreen = splashScreen;
        this.sessionService = sessionService;
        this.storageService = storageService;
        this.modalService = modalService;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_principal_principal__["a" /* PrincipalPage */];
        this.introDone = false;
        //  this.rootPage = RegistroPage;
        this.initializeApp();
    }
    MyApp.prototype.hideCamera = function () {
        window.document.querySelector('ion-app').classList.remove('cameraView');
    };
    MyApp.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                //  await this.storageService.clearAll();
                this.platform.ready().then(function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                this.statusBar.styleDefault();
                                this.splashScreen.hide();
                                _a = this;
                                return [4 /*yield*/, this.sessionService.isIntroDone()];
                            case 1:
                                _a.introDone = _b.sent();
                                console.log('INTRO SHOWN -> ', this.introDone);
                                // this.hideCamera();
                                this.initPushNotification();
                                this.backPressHandle();
                                if (this.introDone) {
                                    this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_principal_principal__["a" /* PrincipalPage */];
                                }
                                else {
                                    this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_codigo_codigo__["a" /* CodigoPage */];
                                }
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    MyApp.prototype.backPressHandle = function () {
        var _this = this;
        var lastTimeBackPress = 0;
        var timePeriodToExit = 2000;
        this.platform.registerBackButtonAction(function () {
            // get current active page
            var view = _this.nav.getActive();
            var childView = _this.nav.getActiveChildNavs();
            console.log('active :', view.component.name);
            switch (view.component.name) {
                case 'PrincipalPage':
                    if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                        _this.platform.exitApp(); //Exit from app
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: 'Pulsa otra vez si quieres salir de Icomunity',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                        lastTimeBackPress = new Date().getTime();
                    }
                    break;
                case 'AcercadePage':
                case 'TelefonosPage':
                case 'PerfilPage':
                case 'DocumentoPage':
                case 'InformacionPage':
                case 'IncidenciaPage':
                case 'MicasaPage':
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_principal_principal__["a" /* PrincipalPage */]);
                    break;
                case 'ListadoPage':
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_micasa_micasa__["a" /* MicasaPage */]);
                    break;
                default:
                    break;
            }
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.initPushNotification = function () {
        var _this = this;
        // to check if we have permission
        this.push.hasPermission()
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
            }
            else {
                console.log('We don\'t have permission to send push notifications');
            }
        });
        // to initialize push notifications
        var options = {
            android: {
                senderID: '634760725285'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false',
                clearBadge: true
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            console.log('Received a notification', notification);
            var type = notification.type;
            if (type == 'incidencia') {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_incidencia_incidencia__["a" /* IncidenciaPage */]);
            }
            else if (type == 'noticia') {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_noticias_noticias__["a" /* NoticiasPage */]);
            }
            else {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]);
            }
            //
        });
        pushObject.on('registration').
            subscribe(function (registration) {
            _this.token = registration.registrationId;
            var token_str = JSON.stringify(_this.token);
            localStorage.setItem("mytoken", token_str);
            console.log("app :", JSON.stringify(registration));
            // this.updateToken(this.token);
        });
        pushObject.on('error').
            subscribe(function (error) {
            return console.error('Error with Push plugin', error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/app/app.html"*/'<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n<loading-modal [hidden]="!modalService.visibleLoadingModal"></loading-modal>\n<alert-modal *ngIf="modalService.alert.visible"></alert-modal>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_11__providers_session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_12__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_13__providers_modal_service__["a" /* ModalService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 644:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PLANTA_LIST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PUERTA_LIST; });
var PLANTA_LIST = ['Entresuelo', 'Bajo', 'Principal', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Atico', 'Porteria'];
var PUERTA_LIST = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'DCHA', 'IZDA', 'CENTRO', 'PORTERIA', 'OTRO'];
//# sourceMappingURL=registro.constants.js.map

/***/ }),

/***/ 645:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanPage = /** @class */ (function () {
    function ScanPage(navCtrl, 
        // private qrScanner: QRScanner,
        navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loader = false;
    }
    ScanPage.prototype.ionViewDidLoad = function () {
        this.showCamera();
        // this.scan();
        console.log('ionViewDidLoad ScanPage');
    };
    ScanPage.prototype.ionViewWillLeave = function () {
        // this.qrScanner.hide();
        this.scanSub.unsubscribe();
        this.hideCamera();
    };
    /* scan(){
      this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          console.log('Camera Permission Given');
           this.scanSub = this.qrScanner.scan().subscribe((text: string) => {
           console.log('Scanned something', text);
           if(text!=undefined && text!=null && text!=''){
            this.qrScanner.hide();
            this.scanSub.unsubscribe();
            this.hideCamera();
            this.popBack(text);
           }
        
          });
  
          this.qrScanner.show();
        } else if (status.denied) {
          console.log('Camera permission denied');
        } else {
          console.log('Permission denied for this runtime.');
        }
      })
      .catch((e: any) => console.log('Error is', e));
  } */
    ScanPage.prototype.popBack = function (code) {
        this.navCtrl.getPrevious().data.code = code;
        this.navCtrl.pop();
    };
    ScanPage.prototype.showCamera = function () {
        window.document.querySelector('ion-app').classList.add('cameraView');
    };
    ScanPage.prototype.hideCamera = function () {
        window.document.querySelector('ion-app').classList.remove('cameraView');
    };
    ScanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-scan',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/scan/scan.html"*/'<!--\n  Generated template for the ScanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>scan</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/scan/scan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ScanPage);
    return ScanPage;
}());

//# sourceMappingURL=scan.js.map

/***/ }),

/***/ 659:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TruncatePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TruncatePipe = /** @class */ (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (value) {
        var description = '';
        if (value.length > 40) {
            description = value.substring(0, 40) + '...';
        }
        return description;
    };
    TruncatePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'truncate'
        })
    ], TruncatePipe);
    return TruncatePipe;
}());

//# sourceMappingURL=truncate.pipe.js.map

/***/ }),

/***/ 660:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MomentPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MomentPipe = /** @class */ (function () {
    function MomentPipe() {
    }
    MomentPipe.prototype.transform = function (value, dateFormat) {
        return __WEBPACK_IMPORTED_MODULE_1_moment__(value).format(dateFormat);
    };
    MomentPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'dateFormat' })
    ], MomentPipe);
    return MomentPipe;
}());

//# sourceMappingURL=date.pipe.js.map

/***/ }),

/***/ 661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ComponentsHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ComponentsHeaderComponent = /** @class */ (function () {
    function ComponentsHeaderComponent() {
        console.log('Hello ComponentsHeaderComponent Component');
        this.text = 'Hello World';
    }
    ComponentsHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'components-header',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-header/components-header.html"*/'<div class="c-header">\n    <div class="c-header__container">\n     <p class="c-header__container__text">ICOMUNITY</p>\n    </div>\n    </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-header/components-header.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsHeaderComponent);
    return ComponentsHeaderComponent;
}());

//# sourceMappingURL=components-header.js.map

/***/ }),

/***/ 662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ComponentsFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ComponentsFooterComponent = /** @class */ (function () {
    function ComponentsFooterComponent() {
        console.log('Hello ComponentsFooterComponent Component');
        this.text = 'Hello World';
    }
    ComponentsFooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'components-footer',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-footer/components-footer.html"*/'<div class="c-footer">\n    <div class="c-footer__container">\n      <p class="c-footer__container__text"> @Powered by ICOMUNITY 2020 </p>\n     </div>\n    </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-footer/components-footer.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsFooterComponent);
    return ComponentsFooterComponent;
}());

//# sourceMappingURL=components-footer.js.map

/***/ }),

/***/ 663:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsSidemenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_home_home__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_incidencia_incidencia__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_informacion_informacion__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_telefonos_telefonos__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_documento_documento__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_micasa_micasa__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_perfil_perfil__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_acercade_acercade__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the ComponentsSidemenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ComponentsSidemenuComponent = /** @class */ (function () {
    function ComponentsSidemenuComponent() {
    }
    ComponentsSidemenuComponent.prototype.ngOnInit = function () {
        this.initSideMenu();
    };
    ComponentsSidemenuComponent.prototype.initSideMenu = function () {
        this.pages = [
            { title: 'Pagina principal', component: __WEBPACK_IMPORTED_MODULE_1__pages_home_home__["a" /* HomePage */], icon: 'home' },
            { title: 'Incidencias', component: __WEBPACK_IMPORTED_MODULE_2__pages_incidencia_incidencia__["a" /* IncidenciaPage */], icon: 'warning' },
            { title: 'Solicitar informacion', component: __WEBPACK_IMPORTED_MODULE_3__pages_informacion_informacion__["a" /* InformacionPage */], icon: 'information-circle' },
            { title: 'Telefonos de interes', component: __WEBPACK_IMPORTED_MODULE_4__pages_telefonos_telefonos__["a" /* TelefonosPage */], icon: 'call' },
            { title: 'Mandar documento', component: __WEBPACK_IMPORTED_MODULE_5__pages_documento_documento__["a" /* DocumentoPage */], icon: 'document' },
            { title: 'Mi casa', component: __WEBPACK_IMPORTED_MODULE_6__pages_micasa_micasa__["a" /* MicasaPage */], icon: 'construct' },
            { title: 'Perfilooo', component: __WEBPACK_IMPORTED_MODULE_7__pages_perfil_perfil__["a" /* PerfilPage */], icon: 'person' },
            { title: 'Acerca de', component: __WEBPACK_IMPORTED_MODULE_8__pages_acercade_acercade__["a" /* AcercadePage */], icon: 'at' }
        ];
    };
    ComponentsSidemenuComponent.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["Nav"])
    ], ComponentsSidemenuComponent.prototype, "nav", void 0);
    ComponentsSidemenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'components-sidemenu',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-sidemenu/components-sidemenu.html"*/'<div class="c-sidemenu">\n  <ion-menu [content]="content">\n    <ion-header>\n      <ion-toolbar color="primary">\n        <ion-title>Mi Comunidad</ion-title>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content class="c-sidemenu__container">\n      <ion-list>\n        <button class="menu-item" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n          <ion-icon name="{{p.icon}}" item-left></ion-icon>\n          <div id="menu-item-text">{{p.title}}</div>\n        </button>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n</div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/components-sidemenu/components-sidemenu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsSidemenuComponent);
    return ComponentsSidemenuComponent;
}());

//# sourceMappingURL=components-sidemenu.js.map

/***/ }),

/***/ 664:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_modal_service__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AlertComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var AlertComponent = /** @class */ (function () {
    function AlertComponent(modalService) {
        this.modalService = modalService;
        console.log('Hello AlertComponent Component');
        this.text = 'Hello World';
    }
    AlertComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'alert-modal',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/alert/alert.html"*/'<div class="c-alert-modal">\n    <div class="c-alert-modal__container animate__animated animate__slideInDown">\n        <div class="c-alert-modal__container__main" \n        [class.success]="modalService.alert.success"\n        [class.error]="!modalService.alert.success"\n        >\n                <p *ngIf="modalService.alert.success">Operación realizada con exito </p>\n                <i class="fas fa-check-circle" *ngIf="modalService.alert.success"></i>\n\n                <p *ngIf="!modalService.alert.success"> Error al realizar la operación </p>\n                <i class="fas fa-exclamation-circle" *ngIf="!modalService.alert.success"></i>\n        </div>\n    </div>\n</div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/alert/alert.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_modal_service__["a" /* ModalService */]])
    ], AlertComponent);
    return AlertComponent;
}());

//# sourceMappingURL=alert.js.map

/***/ }),

/***/ 665:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_modal_service__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoadingModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var LoadingModalComponent = /** @class */ (function () {
    function LoadingModalComponent(modalService) {
        this.modalService = modalService;
    }
    LoadingModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'loading-modal',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/loading-modal/loading-modal.html"*/'<div class="c-loadingmodal">\n    <div class="c-loadingmodal__container">\n        <div class="spinner">\n            <div class="cube1"></div>\n            <div class="cube2"></div>\n          </div>\n    </div>\n    </div>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/components/loading-modal/loading-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_modal_service__["a" /* ModalService */]])
    ], LoadingModalComponent);
    return LoadingModalComponent;
}());

//# sourceMappingURL=loading-modal.js.map

/***/ }),

/***/ 666:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomHttpInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_modal_service__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomHttpInterceptor = /** @class */ (function () {
    function CustomHttpInterceptor(modalService) {
        this.modalService = modalService;
        this.requests = [];
        console.log('INTERCEPTOR INIT');
    }
    CustomHttpInterceptor.prototype.removeRequest = function (req) {
        var i = this.requests.indexOf(req);
        this.requests.splice(i, 1);
        this.modalService.hideLoadingModal();
        console.log('INTERCEPTOR END');
    };
    CustomHttpInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        console.log('INTERCEPTOR START');
        this.requests.push(req);
        this.modalService.showLoadingModal();
        return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].create(function (observer) {
            var subscription = next.handle(req)
                .subscribe(function (event) {
                if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpResponse */]) {
                    _this.removeRequest(req);
                    observer.next(event);
                }
            }, function (err) { _this.removeRequest(req); observer.error(err); }, function () { _this.removeRequest(req); observer.complete(); });
            // teardown logic in case of cancelled requests
            return function () {
                _this.removeRequest(req);
                subscription.unsubscribe();
            };
        });
    };
    CustomHttpInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_modal_service__["a" /* ModalService */]])
    ], CustomHttpInterceptor);
    return CustomHttpInterceptor;
}());

//# sourceMappingURL=http-interceptor.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__payment_payment__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__local_resume_local_resume__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_session_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var LocalPage = /** @class */ (function () {
    /*   fakeLocalBook: LocalBookModel = {
        id: '0',
        userId: 'test',
        code: 'AAA222',
        day: new Date(),
        formattedDay: '',
        hours: ['10','12','13'],
        active: true,
        username: 'steve berek',
        address: 'cabezo buenavista',
        amount: '10',
        rangeTime: '',
        createdAt: new Date(),
        formattedDate: ''
      }; */
    function LocalPage(navCtrl, nativeStorage, loadingCtrl, databaseprovider, alertCtrl, restProvider, storageService, sessionService) {
        this.navCtrl = navCtrl;
        this.nativeStorage = nativeStorage;
        this.loadingCtrl = loadingCtrl;
        this.databaseprovider = databaseprovider;
        this.alertCtrl = alertCtrl;
        this.restProvider = restProvider;
        this.storageService = storageService;
        this.sessionService = sessionService;
        this.list = "mis-res";
        this.optionsRange = {
            pickMode: "single",
            from: __WEBPACK_IMPORTED_MODULE_3_moment__().toDate(),
            to: __WEBPACK_IMPORTED_MODULE_3_moment__()
                .add(1, "month")
                .endOf("month")
                .toDate(),
            showMonthPicker: true,
            monthPickerFormat: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
            weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekStart: 1,
        };
        this.hoursList = [];
        this.bool_hours = false;
        this.localReservations = [];
        this.bool_res_list = false;
        this.selectedHours = [];
        this.countHours = 0;
        this.message = "Usted no tiene ninguna reserva pendiente";
        this.activeCalendar = true;
        this.date1 = "";
        this.date2 = "";
        this.rest = restProvider;
    }
    LocalPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                this.initAuthProcess();
                return [2 /*return*/];
            });
        });
    };
    LocalPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        this.fetchUserInfos();
                        this.bool_hours = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    LocalPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        this.getUserReservation();
                        return [2 /*return*/];
                }
            });
        });
    };
    LocalPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionService.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log("token process -> ", authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    LocalPage.prototype.setHoursStartEndReservas = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.localReservations.forEach(function (item) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                item.end = 0;
                                item.end = Number(item.hours[item.hours.length - 1]);
                                item.end++;
                                item.start = Number(item.hours[0]);
                                item.rangeTime = item.start + "h - " + item.end + "h";
                                item.state = item.active ? 'activo' : 'cancelado';
                                console.log("RESERVA ITEM START", item.start);
                                console.log("RESERVA ITEM END", item.end);
                                return [2 /*return*/];
                            });
                        }); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LocalPage.prototype.onTabsChange = function () {
        console.log('TABS CHANGES');
        this.hour_selected = null;
    };
    LocalPage.prototype.setCalendarRange = function (showFirstMonth, showSecondMonth) {
        console.log("SETTING CALENDAR RANGE  ", showFirstMonth + " ---- " + showSecondMonth);
        if (!showFirstMonth && !showSecondMonth) {
            this.activeCalendar = false;
            this.message =
                "Usted ya tiene dos reservas pendientes, tendrá que esperar que pase la fecha de la primera antes de poder hacer una nueva reserva";
        }
        else if (!showSecondMonth) {
            this.activeCalendar = true;
            this.optionsRange = {
                pickMode: "single",
                from: __WEBPACK_IMPORTED_MODULE_3_moment__().toDate(),
                to: __WEBPACK_IMPORTED_MODULE_3_moment__()
                    .add(1, "month")
                    .add(1, "day")
                    .date(0)
                    .toDate(),
                showMonthPicker: true
            };
            this.message = "Usted ya tiene una reserva pendiente el " + this.date2;
        }
        else if (!showFirstMonth) {
            this.activeCalendar = true;
            this.optionsRange = {
                pickMode: "single",
                from: __WEBPACK_IMPORTED_MODULE_3_moment__()
                    .add(1, "month")
                    .startOf("month")
                    .toDate(),
                to: __WEBPACK_IMPORTED_MODULE_3_moment__()
                    .add(1, "month")
                    .endOf("month")
                    .toDate(),
                showMonthPicker: true
            };
            this.message = "Usted ya tiene una reserva pendiente el " + this.date1;
        }
        else {
            this.activeCalendar = true;
            this.optionsRange = {
                pickMode: "single",
                from: __WEBPACK_IMPORTED_MODULE_3_moment__().toDate(),
                to: __WEBPACK_IMPORTED_MODULE_3_moment__()
                    .endOf("month")
                    .toDate(),
                showMonthPicker: true
            };
        }
        console.log("OPTION RANGE", JSON.stringify(this.optionsRange));
    };
    LocalPage.prototype.checkReservations = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var showFirstMonth, showSecondMonth;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        showFirstMonth = true;
                        showSecondMonth = true;
                        console.log("end first month", __WEBPACK_IMPORTED_MODULE_3_moment__()
                            .add(1, "month")
                            .date(0)
                            .toDate()
                            .toDateString());
                        console.log("end second month", __WEBPACK_IMPORTED_MODULE_3_moment__()
                            .add(2, "month")
                            .date(0)
                            .toDate()
                            .toDateString());
                        return [4 /*yield*/, this.localReservations.forEach(function (item) { return __awaiter(_this, void 0, void 0, function () {
                                var element, date1, date2;
                                return __generator(this, function (_a) {
                                    console.log("FOR EACH ITEM:", JSON.stringify(item));
                                    try {
                                        element = __WEBPACK_IMPORTED_MODULE_3_moment__(item.day);
                                        date1 = __WEBPACK_IMPORTED_MODULE_3_moment__().toDate();
                                        date2 = __WEBPACK_IMPORTED_MODULE_3_moment__()
                                            .add(1, "month")
                                            .toDate();
                                        console.log("date 1: ", date1);
                                        console.log("date 2: ", date2);
                                        console.log("element: ", element);
                                        //  let diff1 = moment().endOf('month').diff(element,'days');
                                        // let diff2 = moment().add(1,'month').endOf('month').diff(element,'days');
                                        if (__WEBPACK_IMPORTED_MODULE_3_moment__(date1).isSame(element, "month") &&
                                            __WEBPACK_IMPORTED_MODULE_3_moment__(date1).isSame(element, "year")) {
                                            showFirstMonth = false;
                                            this.date1 = __WEBPACK_IMPORTED_MODULE_3_moment__(item.day).format("DD-MM-YYYY");
                                        }
                                        if (__WEBPACK_IMPORTED_MODULE_3_moment__(date2).isSame(element, "month") &&
                                            __WEBPACK_IMPORTED_MODULE_3_moment__(date2).isSame(element, "year")) {
                                            showSecondMonth = false;
                                            this.date2 = __WEBPACK_IMPORTED_MODULE_3_moment__(item.day).format("DD-MM-YYYY");
                                        }
                                        console.log("show first month: ", showFirstMonth);
                                        console.log("show second month: ", showSecondMonth);
                                        /*   console.log('diff 1',diff1);
                                        console.log('diff 2',diff2);
                                        if(diff1<30)
                                        showFirstMonth=false;
                                        if(diff2<30)
                                        showSecondMonth=false; */
                                    }
                                    catch (error) {
                                        console.log("CATCH ERROR", error);
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        console.log("boolean first month", showFirstMonth);
                        console.log("boolean second month", showSecondMonth);
                        this.setCalendarRange(showFirstMonth, showSecondMonth);
                        return [2 /*return*/];
                }
            });
        });
    };
    LocalPage.prototype.showReserva = function (item) {
        console.log("opening reserva...", JSON.stringify(item));
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__local_resume_local_resume__["a" /* LocalResumePage */], {
            comunity: this.comunidad,
            adress: this.perfil["adress"],
            name: this.perfil["name"],
            lastname: this.perfil["lastname"],
            day: item.day,
            hours: item.hours,
            start: item.start,
            end: item.end
        });
    };
    LocalPage.prototype.anularReserva = function (index) {
        var _this = this;
        this.presentLoading();
        var day = this.localReservations[index]["day"];
        var hour = this.localReservations[index]["hour"];
        this.rest.anularReserva(this.comunidad.code, day, hour).subscribe(function (response) {
            _this.dismissLoading();
            var alert = _this.alertCtrl.create({
                title: "¡ Exito !",
                subTitle: "Esta reserva ha sido cancelada correctamente",
                buttons: ["OK"]
            });
            alert.present();
            alert.onDidDismiss(function (res) {
                _this.localReservations = null;
                _this.getUserReservation();
                _this.list = "mis-res";
            });
        }, function (error) {
            console.log("error anular : " + error);
        });
    };
    LocalPage.prototype.successAnular = function () { };
    LocalPage.prototype.getUserReservation = function () {
        var _this = this;
        this.presentLoading();
        console.log("code house :" + this.perfil["code_house"]);
        this.rest.getUserReservationsLocal(this.perfil["code_house"]).subscribe(function (response) {
            console.log("USER RESERVATION RESPONSE:" + JSON.stringify(response));
            _this.dismissLoading();
            console.log("user reservations response", JSON.stringify(response));
            _this.localReservations = response;
            var size = Object.keys(response).length;
            if (size > 0)
                _this.bool_res_list = true;
            else
                _this.bool_res_list = false;
            if (size > 0) {
                _this.checkReservations();
                _this.setHoursStartEndReservas();
                _this.mapUserReservations();
            }
        }, function (error) {
            console.log("GET USER RESERVATION ERROR:" + error);
        });
    };
    LocalPage.prototype.mapUserReservations = function () {
        var today = new Date();
        if (this.localReservations && this.localReservations.length > 0) {
            this.localReservations.forEach(function (item) {
                var day = item.day;
                var date = new Date(day);
                var diff = date.getTime() - today.getTime();
                console.log('FECHA -> ', day);
                console.log('DIFF -> ', diff);
                if (diff < 0) {
                    item.state = 'caducado';
                    item.active = false;
                }
            });
        }
    };
    LocalPage.prototype.reservar = function () {
        var _this = this;
        if (this.hour_selected != undefined && this.hour_selected != null) {
            this.presentLoading();
            this.rest
                .reservarLocal(this.comunidad.code, this.perfil["code_house"], this.date, this.selectedHours)
                .subscribe(function (response) {
                _this.dismissLoading();
                _this.successReserva();
            }, function (error) {
                console.log(" res error :" + error);
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Ojo !",
                subTitle: "No has seleccionado ninguna hora",
                buttons: ["OK"]
            });
            alert_1.present();
        }
    };
    LocalPage.prototype.successReserva = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "¡ Enhorabuena !",
            subTitle: "reserva finalizada con exito",
            buttons: ["OK"]
        });
        alert.present();
        alert.onDidDismiss(function (res) {
            _this.getUserReservation();
            _this.list = "mis-res";
        });
    };
    LocalPage.prototype.getHoursByDay = function (day) {
        var _this = this;
        this.bool_hours = false;
        this.hour_selected = null;
        this.presentLoading();
        console.log("code : " + this.comunidad.code + " day :" + day);
        this.rest.getHoursByDateLocal(this.comunidad.code, day).subscribe(function (res) {
            var result = null;
            var table = [];
            _this.dismissLoading();
            _this.response = JSON.stringify(res);
            console.log("json response : " + JSON.stringify(res));
            result = res;
            var size = Object.keys(res).length;
            for (var i = 0; i < size; i++) {
                if (result[i]["hours"] != undefined) {
                    result[i]["hours"].forEach(function (element) {
                        table.push(element);
                        console.log("hour : " + table[i]);
                    });
                }
            }
            if (_this.isEmpty(table)) {
                _this.bool_hours = true;
                _this.hoursList = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
            }
            else {
                _this.bool_hours = true;
                var len = Object.keys(table).length;
                var temp = [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21"
                ];
                for (var jdex = 0; jdex < len; jdex++) {
                    for (var index = 0; index < 13; index++) {
                        if (table[jdex] === temp[index])
                            temp.splice(index, 1);
                    }
                }
                _this.hoursList = temp;
            }
        }, function (error) {
            console.log("hours error :" + error);
        });
    };
    LocalPage.prototype.onChange = function (date) {
        this.date = __WEBPACK_IMPORTED_MODULE_3_moment__(date).format("YYYY-MM-DD");
        this.dateToShow = __WEBPACK_IMPORTED_MODULE_3_moment__(this.date).format("DD-MM-YYYY");
        var day = this.date.toString();
        this.getHoursByDay(day);
        this.selectedHours = [];
    };
    LocalPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    LocalPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    LocalPage.prototype.isEmpty = function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    LocalPage.prototype.isReservable = function () {
        console.log(JSON.stringify(this.selectedHours));
        if (!this.checkHours())
            this.alertBadHours();
        else
            this.goToPayment();
    };
    LocalPage.prototype.goToPayment = function () {
        this.countHours = this.selectedHours.length;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__payment_payment__["a" /* PaymentPage */], {
            date: this.date,
            hours: this.selectedHours,
            count: this.countHours,
            code: this.comunidad.code
        });
    };
    LocalPage.prototype.checkHours = function () {
        if (this.selectedHours.length == 1) {
            return true;
        }
        else if (this.selectedHours.length == 0) {
            return false;
        }
        else {
            for (var i = 1; i < this.selectedHours.length; i++) {
                if (this.selectedHours[i] - this.selectedHours[i - 1] != 1)
                    return false;
            }
            return true;
        }
    };
    LocalPage.prototype.alertBadHours = function () {
        var alert = this.alertCtrl.create({
            title: " Error ",
            subTitle: " ¡ Las Horas seleccionadas no están correctas , no se pueden seleccionar horas sueltas !",
            buttons: ["OK"]
        });
        alert.present();
    };
    LocalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-local",template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/local/local.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Reserva de local</ion-title>\n    </ion-navbar>\n    <ion-segment  (ionChange)="onTabsChange()" [(ngModel)]="list" padding>\n        <ion-segment-button value="nueva-res">\n            Nueva reserva\n        </ion-segment-button>\n        <ion-segment-button value="mis-res">\n            Historico res.\n        </ion-segment-button>\n    </ion-segment>\n</ion-header>\n\n<ion-content>\n    <div class="c-local">\n        <div [ngSwitch]="list">\n            <ion-list *ngSwitchCase="\'nueva-res\'">\n                <div id="content-nueva">\n                    <ion-calendar *ngIf="activeCalendar" [(ngModel)]="date" [options]="optionsRange" [type]="type"\n                        [format]="\'YYYY-MM-DD\'" (onChange)="onChange($event)">\n                    </ion-calendar>\n                    <div class="c-local__container__selection" *ngIf="bool_hours" padding>\n                        <div class="c-local__container__selection__date">\n                            <p class="title-text"> Horas disponibles para el : {{dateToShow}} </p>\n                        </div>\n\n                        <ion-item>\n                            <ion-label>Horas</ion-label>\n                            <ion-select [(ngModel)]="selectedHours" (change)="show()" multiple="true"\n                                interface="action-sheet">\n                                <ion-option *ngFor="let hour of hoursList" [value]="hour">{{hour}} h\n                                </ion-option>\n                            </ion-select>\n                        </ion-item>\n                        <button class="c-local__container__selection__btn  button-res" \n                        (click)="isReservable()" ion-button \n                           padding>\n                            Verificar disponibilidad\n                        </button>\n                    </div>\n                    <p class="message"> {{message}} </p>\n                </div>\n            </ion-list>\n\n            <ion-list *ngSwitchCase="\'mis-res\'" padding>\n                <div id="content-mis">\n                    <div *ngIf="bool_res_list">\n                            <ion-card class="c-local__container__list__card" *ngFor="let reserva of localReservations; let index = index"\n                            padding>\n                            <p class="label-reserva">reserva de local</p>\n                            <div class="c-local__container__list__row__item">\n                              <p class="c-local__container__list__row__item__label">Fecha alta:</p>\n                              <p class="c-local__container__list__row__item__value">{{reserva.createdAt | date: \'dd/MM/yyyy HH:mm\'}}\n                              </p>\n                            </div>\n                            <div class="c-local__container__list__row__item">\n                              <p class="c-local__container__list__row__item__label">Fecha reserva:</p>\n                              <p class="c-local__container__list__row__item__value">{{reserva.day | date: \'dd/MM/yyyy\'}}</p>\n                            </div>\n                            <div class="c-local__container__list__row__item">\n                              <p class="c-local__container__list__row__item__label">Franja horaria:</p>\n                              <p class="c-local__container__list__row__item__value">{{reserva.rangeTime}}</p>\n                            </div>\n                            <div class="c-local__container__list__row__item">\n                              <p class="c-local__container__list__row__item__label">Estado:</p>\n                              <p class="c-local__container__list__row__item__value" \n                              [class.active]="reserva.active === true"\n                                [class.inactive]="reserva.state === \'cancelado\'"\n                                [class.caducado]="reserva.state === \'caducado\'"\n                                \n                                >{{reserva.state}}</p>\n                            </div>\n                            \n                          </ion-card>\n                    </div>\n                    <div class="c-local__container__empty" *ngIf="!bool_res_list">\n                        <p >No se han encontrado reservas de local.</p>\n                    </div>\n                </div>\n            </ion-list>\n        </div>\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/local/local.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_9__providers_session_service__["a" /* SessionService */]])
    ], LocalPage);
    return LocalPage;
}());

//# sourceMappingURL=local.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__noticias_noticias__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__padel_padel__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__local_local__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_push__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_market__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__piscina_piscina__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, push, restProvider, databaseprovider, storage, platform, market, app, alertCtrl, storageService, sessionService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.push = push;
        this.restProvider = restProvider;
        this.databaseprovider = databaseprovider;
        this.storage = storage;
        this.platform = platform;
        this.market = market;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.sessionService = sessionService;
        this.loadingCtrl = loadingCtrl;
        this.sincroOK = false;
        this.images = {};
        this.mostrarPiscina = false;
        this.buildingIcon = 'assets/icon/building.png';
        this.rest = restProvider;
    }
    HomePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.initAuthProcess();
                this.initApp();
                this.icomunityUpdate();
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.initApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        console.log('FETCH COMUNIDAD -> ', this.comunidad);
                        if (this.comunidad && this.comunidad.code) {
                            this.showPiscinaButton();
                            this.fetchMasterData();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionService.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.loadComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _b.sent();
                        console.log('STORAGE COMUNIDAD -> ', this.comunidad);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.openReservaPiscina = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__piscina_piscina__["a" /* PiscinaPage */]);
    };
    HomePage.prototype.fetchMasterData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fetchComunidad()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.fetchServices()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.fetchPhones()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.showPiscinaButton = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, feat;
            return __generator(this, function (_b) {
                if (this.comunidad && this.comunidad.features.length > 0) {
                    for (_i = 0, _a = this.comunidad.features; _i < _a.length; _i++) {
                        feat = _a[_i];
                        if (feat === 'piscina')
                            this.mostrarPiscina = true;
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.fetchPhones = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider.getPhonesByCode(this.comunidad.code)
                            .subscribe(function (response) {
                            console.log(' PHONES DATA -> ', response);
                            if (response) {
                                var phones = response;
                                _this.storageService.savePhones(phones);
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.fetchServices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.restProvider.getServicesByCode(this.comunidad.code)
                            .subscribe(function (response) {
                            console.log(' SERVICES DATA -> ', response);
                            if (response) {
                                var services = response;
                                _this.storageService.saveServices(services);
                            }
                            else {
                                // SHOW ERROR MESSAGE
                            }
                        }, function (error) {
                            console.log(' ERROR MANDATORY APP DATA -> ', error);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.fetchComunidad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('FETCH LOAD COMUNIDAD -> ', this.comunidad);
                        return [4 /*yield*/, this.restProvider.getComunityByCode(this.comunidad.code)
                                .subscribe(function (response) {
                                if (response[0]) {
                                    var comunidad = response[0];
                                    _this.storageService.saveComunidad(comunidad);
                                    _this.comunidad = comunidad;
                                    console.log(' COMUNIDAD DATA -> ', response[0]);
                                    // this.loadComunidad();
                                }
                                else {
                                    // SHOW ERROR MESSAGE
                                }
                            }, function (error) {
                                console.log(' ERROR MANDATORY APP DATA -> ', error);
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.hideCamera = function () {
        window.document.querySelector('ion-app').classList.remove('cameraView');
    };
    HomePage.prototype.presentRGPD = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Revisión de nuestra politica de privacidad',
            message: '<div style="font-size:1.4em;">' +
                'Proteger y defender tu privacidad es una parte central de la actividad de Icomunity.' +
                ' Con el fin de ofrecerte aún más transparencia con respeto a los datos que Icomunity recopila acerca de ti ' +
                'como se usan y que controles tienen sobre tus datos personales, Icomunity actualizará los '
                + '<a href="https://app.berekstan.com/index.php/condiciones-de-uso/" >condiciones de uso</a>' +
                ' y la <a href="https://app.berekstan.com/index.php/condiciones-de-uso/" > politica de privacidad</a>' +
                '<br> <br>' +
                'Para seguir usando Icomunity debes aceptar la <a href="https://app.berekstan.com/index.php/condiciones-de-uso/" >politica de privacidad</a>' +
                '</div>',
            buttons: [
                {
                    text: 'Aceptar y continuar',
                    handler: function () {
                        _this.aceptRGPD();
                    }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.aceptRGPD = function () {
        var _this = this;
        this.rest.aceptarRGPD(this.user_email)
            .subscribe(function (response) {
            var result = response['result'];
            if (result) {
                _this.successAlert();
            }
            else {
                _this.errorAlert();
            }
        });
    };
    HomePage.prototype.getRGPD = function () {
        var _this = this;
        this.rest.getRGPD(this.user_email)
            .subscribe(function (response) {
            var result = response['result'];
            if (result) {
            }
            else {
                _this.presentRGPD();
            }
        });
    };
    HomePage.prototype.getImages = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getImages().then(function (data) {
                    var images = data;
                    _this.com_image = images['com_image'];
                    console.log("home data : " + JSON.stringify(data));
                    console.log("home com_image : " + _this.com_image);
                }).catch(function (error) {
                    console.log('home error get image ', JSON.stringify(error));
                });
            }
        });
    };
    HomePage.prototype.getUserInfo = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getUser().then(function (data) {
                    _this.perfil = data;
                    _this.user_email = _this.perfil['email'];
                    // this.getRGPD(); // YA NO SE CHECKEA ESTA OBLIGATORIO AL HACER REGISTER
                    _this.updateAuthTokenLocally();
                    _this.initPushNotification();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        });
    };
    HomePage.prototype.getComunidad = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.getComunity().then(function (data) {
                    var comunidad = data;
                    _this.com_code = comunidad['code'];
                    var code = comunidad['code'];
                    _this.com_type = comunidad['type'];
                    _this.com_type_comunity = comunidad['type_comunity'];
                    _this.com_name = comunidad['name'];
                    _this.com_admin_name = comunidad['admin_name'];
                    _this.com_admin_email = comunidad['admin_email'];
                    _this.com_admin_phone = comunidad['admin_phone'];
                    _this.com_admin_adress = comunidad['admin_adress'];
                    console.log('data comunity:', JSON.stringify(data));
                    _this.rest.getComunityByCode(code)
                        .subscribe(function (data) {
                        console.log('home rest comunity', JSON.stringify(data));
                        if (!data) {
                        }
                        else {
                            var codeCom_1 = data[0]['code'];
                            var adminName_1 = data[0]['admin_name'];
                            var email_1 = data[0]['admin_email'];
                            var phone_1 = data[0]['admin_phone'];
                            var adress_1 = data[0]['admin_adress'];
                            var comunidad_1 = data[0]['name'];
                            var type_1 = data[0]['type'];
                            var type_comunity_1 = data[0]['type_comunity'];
                            //IF TYPE COMUNITY HAS CHANGED, UPDATE COMUNIDAD
                            if (_this.com_type_comunity != type_comunity_1) {
                                _this.com_type_comunity = type_comunity_1;
                                console.log('home update : ' + JSON.stringify(data));
                                console.log("updated type comunity : " + type_1);
                                _this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
                                    if (rdy) {
                                        _this.databaseprovider.updateComunity(codeCom_1, type_1, type_comunity_1, comunidad_1, adminName_1, email_1, phone_1, adress_1);
                                    }
                                });
                            }
                        }
                    }, function (error) {
                        console.log(error);
                    });
                })
                    .catch(function (error) {
                    console.log(error);
                });
                ;
            }
        });
    };
    HomePage.prototype.goToNoticiasPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__noticias_noticias__["a" /* NoticiasPage */]);
    };
    HomePage.prototype.goToPadelPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__padel_padel__["a" /* PadelPage */]);
    };
    HomePage.prototype.goToLocalPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__local_local__["a" /* LocalPage */]);
    };
    HomePage.prototype.initPushNotification = function () {
        var _this = this;
        // to check if we have permission
        this.push.hasPermission()
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
            }
            else {
                console.log('We don\'t have permission to send push notifications');
            }
        });
        // to initialize push notifications
        var options = {
            android: {
                senderID: '634760725285'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false',
                clearBadge: true
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            console.log('Received a notification', notification);
            //Notification Display Section
            //this.navCtrl.push(NoticiasPage);
            //
        });
        pushObject.on('registration').
            subscribe(function (registration) {
            _this.token = registration.registrationId;
            console.log(JSON.stringify(registration));
            _this.updateToken(_this.token);
        });
        pushObject.on('error').
            subscribe(function (error) {
            return console.error('Error with Push plugin', error);
        });
    };
    HomePage.prototype.updateToken = function (token) {
        this.rest.updatePushToken(this.perfil['email'], token)
            .subscribe(function (data) {
            var result = data['result'];
            if (!result) {
                console.log('push token updated error');
            }
            else {
                console.log('push token updated successfully');
            }
        }, function (error) {
        });
    };
    HomePage.prototype.updateAuthTokenLocally = function () {
        var _this = this;
        this.storage.get('auth_token').then(function (result) {
            if (result) {
                var auth_token = result;
                console.log("auth_token : " + auth_token);
            }
            else {
                _this.rest.updateAuthToken(_this.perfil['email'])
                    .subscribe(function (data) {
                    var result = data['result'];
                    if (!result) {
                        console.log('auth token updated error');
                    }
                    else {
                        var auth_token = result;
                        _this.storage.set("auth_token", auth_token);
                    }
                }, function (error) {
                });
            }
        })
            .catch(function (error) {
        });
    };
    HomePage.prototype.icomunityUpdate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var show;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionService.isUpdateShown()];
                    case 1:
                        show = _a.sent();
                        console.log('UPDATE SHOWN -> ', show);
                        if (!show) {
                            if (this.platform.is('ios')) {
                                this.icomunityUpdateAlert('ios');
                            }
                            else if (this.platform.is('android')) {
                                this.icomunityUpdateAlert('android');
                            }
                            this.sessionService.setUpdateShown();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.icomunityUpdateAlert = function (store) {
        var _this = this;
        this.rest.getVersion(store)
            .subscribe(function (response) {
            var versionWeb = response['result'].version;
            console.log('version web :', versionWeb);
            _this.getVersionNumber(versionWeb, store);
        });
    };
    HomePage.prototype.getVersionNumber = function (versionWeb, store) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var versionNumber, alert_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.app.getVersionNumber()];
                    case 1:
                        versionNumber = _a.sent();
                        console.log('version number:', versionNumber);
                        if (versionNumber == versionWeb) {
                        }
                        else {
                            alert_1 = this.alertCtrl.create({
                                title: 'iComunity actualización',
                                subTitle: 'Una nueva versión de iComunity está disponible, actualizala para disfrutar de las novedades',
                                buttons: [
                                    {
                                        text: 'Aceptar y actualizar',
                                        handler: function () {
                                            if (store == 'ios')
                                                _this.market.open('icomunity/id1325772871');
                                            else
                                                _this.market.open('developerteam.icomunity');
                                        }
                                    }
                                ]
                            });
                            alert_1.present();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.successAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Exito !',
            subTitle: 'Se ha realizado la operación correctamente',
            buttons: ['Ok']
        });
        alert.present();
    };
    HomePage.prototype.errorAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Error !',
            subTitle: 'No se ha podido realizar la operación, por favor compruebe más tarde',
            buttons: ['Ok']
        });
        alert.present();
    };
    HomePage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    HomePage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/home/home.html"*/'  <ion-header>\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n\n      <ion-buttons end>\n        <button class="toolbar-button" (click)="goToNoticiasPage()" *ngIf="comunidad" ion-button icon-only>\n          <ion-icon class="tele" name="paper" end></ion-icon>\n        </button>\n\n        <a href="tel: {{comunidad.admin_phone}}" *ngIf="comunidad"><button class="toolbar-button" ion-button icon-only>\n            <ion-icon class="fono" name="call" end></ion-icon>\n          </button></a>\n\n      </ion-buttons>\n    </ion-navbar>\n\n  </ion-header>\n\n  <ion-content class="c-home" padding >\n    <ion-scroll class="c-home__container" scrollY="true" style="width: 100%; height: 100%;">\n      <div class="c-home__com" *ngIf="comunidad">\n        <p class="c-home__com_label" id="comunidad-label">COMUNIDAD DE PROPIETARIOS</p>\n        <p class="c-home__com_name"> {{comunidad.name}}</p>\n      </div>\n  \n\n      <ion-card class="c-home__card" *ngIf="comunidad">\n        <img *ngIf="comunidad.com_image" [src]="comunidad.com_image" />\n        <img *ngIf="!comunidad.com_image || (comunidad.com_image === \'\')" [src]="buildingIcon" />\n        <ion-card-content>\n          <ion-card-title>\n            {{comunidad.admin_name}}\n          </ion-card-title>\n          <p>\n            {{comunidad.admin_adress}}<br>\n            T: {{comunidad.admin_phone}}\n          </p>\n        </ion-card-content>\n      </ion-card>\n      <ion-card class="c-home__cardpiscina" *ngIf="mostrarPiscina" (click)="openReservaPiscina()">\n            <p>Reservar piscina</p>\n            <ion-icon name="reader-outline"></ion-icon>\n        </ion-card>\n\n    <div>\n        <div class="c-home__com" *ngIf="!comunidad">\n            <p class="c-home__com_label" id="comunidad-label">COMUNIDAD DE PROPIETARIOS</p>\n            <p class="c-home__com_name"> </p>\n          </div>\n    </div>\n        <ion-card class="c-home__card" *ngIf="!comunidad">\n            <img [src]="buildingIcon" />\n            <ion-card-content>\n              <ion-card-title class="txt-center">\n                BIENVENIDO EN ICOMUNITY\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n    </ion-scroll>\n  </ion-content>\n'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_market__["a" /* Market */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_11__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_12__providers_session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MicasaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__listado_listado__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the MicasaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MicasaPage = /** @class */ (function () {
    function MicasaPage(navCtrl, navParams, databaseProvider, loadingCtrl, restProvider, alertCtrl, storageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseProvider = databaseProvider;
        this.loadingCtrl = loadingCtrl;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.itemList = [
            {
                icon: "assets/icon/alba.png",
                name: "Albañileria",
                type: "albanileria"
            },
            {
                icon: "assets/icon/key.png",
                name: "Cerrajeria",
                type: "cerrajeria"
            },
            {
                icon: "assets/icon/ventana.png",
                name: "Cristaleria",
                type: "cristaleria"
            },
            {
                icon: "assets/icon/decora.png",
                name: "Decoracion",
                type: "decoracion"
            },
            {
                icon: "assets/icon/water.png",
                name: "Desatascos",
                type: "desatascos"
            },
            {
                icon: "assets/icon/elec.png",
                name: "Electricidad",
                type: "electricidad"
            },
            {
                icon: "assets/icon/fonta.png",
                name: "Fontaneria",
                type: "fontaneria"
            },
            {
                icon: "assets/icon/herramientas.png",
                name: "Herramientas",
                type: "herramientas"
            },
            {
                icon: "assets/icon/inmo.png",
                name: "Inmobiliaria",
                type: "inmobiliaria"
            },
            {
                icon: "assets/icon/mudanzas.png",
                name: "Mudanzas",
                type: "mudanzas"
            },
            {
                icon: "assets/icon/paint.png",
                name: "Pintura",
                type: "pintura"
            },
            {
                icon: "assets/icon/reformas.png",
                name: "Reformas",
                type: "reformas"
            },
            {
                icon: "assets/icon/seguro.png",
                name: "Seguros del hogar",
                type: "seguros"
            },
        ];
        this.electricidad = [];
        this.fontaneria = [];
        this.cerrajeria = [];
        this.albanileria = [];
        this.cristaleria = [];
        this.decoracion = [];
        this.desatascos = [];
        this.herramientas = [];
        this.inmobiliaria = [];
        this.mudanzas = [];
        this.pintura = [];
        this.reformas = [];
        this.seguros = [];
        this.active = false;
        this.infoIcon = 'assets/icon/informacion.svg';
    }
    MicasaPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.init();
                return [2 /*return*/];
            });
        });
    };
    MicasaPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _c.sent();
                        if (this.comunidad && this.comunidad.features && this.comunidad.features.length > 0) {
                            for (_i = 0, _b = this.comunidad.features; _i < _b.length; _i++) {
                                feat = _b[_i];
                                if (feat === 'micasa') {
                                    this.active = true;
                                    this.getServices();
                                }
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MicasaPage.prototype.getServices = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: "Cargando...",
            duration: 5000
        });
        this.loader.present();
        this.restProvider.getServicesMiCasa()
            .subscribe(function (data) {
            _this.loader.dismiss();
            var response = data['result'];
            if (!response) {
            }
            else {
                for (var i in response) {
                    if (response[i]['type'] === 'electricidad')
                        _this.electricidad.push(response[i]);
                    else if (response[i]['type'] === 'fontaneria')
                        _this.fontaneria.push(response[i]);
                    else if (response[i]['type'] === 'cerrajeria')
                        _this.cerrajeria.push(response[i]);
                    else if (response[i]['type'] === 'albanileria')
                        _this.albanileria.push(response[i]);
                    else if (response[i]['type'] === 'cristaleria')
                        _this.cristaleria.push(response[i]);
                    else if (response[i]['type'] === 'decoracion')
                        _this.decoracion.push(response[i]);
                    else if (response[i]['type'] === 'desatascos')
                        _this.desatascos.push(response[i]);
                    else if (response[i]['type'] === 'herramientas')
                        _this.herramientas.push(response[i]);
                    else if (response[i]['type'] === 'inmobiliaria')
                        _this.inmobiliaria.push(response[i]);
                    else if (response[i]['type'] === 'mudanzas')
                        _this.mudanzas.push(response[i]);
                    else if (response[i]['type'] === 'pintura')
                        _this.pintura.push(response[i]);
                    else if (response[i]['type'] === 'reformas')
                        _this.reformas.push(response[i]);
                    else if (response[i]['type'] === 'seguros')
                        _this.seguros.push(response[i]);
                }
            }
            console.log("elec list : " + JSON.stringify(_this.electricidad));
        }, function (error) {
            console.log(error);
        });
    };
    MicasaPage.prototype.openListado = function (type) {
        console.log("type: " + type);
        var list;
        var title;
        switch (type) {
            case "electricidad":
                list = this.electricidad;
                break;
            case "fontaneria":
                list = this.fontaneria;
                break;
            case "cerrajeria":
                list = this.cerrajeria;
                break;
            case "albanileria":
                list = this.albanileria;
                break;
            case "cristaleria":
                list = this.cristaleria;
                break;
            case "decoracion":
                list = this.decoracion;
                break;
            case "desatascos":
                list = this.desatascos;
                break;
            case "herramientas":
                list = this.herramientas;
                break;
            case "inmobiliaria":
                list = this.inmobiliaria;
                break;
            case "mudanzas":
                list = this.mudanzas;
                break;
            case "pintura":
                list = this.pintura;
                break;
            case "reformas":
                list = this.reformas;
                break;
            case "seguros":
                list = this.seguros;
                break;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__listado_listado__["a" /* ListadoPage */], {
            list: list,
            type: type
        });
    };
    MicasaPage.prototype.showNotAllow = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Opsss...',
            message: 'No puedes acceder a este apartado ',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__principal_principal__["a" /* PrincipalPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    MicasaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-micasa',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/micasa/micasa.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Mi casa</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content class="c-micasa" padding>\n\n  <div class="c-micasa__inactive" *ngIf="!active">\n    <img [src]="infoIcon" />\n    <p>La gestión de micasa está inabilitada en tu comunidad,\n      contacta con tu administrador/a para más información </p>\n  </div>\n  <div class="c-micasa__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n    <ion-list class="c-micasa__list">\n      <ion-item class="c-micasa__list__item" (click)="openListado(item.type)"\n        *ngFor="let item of itemList ; let index=index">\n        <ion-avatar item-left>\n          <img [src]="item.icon">\n        </ion-avatar>\n        <h2>{{item.name}}</h2>\n        <ion-icon class="arrow" name="arrow-forward" item-end></ion-icon>\n      </ion-item>\n    </ion-list>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/micasa/micasa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_6__providers_storage_service__["a" /* StorageService */]])
    ], MicasaPage);
    return MicasaPage;
}());

//# sourceMappingURL=micasa.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IncidenciaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__principal_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_session_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_modal_service__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var IncidenciaPage = /** @class */ (function () {
    function IncidenciaPage(navCtrl, alertCtrl, loadingCtrl, camera, actionSheetCtrl, platform, toastCtrl, restProvider, storageService, sessionService, modalService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        this.storageService = storageService;
        this.sessionService = sessionService;
        this.modalService = modalService;
        this.image = null;
        this.lastImage = null;
        this.services = [];
        this.isFoto = false;
        this.perfil = {};
        this.list = "reciente";
        this.incidenciaVacia = {
            title: "Incidencias",
            category: null,
            description: "No hay incidencias recientes ",
            type: null,
            created_at: "",
            day: "",
            state: 0,
            truncating: false
        };
        this.incidenciaVaciaTable = [this.incidenciaVacia];
        this.Incidencias = [];
        this.bool_empty = false;
        this.limit = 40;
        this.active = false;
        this.infoIcon = 'assets/icon/informacion.svg';
        this.truncating = true;
        this.rest = restProvider;
    }
    IncidenciaPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.initAuthProcess();
                this.init();
                return [2 /*return*/];
            });
        });
    };
    IncidenciaPage.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _i, _b, feat, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchComunidad()];
                    case 1:
                        _a.comunidad = _d.sent();
                        if (!(this.comunidad && this.comunidad.features && this.comunidad.features.length > 0)) return [3 /*break*/, 5];
                        _i = 0, _b = this.comunidad.features;
                        _d.label = 2;
                    case 2:
                        if (!(_i < _b.length)) return [3 /*break*/, 5];
                        feat = _b[_i];
                        if (!(feat === 'incidencias')) return [3 /*break*/, 4];
                        this.active = true;
                        _c = this;
                        return [4 /*yield*/, this.storageService.fetchServices()];
                    case 3:
                        _c.services = _d.sent();
                        this.fetchIncidencias();
                        _d.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5:
                        this.fetchUserInfos();
                        return [2 /*return*/];
                }
            });
        });
    };
    IncidenciaPage.prototype.fetchUserInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storageService.fetchUserInfos()];
                    case 1:
                        _a.perfil = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    IncidenciaPage.prototype.initAuthProcess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var authToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sessionService.getAuthToken()];
                    case 1:
                        authToken = _a.sent();
                        console.log('token process -> ', authToken);
                        this.restProvider.setAuthToken(authToken);
                        return [2 /*return*/];
                }
            });
        });
    };
    IncidenciaPage.prototype.fetchIncidencias = function () {
        var _this = this;
        this.presentLoading();
        this.restProvider.doGetIncidencias(this.comunidad.code)
            .subscribe(function (response) {
            _this.dismissLoading();
            if (response && response.result && response.result.length > 0) {
                _this.Incidencias = response.result;
                _this.storageService.saveIncidencias(_this.Incidencias);
            }
            else {
                _this.Incidencias = _this.storageService.fetchIncidencias();
            }
        }, function (error) {
            _this.dismissLoading();
            console.log(' ERROR FETCH INCIDENCIAS DATA -> ', error);
        });
    };
    IncidenciaPage.prototype.initializeItems = function () {
        this.tmpIncidenciaJSON = this.incidenciaJSON;
    };
    IncidenciaPage.prototype.enviar = function () {
        this.sendEmailToAdmin();
    };
    IncidenciaPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Selecciona',
            buttons: [
                {
                    text: 'Coger de la galeria',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Usar Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    IncidenciaPage.prototype.takePicture = function (sourceType) {
        // Create options for the Camera Dialog
        var _this = this;
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            _this.base64Image = 'data:image/jpeg;base64,' + imagePath;
            _this.base64ImageString = imagePath;
        }, function (err) {
            console.error(err);
            _this.presentToast('Error while selecting image.');
        });
    };
    IncidenciaPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    IncidenciaPage.prototype.sendEmailToAdmin = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mandando el correo ..."
        });
        loader.present();
        var email;
        for (var i = 0; i < this.services.length; i++) {
            if (this.services[i]['type'] == this.categoriaType) {
                email = this.services[i]['email'];
            }
        }
        var options;
        if (this.base64ImageString) {
            if ((email == 'vacio') || (email == '') || (email == undefined)) {
                options = {
                    category: this.categoriaType,
                    comunidad: this.comunidad.name,
                    code: this.comunidad.code,
                    image: this.base64ImageString,
                    admin_email: this.comunidad.admin_email,
                    adress: this.perfil['adress'],
                    phone: this.perfil['phone'],
                    user_email: this.perfil['email'],
                    description: this.description,
                    username: this.perfil['name'],
                    type: this.tipo
                };
            }
            else {
                options = {
                    category: this.categoriaType,
                    comunidad: this.comunidad.name,
                    code: this.comunidad.code,
                    image: this.base64ImageString,
                    admin_email: this.comunidad.admin_email,
                    adress: this.perfil['adress'],
                    phone: this.perfil['phone'],
                    user_email: this.perfil['email'],
                    description: this.description,
                    username: this.perfil['name'],
                    type: this.tipo,
                    destinatorio: email
                };
            }
        }
        else {
            if ((email == 'vacio') || (email == '') || (email == undefined)) {
                options = {
                    category: this.categoriaType,
                    comunidad: this.comunidad.name,
                    code: this.comunidad.code,
                    admin_email: this.comunidad.admin_email,
                    adress: this.perfil['adress'],
                    phone: this.perfil['phone'],
                    user_email: this.perfil['email'],
                    description: this.description,
                    username: this.perfil['name'],
                    type: this.tipo
                };
            }
            else {
                options = {
                    category: this.categoriaType,
                    comunidad: this.comunidad.name,
                    code: this.comunidad.code,
                    admin_email: this.comunidad.admin_email,
                    adress: this.perfil['adress'],
                    phone: this.perfil['phone'],
                    user_email: this.perfil['email'],
                    description: this.description,
                    destinatorio: email,
                    username: this.perfil['name'],
                    type: this.tipo
                };
            }
        }
        this.rest.sendIncidencia(options)
            .subscribe(function (data) {
            var result = data['result'];
            loader.dismiss();
            if (!result) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Email Error',
                    subTitle: 'Ha habido un error, pruebe más tarde por favor',
                    buttons: ['OK']
                });
                alert_1.present();
            }
            else {
                _this.showConfirm();
                _this.sendPuahIncidencia();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Email Error',
                subTitle: 'Ha habido un error, pruebe más tarde por favor',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    IncidenciaPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Email confirmacion',
            message: 'Se ha notificado correctamente la incidencia al administrador y al gremio ',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__principal_principal__["a" /* PrincipalPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    IncidenciaPage.prototype.sendPuahIncidencia = function () {
        this.rest.sendIncidenciaPush(this.comunidad.code)
            .subscribe(function (response) {
        });
    };
    IncidenciaPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando ...",
            duration: 3000
        });
        this.loader.present();
    };
    IncidenciaPage.prototype.dismissLoading = function () {
        this.loader.dismiss();
    };
    IncidenciaPage.prototype.checkFields = function () {
        var message = "";
        console.log("categoria : " + this.categoriaType + "tipo : " + this.tipo + "descripcion : " + this.description);
        for (var index = 0; index < 3; index++) {
            if (this.categoriaType == undefined)
                message = "Debes elegir una categoria";
            else if (this.tipo == undefined)
                message = "Debes elegir un tipo ";
            else if (this.description == undefined)
                message = "Debes poner una descripcion ";
        }
        if (message == "") {
            this.enviar();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: "Campo requerido",
                subTitle: message,
                buttons: ['OK']
            });
            alert_2.present();
        }
    };
    IncidenciaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-incidencia',template:/*ion-inline-start:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/incidencia/incidencia.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Incidencia</ion-title>\n  </ion-navbar>\n  <ion-toolbar *ngIf="active">\n    <ion-segment [(ngModel)]="list" >\n      <ion-segment-button value="reciente">\n        Recientes\n      </ion-segment-button>\n      <ion-segment-button value="nueva">\n        Añadir nueva\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class="c-incidencia" >\n<div class="c-incidencia__inactive" *ngIf="!active">\n  <img [src]="infoIcon" />\n  <p>La gestión de incidencias está inabilitada en tu comunidad, \n    contacta con tu administrador/a para más información </p>\n</div>\n  <div class="c-incidencia__active" [ngSwitch]="list" scrollY="true" *ngIf="active">\n    <br>\n    <div class="c-incidencia__empty"  *ngIf="Incidencias.length === 0">\n\n    </div>\n    <div class="c-incidencia__active__filled"  *ngIf="Incidencias.length > 0">\n        <ion-list class="c-incidencia__list" *ngSwitchCase="\'reciente\'" >\n            <div class="c-incidencia__active__filled__list"  id="content-reciente">\n              <ion-card class="c-incidencia__card" text-wrap *ngFor="let incidencia of Incidencias ; let i = index;">\n                  <ion-card-content>\n                      <div class="c-incidencia__card__fecha_estado">\n                          <p class="fecha">{{incidencia.create}}</p>\n                          <p class="pendiente" *ngIf="incidencia.state==1">Pendiente</p>\n                          <p class="solucionado" *ngIf="!(incidencia.state==1)">Solucionado</p>\n                        </div>\n                \n                        <h2 *ngIf="incidencia.category">\n                          <b>Categoria :</b> {{incidencia.category}}</h2>\n                        <h2 *ngIf="incidencia.type">\n                          <b>Tipo :</b> {{incidencia.type}}</h2>\n                \n                        <div *ngIf="incidencia.description">\n                          <div *ngIf="incidencia.description.length <= limit"> {{incidencia.description}}</div>\n                          <div class="group-truncate" *ngIf="!incidencia.truncating && incidencia.description.length > limit">\n                            {{incidencia.description | truncate }}\n                            <p class="btn-truncate" (click)="incidencia.truncating = true">mostrar más...</p>\n                          </div>\n                        </div>\n                        <div class="group-truncate" *ngIf="incidencia.truncating && incidencia.description.length > limit">\n                          {{incidencia.description}}\n                          <p class="btn-truncate" (click)="incidencia.truncating = false"> ...mostrar menos</p>\n                        </div>\n                    </ion-card-content>\n              </ion-card>\n            </div>\n        \n          </ion-list>\n      \n      </div>\n    \n    <!-- NUEVA INCIDENCIA -->\n\n    <ion-list *ngSwitchCase="\'nueva\'">\n      <!-- BACKGROUND WALLPAPER -->\n   <!--    <div class="c-incidencia__wallpaper">\n        <img src="assets/imgs/inc_back.jpg">\n      </div> -->\n      <div id="content-nueva">\n\n        <div class="c-incidencia__block">\n          <p class="c-incidencia__label"> Categoria de incidencia </p>\n          <ion-item class="c-incidencia__spinner">\n            <ion-label>Categoria</ion-label>\n            <ion-select [(ngModel)]="categoriaType" interface="action-sheet">\n              <ion-option *ngFor="let service of services" value="{{service.type}}">{{service.type}}\n              </ion-option>\n            </ion-select>\n          </ion-item>\n        </div>\n        <div class="c-incidencia__block">\n          <p class="c-incidencia__label"> Tipo de incidencia </p>\n              <ion-item class="c-incidencia__spinner">\n                <ion-label>tipo</ion-label>\n                <ion-select [(ngModel)]="tipo" interface="action-sheet">\n                  <ion-option value="Averia">Averia</ion-option>\n                  <ion-option value="Rotura">Rotura</ion-option>\n                  <ion-option value="Deterioro">Deterioro</ion-option>\n                  <ion-option value="Extravio">Extravio</ion-option>\n                  <ion-option value="Robo">Robo</ion-option>\n                  <ion-option value="Otro">Otro</ion-option>\n                </ion-select>\n              </ion-item>\n        </div>\n        <div class="c-incidencia__block">\n          <p class="c-incidencia__label"> Descripcion </p>\n          <div class="c-incidencia__description">\n            <textarea class="c-incidencia__description__area" [(ngModel)]="description" name="text" id=""> </textarea>\n          </div>\n        </div>\n        <div class="c-incidencia__block" *ngIf="base64Image">\n          <div class="profile-image-wrapper">\n            <div class="profile-image" *ngIf="base64Image">\n              <img [src]="base64Image" />\n            </div>\n          </div>\n          </div>\n          <div class="c-incidencia__block extra-margin">\n            <button class="c-incidencia__button" ion-button icon-left (tap)="presentActionSheet()" large full padding>\n              <ion-icon name="camera"></ion-icon>\n              Tomar fotografia\n            </button>\n            <br>\n            <button class="c-incidencia__button" (click)="checkFields()" ion-button icon-left large full padding>\n              <ion-icon name="send"></ion-icon>\n              Enviar\n            </button>\n          </div>\n      </div>\n    </ion-list>\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"/Users/steveberekoutou/Desktop/test/icomunity-mobile-ionic/src/pages/incidencia/incidencia.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_7__providers_modal_service__["a" /* ModalService */]])
    ], IncidenciaPage);
    return IncidenciaPage;
}());

//# sourceMappingURL=incidencia.js.map

/***/ })

},[454]);
//# sourceMappingURL=main.js.map